var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
  

        var placeholder = "...";

  

        $(".select2-allow-clear").select2({
            allowClear: true,
            placeholder: placeholder,
            width: null
        });

   
     

    

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

}();

