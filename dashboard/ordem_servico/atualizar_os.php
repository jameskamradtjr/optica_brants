<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../sweetalert/js/sweetalert.min.js"></script>

  <title></title>

</head>

<body>

  <?php

  if (!isset($_SESSION)) session_start();
  session_regenerate_id(true);

  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

  //função para gravar os valores no mysql
  function moeda($get_valor) {
  $source = array('.', ','); 
  $replace = array('', '.');
  $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
  return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql

// lista post

$id = mysqli_real_escape_string($con,$_POST['editar_id']);
$FK_usuarios_admin_id = mysqli_real_escape_string($con,$_POST['editar_FK_usuarios_admin_id']);
$id_loja = mysqli_real_escape_string($con,$_POST['editar_id_loja']);
$data_gerado = mysqli_real_escape_string($con,$_POST['editar_data_gerado']);
$data_combinado_entrega = mysqli_real_escape_string($con,$_POST['editar_data_combinado_entrega']);
$cliente = mysqli_real_escape_string($con,$_POST['editar_cliente']);
$fk_id_cliente = mysqli_real_escape_string($con,$_POST['editar_fk_id_cliente']);
$nr_os = mysqli_real_escape_string($con,$_POST['editar_nr_os']);
$vendedor = mysqli_real_escape_string($con,$_POST['editar_vendedor']);
$fk_id_vendedor = mysqli_real_escape_string($con,$_POST['editar_fk_id_vendedor']);
$observacao = mysqli_real_escape_string($con,$_POST['editar_observacao']);
$pago = mysqli_real_escape_string($con,$_POST['editar_pago']);
$a_pagar = mysqli_real_escape_string($con,$_POST['editar_a_pagar']);
$status_cliente = mysqli_real_escape_string($con,$_POST['editar_status_cliente']);
$status_loja = mysqli_real_escape_string($con,$_POST['editar_status_loja']);

// lista post

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';

if( isset($_POST['salvar_ordem_servico']) and isset($_SESSION['idadmin']) )
{ 




  //query
  $query_atualizar_ordem_servico = "update ordem_servico set id_loja='$id_loja',data_gerado='$data_gerado',data_combinado_entrega='$data_combinado_entrega',cliente='$cliente',fk_id_cliente='$fk_id_cliente',nr_os='$nr_os',vendedor='$vendedor',fk_id_vendedor='$fk_id_vendedor',observacao='$observacao',pago='$pago',a_pagar='$a_pagar',status_cliente='$status_cliente',status_loja='$status_loja' where id = '$id' "; 
  $update_go_ordem_servico = mysqli_query($con, $query_atualizar_ordem_servico);
  //query



  if(isset($_SESSION['idadmin'])){




  if($update_go_ordem_servico){


  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Atualizado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/perfil_cliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente'    } });});</script>";

}else{
echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/perfil_cliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente'    } });});</script>";
}


}else
{
 echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/perfil_cliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente'    } });});</script>";
}

}

//--------------------------------------------------APAGAR	

if(isset($_POST['excluir_ordem_servico']) and isset($_SESSION['idadmin']) ){

$editar_id = mysqli_real_escape_string($con,$_POST['editar_id']);

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';



$sql_del_serv = "DELETE FROM ordem_servico WHERE id='$editar_id' ";
$del_serv = $con->query($sql_del_serv);

$sql_del_serv = "DELETE FROM relacionamento_indicacao_os WHERE fk_id_os='$editar_id' ";
$del_serv = $con->query($sql_del_serv);

//produto
   $select_query_produtos = "SELECT SQL_CACHE * FROM relacionamento_produto_os WHERE fk_id_os = '$editar_id' ";
   $select_prods = $con->query($select_query_produtos);     


$sql_del_serv = "DELETE FROM assistencia WHERE fk_id_os='$editar_id' ";
$del_serv = $con->query($sql_del_serv);


 $sql_del_serv = "DELETE FROM relacionamento_assistencia_os WHERE fk_id_os='$editar_id' ";
 $del_serv = $con->query($sql_del_serv);


//apagar desconto
    $select_queryrrelacionamento_desconto_os = "SELECT SQL_CACHE * FROM relacionamento_desconto_os WHERE fk_id_os = '$editar_id' ";
    $selectrelacionamento_desconto_os = $con->query($select_queryrrelacionamento_desconto_os); 


while ($selectrelacionamento_desconto_os_oslista = $selectrelacionamento_desconto_os->fetch_assoc()): 


 $sql_del_serv = "DELETE FROM contasapagar WHERE id= '".$selectrelacionamento_desconto_os_oslista['fk_id_despesa']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;
//apagar desconto


 //apagar devolucao
    $select_queryrrelacionamento_devolucao_os = "SELECT SQL_CACHE * FROM relacionamento_devolucao_os WHERE fk_id_os = '$editar_id' ";
    $selectrelacionamento_devolucao_os= $con->query($select_queryrrelacionamento_devolucao_os); 


while ($selectrelacionamento_devolucao_os_oslista = $selectrelacionamento_devolucao_os->fetch_assoc()): 


 $sql_del_serv = "DELETE FROM contasapagar WHERE id= '".$selectrelacionamento_devolucao_os_oslista['fk_id_despesa']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;
//apagar devolucao
        
   

   while ($select_prodslista = $select_prods->fetch_assoc()): 

        //descontar estoque
         

            $select_query_fk_id_produto = "SELECT SQL_CACHE quant_est,id FROM produtos WHERE id = '".$select_prodslista['fk_id_produto']."' ";
            $select_prodsfk_id_produto = $con->query($select_query_fk_id_produto);             
            $select_prodslistafk_id_produto = $select_prodsfk_id_produto->fetch_assoc();

            $quant_est = $select_prodslistafk_id_produto['quant_est'];

            $quant_est_final = $quant_est + $select_prodslista['quantidade']; 

               $query_atualizar_produtos = "update produtos set quant_est='$quant_est_final' where id = '".$select_prodslista['fk_id_produto']."' ";
               $update_prod = mysqli_query($con, $query_atualizar_produtos);

         
           //descontar estoque

   endwhile;

$sql_del_serv = "DELETE FROM relacionamento_produto_os WHERE fk_id_os='$editar_id' ";
$del_serv = $con->query($sql_del_serv);


//produto

//apagar pagamento
    $select_queryrelacionamento_pagamentos_os = "SELECT SQL_CACHE * FROM relacionamento_pagamentos_os WHERE fk_id_os = '$editar_id' ";
    $selectrelacionamento_pagamentos_os = $con->query($select_queryrelacionamento_pagamentos_os); 


while ($selectrelacionamento_pagamentos_oslista = $selectrelacionamento_pagamentos_os->fetch_assoc()): 


 $sql_del_serv = "DELETE FROM contasareceber WHERE id= '".$selectrelacionamento_pagamentos_oslista['fk_id_pagamentos']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;
//apagar pagamento

 //apagar receitas
    $select_queryrelacionamento_receita_os = "SELECT SQL_CACHE * FROM relacionamento_receita_os WHERE relacionamento_receita_os = '$editar_id' ";
    $selectrelacionamento_receita_os = $con->query($select_queryrelacionamento_receita_os); 


while ($selectrelacionamento_receita_oslistalista = $selectrelacionamento_receita_os->fetch_assoc()): 


 $sql_del_serv = "DELETE FROM receita_medica WHERE id= '".$selectrelacionamento_receita_oslistalista['fk_id_receita']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;
//apagar receitas

  //apagar pedido lab
    $select_relacionamento_pedidolaboratorio_os = "SELECT SQL_CACHE * FROM relacionamento_pedidolaboratorio_os WHERE fk_id_os = '$editar_id' ";
    $selectrelacionamento_pedidolaboratorio_os = $con->query($select_relacionamento_pedidolaboratorio_os); 


while ($selectrelacionamento_pedidolaboratorio_oslista = $selectrelacionamento_pedidolaboratorio_os->fetch_assoc()): 



 $sql_del_serv = "DELETE FROM pedido_laboratorio WHERE id= '".$selectrelacionamento_pedidolaboratorio_oslista['fk_id_pedido_laboratorio']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;


  $sql_del_serv = "DELETE FROM relacionamento_pedidolaboratorio_os WHERE fk_id_os= '$editar_id'  ";
  $del_serv = $con->query($sql_del_serv);
//apagar pedido lab

if($del_serv){
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Deletado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/perfil_cliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente'    } });});</script>";
}else{
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/perfil_cliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente'    } });});</script>";
} 
}


if(!isset($_POST['excluir_ordem_servico']) and !isset($_POST['salvar_ordem_servico'])  ){

echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_servicos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_servicos'    } });});</script>";

} 




?>

</body>
</html>