
(function($) {
  
  RemoveTableRowProdutoOs = function(handler) {
    var tr = $(handler).closest('tr');

    tr.fadeOut(400, function(){ 
      tr.remove();    

       //SOMA DOS VALORES-------------------------------------------------------------------------

       valor_soma_produtos = 0; 
       $('.soma_produtos').each(function(i){        

        
         valor_soma_produtos = parseFloat($(this).val()) + parseFloat(valor_soma_produtos) ;     

       }); 

     $("#tabela_pagamentosos tbody tr").remove(); 
     $("#pago").val(0); 
     $("#a_pagar").val(0); 
     $("#desconto").val('');

 //SOMA DOS VALORES----------------------------------------------------------------------------
 

 $("#div_total_produtos").empty();

 $("#div_total_produtos").append('<p>Total R$<input readonly id="total_produtos" name="total_produtos" value="'+valor_soma_produtos.toFixed(2)+'" type="text"  class="form-control"></p>')
 
 
 
}); 

    return false;
  };
  
  AddTableRowProdutoOs = function() {


     $("#tabela_pagamentosos tbody tr").remove(); 
     $("#pago").val(0); 
     $("#a_pagar").val(0); 
     $("#desconto").val('');
    
		//pega a quantidade de linhas pra poder ter o id certo
    var tabela = document.getElementById('tabela_produtosos');
    var linhas = tabela.getElementsByTagName('tr');
    
    var idcelula = linhas.length -1;
    
    var quant_produto = document.getElementById('quant_produto').value;
    
    
    
    var produto = document.getElementById('produto');
    var produtodescselecionado = produto.options[produto.selectedIndex].value;
    var retorno = produtodescselecionado.split("|");
    
    
    var id_produto = retorno[0] ;
    
    var descricao_produto = retorno[1];
    
    var valor_produto =retorno[2] ;

    var total_produtos = valor_produto * quant_produto;
    
    
    
    
    if(  (quant_produto !== '') && (descricao_produto !== '') && (valor_produto !== '')   ){	
     
      var newRow = $("<tr>");
      var cols = "";

      cols += '<td id="'+idcelula+'id_produto" >'+id_produto+'<input style="display:none" id="'+idcelula+'id_produto" name="id_produto[]" value="'+id_produto+'|'+descricao_produto+'|'+valor_produto+'|'+quant_produto+'" type="text"  class="form-control">  </td>';
      cols += '<td id="'+idcelula+'descricao_produto" >'+descricao_produto+'  </td>';
      cols += '<td id="'+idcelula+'quant_produto" >'+quant_produto+'  </td>';
      cols += '<td id="'+idcelula+'valor_produto" >'+valor_produto+'  </td>';
      cols += '<td id="'+idcelula+'total_produtos" ><input readonly id="'+idcelula+'total_produtos" name="'+idcelula+'total_produtos" value="'+total_produtos+'" type="text"  class="soma_produtos form-control">  </td>';
      cols += '<td ><a onclick="RemoveTableRowProdutoOs(this)" href="javascript:;" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-trash-o"></i></a>  </td>';
      

      
      newRow.append(cols);
      $("#tabela_produtosos").append(newRow);

    //SOMA DOS VALORES-------------------------------------------------------------------------

    valor_soma_produtos = 0; 
    $('.soma_produtos').each(function(i){        

      
     valor_soma_produtos = parseFloat($(this).val()) + parseFloat(valor_soma_produtos) ;     

   }); 

 //SOMA DOS VALORES----------------------------------------------------------------------------
 

 $("#div_total_produtos").empty();

 $("#div_total_produtos").append('<p>Total R$<input readonly id="total_produtos" name="total_produtos" value="'+valor_soma_produtos.toFixed(2)+'" type="text"  class="form-control"></p>')
 
   //coloca data hoje no vencimento 
 }else{alert('Preencha Produto e quantidade para  Poder Adicionar!');};

 
 
 return false;
};
})(jQuery);

