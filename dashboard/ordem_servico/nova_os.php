<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../sweetalert/js/sweetalert.min.js"></script>

  <title> </title>

</head>

<body>

  <?php

  if (!isset($_SESSION)) session_start();
  session_regenerate_id(true);

  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

    date_default_timezone_set('America/Sao_Paulo');


  //função para gravar os valores no mysql
  function moeda($get_valor) {
    $source = array('.', ','); 
    $replace = array('', '.');
  $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
  return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql


if(isset($_POST['cliente']))
{


  //lista post dados os-------------
  $id_loja = mysqli_real_escape_string($con,$_POST['id_loja']);
  
  $data_gerado = mysqli_real_escape_string($con,$_POST['data_gerado']);

  $cliente_array = mysqli_real_escape_string($con,$_POST['cliente']);
  list($fk_id_cliente,$cliente) = explode('|', $cliente_array); 


  $tipo_os_array = mysqli_real_escape_string($con,$_POST['tipo_os']);
  list($fk_id_tipo_os,$tipo_os) = explode('|', $tipo_os_array); 

  //deixar em branco o nr os fica o id
  $nr_os = mysqli_real_escape_string($con,$_POST['nr_os']);

  $vendedor_array = mysqli_real_escape_string($con,$_POST['vendedor']);
  list($fk_id_vendedor,$vendedor) = explode('|', $vendedor_array);  

  $observacao = mysqli_real_escape_string($con,$_POST['observacao']);
  $pago = mysqli_real_escape_string($con,$_POST['pago']);
  $pago = moeda($pago);
  $a_pagar = mysqli_real_escape_string($con,$_POST['a_pagar']);
  $a_pagar = moeda($a_pagar);
  $desconto = mysqli_real_escape_string($con,$_POST['desconto']);
  $desconto = moeda($desconto);
  $status_cliente = mysqli_real_escape_string($con,$_POST['status_cliente']);
  $status_loja = mysqli_real_escape_string($con,$_POST['status_loja']); 
  $data_combinado_entrega = mysqli_real_escape_string($con,$_POST['data_combinado_entrega']); 
  $hora_combinado_entrega = mysqli_real_escape_string($con,$_POST['hora_entrega']); 
  $data_combinado_entrega = $data_combinado_entrega.' '.$hora_combinado_entrega.':00';

if (isset($_POST['desconto'])){

$observacao = $observacao.', Desconto: R$'.$desconto;

}
  

  //assistencia
  $identificacao_assistencia = mysqli_real_escape_string($con,$_POST['identificacao_assistencia']);  
  $obs_assistencia = mysqli_real_escape_string($con,$_POST['obs_assistencia']); 
  $defeito_assistencia = mysqli_real_escape_string($con,$_POST['defeito_assistencia']); 
  $servico_assistencia = mysqli_real_escape_string($con,$_POST['servico_assistencia']); 

  //assistencia

  //lista post dados os-------------






  $FK_usuarios_admin_id = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';

  $relacionamento = date("dmYhis");
  $relacionamento = $FK_usuarios_admin_id.$relacionamento;


  if(isset($_SESSION['idadmin'])){



  //query dados os
    $query_insert_ordem_servico = "INSERT INTO ordem_servico (desconto,fk_id_tipo_os,tipo_os,FK_usuarios_admin_id,id_loja,data_gerado,data_combinado_entrega,cliente,fk_id_cliente,nr_os,vendedor,fk_id_vendedor,observacao,pago,a_pagar,status_cliente,status_loja) VALUES ('$desconto','$fk_id_tipo_os','$tipo_os','$FK_usuarios_admin_id','$id_loja','$data_gerado','$data_combinado_entrega','$cliente','$fk_id_cliente','$nr_os','$vendedor','$fk_id_vendedor','$observacao','$pago','$a_pagar','$status_cliente','$status_loja')";
    $insert_go_ordem_servico= mysqli_query($con, $query_insert_ordem_servico);
    $FK_id_ordem_servico = mysqli_insert_id($con);
  //query dados os

    if($insert_go_ordem_servico){ 


      include 'classes_salvar_os/indicacao.php';

      include 'classes_salvar_os/produtos.php';

      include 'classes_salvar_os/pagamentos.php';

      include 'classes_salvar_os/desconto.php';


      if(trim($tipo_os) <> 'Assistência'){

      include 'classes_salvar_os/receita_medica.php';

      include 'classes_salvar_os/pedido_lab.php';

     } 

      include 'classes_salvar_os/assistencia.php';

    

      ?>

      <script>

        var inputPost = <?php echo $FK_id_ordem_servico; ?>;

        var idadmin = <?php echo $FK_usuarios_admin_id; ?>;

        var id_lojaPost = <?php echo $id_loja; ?>;



   
        $.post("pdf_optica/impressao_os.php", {input:inputPost,id_loja:id_lojaPost},
          function(data){



          }
          , "html");  

   
      </script>



      <?php


      include '../classes/log.php';
      $novo_log = new Log();
      $novo_log->setLog($_SESSION['user_email'],'Cadastro','Ordem de Serviço',$FK_usuarios_admin_id,'Criando uma nova OS '.$cliente,$con);
      $novo_log->gravar();


      echo "<script>jQuery(function(){swal({   title: 'Dados Salvos',   text: 'O que deseja Fazer?',   type: 'warning',   showCancelButton: true,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Abrir PDF O.S',   cancelButtonText: 'Voltar Tela Cliente',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success'); window.open('../docs_empresas/$FK_usuarios_admin_id/anexos/os/$FK_id_ordem_servico.pdf', '_blank');top.location.href='../view/ordem_servico?id=$fk_id_cliente'   } else {     swal('OS', 'Confira a lista!', 'success');top.location.href='../view/ordem_servico?id=$fk_id_cliente'    } });});</script>";    







    }else{
      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível Cadastrar!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/ordem_servico'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/ordem_servico'    } });});</script>";
    }


  }else
  {
   echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não Logado!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/ordem_servico'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/ordem_servico'    } });});</script>";
 }

}else
{
 echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/ordem_servico'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/ordem_servico'    } });});</script>";
}







//--------------------------------------------------		

?>

</body>
</html>