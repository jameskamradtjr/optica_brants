 $('#cpf').mask('000.000.000-00', {reverse: true}); 

  $('#valor_pagamento').mask('#.##0,00', {reverse: true});
  $('#pago').mask('#.##0,00', {reverse: true});
  $('#a_pagar').mask('#.##0,00', {reverse: true});
  $('#total_produtos').mask('#.##0,00', {reverse: true});
  $('#valor_novo_produto').mask('#.##0,00', {reverse: true});
  $('#desconto').mask('#.##0,00', {reverse: true});