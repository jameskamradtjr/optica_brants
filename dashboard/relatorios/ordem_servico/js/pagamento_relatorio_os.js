
(function($) {

	RemoveTableRowPagamentoOs = function(handler) {
		var tr = $(handler).closest('tr');

		tr.fadeOut(400, function(){ 
			tr.remove(); 


			//SOMA DOS VALORES-------------------------------------------------------------------------

			valor_soma_pagamentos = 0; 
			$('.soma_pagamentos').each(function(i){        

           // valor_soma_pagamentos = parseFloat($(this).val().replace( '.', '' ).replace( ',', '.' )) + valor_soma_pagamentos ;  

           valor_soma_pagamentos = parseFloat($(this).val()) + parseFloat(valor_soma_pagamentos) ;     

       }); 

         

			var valor_produtos = $('#total_produtos').val().replace( '.', '' ).replace( ',', '.' );

        

            var a_pagar_sinal = $('#a_pagar_sinal').val().replace( '.', '' ).replace( ',', '.' );

            var pago_sinal = $('#pago_sinal').val().replace( '.', '' ).replace( ',', '.' );

            var desconto_sinal = $("#desconto_sinal").val().replace( '.', '' ).replace( ',', '.' );

             var desconto = $("#desconto").val().replace( '.', '' ).replace( ',', '.' );  


    if (!desconto){desconto = 0};  
    
    if (!valor_produtos){valor_produtos = 0}; 
    
    if (!pago_sinal){pago_sinal = 0}; 
    if (!desconto_sinal){desconto_sinal = 0};

    desconto = parseFloat(desconto_sinal) + parseFloat(desconto) ;
    
    valor_produtos = parseFloat(valor_produtos) - parseFloat(desconto) ; 
     


           
           var valor_faltante_sinal = parseFloat(valor_produtos) - parseFloat(a_pagar_sinal);

           valor_soma_pagamentos = parseFloat(valor_soma_pagamentos) +  parseFloat(pago_sinal);

           var valor_faltante = parseFloat(valor_produtos) - parseFloat(valor_soma_pagamentos);

           valor_faltante = Math.abs(valor_faltante);


        //$('#troco_comanda').val(valorfinal.toFixed(2));


    //SOMA DOS VALORES----------------------------------------------------------------------------

    $("#div_total_pagamentos").empty();

   $("#div_total_pagamentos").append('<p><h4>Valor Total: <strong>R$<input value="'+valor_soma_pagamentos.toFixed(2)+'" readonly  type="text" id="pago" name="pago" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)"></strong> </p> <p>Faltante: <strong>R$<input value="'+valor_faltante.toFixed(2)+'" readonly  type="text" id="a_pagar" name="a_pagar" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)"></strong></h4></p> ');
 


}); 

		return false;




	};

	AddTableRowPagamentoOs = function() {





		//pega a quantidade de linhas pra poder ter o id certo
		var tabela = document.getElementById('tabela_pagamentosos');
		var linhas = tabela.getElementsByTagName('tr');

		var idcelula = linhas.length -1;
		
		var data_caixa = document.getElementById('data_caixa').value;
		var forma_pagamento = document.getElementById('forma_pagamento').value;
		var valor_pagamento = $('#valor_pagamento').val().replace( '.', '' ).replace( ',', '.' );
        
        var pago_sinal = $('#pago_sinal').val().replace( '.', '' ).replace( ',', '.' );
      

		var parcela_pagamento = document.getElementById('parcela_pagamento').value;

		var parcela_pagamento = document.getElementById('parcela_pagamento').value;

    //categoria---------
    var categoria_pagamento = document.getElementById('categoria_pagamento');
    var categoria_pagamentoselecionado = categoria_pagamento.options[categoria_pagamento.selectedIndex].value;
    var retorno_cat = categoria_pagamentoselecionado.split("|");
    
    var grupo_categoria = retorno_cat[0] 

    var id_categoria = retorno_cat[1] ;
    
    var categoria_pagamento = retorno_cat[2];
    //categoria---------

    //conta---------
    var conta_pagamento = document.getElementById('conta_pagamento');
    var conta_pagamentoselecionado = conta_pagamento.options[conta_pagamento.selectedIndex].value;
    var retorno_conta = conta_pagamentoselecionado.split("|");
    
    
    var id_conta = retorno_conta[0] ;
    
    var conta_pagamento = retorno_conta[1];
    //conta---------


            //SOMA DOS VALORES-------------------------------------------------------------------------

            var valor_soma_pagamentos = 0; 
            $('.soma_pagamentos').each(function(i){        

         
            valor_soma_pagamentos = parseFloat($(this).val()) + parseFloat(valor_soma_pagamentos) ;     
          
            }); 

          


            var valor_produtos = parseFloat($('#total_produtos').val().replace( '.', '' ).replace( ',', '.' )) ; 

            var a_pagar_sinal = parseFloat($('#a_pagar_sinal').val().replace( '.', '' ).replace( ',', '.' ));

                   var desconto_sinal = $("#desconto_sinal").val().replace( '.', '' ).replace( ',', '.' );

             var desconto = $("#desconto").val().replace( '.', '' ).replace( ',', '.' );  


    if (!desconto){desconto = 0};  
   
    if (!valor_produtos){valor_produtos = 0}; 
 
    if (!pago_sinal){pago_sinal = 0}; 
    if (!desconto_sinal){desconto_sinal = 0};

    desconto = parseFloat(desconto_sinal) + parseFloat(desconto) ;
    
    valor_produtos = parseFloat(valor_produtos) - parseFloat(desconto) ; 

                     

            var valor_pagamento_verificacao_se_e_maior = parseFloat(valor_soma_pagamentos) + parseFloat(valor_pagamento);

           


            var valor_faltante_sinal = parseFloat(valor_produtos) - parseFloat(pago_sinal);

           


          
            //SOMA DOS VALORES----------------------------------------------------------------------------




    if( (id_conta !== '') && (id_categoria !== '') &&  (data_caixa !== '') && (forma_pagamento !== '') && (valor_pagamento !== '') && (parcela_pagamento !== '')  ){	

    	if( parseFloat(valor_pagamento_verificacao_se_e_maior) <= parseFloat(valor_faltante_sinal)   ){	

    		var newRow = $("<tr>");
    		var cols = "";

    		cols += '<td id="'+idcelula+'id_pagamento" >'+idcelula+'<input style="display:none" id="'+idcelula+'id_pagamento" name="id_pagamento[]" value="'+grupo_categoria+'|'+id_categoria+'|'+categoria_pagamento+'|'+id_conta+'|'+conta_pagamento+'|'+data_caixa+'|'+forma_pagamento+'|'+valor_pagamento+'|'+parcela_pagamento+'" type="text"  class="form-control">  </td>';
    		cols += '<td id="'+idcelula+'forma_pagamento" >'+forma_pagamento+'  </td>';
            cols += '<td id="'+idcelula+'conta_pagamento" >'+conta_pagamento+'  </td>';
            cols += '<td id="'+idcelula+'categoria_pagamento" >'+categoria_pagamento+'  </td>';
    		cols += '<td id="'+idcelula+'parcela_pagamento" >'+parcela_pagamento+'  </td>';
    		cols += '<td id="'+idcelula+'valor_pagamento" ><input readonly id="'+idcelula+'soma_pagamentos" name="'+idcelula+'soma_pagamentos" value="'+valor_pagamento+'" type="text"  class="soma_pagamentos form-control"> </td>';

    		cols += '<td id="'+idcelula+'data_caixa" >'+data_caixa+'  </td>';
    		cols += '<td ><a onclick="RemoveTableRowPagamentoOs(this)" href="javascript:;" class="btn btn-outline btn-circle dark btn-sm black"><i class="fa fa-trash-o"></i></a>  </td>';



    		newRow.append(cols);
    		$("#tabela_pagamentosos").append(newRow);


    //SOMA DOS VALORES-------------------------------------------------------------------------

    valor_soma_pagamentos = 0; 
    $('.soma_pagamentos').each(function(i){        

           // valor_soma_pagamentos = parseFloat($(this).val().replace( '.', '' ).replace( ',', '.' )) + valor_soma_pagamentos ;  

           valor_soma_pagamentos = parseFloat($(this).val()) + parseFloat(valor_soma_pagamentos) ;     

       }); 


    valor_soma_pagamentos = parseFloat(valor_soma_pagamentos) +  parseFloat(pago_sinal);

    var valor_faltante = parseFloat(valor_soma_pagamentos) - parseFloat(valor_produtos)   ;

    valor_faltante = Math.abs(valor_faltante);
        //$('#troco_comanda').val(valorfinal.toFixed(2)); //fora do loop


    //SOMA DOS VALORES----------------------------------------------------------------------------

    $("#div_total_pagamentos").empty();

    $("#div_total_pagamentos").append('<p><h4>Valor Total: <strong>R$<input value="'+valor_soma_pagamentos.toFixed(2)+'" readonly  type="text" id="pago" name="pago" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)"></strong> </p> <p>Faltante: <strong>R$<input value="'+valor_faltante.toFixed(2)+'" readonly  type="text" id="a_pagar" name="a_pagar" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)"></strong></h4></p> ');

}else{alert('Valor Maior que os Produtos e Serviços!');};

   //coloca data hoje no vencimento 
}else{alert('Preencha Forma Pagamento e quantidade para Poder Adicionar!');};



return false;
};
})(jQuery);

