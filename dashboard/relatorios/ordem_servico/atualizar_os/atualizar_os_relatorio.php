<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../../../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../../../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../../../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../../../sweetalert/js/sweetalert.min.js"></script>

  <title></title>

</head>

<body>

  <?php

  if (!isset($_SESSION)) session_start();
  session_regenerate_id(true);

  include '../../../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

  //função para gravar os valores no mysql
  function moeda($get_valor) {
  $source = array('.', ','); 
  $replace = array('', '.');
  $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
  return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql

// lista post

$editar_id = mysqli_real_escape_string($con,$_POST['editar_id']);

$FK_id_ordem_servico = mysqli_real_escape_string($con,$_POST['editar_id']);



$data_combinado_entrega = mysqli_real_escape_string($con,$_POST['data_combinado_entrega']);



  $vendedor_array = mysqli_real_escape_string($con,$_POST['vendedor']);
  list($fk_id_vendedor,$vendedor) = explode('|', $vendedor_array); 

  $tipo_os_array = mysqli_real_escape_string($con,$_POST['tipo_os']);
  list($fk_id_tipo_os,$tipo_os) = explode('|', $tipo_os_array); 


$observacao = mysqli_real_escape_string($con,$_POST['observacao']);

$pago = mysqli_real_escape_string($con,$_POST['pago']);
$a_pagar = mysqli_real_escape_string($con,$_POST['a_pagar']);

if(($pago == 0) or (empty($pago)) ){

 $pago = mysqli_real_escape_string($con,$_POST['pago_sinal']); 
 $a_pagar = mysqli_real_escape_string($con,$_POST['a_pagar_sinal']);

}

$a_pagar_sinal = mysqli_real_escape_string($con,$_POST['a_pagar_sinal']);
$pago_sinal = mysqli_real_escape_string($con,$_POST['pago_sinal']);










$status_cliente = mysqli_real_escape_string($con,$_POST['status_cliente']);
$status_loja = mysqli_real_escape_string($con,$_POST['status_loja']);

$status_loja = mysqli_real_escape_string($con,$_POST['status_loja']);

$devolucao = mysqli_real_escape_string($con,$_POST['devolver']);

$desconto = mysqli_real_escape_string($con,$_POST['desconto']);

$desconto_sinal = mysqli_real_escape_string($con,$_POST['desconto_sinal']);

if (!empty($desconto)){

$observacao = $observacao.', Desconto: R$'.$desconto;

}


  $id_loja =  $_SESSION['id_empresa'];

  $FK_id_despesa =  $_SESSION['id_empresa'];

// lista post

  
  //assistencia
  $identificacao_assistencia = mysqli_real_escape_string($con,$_POST['identificacao_assistencia']);  
  $obs_assistencia = mysqli_real_escape_string($con,$_POST['obs_assistencia']); 
  $defeito_assistencia = mysqli_real_escape_string($con,$_POST['defeito_assistencia']); 
  $servico_assistencia = mysqli_real_escape_string($con,$_POST['servico_assistencia']); 

  //assistencia

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';  
$FK_usuarios_admin_id = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';  

if( isset($_POST['salvar_ordem_servico']) and isset($_SESSION['idadmin']) )
{


  //query
  $query_atualizar_ordem_servico = "update ordem_servico set fk_id_tipo_os='$fk_id_tipo_os',tipo_os='$tipo_os',devolucao='$devolucao',data_combinado_entrega='$data_combinado_entrega',vendedor='$vendedor',fk_id_vendedor='$fk_id_vendedor',observacao='$observacao',pago='$pago',a_pagar='$a_pagar',status_cliente='$status_cliente',status_loja='$status_loja' where id = '$editar_id' "; 
  $update_go_ordem_servico = mysqli_query($con, $query_atualizar_ordem_servico);
  //query






  if(isset($_SESSION['idadmin'])){




  if($update_go_ordem_servico){



     include 'includes/indicacao.php';

      include 'includes/produtos.php';



      include 'includes/pagamentos.php';


      include 'includes/desconto.php';


      include 'includes/devolucao.php';



      if(trim($tipo_os) <> 'Assistência'){

      include 'includes/receita_medica.php';

      include 'includes/pedido_lab.php';

     } 

      include 'includes/assistencia.php';


 ?>

      <script>

        var inputPost = <?php echo $editar_id; ?>;

        var idadmin = <?php echo $FK_usuarios_admin_id; ?>;

        var id_lojaPost = <?php echo $id_loja; ?>;



   
        $.post("../../../ordem_servico/pdf_optica/impressao_os.php", {input:inputPost,id_loja:id_lojaPost},
          function(data){



          }
          , "html");  

   
      </script>

    
 <?php

      include '../../../classes/log.php';
      $novo_log = new Log();
      $novo_log->setLog($_SESSION['user_email'],'Cadastro','Ordem de Serviço',$FK_usuarios_admin_id,'Criando uma nova OS '.$cliente,$con);
      $novo_log->gravar();


  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Atualizado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../../../view/relatorios'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../../view/relatorios'    } });});</script>";

}else{
echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../../view/relatorios'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../../view/relatorios'    } });});</script>";
}


}else
{
 echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../../view/relatorios'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../../view/relatorios'    } });});</script>";
}

}














//--------------------------------------------------APAGAR	

if(isset($_POST['excluir_ordem_servico']) and isset($_SESSION['idadmin']) ){

$editar_id = mysqli_real_escape_string($con,$_POST['editar_id']);

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';



 
$sql_del_serv = "DELETE FROM ordem_servico WHERE id='$editar_id' ";
$del_serv = $con->query($sql_del_serv);

$sql_del_serv = "DELETE FROM relacionamento_indicacao_os WHERE fk_id_os='$editar_id' ";
$del_serv = $con->query($sql_del_serv);


$sql_del_serv = "DELETE FROM assistencia WHERE fk_id_os='$editar_id' ";
$del_serv = $con->query($sql_del_serv);


 $sql_del_serv = "DELETE FROM relacionamento_assistencia_os WHERE fk_id_os='$editar_id' ";
 $del_serv = $con->query($sql_del_serv);



//apagar desconto
    $select_queryrrelacionamento_desconto_os = "SELECT SQL_CACHE * FROM relacionamento_desconto_os WHERE fk_id_os = '$editar_id' ";
    $selectrelacionamento_desconto_os = $con->query($select_queryrrelacionamento_desconto_os); 


while ($selectrelacionamento_desconto_os_oslista = $selectrelacionamento_desconto_os->fetch_assoc()): 


 $sql_del_serv = "DELETE FROM contasapagar WHERE id= '".$selectrelacionamento_desconto_os_oslista['fk_id_despesa']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;
//apagar desconto


 //apagar devolucao
    $select_queryrrelacionamento_devolucao_os = "SELECT SQL_CACHE * FROM relacionamento_devolucao_os WHERE fk_id_os = '$editar_id' ";
    $selectrelacionamento_devolucao_os= $con->query($select_queryrrelacionamento_devolucao_os); 


while ($selectrelacionamento_devolucao_os_oslista = $selectrelacionamento_devolucao_os->fetch_assoc()): 


 $sql_del_serv = "DELETE FROM contasapagar WHERE id= '".$selectrelacionamento_devolucao_os_oslista['fk_id_despesa']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;
//apagar devolucao



//produto
   $select_query_produtos = "SELECT SQL_CACHE * FROM relacionamento_produto_os WHERE fk_id_os = '$editar_id' ";
   $select_prods = $con->query($select_query_produtos);             
   

   while ($select_prodslista = $select_prods->fetch_assoc()): 

        //descontar estoque
         

            $select_query_fk_id_produto = "SELECT SQL_CACHE quant_est,id FROM produtos WHERE id = '".$select_prodslista['fk_id_produto']."' ";
            $select_prodsfk_id_produto = $con->query($select_query_fk_id_produto);             
            $select_prodslistafk_id_produto = $select_prodsfk_id_produto->fetch_assoc();

            $quant_est = $select_prodslistafk_id_produto['quant_est'];

            $quant_est_final = $quant_est + $select_prodslista['quantidade']; 

               $query_atualizar_produtos = "update produtos set quant_est='$quant_est_final' where id = '".$select_prodslista['fk_id_produto']."' ";
               $update_prod = mysqli_query($con, $query_atualizar_produtos);

         
           //descontar estoque

   endwhile;

$sql_del_serv = "DELETE FROM relacionamento_produto_os WHERE fk_id_os='$editar_id' ";
$del_serv = $con->query($sql_del_serv);


//produto

//apagar pagamento
    $select_queryrelacionamento_pagamentos_os = "SELECT SQL_CACHE * FROM relacionamento_pagamentos_os WHERE fk_id_os = '$editar_id' ";
    $selectrelacionamento_pagamentos_os = $con->query($select_queryrelacionamento_pagamentos_os); 


while ($selectrelacionamento_pagamentos_oslista = $selectrelacionamento_pagamentos_os->fetch_assoc()): 


 $sql_del_serv = "DELETE FROM contasareceber WHERE id= '".$selectrelacionamento_pagamentos_oslista['fk_id_pagamentos']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;
//apagar pagamento

 //apagar receitas
    $select_queryrelacionamento_receita_os = "SELECT SQL_CACHE * FROM relacionamento_receita_os WHERE relacionamento_receita_os = '$editar_id' ";
    $selectrelacionamento_receita_os = $con->query($select_queryrelacionamento_receita_os); 


while ($selectrelacionamento_receita_oslistalista = $selectrelacionamento_receita_os->fetch_assoc()): 


 $sql_del_serv = "DELETE FROM receita_medica WHERE id= '".$selectrelacionamento_receita_oslistalista['fk_id_receita']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;
//apagar receitas

  //apagar pedido lab
    $select_relacionamento_pedidolaboratorio_os = "SELECT SQL_CACHE * FROM relacionamento_pedidolaboratorio_os WHERE fk_id_os = '$editar_id' ";
    $selectrelacionamento_pedidolaboratorio_os = $con->query($select_relacionamento_pedidolaboratorio_os); 


while ($selectrelacionamento_pedidolaboratorio_oslista = $selectrelacionamento_pedidolaboratorio_os->fetch_assoc()): 



 $sql_del_serv = "DELETE FROM pedido_laboratorio WHERE id= '".$selectrelacionamento_pedidolaboratorio_oslista['fk_id_pedido_laboratorio']."' ";
 $del_serv = $con->query($sql_del_serv);

 endwhile;

     $sql_del_serv = "DELETE FROM relacionamento_pedidolaboratorio_os WHERE fk_id_os= '$editar_id'  ";
     $del_serv = $con->query($sql_del_serv);

//apagar pedido lab

if($del_serv){
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Deletado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../../../view/relatorios'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../../view/relatorios'    } });});</script>";
}else{
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../../view/relatorios'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../../view/relatorios'    } });});</script>";
} 
}


if(!isset($_POST['excluir_ordem_servico']) and !isset($_POST['salvar_ordem_servico'])  ){

echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../../view/relatorios'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../../view/relatorios'    } });});</script>";

} 




?>

</body>
</html>