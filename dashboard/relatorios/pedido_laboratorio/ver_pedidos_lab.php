<?php

include '../../sys/init.php';


 $id_ped_lab =  mysqli_real_escape_string($con,$_POST['id_ped_lab']);

  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
if (!$con->set_charset("utf8")) {}   

	if (filter_var($id_ped_lab, FILTER_VALIDATE_INT)) {

		$select_query_os = "SELECT SQL_CACHE * FROM pedido_laboratorio WHERE id = '".$id_ped_lab."' ";

	    $select_os = $con->query($select_query_os); 

	    $select_os_go= $select_os->fetch_assoc();


	    print '

	                             <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Código Pedido</label>
                                             <input readonly value="'.$select_os_go['id'].'" type="text" id="editar_id" name="editar_id" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                        </div>
                                    </div>

                                       <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Status Pedido</label>
                                                <select  type="text" id="status_pedido_lab" name="status_pedido_lab" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">  

                                                      <option value="'.$select_os_go['status_laboratorio'].'">'.$select_os_go['status_laboratorio'].'</option>
                                                      <option value="Enviar">Enviar</option>
                                                      <option value="Enviado">Enviado</option>
                                                      <option value="Entregue">Entregue</option>
                                                

                                                </select>  

                                           </div>
                                       </div>

                                        <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Data OS</label>
                                            <input readonly  value="'.date('Y-m-d',strtotime($select_os_go['data_os'])).'" type="date" required id="data_os_lab" name="data_os_lab" class="form-control" >

                                         </div>
                                        </div>



                                       <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Data Entregar ao Cliente</label>
                                            <input  value="'.date('Y-m-d',strtotime($select_os_go['data_entrega'])).'" type="date" required id="data_entrega_cliente" name="data_entrega_cliente" class="form-control" >

                                        </div>
                                    </div>

                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Data Entrega do Laboratorio</label>
                                            <input  value="'.date('Y-m-d',strtotime($select_os_go['data_previsao_entrega_laboratorio'])).'" type="date" required id="data_previsao_entrega_laboratorio" name="data_previsao_entrega_laboratorio" class="form-control" >

                                         </div>
                                        </div>

                                      <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Data Enviado ao Laboratorio</label>
                                            <input value="'.date('Y-m-d',strtotime($select_os_go['data_enviado_laboratorio'])).'" type="date" required id="data_enviado_lab" name="data_enviado_lab" class="form-control" >

                                         </div>
                                        </div>  


                                </div>    

                                 <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Observação</label>
                                             <input value="'.$select_os_go['observacao'].'" type="text" id="obs_pedido_lab" name="obs_pedido_lab" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                        </div>
                                    </div>
                                 </div> 

	     ';
	} 