<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../../sweetalert/js/sweetalert.min.js"></script>

  <title> </title>

</head>

<body>

  <?php

  if (!isset($_SESSION)) session_start();
  session_regenerate_id(true);

  include '../../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

    date_default_timezone_set('America/Sao_Paulo');


  //função para gravar os valores no mysql
  function moeda($get_valor) {
    $source = array('.', ','); 
    $replace = array('', '.');
  $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
  return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql


if(isset($_POST['gerar_pdf_lab']))
{


  $id_pdl = mysqli_real_escape_string($con,$_POST['id_pdl']);
  $FK_usuarios_admin_id = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';

  $relacionamento = date("dmYhis");
  $relacionamento = $FK_usuarios_admin_id.$relacionamento;


  if(isset($_SESSION['idadmin'])){




      ?>

      <script>

        var inputPost = <?php echo $id_pdl; ?>;

     



   
        $.post("impressao_os_pdl.php", {input:inputPost},
          function(data){



          }
          , "html");  

   
      </script>



      <?php


      echo "<script>jQuery(function(){swal({   title: 'Dados Salvos',   text: 'O que deseja Fazer?',   type: 'warning',   showCancelButton: true,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Abrir PDF Laboratório',   cancelButtonText: 'Voltar Tela Cliente',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success'); window.open('../../docs_empresas/$FK_usuarios_admin_id/anexos/os/pedido_lab$id_pdl.pdf', '_blank');top.location.href='../../view/relatorios'   } else {     swal('OS', 'Confira a lista!', 'success');top.location.href='../../view/relatorios'    } });});</script>";    







    }else{
      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível Cadastrar!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/relatorios'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/relatorios'    } });});</script>";
    }


  }else
  {
   echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não Logado!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/relatorios'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/relatorios'    } });});</script>";
 }







//--------------------------------------------------		

?>

</body>
</html>