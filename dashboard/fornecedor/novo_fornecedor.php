<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


    <!-- Custom functions file -->
    <script src="../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../sweetalert/js/sweetalert.min.js"></script>

    <title> </title>

  </head>

  <body>

<?php

if (!isset($_SESSION)) session_start();
session_regenerate_id(true);

  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

  date_default_timezone_set('America/Sao_Paulo');
		
		
	

          if(isset($_POST['adicionar_cad_fornecedor']))
		  {
        
      
			   //lista post

         $razao_social = mysqli_real_escape_string($con,$_POST['razao_social']);
         $nome_fantasia = mysqli_real_escape_string($con,$_POST['nome_fantasia']);
         $endereco = mysqli_real_escape_string($con,$_POST['endereco']);
         $cep = mysqli_real_escape_string($con,$_POST['cep']);
         $cnpj = mysqli_real_escape_string($con,$_POST['cnpj']);
         $contato = mysqli_real_escape_string($con,$_POST['contato']);
         $telefone = mysqli_real_escape_string($con,$_POST['telefone']);
         $celular = mysqli_real_escape_string($con,$_POST['celular']);
         $FK_usuarios_admin_id = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';
         $idempresa = $_SESSION['id_empresa'];


         //lista post

       	 $idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';
		     	

            
             if(isset($_SESSION['idadmin'])){
               
              //query
               $query_insert_cad_fornecedor = "INSERT INTO cad_fornecedor (fk_id_empresa,razao_social,nome_fantasia,endereco,cep,cnpj,contato,telefone,celular,FK_usuarios_admin_id) VALUES ('$idempresa','$razao_social','$nome_fantasia','$endereco','$cep','$cnpj','$contato','$telefone','$celular','$FK_usuarios_admin_id')";
               $insert_go_cad_fornecedor= mysqli_query($con, $query_insert_cad_fornecedor);
              //query         
   

                if($insert_go_cad_fornecedor){				
			            
				       	echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Fornecedor Cadastrado com Sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível Cadastrar!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";
              }


	         }else
			  {
		  	  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não Logado!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";
        }
	
      }else
			{
			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";
      }
			





    	
//--------------------------------------------------		

?>

</body>
</html>