<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../sweetalert/js/sweetalert.min.js"></script>

  <title></title>

</head>

<body>

  <?php

  if (!isset($_SESSION)) session_start();
  session_regenerate_id(true);

  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

  //função para gravar os valores no mysql
  function moeda($get_valor) {
  $source = array('.', ','); 
  $replace = array('', '.');
  $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
  return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql

// lista post

$id = mysqli_real_escape_string($con,$_POST['editar_id']);
$razao_social = mysqli_real_escape_string($con,$_POST['editar_razao_social']);
$nome_fantasia = mysqli_real_escape_string($con,$_POST['editar_nome_fantasia']);
$endereco = mysqli_real_escape_string($con,$_POST['editar_endereco']);
$cep = mysqli_real_escape_string($con,$_POST['editar_cep']);
$cnpj = mysqli_real_escape_string($con,$_POST['editar_cnpj']);
$contato = mysqli_real_escape_string($con,$_POST['editar_contato']);
$telefone = mysqli_real_escape_string($con,$_POST['editar_telefone']);
$celular = mysqli_real_escape_string($con,$_POST['editar_celular']);

// lista post

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';

if( isset($_POST['salvar_cad_fornecedor']) and isset($_SESSION['idadmin']) )
{

  
  

  //query
$query_atualizar_cad_fornecedor = "update cad_fornecedor set razao_social='$razao_social',nome_fantasia='$nome_fantasia',endereco='$endereco',cep='$cep',cnpj='$cnpj',contato='$contato',telefone='$telefone',celular='$celular' where id = '$id' "; 
$update_go_cad_fornecedor = mysqli_query($con, $query_atualizar_cad_fornecedor);

  //query

  if(isset($_SESSION['idadmin'])){

 


  if($update_go_cad_fornecedor){


  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Atualizado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";

}else{
echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";
}


}else
{
 echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";
}

}

//--------------------------------------------------APAGAR	

if(isset($_POST['excluir_cad_fornecedor']) and isset($_SESSION['idadmin']) ){

$editar_id = mysqli_real_escape_string($con,$_POST['editar_id']);

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';



$sql_del_cad_fornecedor = "DELETE FROM cad_fornecedor WHERE id='$editar_id' ";
$del_cad_fornecedor = $con->query($sql_del_cad_fornecedor);


if($del_cad_fornecedor){
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Deletado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";
}else{
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";
} 
}


if(!isset($_POST['excluir_cad_fornecedor']) and !isset($_POST['salvar_cad_fornecedor'])  ){

echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_fornecedores'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_fornecedores'    } });});</script>";

} 




?>

</body>
</html>