<?php

if (!isset($_SESSION)) session_start();// A sessão precisa ser iniciada em cada página diferente


include '../sys/init.php';
  
 date_default_timezone_set('America/Sao_Paulo');
  
   // Calcula o total contas a pagar
    $select_contasapagarcaixa = "SELECT SQL_CACHE SUM(valor_bruto) as 'totalcpcaixa' FROM  contasapagar WHERE ( FK_usuarios_admin_id = '".$_SESSION['idadmin']."' and fk_id_empresa = '".$_SESSION['id_empresa']."' and data_pagamento = CURDATE() and status='Pago' )  ";
     if ($result = $con->query($select_contasapagarcaixa)) {
      if ($result->num_rows) {
       $row = $result->fetch_assoc();
       $contasapagarcaixa = $row['totalcpcaixa'];
     }
     } else {
     $contasapagarcaixa = '0';
    };

   // Calcula o total contas a receber
    $select_contasarecebercaixa= "SELECT SQL_CACHE SUM(valor_bruto) as 'totalcrcaixa' FROM  contasareceber WHERE ( FK_usuarios_admin_id = '".$_SESSION['idadmin']."' and fk_id_empresa = '".$_SESSION['id_empresa']."' and data_pagamento = CURDATE() and status='Pago' )  ";
     if ($result = $con->query($select_contasarecebercaixa)) {
      if ($result->num_rows) {
       $row = $result->fetch_assoc();
       $contasarecebercaixa = $row['totalcrcaixa'];
     }
     } else {
     $contasarecebercaixa = '0';
    };

    $saldohoje = $contasarecebercaixa - abs($contasapagarcaixa);



 // Calcula o total contas a receber
    $select_assistencia= "SELECT SQL_CACHE SUM(contasareceber.valor_bruto) as 'totalasscaixa' FROM  contasareceber INNER JOIN relacionamento_pagamentos_os ON contasareceber.id = relacionamento_pagamentos_os.fk_id_pagamentos INNER JOIN ordem_servico ON relacionamento_pagamentos_os.fk_id_os = ordem_servico.id WHERE (ordem_servico.tipo_os='Assistência' and contasareceber.FK_usuarios_admin_id = '".$_SESSION['idadmin']."' and contasareceber.fk_id_empresa = '".$_SESSION['id_empresa']."' and contasareceber.data_pagamento = CURDATE() and contasareceber.status='Pago' )  ";
     if ($result = $con->query($select_assistencia)) {
      if ($result->num_rows) {
       $row = $result->fetch_assoc();
       $asscaixa = $row['totalasscaixa'];
     }
     } else {
     $asscaixa = '0';
    };


    // Calcula o total contas a receber
    $select_opt= "SELECT SQL_CACHE SUM(contasareceber.valor_bruto) as 'totalopcaixa' FROM  contasareceber INNER JOIN relacionamento_pagamentos_os ON contasareceber.id = relacionamento_pagamentos_os.fk_id_pagamentos INNER JOIN ordem_servico ON relacionamento_pagamentos_os.fk_id_os = ordem_servico.id WHERE (ordem_servico.tipo_os='Óptica' and contasareceber.FK_usuarios_admin_id = '".$_SESSION['idadmin']."' and contasareceber.fk_id_empresa = '".$_SESSION['id_empresa']."' and contasareceber.data_pagamento = CURDATE() and contasareceber.status='Pago' )  ";
     if ($result = $con->query($select_opt)) {
      if ($result->num_rows) {
       $row = $result->fetch_assoc();
       $opcaixa = $row['totalopcaixa'];
     }
     } else {
     $opcaixa = '0';
    };


 ?>