<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom functions file -->
    <script src="../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../sweetalert/js/sweetalert.min.js"></script>

    <title>Agenda </title>

  </head>

  <body>


<?php


  include '../sys/init';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    
  


			
          if(isset($_POST['atualizar_agendamento']))
		    {
	
	          $idagendamento = isset($_POST['idagendamento']) ? $_POST['idagendamento'] : '';
            $cor_agendamento = isset($_POST['cor_agendamento']) ? $_POST['cor_agendamento'] : '';
            $title_agendamento = isset($_POST['title_agendamento']) ? $_POST['title_agendamento'] : '';
         
           
            
            $cliente_agendamento_array = isset($_POST['cliente_agendamento']) ? $_POST['cliente_agendamento'] : '';
						list($cliente_id,$cliente) = explode('|', $cliente_agendamento_array);
            
            $representante_agendamento_array = isset($_POST['representante_agendamento']) ? $_POST['representante_agendamento'] : '';
						list($representante_id,$representante) = explode('|', $representante_agendamento_array);
            
            $datainicial_agendamento = isset($_POST['datainicial_agendamento']) ? $_POST['datainicial_agendamento'] : '';
            $horainicial_agendamento = isset($_POST['horainicial_agendamento']) ? $_POST['horainicial_agendamento'] : '';
            $horefinal_agendamento = isset($_POST['horefinal_agendamento']) ? $_POST['horefinal_agendamento'] : '';
            
              
            $start = implode("-",array_reverse(explode("/",$datainicial_agendamento))); 
            $start_completo = $start.' '.$horainicial_agendamento.':00';
            $end_completo = $start.' '.$horefinal_agendamento.':00';
					  
            $idadmin = $_SESSION['idadmin'];
	
         
	           $log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Atualizou','calendario')";
             $insert_log_eventos = mysqli_query($con, $log_eventos);
							  	

					 

                $query_atualizar_agenda = "update calendario set title='$title_agendamento',start='$start_completo',end='$end_completo',color='$cor_agendamento',fk_id_responsavel_id='$representante_id',tecnico_responsavel='$representante',FK_usuarios_admin_id='$idadmin',cliente='$cliente',fk_id_cliente='$cliente_id' where id='$idagendamento'";
                $update_atualizar_agenda = mysqli_query($con, $query_atualizar_agenda);


		    
                  if($update_atualizar_agenda){
					
			     
					echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Agendamento Atualizado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/calendario'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/calendario'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/calendario'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/calendario'    } });});</script>";
              }

		    }
	
   
//--------------------------------------------------


   if(isset($_POST['excluir_agendamento']))
		  {

 	      $idagendamento = isset($_POST['idagendamento']) ? $_POST['idagendamento'] : '';
		

		
	    	$sqldeletaragenda = "delete from calendario where id = '$idagendamento' ";
		    $deletaragenda = mysqli_query($con, $sqldeletaragenda);
		
		
		      if($deletaragenda)
			  {

					echo "<script>jQuery(function(){swal({   title: 'Deletar',   text: 'Agendamento apagado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/calendario'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/calendario'    } });});</script>";

					
              }else{

                     echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/clientes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/clientes'    } });});</script>";
                   }
	 

	     }

?>

</body>
</html>