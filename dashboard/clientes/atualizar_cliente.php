<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom functions file -->
    <script src="../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../sweetalert/js/sweetalert.min.js"></script>

    <title>Cliente </title>

  </head>

  <body>


<?php


  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    
  


			
          if(isset($_POST['salvar_alteracoes_cliente']))
		    {
	
	          $idcliente = isset($_POST['id_cliente_edicao']) ? $_POST['id_cliente_edicao'] : '';
						$pessoa = isset($_POST['pessoa']) ? $_POST['pessoa'] : '';
						$cnpj = isset($_POST['cnpj']) ? $_POST['cnpj'] : '';
            $nome = isset($_POST['nome']) ? $_POST['nome'] : '';
            $nome = ucwords(strtolower($nome));
            $data_nascimento = isset($_POST['data_nascimento']) ? $_POST['data_nascimento'] : '';
            $interesses ='';
            $telefone = isset($_POST['telefone']) ? $_POST['telefone'] : '';
            $celular = isset($_POST['celular']) ? $_POST['celular'] : '';
            $email = isset($_POST['email']) ? $_POST['email'] : '';
            $cpf = isset($_POST['cpf']) ? $_POST['cpf'] : '';
            $rg = isset($_POST['rg']) ? $_POST['rg'] : '';
            $endereco = isset($_POST['endereco']) ? $_POST['endereco'] : '';
            $cep = isset($_POST['cep']) ? $_POST['cep'] : '';
            $complemento_endereco = '';
						$como_conheceu = isset($_POST['como_conheceu']) ? $_POST['como_conheceu'] : '';
            
            $porcentagem = '';
            $fk_id_responsavel_id = '';
            $tecnico_responsavel = '';
            
            $status_atendimento = isset($_POST['status_atendimento']) ? $_POST['status_atendimento'] : '';
            $observacoes = isset($_POST['observacoes']) ? $_POST['observacoes'] : '';
           
            
            $cidade_array = isset($_POST['cidade']) ? $_POST['cidade'] : '';
						list($cidade,$cod_cidades) = explode('|', $cidade_array);
            
            $estado_array = isset($_POST['estado']) ? $_POST['estado'] : '';
						list($uf,$cod_estados) = explode('|', $estado_array);
            
        
            $data_nascimento = implode("-",array_reverse(explode("/",$data_nascimento))); 
           
       
					  
              $idadmin = $_SESSION['idadmin'];
	
         
	            	

					 

                $query_cadastro_cliente = "update cad_clientes set como_conheceu='$como_conheceu', cnpj='$cnpj',pessoa='$pessoa', nome='$nome',data_nascimento='$data_nascimento',interesses='$interesses',telefone='$telefone',celular='$celular',email='$email',cpf='$cpf',rg='$rg',endereco='$endereco',cep='$cep',complemento_endereco='$complemento_endereco',porcentagem='$porcentagem',fk_id_responsavel_id='$fk_id_responsavel_id',tecnico_responsavel='$tecnico_responsavel',status_atendimento='$status_atendimento',observacoes='$observacoes',cidade='$cidade',cod_cidades='$cod_cidades',uf='$uf',cod_estados='$cod_estados' where id='$idcliente'";
                $update_cadastro_cliente = mysqli_query($con, $query_cadastro_cliente);
						
						    $log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Atualizou','clientes')";
                $insert_log_eventos = mysqli_query($con, $log_eventos);
							


		    
                  if($update_cadastro_cliente){
					
			     
					echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Cliente Atualizado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/perfil_cliente?id=$idcliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente?id=$idcliente'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/perfil_cliente?id=$idcliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente?id=$idcliente'    } });});</script>";
              }

		    }
	
   
//--------------------------------------------------


   if(isset($_POST['excluir_cliente']))
		  {

 	      $idcliente = isset($_POST['id_cliente_edicao']) ? $_POST['id_cliente_edicao'] : '';
		

		
	    	$sqldeletarcliente = "delete from cad_clientes where id = '$idcliente' ";
		    $deletarcliente = mysqli_query($con, $sqldeletarcliente);
		
		
		      if($deletarcliente)
			  {

					echo "<script>jQuery(function(){swal({   title: 'Deletar',   text: 'Cliente apagado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/clientes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/clientes'    } });});</script>";

					
              }else{

                     echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/clientes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/clientes'    } });});</script>";
                   }
	 

	     }

?>

</body>
</html>