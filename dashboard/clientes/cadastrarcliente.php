<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
	<link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="../sweetalert/css/main.css" rel="stylesheet">
	<!-- Scroll Menu -->
	<link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


	<!-- Custom functions file -->
	<script src="../sweetalert/js/functions.js"></script>
	<!-- Sweet Alert Script -->
	<script src="../sweetalert/js/sweetalert.min.js"></script>

	<title>Clientes </title>

</head>

<body>

	<?php


	include '../sys/init.php';

	date_default_timezone_set('America/Sao_Paulo');

  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
	if (!$con->set_charset("utf8")) {}   





  if(isset($_POST['cadastrar_cliente']))
  {

  	$check_cpf =0;
  	$nome_cliente = isset($_POST['nome']) ? $_POST['nome'] : ''; 
    $nome_cliente = ucwords(strtolower($nome_cliente));

  	$celular = isset($_POST['celular']) ? $_POST['celular'] : '';
  	$cpf_cliente = isset($_POST['cpf']) ? $_POST['cpf'] : '';
  	$rg = isset($_POST['rg']) ? $_POST['rg'] : '';
  	$email = isset($_POST['email']) ? $_POST['email'] : '';
  	$status_atendimento = isset($_POST['status_atendimento']) ? $_POST['status_atendimento'] : '';
  	$observacoes = isset($_POST['observacoes']) ? $_POST['observacoes'] : '';

  	$cpf_titular = isset($_POST['cpf_titular']) ? $_POST['cpf_titular'] : '';


  	$idempresa = $_SESSION['id_empresa'];

  	$idadmin =$_SESSION['idadmin'];




	        //se tiver cpf verifica se ja esta cadastrado
  	if ($cpf_cliente != '')
  	{

  		$select_cpf = "SELECT * FROM cad_clientes WHERE cpf = '".$cpf_cliente."' and FK_usuarios_admin_id = '".$idadmin."' and id_empresa = '".$idempresa."' LIMIT 1";

  		$run_select_cpf = mysqli_query($con, $select_cpf); 

  		$check_cpf = mysqli_num_rows($run_select_cpf);  


  		if($check_cpf>0 ){echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Cliente já cadastrado',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/clientes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/clientes'    } });});</script>"; } 

  	}

  	if($check_cpf==0)

  	{

  		$query_cadastro_cliente = "INSERT INTO cad_clientes (observacoes,nome,celular,cpf,rg,email,FK_usuarios_admin_id,status_atendimento,data_registro,saldo,fk_id_responsavel_id,tecnico_responsavel,id_empresa,empresa) VALUES ('$observacoes','$nome_cliente','$celular','$cpf_cliente','$rg','$email','$idadmin','$status_atendimento',curdate(),'0','$idrepresentante','$representante','$idempresa','$empresa')";
  		$insert_cadastro_cliente = mysqli_query($con, $query_cadastro_cliente);

	     	       //pega chave estrangeira
  		$FK_cliente_admin_id = mysqli_insert_id($con);

  		$log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Novo','clientes')";
  		$insert_log_eventos = mysqli_query($con, $log_eventos);



  	
  		

  		if($insert_cadastro_cliente){



  			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Cliente cadastrado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/clientes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/clientes'    } });});</script>";

  		}else{
  			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/clientes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/clientes'    } });});</script>";
  		}


  	}

  }else
  {
  	echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/clientes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/clientes'    } });});</script>";


  }






//--------------------------------------------------		

  ?>

</body>
</html>