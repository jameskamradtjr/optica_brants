<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom functions file -->
    <script src="../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../sweetalert/js/sweetalert.min.js"></script>

    <title>GestãoInk </title>

  </head>

  <body>


<?php


  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    
  


			
          if(isset($_POST['salvar_agendamento']))
		    {
	
	         
            $cor_agendamento = isset($_POST['cor_agendamento']) ? $_POST['cor_agendamento'] : '';
            $title_agendamento = isset($_POST['title_agendamento']) ? $_POST['title_agendamento'] : '';
         
           $cliente_id = isset($_POST['id_cliente_edicao']) ? $_POST['id_cliente_edicao'] : '';
					 $cliente	= isset($_POST['nome_cliente_edicao']) ? $_POST['nome_cliente_edicao'] : ''; 
						
					   if(isset($_POST['alerta']))
		         {
	            $alerta = true;
						 }else{$alerta = false;}
           
            
            $representante_agendamento_array = isset($_POST['representante_agendamento']) ? $_POST['representante_agendamento'] : '';
						list($representante_id,$representante) = explode('|', $representante_agendamento_array);
            
            $datainicial_agendamento = isset($_POST['datainicial_agendamento']) ? $_POST['datainicial_agendamento'] : '';
            $horainicial_agendamento = isset($_POST['horainicial_agendamento']) ? $_POST['horainicial_agendamento'] : '';
            $horefinal_agendamento = isset($_POST['horefinal_agendamento']) ? $_POST['horefinal_agendamento'] : '';
            $status_atendimento = isset($_POST['status_atendimento']) ? $_POST['status_atendimento'] : '';
              
            $start = implode("-",array_reverse(explode("/",$datainicial_agendamento))); 
            $start_completo = $start.' '.$horainicial_agendamento.':00';
            $end_completo = $start.' '.$horefinal_agendamento.':00';
					  
            $idadmin = $_SESSION['idadmin'];
	
         
	            	

					 

                $query_agenda = "INSERT INTO calendario (status_alerta,title,start,end,color,fk_id_responsavel_id,tecnico_responsavel,FK_usuarios_admin_id,cliente,fk_id_cliente,alerta) values ('$status_atendimento','$title_agendamento','$start_completo','$end_completo','$cor_agendamento','$representante_id','$representante','$idadmin','$cliente','$cliente_id','$alerta') ";
                $query_go_agenda = mysqli_query($con, $query_agenda);
						
						    $log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Atualizou','Alertas')";
                $insert_log_eventos = mysqli_query($con, $log_eventos);


		    
                  if($query_go_agenda){
					
			     
					echo "<script>jQuery(function(){swal({   title: 'Agenda',   text: 'Agendamento adicionado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/calendario'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/calendario'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/calendario'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/calendario'    } });});</script>";
              }

		    }
	
   


?>

</body>
</html>