<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
	<link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="../../sweetalert/css/main.css" rel="stylesheet">
	<!-- Scroll Menu -->
	<link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


	<!-- Custom functions file -->
	<script src="../../sweetalert/js/functions.js"></script>
	<!-- Sweet Alert Script -->
	<script src="../../sweetalert/js/sweetalert.min.js"></script>

	<title>GestãoInk </title>

</head>

<body>

	<?php

	if (!isset($_SESSION)) session_start();

	include '../../sys/init.php';



  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
	if (!$con->set_charset("utf8")) {}    


		date_default_timezone_set('America/Sao_Paulo');

		//função para gravar os valores no mysql
	function moeda($get_valor) {
		$source = array('.', ','); 
		$replace = array('', '.');
	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
	return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql

if(isset($_POST['salvar_receita']))
{

         //funcao converte a data br para a do mysql
	function inverteData($data){
		if(count(explode("/",$data)) > 1){
			return implode("-",array_reverse(explode("/",$data)));
		}elseif(count(explode("-",$data)) > 1){
			return implode("/",array_reverse(explode("-",$data)));
		}
	}
			 //funcao converte a data br para a do mysql

					//verifica tamanho do arquivo se for muito grande nao passa
	if ($_FILES["contasareceber_anexar"]["size"] > 5000000) {
		echo "<script>jQuery(function(){swal({   title: 'Anexo Contas',   text: 'Arquivo muito grande!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/../financeiro'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/../financeiro'    } });});</script>";

		exit;
	}


	$contasareceber_conta_array= isset($_POST['conta_receita']) ? $_POST['conta_receita'] : '';
	list($idcontasareceber_conta,$contasareceber_conta) = explode('|', $contasareceber_conta_array);

	$contasareceber_cliente_array= isset($_POST['contasareceber_cliente']) ? $_POST['contasareceber_cliente'] : '';
	list($idcontasareceber_cliente,$contasareceber_cliente) = explode('|', $contasareceber_cliente_array);  

	$contasareceber_categoria_array= isset($_POST['categoria_receita']) ? $_POST['categoria_receita'] : '';
	list($grupo_categoria,$idcontasareceber_categoria,$contasareceber_categoria) = explode('|', $contasareceber_categoria_array);  


	$contasareceber_descricao= isset($_POST['descricao_receita']) ? $_POST['descricao_receita'] : '';
	$contasareceber_vencimento= isset($_POST['data_receita']) ? $_POST['data_receita'] : '';

	$contasareceber_valor_bruto= isset($_POST['valor_receita']) ? $_POST['valor_receita'] : '';
	$contasareceber_valor_bruto = moeda($contasareceber_valor_bruto);

	$idempresa = $_SESSION['id_empresa'];




	$contasareceber_parcelas= isset($_POST['parcelas_receita']) ? $_POST['parcelas_receita'] : '';
	$contasareceber_pagamento= isset($_POST['contasareceber_pagamento']) ? $_POST['contasareceber_pagamento'] : '';
	$contasareceber_obs= isset($_POST['contasareceber_obs']) ? $_POST['contasareceber_obs'] : '';
	$contasareceber_status= isset($_POST['status_receita']) ? $_POST['status_receita'] : '';

	$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';
	$idcolab = isset($_SESSION['idcolab']) ? $_SESSION['idcolab'] : '';	


	$periodicidade = 'mensal';

	$relacionamento = date("dmYhis");
	$relacionamento = $idadmin.$relacionamento;




	$dataprimeirovencimento = $contasareceber_vencimento;


      //    $contasareceber_valor_bruto  = $contasareceber_valor_bruto / 	$contasareceber_parcelas;	  
//$contasareceber_valor_liquido = $contasareceber_valor_liquido / $contasareceber_parcelas;

	if(isset($_POST['salvar_receita'])){



		for ($nr=1; $nr<=$contasareceber_parcelas; $nr++){

			if($contasareceber_status == 'Pago'){	

				$query_cadastro_contasareceber = "INSERT INTO contasareceber (fk_id_empresa,data_pagamento,relacionamento,grupo_categoria,descricao,categoria,FK_categoria_id,cliente,fk_id_cliente,data_vencimento,data_cadastro,conta,fk_conta_id,valor_liquido,valor_bruto,parcelas,parcela,status,observacao,forma_pagamento,FK_usuarios_admin_id,FK_usuarios_colab_id) VALUES ('$idempresa','$contasareceber_vencimento','$relacionamento','$grupo_categoria','$contasareceber_descricao','$contasareceber_categoria','$idcontasareceber_categoria','$contasareceber_cliente','$idcontasareceber_cliente','$contasareceber_vencimento',CURDATE(),'$contasareceber_conta','$idcontasareceber_conta','$contasareceber_valor_liquido','$contasareceber_valor_bruto','$contasareceber_parcelas','$nr','$contasareceber_status','$contasareceber_obs','$contasareceber_pagamento','$idadmin','$idcolab')";

				$insert_cadastro_contasareceber = mysqli_query($con, $query_cadastro_contasareceber);		   

			}else{

				$query_cadastro_contasareceber = "INSERT INTO contasareceber (fk_id_empresa,relacionamento,grupo_categoria,descricao,categoria,FK_categoria_id,cliente,fk_id_cliente,data_vencimento,data_cadastro,conta,fk_conta_id,valor_liquido,valor_bruto,parcelas,parcela,status,observacao,forma_pagamento,FK_usuarios_admin_id,FK_usuarios_colab_id) VALUES ('$idempresa','$relacionamento','$grupo_categoria','$contasareceber_descricao','$contasareceber_categoria','$idcontasareceber_categoria','$contasareceber_cliente','$idcontasareceber_cliente','$contasareceber_vencimento',CURDATE(),'$contasareceber_conta','$idcontasareceber_conta','$contasareceber_valor_liquido','$contasareceber_valor_bruto','$contasareceber_parcelas','$nr','$contasareceber_status','$contasareceber_obs','$contasareceber_pagamento','$idadmin','$idcolab')";

				$insert_cadastro_contasareceber = mysqli_query($con, $query_cadastro_contasareceber);		   


			}


								 //deixa data vencimento conforme periodicidade
			switch ($periodicidade) {
				case 'Mensal':
				$contasareceber_vencimento = date('d/m/Y',strtotime($nr.' month', strtotime($dataprimeirovencimento)));
				$contasareceber_vencimento = inverteData($contasareceber_vencimento);
				break;
				case 'Trimestral':									
				$perid=$nr*3; 
				$contasareceber_vencimento = date('d/m/Y',strtotime($perid.' month', strtotime($dataprimeirovencimento)));
				$contasareceber_vencimento = inverteData($contasareceber_vencimento);
				break;
				case 'Semestral':
				$perid=$nr*6;
				$contasareceber_vencimento = date('d/m/Y',strtotime($perid.' month', strtotime($dataprimeirovencimento)));
				$contasareceber_vencimento = inverteData($contasareceber_vencimento);
				break;
				case 'Anual':
				$perid=$nr*12;
				$contasareceber_vencimento = date('d/m/Y',strtotime($perid.' month', strtotime($dataprimeirovencimento)));
				$contasareceber_vencimento = inverteData($contasareceber_vencimento);
				break;

				default:
				$contasareceber_vencimento = date('d/m/Y',strtotime($nr.' month', strtotime($dataprimeirovencimento)));
				$contasareceber_vencimento = inverteData($contasareceber_vencimento);
			}


			$contasareceber_status = 'A Pagar';		

			if ( $nr == 1){

				if(!empty($_FILES['contasareceber_anexar']['name'])){
     				           $nome_real=mysqli_insert_id($con);//pega o id criado
     				           $arquivo = $nome_real.'.'.pathinfo($_FILES['contasareceber_anexar']['name'], PATHINFO_EXTENSION);
     				           move_uploaded_file($_FILES["contasareceber_anexar"]["tmp_name"],"../../estudios/$idadmin/anexos/receitas/$arquivo");
     				           $arquivo = "http://gestaoink.com.br/dashboard/estudios/$idadmin/anexos/receitas/$arquivo";
		                   //atualiza o caminho da foto com o id unico
     				           $queryupdate = "UPDATE contasareceber SET anexo='$arquivo' WHERE ID='$nome_real'";
     				           $update = mysqli_query($con, $queryupdate);
     				       }

     				   }

     				}

     				if($insert_cadastro_contasareceber){

     					$log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Nova','receita')";
     					$insert_log_eventos = mysqli_query($con, $log_eventos);


     					echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Nova conta cadastrada com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/../movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/../movimentacoes'    } });});</script>";

     				}else{
     					echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/../movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/../movimentacoes'    } });});</script>";
     				}


     			}else
     			{
     				echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/../movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/../movimentacoes'    } });});</script>";
     			}

     		}else
     		{
     			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/../movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/../movimentacoes'    } });});</script>";
     		}







//--------------------------------------------------		

     		?>

     	</body>
     	</html>