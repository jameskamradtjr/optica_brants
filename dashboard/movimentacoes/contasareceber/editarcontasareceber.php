<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


    <!-- Custom functions file -->
    <script src="../../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../../sweetalert/js/sweetalert.min.js"></script>

    <title>GestãoInk </title>

  </head>

  <body>

<?php

if (!isset($_SESSION)) session_start();

  include '../../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

  
 date_default_timezone_set('America/Sao_Paulo');
		
		//função para gravar os valores no mysql
 function moeda($get_valor) {
	$source = array('.', ','); 
	$replace = array('', '.');
	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
	return $valor; //retorna o valor formatado para gravar no banco
  }
//função para gravar os valores no mysql

          if(isset($_POST['editar_salvar_recebimento']))
		  {
        
         //funcao converte a data br para a do mysql
			  function inverteData($data){
              if(count(explode("/",$data)) > 1){
                 return implode("-",array_reverse(explode("/",$data)));
                   }elseif(count(explode("-",$data)) > 1){
                      return implode("/",array_reverse(explode("-",$data)));
                      }
                   }
			 //funcao converte a data br para a do mysql
				
				if ($_FILES["contasareceber_anexar_edicao"]["size"] > 5000000) {
        	echo "<script>jQuery(function(){swal({   title: 'Anexo Contas',   text: 'Arquivo muito grande!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/../financeiro'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/../financeiro'    } });});</script>";

          exit;
        }
			 
			   $contasareceber_id= isset($_POST['ideditarreceita']) ? $_POST['ideditarreceita'] : '';
        
	       $contasareceber_conta_array= isset($_POST['editar_conta_recebimento']) ? $_POST['editar_conta_recebimento'] : '';
		     list($idcontasareceber_conta,$contasareceber_conta) = explode('|', $contasareceber_conta_array);
        
         $contasareceber_cliente_array= isset($_POST['contasareceber_cliente_edicao']) ? $_POST['contasareceber_cliente_edicao'] : '';
		     list($idcontasareceber_cliente,$contasareceber_cliente) = explode('|', $contasareceber_cliente_array);  
        
         $contasareceber_categoria_array= isset($_POST['editar_categoria_recebimento']) ? $_POST['editar_categoria_recebimento'] : '';
		     list($grupo_categoria,$idcontasareceber_categoria,$contasareceber_categoria) = explode('|', $contasareceber_categoria_array);  
	        
	     
          $contasareceber_descricao= isset($_POST['editar_descricao_receita']) ? $_POST['editar_descricao_receita'] : '';
          $contasareceber_vencimento= isset($_POST['editar_data_recebimento']) ? $_POST['editar_data_recebimento'] : '';
          
          $contasareceber_valor_bruto= isset($_POST['editar_valor_recebimento']) ? $_POST['editar_valor_recebimento'] : '';
				  $contasareceber_valor_bruto = moeda($contasareceber_valor_bruto);
				 
         // $contasareceber_valor_liquido= isset($_POST['contasareceber_valor_liquido_edicao']) ? $_POST['contasareceber_valor_liquido_edicao'] : '';
				 // $contasareceber_valor_liquido = moeda($contasareceber_valor_liquido);
				
				
          $contasareceber_parcela= isset($_POST['editar_parcelas_recebimento']) ? $_POST['editar_parcelas_recebimento'] : '';
         // $contasareceber_pagamento= isset($_POST['contasareceber_pagamento_edicao']) ? $_POST['contasareceber_pagamento_edicao'] : '';
        //  $contasareceber_obs= isset($_POST['contasareceber_obs_edicao']) ? $_POST['contasareceber_obs_edicao'] : '';
          $contasareceber_status= isset($_POST['editar_status_recebimento']) ? $_POST['editar_status_recebimento'] : '';
        
           $contasareceber_relacionamento= isset($_POST['relacionamento_receita_edicao']) ? $_POST['relacionamento_receita_edicao'] : '';
   		  
           $idadmin ='1';
         
				   $day = date('d', strtotime($contasareceber_vencimento));

   		  
       	  
            
             if(isset($_POST['editar_salvar_recebimento'])){
              
           //     if(isset($_POST['todoscontasareceber'])){
									
								//TODOS---------------------	
									
						//		if($contasareceber_status == 'Pago'){	
			       
					//		  $query_cadastro_contasareceber = "update contasareceber set data_pagamento=CONCAT(DATE_FORMAT(data_vencimento, '%Y-%m-'), grupo_categoria='$grupo_categoria', descricao = '$contasareceber_descricao',categoria = '$contasareceber_categoria' ,FK_categoria_id = '$idcontasareceber_categoria',cliente = '$contasareceber_cliente',fk_id_cliente = '$idcontasareceber_cliente',data_vencimento = CONCAT(DATE_FORMAT(data_vencimento, '%Y-%m-'), '$day'),conta = '$contasareceber_conta',fk_conta_id = '$idcontasareceber_conta',valor_liquido = '$contasareceber_valor_liquido',valor_bruto = '$contasareceber_valor_bruto',observacao = '$contasareceber_obs',forma_pagamento = '$contasareceber_pagamento' where (relacionamento = '$contasareceber_relacionamento' and parcela >= '$contasareceber_parcela' ) ";
                             
          //      $insert_cadastro_contasareceber = mysqli_query($con, $query_cadastro_contasareceber);		   
							
									
							
					//			$query_cadastro_contasareceber2 = "update contasareceber set data_pagamento='$contasareceber_vencimento', grupo_categoria='$grupo_categoria', descricao = '$contasareceber_descricao',categoria = '$contasareceber_categoria' ,FK_categoria_id = '$idcontasareceber_categoria',cliente = '$contasareceber_cliente',fk_id_cliente = '$idcontasareceber_cliente',data_vencimento = '$contasareceber_vencimento',conta = '$contasareceber_conta',fk_conta_id = '$idcontasareceber_conta',valor_liquido = '$contasareceber_valor_liquido',valor_bruto = '$contasareceber_valor_bruto',status = '$contasareceber_status',observacao = '$contasareceber_obs',forma_pagamento = '$contasareceber_pagamento' where id = '$contasareceber_id' ";
                             
         //       $insert_cadastro_contasareceber2 = mysqli_query($con, $query_cadastro_contasareceber2);
									
					//			}else{
									
		     //       $query_cadastro_contasareceber = "update contasareceber set grupo_categoria='$grupo_categoria', descricao = '$contasareceber_descricao',categoria = '$contasareceber_categoria' ,FK_categoria_id = '$idcontasareceber_categoria',cliente = '$contasareceber_cliente',fk_id_cliente = '$idcontasareceber_cliente',data_vencimento = CONCAT(DATE_FORMAT(data_vencimento, '%Y-%m-'), '$day'),conta = '$contasareceber_conta',fk_conta_id = '$idcontasareceber_conta',valor_liquido = '$contasareceber_valor_liquido',valor_bruto = '$contasareceber_valor_bruto',observacao = '$contasareceber_obs',forma_pagamento = '$contasareceber_pagamento' where (relacionamento = '$contasareceber_relacionamento' and parcela >= '$contasareceber_parcela' ) ";
                             
         //       $insert_cadastro_contasareceber = mysqli_query($con, $query_cadastro_contasareceber);		   
							
									
							
					//			$query_cadastro_contasareceber2 = "update contasareceber set grupo_categoria='$grupo_categoria', descricao = '$contasareceber_descricao',categoria = '$contasareceber_categoria' ,FK_categoria_id = '$idcontasareceber_categoria',cliente = '$contasareceber_cliente',fk_id_cliente = '$idcontasareceber_cliente',data_vencimento = '$contasareceber_vencimento',conta = '$contasareceber_conta',fk_conta_id = '$idcontasareceber_conta',valor_liquido = '$contasareceber_valor_liquido',valor_bruto = '$contasareceber_valor_bruto',status = '$contasareceber_status',observacao = '$contasareceber_obs',forma_pagamento = '$contasareceber_pagamento' where id = '$contasareceber_id' ";
                             
          //      $insert_cadastro_contasareceber2 = mysqli_query($con, $query_cadastro_contasareceber2);							
									
									
				//				}	
									
							//TODOS---------------------		
							
       //      }else{
									
									//unico---------------------	
									
								if($contasareceber_status == 'Pago'){	
			          
									$query_cadastro_contasareceber = "update contasareceber set data_pagamento='$contasareceber_vencimento', grupo_categoria='$grupo_categoria', descricao = '$contasareceber_descricao',categoria = '$contasareceber_categoria' ,FK_categoria_id = '$idcontasareceber_categoria',cliente = '$contasareceber_cliente',fk_id_cliente = '$idcontasareceber_cliente',data_vencimento = '$contasareceber_vencimento',conta = '$contasareceber_conta',fk_conta_id = '$idcontasareceber_conta',valor_liquido = '$contasareceber_valor_liquido',valor_bruto = '$contasareceber_valor_bruto',status = '$contasareceber_status',observacao = '$contasareceber_obs',forma_pagamento = '$contasareceber_pagamento' where id = '$contasareceber_id' ";
                             
                  $insert_cadastro_contasareceber = mysqli_query($con, $query_cadastro_contasareceber);	
									
								}else{
									
									$query_cadastro_contasareceber = "update contasareceber set grupo_categoria='$grupo_categoria', descricao = '$contasareceber_descricao',categoria = '$contasareceber_categoria' ,FK_categoria_id = '$idcontasareceber_categoria',cliente = '$contasareceber_cliente',fk_id_cliente = '$idcontasareceber_cliente',data_vencimento = '$contasareceber_vencimento',conta = '$contasareceber_conta',fk_conta_id = '$idcontasareceber_conta',valor_liquido = '$contasareceber_valor_liquido',valor_bruto = '$contasareceber_valor_bruto',status = '$contasareceber_status',observacao = '$contasareceber_obs',forma_pagamento = '$contasareceber_pagamento' where id = '$contasareceber_id' ";
                             
                  $insert_cadastro_contasareceber = mysqli_query($con, $query_cadastro_contasareceber);	
						
									
								}	
								
								//unico---------------------		
									
							//	}
           		    
                if($insert_cadastro_contasareceber){
									
									
									//if(!empty($_FILES['contasareceber_anexar_edicao']['name'])){
     				      //     $nome_real=$contasareceber_id;//pega o id criado
									//		 $arquivo = $nome_real.'.'.pathinfo($_FILES['contasareceber_anexar_edicao']['name'], PATHINFO_EXTENSION);
                  //     move_uploaded_file($_FILES["contasareceber_anexar_edicao"]["tmp_name"],"../../estudios/$idadmin/anexos/receitas/$arquivo");
                  //     $arquivo = "http://gestaoink.com.br/dashboard/estudios/$idadmin/anexos/receitas/$arquivo";
		                   //atualiza o caminho da foto com o id unico
					       //      $queryupdate = "UPDATE contasareceber SET anexo='$arquivo' WHERE ID='$nome_real'";
                 //      $update = mysqli_query($con, $queryupdate);
				         //    }
					
								$log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Atualizou','receitas')";
                $insert_log_eventos = mysqli_query($con, $log_eventos);
			            
				       	echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Conta editada com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";
              }


	         }else
			  {
		  	  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";
        }
	
      }
		
		if(isset($_POST['excluir_contasareceber_edicao']))
			{
				
				
		//		 $contasareceber_relacionamento= isset($_POST['relacionamento_receita_edicao']) ? $_POST['relacionamento_receita_edicao'] : '';
         $contasareceber_id= isset($_POST['ideditarreceita']) ? $_POST['ideditarreceita'] : '';
     //    $contasareceber_parcela= isset($_POST['contasareceber_parcela_edicao']) ? $_POST['contasareceber_parcela_edicao'] : '';
						  
				
				  //  if(isset($_POST['todoscontasareceber'])){
							
			     //       $query_cadastro_contasareceber = "delete from contasareceber where (relacionamento = '$contasareceber_relacionamento' and parcela >= '$contasareceber_parcela' )";
            //      $delete_cadastro_contasareceber = mysqli_query($con, $query_cadastro_contasareceber); 
							
					
 	
							 
						//	 }else{
							    $query_cadastro_contasareceber = " delete from contasareceber where id = '$contasareceber_id' ";
                  $delete_cadastro_contasareceber = mysqli_query($con, $query_cadastro_contasareceber);
							    
							  
							
							 //} 
				
		             if($delete_cadastro_contasareceber){
									
		            
				       	echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Conta apagada com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";
              }
			
			
			}
			

    	
//--------------------------------------------------		

?>

</body>
</html>