<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
	<link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="../../sweetalert/css/main.css" rel="stylesheet">
	<!-- Scroll Menu -->
	<link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


	<!-- Custom functions file -->
	<script src="../../sweetalert/js/functions.js"></script>
	<!-- Sweet Alert Script -->
	<script src="../../sweetalert/js/sweetalert.min.js"></script>

	<title>GestãoInk </title>

</head>

<body>

	<?php

	if (!isset($_SESSION)) session_start();

	include '../../sys/init.php';



  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
	if (!$con->set_charset("utf8")) {}    

		date_default_timezone_set('America/Sao_Paulo');


		//função para gravar os valores no mysql
	function moeda($get_valor) {
		$source = array('.', ','); 
		$replace = array('', '.');
	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
	return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql


if(isset($_POST['salvar_pagamento']))
{

        //funcao converte a data br para a do mysql
	function inverteData($data){
		if(count(explode("/",$data)) > 1){
			return implode("-",array_reverse(explode("/",$data)));
		}elseif(count(explode("-",$data)) > 1){
			return implode("/",array_reverse(explode("-",$data)));
		}
	}
			 //funcao converte a data br para a do mysql



	$contasapagar_conta_array= isset($_POST['conta_despesa']) ? $_POST['conta_despesa'] : '';
	list($idcontasapagar_conta,$contasapagar_conta) = explode('|', $contasapagar_conta_array);

	$contasapagar_fornecedor_array= isset($_POST['contasapagar_fornecedor']) ? $_POST['contasapagar_fornecedor'] : '';
	list($idcontasapagar_fornecedor,$contasapagar_fornecedor) = explode('|', $contasapagar_fornecedor_array);

	$contasapagar_categoria_array= isset($_POST['categoria_despesa']) ? $_POST['categoria_despesa'] : '';
	list($grupo_categoria,$idcontasapagar_categoria,$contasapagar_categoria) = explode('|', $contasapagar_categoria_array);

	$contasapagar_descricao= isset($_POST['descricao_despesa']) ? $_POST['descricao_despesa'] : '';				  

	$contasapagar_vencimento= isset($_POST['data_despesa']) ? $_POST['data_despesa'] : '';

	$contasapagar_valor_bruto= isset($_POST['valor_despesa']) ? $_POST['valor_despesa'] : '';
	$contasapagar_valor_bruto = moeda($contasapagar_valor_bruto);
	$contasapagar_valor_bruto = '-'.$contasapagar_valor_bruto;

	$contasapagar_parcelas= isset($_POST['parcelas_despesa']) ? $_POST['parcelas_despesa'] : '';
	$contasapagar_pagamento= isset($_POST['contasapagar_pagamento']) ? $_POST['contasapagar_pagamento'] : '';
	$contasapagar_obs= isset($_POST['contasapagar_obs']) ? $_POST['contasapagar_obs'] : '';
	$contasapagar_status= isset($_POST['status_despesa']) ? $_POST['status_despesa'] : '';
	$idempresa = $_SESSION['id_empresa'];

				//  $periodicidade = isset($_POST['periodicidade_contasapagar']) ? $_POST['periodicidade_contasapagar'] : '';

	$periodicidade = 'Mensal';
	$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';
	$idcolab = isset($_SESSION['idcolab']) ? $_SESSION['idcolab'] : '';		

	$relacionamento = date("dmYhis");
	$relacionamento = $idadmin.$relacionamento;
				//  $contasapagar_vencimento = inverteData($contasapagar_vencimento);
	$dataprimeirovencimento = $contasapagar_vencimento;

				//  $contasapagar_valor_bruto  = $contasapagar_valor_bruto / 	$contasapagar_parcelas;	  
       	//  $contasapagar_valor_liquido = $contasapagar_valor_liquido / $contasapagar_parcelas;



	if(isset($_POST['descricao_despesa'])){

		for ($nr=1; $nr<=$contasapagar_parcelas; $nr++){

			if($contasapagar_status == 'Pago'){

				$query_cadastro_contasapagar = "INSERT INTO contasapagar (fk_id_empresa,data_pagamento,periodicidade,relacionamento,grupo_categoria,descricao,categoria,FK_categoria_id,fornecedor,FK_fornecedor_id,data_vencimento,data_cadastro,conta,fk_conta_id,valor_liquido,valor_bruto,parcelas,parcela,status,observacao,forma_pagamento,FK_usuarios_admin_id,FK_usuarios_colab_id) VALUES ('$idempresa','$contasapagar_vencimento','$periodicidade','$relacionamento','$grupo_categoria','$contasapagar_descricao','$contasapagar_categoria','$idcontasapagar_categoria','$contasapagar_fornecedor','$idcontasapagar_fornecedor','$contasapagar_vencimento',CURDATE(),'$contasapagar_conta','$idcontasapagar_conta','$contasapagar_valor_liquido','$contasapagar_valor_bruto','$contasapagar_parcelas','$nr','$contasapagar_status','$contasapagar_obs','$contasapagar_pagamento','$idadmin','$idcolab')";
				$insert_cadastro_contasapagar = mysqli_query($con, $query_cadastro_contasapagar);  

			}else{

				$query_cadastro_contasapagar = "INSERT INTO contasapagar (fk_id_empresa,periodicidade,relacionamento,grupo_categoria,descricao,categoria,FK_categoria_id,fornecedor,FK_fornecedor_id,data_vencimento,data_cadastro,conta,fk_conta_id,valor_liquido,valor_bruto,parcelas,parcela,status,observacao,forma_pagamento,FK_usuarios_admin_id,FK_usuarios_colab_id) VALUES ('$idempresa','$periodicidade','$relacionamento','$grupo_categoria','$contasapagar_descricao','$contasapagar_categoria','$idcontasapagar_categoria','$contasapagar_fornecedor','$idcontasapagar_fornecedor','$contasapagar_vencimento',CURDATE(),'$contasapagar_conta','$idcontasapagar_conta','$contasapagar_valor_liquido','$contasapagar_valor_bruto','$contasapagar_parcelas','$nr','$contasapagar_status','$contasapagar_obs','$contasapagar_pagamento','$idadmin','$idcolab')";
				$insert_cadastro_contasapagar = mysqli_query($con, $query_cadastro_contasapagar);  

			}	 

							 //deixa data vencimento conforme periodicidade
			switch ($periodicidade) {
				case 'Mensal':
				$contasapagar_vencimento = date('d/m/Y',strtotime($nr.' month', strtotime($dataprimeirovencimento)));
				$contasapagar_vencimento = inverteData($contasapagar_vencimento);
				break;
				case 'Trimestral':									
				$perid=$nr*3; 
				$contasapagar_vencimento = date('d/m/Y',strtotime($perid.' month', strtotime($dataprimeirovencimento)));
				$contasapagar_vencimento = inverteData($contasapagar_vencimento);
				break;
				case 'Semestral':
				$perid=$nr*6;
				$contasapagar_vencimento = date('d/m/Y',strtotime($perid.' month', strtotime($dataprimeirovencimento)));
				$contasapagar_vencimento = inverteData($contasapagar_vencimento);
				break;
				case 'Anual':
				$perid=$nr*12;
				$contasapagar_vencimento = date('d/m/Y',strtotime($perid.' month', strtotime($dataprimeirovencimento)));
				$contasapagar_vencimento = inverteData($contasapagar_vencimento);
				break;

				default:
				$contasapagar_vencimento = date('d/m/Y',strtotime($nr.' month', strtotime($dataprimeirovencimento)));
				$contasapagar_vencimento = inverteData($contasapagar_vencimento);
			}




			$contasapagar_status = 'A Pagar';	 
								 //anexa o arquivo se existir no primeiro registro apenas
			if ( $nr == 1){

				if(!empty($_FILES['contasapagar_anexar']['name'])){
     				           $nome_real=mysqli_insert_id($con);//pega o id criado
     				           $arquivo = $nome_real.'.'.pathinfo($_FILES['contasapagar_anexar']['name'], PATHINFO_EXTENSION);
     				           move_uploaded_file($_FILES["contasapagar_anexar"]["tmp_name"],"../../estudios/$idadmin/anexos/despesas/$arquivo");
     				           $arquivo = "http://gestaoink.com.br/dashboard/estudios/$idadmin/anexos/despesas/$arquivo";
		                   //atualiza o caminho da foto com o id unico
     				           $queryupdate = "UPDATE contasapagar SET anexo='$arquivo' WHERE ID='$nome_real'";
     				           $update = mysqli_query($con, $queryupdate);
     				       }

     				   }

     				}

     				if($insert_cadastro_contasapagar){

     					$log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Nova','despesa')";
     					$insert_log_eventos = mysqli_query($con, $log_eventos);

     					echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Nova conta cadastrada com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";

     				}else{
     					echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";
     				}


     			}else
     			{
     				echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";
     			}

     		}else
     		{
     			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";
     		}







//--------------------------------------------------		

     		?>

     	</body>
     	</html>