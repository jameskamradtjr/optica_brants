<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


    <!-- Custom functions file -->
    <script src="../../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../../sweetalert/js/sweetalert.min.js"></script>

    <title>GestãoInvest </title>

  </head>

  <body>

<?php

if (!isset($_SESSION)) session_start();

  include '../../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}   
		
		
			//função para gravar os valores no mysql
 function moeda($get_valor) {
	$source = array('.', ','); 
	$replace = array('', '.');
	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
	return $valor; //retorna o valor formatado para gravar no banco
  }
//função para gravar os valores no mysql

  


          if(isset($_POST['editar_salvar_pagamento']))
		  {
        
        //funcao converte a data br para a do mysql
			  function inverteData($data){
              if(count(explode("/",$data)) > 1){
                 return implode("-",array_reverse(explode("/",$data)));
                   }elseif(count(explode("-",$data)) > 1){
                      return implode("/",array_reverse(explode("-",$data)));
                      }
                   }
			 //funcao converte a data br para a do mysql
				
				date_default_timezone_set('America/Sao_Paulo');
					//verifica tamanho do arquivo se for muito grande nao passa
			
        
         $contasapagar_id= isset($_POST['ideditardespesa']) ? $_POST['ideditardespesa'] : '';
			 
			   $contasapagar_conta_array= isset($_POST['editar_conta_despesa']) ? $_POST['editar_conta_despesa'] : '';
		     list($idcontasapagar_conta,$contasapagar_conta) = explode('|', $contasapagar_conta_array);
        
        // $contasapagar_fornecedor_array= isset($_POST['contasapagar_fornecedor_edicao']) ? $_POST['contasapagar_fornecedor_edicao'] : '';
		    // list($idcontasapagar_fornecedor,$contasapagar_fornecedor) = explode('|', $contasapagar_fornecedor_array);      
	        
	       $contasapagar_descricao= isset($_POST['editar_descricao_despesa']) ? $_POST['editar_descricao_despesa'] : '';
        
         $contasapagar_categoria_array= isset($_POST['editar_categoria_despesa']) ? $_POST['editar_categoria_despesa'] : '';
		     list($grupo_categoria,$idcontasapagar_categoria,$contasapagar_categoria) = explode('|', $contasapagar_categoria_array);
          
          $contasapagar_vencimento= isset($_POST['editar_data_despesa']) ? $_POST['editar_data_despesa'] : '';
          
          $contasapagar_valor_bruto= isset($_POST['editar_valor_despesa']) ? $_POST['editar_valor_despesa'] : '';
				  $contasapagar_valor_bruto = moeda($contasapagar_valor_bruto);
          $contasapagar_valor_bruto = '-'.$contasapagar_valor_bruto;
				
          $contasapagar_parcela= isset($_POST['editar_parcelas_despesa']) ? $_POST['editar_parcelas_despesa'] : '';
         // $contasapagar_pagamento= isset($_POST['contasapagar_pagamento_edicao']) ? $_POST['contasapagar_pagamento_edicao'] : '';
         // $contasapagar_obs= isset($_POST['contasapagar_obs_edicao']) ? $_POST['contasapagar_obs_edicao'] : '';
          $contasapagar_status= isset($_POST['editar_status_despesa']) ? $_POST['editar_status_despesa'] : '';
				  
				  $contasapagar_relacionamento= isset($_POST['relacionamento_despesa_edicao']) ? $_POST['relacionamento_despesa_edicao'] : '';
   		  
          $idadmin = $_SESSION['idadmin'];
				
				
				  //alterando datas subsequentes das parcelas
				
				  $day = date('d', strtotime($contasapagar_vencimento));
				  
   
         
           
             if(isset($_POST['editar_salvar_pagamento'])){
							 
							 
               
	         //    if(isset($_POST['todoscontasapagar'])){ 
							 
								//todos----------------- 
					//			 if($contasapagar_status == 'Pago'){
			    //        $query_cadastro_contasapagar = "UPDATE contasapagar SET data_pagamento=CONCAT(DATE_FORMAT(data_vencimento, '%Y-%m-'), '$day') , grupo_categoria='$grupo_categoria', descricao = '$contasapagar_descricao',categoria = '$contasapagar_categoria' ,FK_categoria_id = '$idcontasapagar_categoria',fornecedor = '$contasapagar_fornecedor',FK_fornecedor_id = '$idcontasapagar_fornecedor',data_vencimento = CONCAT(DATE_FORMAT(data_vencimento, '%Y-%m-'), '$day') ,conta = '$contasapagar_conta',fk_conta_id = '$idcontasapagar_conta',valor_liquido = '$contasapagar_valor_liquido',valor_bruto = '$contasapagar_valor_bruto',status = '$contasapagar_status',observacao = '$contasapagar_obs',forma_pagamento = '$contasapagar_pagamento' where (relacionamento = '$contasapagar_relacionamento' and parcela >= '$contasapagar_parcela' )";
          //        $insert_cadastro_contasapagar = mysqli_query($con, $query_cadastro_contasapagar);  
							   
					//			  $query_cadastro_contasapagar2 = "UPDATE contasapagar SET data_pagamento= '$contasapagar_vencimento', grupo_categoria='$grupo_categoria', descricao = '$contasapagar_descricao',categoria = '$contasapagar_categoria' ,FK_categoria_id = '$idcontasapagar_categoria',fornecedor = '$contasapagar_fornecedor',FK_fornecedor_id = '$idcontasapagar_fornecedor',data_vencimento = '$contasapagar_vencimento',conta = '$contasapagar_conta',fk_conta_id = '$idcontasapagar_conta',valor_liquido = '$contasapagar_valor_liquido',valor_bruto = '$contasapagar_valor_bruto',status = '$contasapagar_status',observacao = '$contasapagar_obs',forma_pagamento = '$contasapagar_pagamento' where id = '$contasapagar_id' ";
           //       $insert_cadastro_contasapagar2 = mysqli_query($con, $query_cadastro_contasapagar2);  	
					//			 }else{
									 
					//				 $query_cadastro_contasapagar = "UPDATE contasapagar SET grupo_categoria='$grupo_categoria', descricao = '$contasapagar_descricao',categoria = '$contasapagar_categoria' ,FK_categoria_id = '$idcontasapagar_categoria',fornecedor = '$contasapagar_fornecedor',FK_fornecedor_id = '$idcontasapagar_fornecedor',data_vencimento = CONCAT(DATE_FORMAT(data_vencimento, '%Y-%m-'), '$day') ,conta = '$contasapagar_conta',fk_conta_id = '$idcontasapagar_conta',valor_liquido = '$contasapagar_valor_liquido',valor_bruto = '$contasapagar_valor_bruto',status = '$contasapagar_status',observacao = '$contasapagar_obs',forma_pagamento = '$contasapagar_pagamento' where (relacionamento = '$contasapagar_relacionamento' and parcela >= '$contasapagar_parcela' )";
          //        $insert_cadastro_contasapagar = mysqli_query($con, $query_cadastro_contasapagar);  
							   
					//			  $query_cadastro_contasapagar2 = "UPDATE contasapagar SET grupo_categoria='$grupo_categoria', descricao = '$contasapagar_descricao',categoria = '$contasapagar_categoria' ,FK_categoria_id = '$idcontasapagar_categoria',fornecedor = '$contasapagar_fornecedor',FK_fornecedor_id = '$idcontasapagar_fornecedor',data_vencimento = '$contasapagar_vencimento',conta = '$contasapagar_conta',fk_conta_id = '$idcontasapagar_conta',valor_liquido = '$contasapagar_valor_liquido',valor_bruto = '$contasapagar_valor_bruto',status = '$contasapagar_status',observacao = '$contasapagar_obs',forma_pagamento = '$contasapagar_pagamento' where id = '$contasapagar_id' ";
          //        $insert_cadastro_contasapagar2 = mysqli_query($con, $query_cadastro_contasapagar2);									 
									 
						//		 } 
								//todos----------------- 
							 
						//	 }else{
								 
								 //unico-------------------------------------
								 if($contasapagar_status == 'Pago'){
							    $query_cadastro_contasapagar = "UPDATE contasapagar SET data_pagamento= '$contasapagar_vencimento',grupo_categoria='$grupo_categoria', descricao = '$contasapagar_descricao',categoria = '$contasapagar_categoria' ,FK_categoria_id = '$idcontasapagar_categoria',fornecedor = '$contasapagar_fornecedor',FK_fornecedor_id = '$idcontasapagar_fornecedor',data_vencimento = '$contasapagar_vencimento',conta = '$contasapagar_conta',fk_conta_id = '$idcontasapagar_conta',valor_liquido = '$contasapagar_valor_liquido',valor_bruto = '$contasapagar_valor_bruto',status = '$contasapagar_status',observacao = '$contasapagar_obs',forma_pagamento = '$contasapagar_pagamento' where id = '$contasapagar_id' ";
                  $insert_cadastro_contasapagar = mysqli_query($con, $query_cadastro_contasapagar); 
								 }else{
									 
								  $query_cadastro_contasapagar = "UPDATE contasapagar SET grupo_categoria='$grupo_categoria', descricao = '$contasapagar_descricao',categoria = '$contasapagar_categoria' ,FK_categoria_id = '$idcontasapagar_categoria',fornecedor = '$contasapagar_fornecedor',FK_fornecedor_id = '$idcontasapagar_fornecedor',data_vencimento = '$contasapagar_vencimento',conta = '$contasapagar_conta',fk_conta_id = '$idcontasapagar_conta',valor_liquido = '$contasapagar_valor_liquido',valor_bruto = '$contasapagar_valor_bruto',status = '$contasapagar_status',observacao = '$contasapagar_obs',forma_pagamento = '$contasapagar_pagamento' where id = '$contasapagar_id' ";
                  $insert_cadastro_contasapagar = mysqli_query($con, $query_cadastro_contasapagar); 								  
									 
								 } 
								 //unico-------------------------------------
								 
							// } 
								 
                 
		    
                if($insert_cadastro_contasapagar){
									
							//		if(!empty($_FILES['contasapagar_anexar_edicao']['name'])){
     				  //         $nome_real=$contasapagar_id;//pega o id criado
							//				 $arquivo = $nome_real.'.'.pathinfo($_FILES['contasapagar_anexar_edicao']['name'], PATHINFO_EXTENSION);
              //         move_uploaded_file($_FILES["contasapagar_anexar_edicao"]["tmp_name"],"../../estudios/$idadmin/anexos/despesas/$arquivo");
              //         $arquivo = "http://gestaoink.com.br/dashboard/estudios/$idadmin/anexos/despesas/$arquivo";
		                   //atualiza o caminho da foto com o id unico
					    //         $queryupdate = "UPDATE contasapagar SET anexo='$arquivo' WHERE ID='$nome_real'";
              //         $update = mysqli_query($con, $queryupdate);
				      //       }
				
			          $log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Atualizou','despesa')";
                $insert_log_eventos = mysqli_query($con, $log_eventos);
									
				       	echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Conta editada com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";
              }


	         }else
			  {
		  	  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";
        }
	
      }
				
		
		
		
		if(isset($_POST['excluir_contasapagar_edicao']))
			{
				
				
			//	 $contasapagar_relacionamento= isset($_POST['relacionamento_despesa_edicao']) ? $_POST['relacionamento_despesa_edicao'] : '';
				 $contasapagar_id= isset($_POST['ideditardespesa']) ? $_POST['ideditardespesa'] : '';
			//	 $contasapagar_parcela= isset($_POST['contasapagar_parcela_edicao']) ? $_POST['contasapagar_parcela_edicao'] : '';
				
				   // if(isset($_POST['todoscontasapagar'])){
							
			      //      $query_cadastro_contasapagar = "delete from contasapagar where (relacionamento = '$contasapagar_relacionamento' and parcela >= '$contasapagar_parcela' )";
            //      $delete_cadastro_contasapagar = mysqli_query($con, $query_cadastro_contasapagar);  
 	
							 
						//	 }else{
							    $query_cadastro_contasapagar = " delete from contasapagar where id = '$contasapagar_id' ";
                  $delete_cadastro_contasapagar = mysqli_query($con, $query_cadastro_contasapagar);  							 
							// } 
				
		             if($delete_cadastro_contasapagar){
									
		            
				       	echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Conta apagada com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/movimentacoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/movimentacoes'    } });});</script>";
              }
			
			
			}
			





    	
//--------------------------------------------------		

?>

</body>
</html>