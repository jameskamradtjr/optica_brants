<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../../sweetalert/js/sweetalert.min.js"></script>

  <title>GestãoInvest </title>

</head>

<body>

  <?php


  include '../../sys/init.php';
  
  date_default_timezone_set('America/Sao_Paulo');
  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}   



    if(isset($_POST['adicionar_empresa']))
    {


     $razaosocial_empresa = isset($_POST['razaosocial_empresa']) ? $_POST['razaosocial_empresa'] : '';
     $nomefantasia_empresa = isset($_POST['nomefantasia_empresa']) ? $_POST['nomefantasia_empresa'] : '';
     $cnpj_empresa = isset($_POST['cnpj_empresa']) ? $_POST['cnpj_empresa'] : '';
     $uf_empresa = isset($_POST['uf_empresa']) ? $_POST['uf_empresa'] : '';
     $cidade_empresa = isset($_POST['cidade_empresa']) ? $_POST['cidade_empresa'] : '';
     $agencia_empresa = isset($_POST['agencia_empresa']) ? $_POST['agencia_empresa'] : '';
     $conta_empresa = isset($_POST['conta_empresa']) ? $_POST['conta_empresa'] : '';
     $endereco_empresa = isset($_POST['endereco_empresa']) ? $_POST['endereco_empresa'] : '';
     $modelo_contrato = isset($_POST['modelo_contrato']) ? $_POST['modelo_contrato'] : '';	




     $banco_array = isset($_POST['banco_empresa']) ? $_POST['banco_empresa'] : '';
     list($idbanco,$banco) = explode('|', $banco_array);


     $idadmin = $_SESSION['idadmin'];







     $query_cadastro_dados_empresa = "INSERT INTO dados_empresa (razao_social,nome_fantasia,cnpj,endereco,cidade,uf,contrato_modelo_id,FK_usuarios_admin_id,banco,id_banco,conta,agencia) VALUES ('$razaosocial_empresa','$nomefantasia_empresa','$cnpj_empresa','$endereco_empresa','$cidade_empresa','$uf_empresa','$modelo_contrato','$idadmin','$banco','$idbanco','$conta_empresa','$agencia_empresa')";
     $insert_cadastro_empresa = mysqli_query($con, $query_cadastro_dados_empresa);
     $FK_dados_empresa_id = mysqli_insert_id($con);

     if(!empty($_FILES['foto']['name']))
     {

      if (($_FILES['foto']['type'] != 'image/jpeg'))
      {        


        $nomefoto_cliente_temporario=isset($_FILES["foto"]["tmp_name"]); 
        move_uploaded_file($_FILES["foto"]["tmp_name"],"../../docs_empresas/".$idadmin."/logo/logo_loja".$FK_usuarios_admin_id.$FK_dados_empresa_id.".jpg");
        $FOTO = "docs_empresas/".$idadmin."/logo/logo_loja".$FK_usuarios_admin_id.$FK_dados_empresa_id.".jpg";  

        $query_usuarios = "update dados_empresa set logo_anexo='$FOTO' where id='$FK_dados_empresa_id'";
        $update_usuario = mysqli_query($con, $query_usuarios);

      }
    }





    if($insert_cadastro_empresa){



      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Empresa cadastrada com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/../configuracoes.php'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/../configuracoes.php'    } });});</script>";

    }else{
      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/../configuracoes.php'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/../configuracoes.php'    } });});</script>";
    }




  }else
  {
   echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/../configuracoes.php'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/../configuracoes.php'    } });});</script>";


 }






//--------------------------------------------------		

 ?>

</body>
</html>