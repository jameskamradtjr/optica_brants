<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
	<link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="../../sweetalert/css/main.css" rel="stylesheet">
	<!-- Scroll Menu -->
	<link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


	<!-- Custom functions file -->
	<script src="../../sweetalert/js/functions.js"></script>
	<!-- Sweet Alert Script -->
	<script src="../../sweetalert/js/sweetalert.min.js"></script>

	<title>GestãoInk </title>

</head>

<body>

	<?php


	include '../../sys/init.php';

	date_default_timezone_set('America/Sao_Paulo');

  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
	if (!$con->set_charset("utf8")) {}   



		if(isset($_POST['editar_usuarios']))
		{

			$id_usuario_edicao = isset($_POST['id_usuario_edicao']) ? $_POST['id_usuario_edicao'] : '';
			$nome_usuario = isset($_POST['editar_nome_usuario']) ? $_POST['editar_nome_usuario'] : '';
			$contato_usuario = isset($_POST['editar_contato_usuario']) ? $_POST['editar_contato_usuario'] : '';
			$documento_usuario = isset($_POST['editar_documento_usuario']) ? $_POST['editar_documento_usuario'] : '';
			$comissao_usuario = isset($_POST['editar_comissao_usuario']) ? $_POST['editar_comissao_usuario'] : '';
			$funcao_usuario_array = isset($_POST['editar_funcao_usuario']) ? $_POST['editar_funcao_usuario'] : '';
			list($nivel_acesso,$funcao_usuario) = explode('|', $funcao_usuario_array);

			$agencia_usuario = isset($_POST['editar_agencia_usuario']) ? $_POST['editar_agencia_usuario'] : '';
			$conta_usuario = isset($_POST['editar_conta_usuario']) ? $_POST['editar_conta_usuario'] : '';
			$observacoes_usuario = isset($_POST['editar_observacoes_usuario']) ? $_POST['editar_observacoes_usuario'] : '';

			


			$editar_empresa_array = isset($_POST['editar_empresa']) ? $_POST['editar_empresa'] : '';
			list($idempresa,$empresa) = explode('|', $editar_empresa_array);


			$banco_array = isset($_POST['editar_banco_usuario']) ? $_POST['editar_banco_usuario'] : '';
			list($idbanco,$banco) = explode('|', $banco_array);

			if(isset($_POST['editar_notificacao']))
			{
				$notificacao = true;	
			}


			$idadmin = $_SESSION['idadmin'];

			

			if(!empty($_FILES['foto']['name']))
			{
				
				if (($_FILES['foto']['type'] != 'image/jpeg'))
				{				 
               //  num eh jpeg nem png   
					echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Foto Apenas no Formato .jpg!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/configuracoes'    } });});</script>" ;
					exit;
				}else
				{

					$nomefoto_cliente_temporario=isset($_FILES["foto"]["tmp_name"]); 
					move_uploaded_file($_FILES["foto"]["tmp_name"],"../../docs_empresas/$idadmin/fotos/colaboradores/$id_usuario_edicao.jpg");
					$FOTO = "docs_empresas/$idadmin/fotos/colaboradores/$id_usuario_edicao.jpg";	

					$query_usuarios = "update usuarios set foto='$FOTO' where id='$id_usuario_edicao'";
			        $update_usuario = mysqli_query($con, $query_usuarios);
	
				}
			}




			$query_cadastro_usu = "update usuarios set relatorio_diario='$notificacao', nome='$nome_usuario',comissao='$comissao_usuario',documento='$documento_usuario',contato='$contato_usuario',banco='$banco',id_banco='$idbanco',conta='$conta_usuario',agencia='$agencia_usuario',obs='$observacoes_usuario',FK_usuarios_admin_id='$idadmin',funcao='$funcao_usuario',fk_id_empresa='$idempresa',nome_empresa='$empresa',id_acesso='$nivel_acesso'  where id='$id_usuario_edicao'";
			$update_cadastro_usuario = mysqli_query($con, $query_cadastro_usu);







			if($update_cadastro_usuario){



				echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Usuário atualizado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";

			}else{
				echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Erro no Cadastro!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";
			}




		}else
		{
			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";

			
		}






//--------------------------------------------------		
		
//--------------------------------------------------


		if(isset($_POST['excluir_usuario']))
		{

			$id_usuario_edicao = isset($_POST['id_usuario_edicao']) ? $_POST['id_usuario_edicao'] : '';



			$sqldeletarusuarios = "delete from usuarios where id = '$id_usuario_edicao' ";
			$deletarusuarios = mysqli_query($con, $sqldeletarusuarios);


			if($deletarusuarios)
			{

				echo "<script>jQuery(function(){swal({   title: 'Deletar',   text: 'Usuário apagado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";


			}else{

				echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";
			}


		}
		
		
		

		?>

	</body>
	</html>