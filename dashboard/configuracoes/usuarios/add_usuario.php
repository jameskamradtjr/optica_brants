<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../../sweetalert/js/sweetalert.min.js"></script>

  <title>GestãoInvest </title>

</head>

<body>

  <?php


  include '../../sys/init.php';
  
  date_default_timezone_set('America/Sao_Paulo');
  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}   



    if(isset($_POST['adicionar_usuarios']))
    {


      $nome_usuario = isset($_POST['nome_usuario']) ? $_POST['nome_usuario'] : '';
      $contato_usuario = isset($_POST['contato_usuario']) ? $_POST['contato_usuario'] : '';
      $documento_usuario = isset($_POST['documento_usuario']) ? $_POST['documento_usuario'] : '';
      $comissao_usuario = isset($_POST['comissao_usuario']) ? $_POST['comissao_usuario'] : '';

      $funcao_usuario_array = isset($_POST['funcao_usuario']) ? $_POST['funcao_usuario'] : '';
      list($nivel_acesso,$funcao_usuario) = explode('|', $funcao_usuario_array);

      $login_usuario = isset($_POST['login_usuario']) ? $_POST['login_usuario'] : '';
      $senha_usuario = isset($_POST['senha_usuario']) ? $_POST['senha_usuario'] : '';
      $senha_usuario = password_hash($senha_usuario, PASSWORD_DEFAULT);
      $agencia_usuario = isset($_POST['agencia_usuario']) ? $_POST['agencia_usuario'] : '';
      $conta_usuario = isset($_POST['conta_usuario']) ? $_POST['conta_usuario'] : '';
      $observacoes_usuario = isset($_POST['observacoes_usuario']) ? $_POST['observacoes_usuario'] : '';


      $editar_empresa_array = isset($_POST['empresa']) ? $_POST['empresa'] : '';
      list($idempresa,$empresa) = explode('|', $editar_empresa_array);

      



      $notificacao = false;
      if(isset($_POST['notificacao']))
      {
        $notificacao = true;	
      }


      $banco_array = isset($_POST['banco_usuario']) ? $_POST['banco_usuario'] : '';
      list($idbanco,$banco) = explode('|', $banco_array);


      $idadmin = $_SESSION['idadmin'];




     $select_login = "SELECT id,email FROM usuarios_admin WHERE email = '".$login_usuario."' LIMIT 1";

    $run_select_login = mysqli_query($con, $select_login);

    $check_login = mysqli_num_rows($run_select_login);


    $select_login_colab = "SELECT id,login FROM usuarios WHERE login = '".$login_usuario."' LIMIT 1";

    $run_select_login_colab = mysqli_query($con, $select_login_colab);

    $check_login_colab = mysqli_num_rows($run_select_login_colab);




    if($check_login>0 ){ echo "<script>jQuery(function(){swal({   title: 'Novo',   text: 'Login já Utilizado',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>"; exit; }

    if($check_login_colab>0 ){ echo "<script>jQuery(function(){swal({   title: 'Novo',   text: 'Login já Utilizado',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>"; exit; } 




      

      $query_cadastro_usuario = "INSERT INTO usuarios (relatorio_diario,nome,comissao,documento,contato,login,senha,banco,id_banco,conta,agencia,obs,FK_usuarios_admin_id,funcao,fk_id_empresa,id_acesso,nome_empresa,status) VALUES ('$notificacao','$nome_usuario','$comissao_usuario','$documento_usuario','$contato_usuario','$login_usuario','$senha_usuario','$banco','$idbanco','$conta_usuario','$agencia_usuario','$observacoes_usuario','$idadmin','$funcao_usuario','$idempresa','$nivel_acesso','$empresa','1')";
      $insert_cadastro_usuario = mysqli_query($con, $query_cadastro_usuario);





      if($insert_cadastro_usuario){



        echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Usuário cadastrado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";

      }else{
        echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";
      }




    }else
    {
     echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";


   }






//--------------------------------------------------		

   ?>

 </body>
 </html>