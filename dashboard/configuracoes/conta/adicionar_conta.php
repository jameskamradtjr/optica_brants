<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


    <!-- Custom functions file -->
    <script src="../../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../../sweetalert/js/sweetalert.min.js"></script>

    <title> </title>

  </head>

  <body>

<?php


  include '../../sys/init.php';
  
  date_default_timezone_set('America/Sao_Paulo');
  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}   

    //função para gravar os valores no mysql
    function moeda($get_valor) {
  	$source = array('.', ','); 
  	$replace = array('', '.');
  	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
  	return $valor; //retorna o valor formatado para gravar no banco
   }
  //função para gravar os valores no mysql


          if(isset($_POST['adicionar_conta']))
		  {
	
	        
	          $valor_inicial = isset($_POST['valor_inicial']) ? $_POST['valor_inicial'] : '';
            $valor_inicial = moeda($valor_inicial);
            $descricao_conta = isset($_POST['descricao_conta']) ? $_POST['descricao_conta'] : '';
       	
             $banco_array = isset($_POST['banco_conta']) ? $_POST['banco_conta'] : '';
						 list($idbanco,$banco) = explode('|', $banco_array);
           
	
						 $idadmin = $_SESSION['idadmin'];

             $idempresa = $_SESSION['id_empresa'];
          
			

	
  
      
				
               $query_adicionar_conta= "INSERT INTO conta_principal (fk_id_empresa,descricao,banco,saldo_inicial,FK_usuarios_admin_id,fk_id_banco) VALUES ('$idempresa','$descricao_conta','$banco','$valor_inicial','$idadmin','$idbanco')";
               $insert_adicionar_conta= mysqli_query($con, $query_adicionar_conta);
		   
	     	     
							 
				
		    
                if($insert_adicionar_conta){
					
			     
			            
					       echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Conta cadastrada com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";
              }


	         
	
            }else
			{
			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/configuracoes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/configuracoes'    } });});</script>";

			
			}





    	
//--------------------------------------------------		

?>

</body>
</html>