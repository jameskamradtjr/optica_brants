<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../sweetalert/js/sweetalert.min.js"></script>

  <title></title>

</head>

<body>

  <?php

  if (!isset($_SESSION)) session_start();
  session_regenerate_id(true);

  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

  //função para gravar os valores no mysql
  function moeda($get_valor) {
  $source = array('.', ','); 
  $replace = array('', '.');
  $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
  return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql

// lista post

$id = mysqli_real_escape_string($con,$_POST['editar_id']);
$descricao = mysqli_real_escape_string($con,$_POST['editar_descricao']);
$comissao_porc = mysqli_real_escape_string($con,$_POST['editar_comissao_porc']);
$comissao_valor = mysqli_real_escape_string($con,$_POST['editar_comissao_valor']);

// lista post

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';

if( isset($_POST['salvar_indicacao']) and isset($_SESSION['idadmin']) )
{

  
  

  //query
   $query_atualizar_indicacao = "update indicacao set descricao='$descricao',comissao_porc='$comissao_porc',comissao_valor='$comissao_valor' where id = '$id' "; 
   $update_go_indicacao = mysqli_query($con, $query_atualizar_indicacao);
  //query



 


  if($update_go_indicacao){


  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Atualizado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/cadastro_indicacao'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_indicacao'    } });});</script>";

}else{
echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_indicacao'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_indicacao'    } });});</script>";
}




}

//--------------------------------------------------APAGAR	

if(isset($_POST['excluir_indicacao']) and isset($_SESSION['idadmin']) ){

$editar_id = mysqli_real_escape_string($con,$_POST['editar_id']);

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';



$sql_del_serv = "DELETE FROM indicacao WHERE id='$editar_id' ";
$del_serv = $con->query($sql_del_serv);


if($del_serv){
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Deletado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/cadastro_indicacao'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_indicacao'    } });});</script>";
}else{
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_indicacao'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_indicacao'    } });});</script>";
} 
}


if(!isset($_POST['excluir_indicacao']) and !isset($_POST['salvar_indicacao'])  ){

echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_indicacao'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_indicacao'    } });});</script>";

} 




?>

</body>
</html>