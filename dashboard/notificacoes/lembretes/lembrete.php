<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


    <!-- Custom functions file -->
    <script src="../../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../../sweetalert/js/sweetalert.min.js"></script>

    <title>GestãoInk </title>

  </head>

  <body>

<?php
		
date_default_timezone_set('America/Sao_Paulo');		

if (!isset($_SESSION)) session_start();// A sessão precisa ser iniciada em cada página diferente

  include '../../sys/init.php';
  

  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}  


		
		
		
	$idcliente= isset($_POST['idclienteemail']) ? $_POST['idclienteemail'] : '';	
	$data_email= isset($_POST['data_email']) ? $_POST['data_email'] : '';	
	$artista_email= isset($_POST['artista_email']) ? $_POST['artista_email'] : '';	
	$idagendamento= isset($_POST['idagendamento']) ? $_POST['idagendamento'] : '';		

  
	//clientes	
  $select_cliente = "SELECT SQL_CACHE * FROM cad_clientes WHERE id = '".$idcliente."' ";

  $cliente = $con->query($select_cliente); 

  $clientelista = $cliente->fetch_assoc(); 
		
		
		//estudio	
  $select_estudio = "SELECT SQL_CACHE * FROM dados_empresa WHERE FK_usuarios_admin_id = '".$_SESSION['idadmin']."' ";

  $estudio = $con->query($select_estudio); 

  $estudiolista = $estudio->fetch_assoc(); 	
    
  $assunto = 'Lembrete';
		
	$logostudio ='https://gestaoink.com.br/dashboard/'.$_SESSION['logo'] ;	
	$linkconfirmar = 'https://gestaoink.com.br/dashboard/calendario/envio_email/confirmacao?id='.$idagendamento.'&status=Confirmado' ;
	$linknaoconfirmar = 'https://gestaoink.com.br/dashboard/calendario/envio_email/confirmacao?id='.$idagendamento.'&status=Aberto' ;
	$cliente = $clientelista['nome'];
	$estudio =$estudiolista['nome'];
  $artista = $artista_email;  
  
	$data = date("d/m/Y",strtotime($data_email));
	$hora = substr($data_email, 11, 16) ;
//  $mensagem = "Olá estamos entrando em contato para confirmar o agendamento no estudio as segue em clique no link: http://gestaoink.com.br/dashboard/calendario/envio_email/confirmacao?id=".$idagendamento;

  $select_emailenvio = "SELECT SQL_CACHE * FROM config_email WHERE FK_usuarios_admin_id = '".$_SESSION['idadmin']."' ";

  $email_envio = $con->query($select_emailenvio); 

  $email_enviolista = $email_envio->fetch_assoc(); 	 
		 
		
		include 'emailhtml';       
		
		
		 date_default_timezone_set('America/Sao_Paulo');

           require_once('../../phpmailer/classmailer');
				
				  
 
           $mailer = new PHPMailer();
           $mailer->CharSet = 'UTF-8';
           $mailer->IsSMTP();
           $mailer->SMTPDebug = 1;
           $mailer->Port = 587; //Indica a porta de conexão para a saída de e-mails. Utilize obrigatoriamente a porta 587.
				
				   //anexxooo
				  // if(!empty($pdfanexo)){$mailer->AddAttachment('../../'.$pdfanexo);}
           
 
           $mailer->Host = 'smtp.gmail.com'; //Onde em 'servidor_de_saida' deve ser alterado por um dos hosts abaixo:

           $mailer->SMTPSecure = 'tls';
 
           $mailer->SMTPAuth = true; //Define se haverá ou não autenticação no SMTP
           $mailer->Username = $email_enviolista['username']; //Informe o e-mai o completo
           $mailer->Password = base64_decode($email_enviolista['password']); //Senha da caixa postal
           $mailer->FromName = $email_enviolista['nome']; //Nome que será exibido para o destinatário
           $mailer->From = $email_enviolista['username']; //Obrigatório ser a mesma caixa postal indicada em "username"
           $mailer->AddAddress($clientelista['email']); //Destinatários
           $mailer->Subject = $assunto;
           $mailer->Body = $mensagem;
				   $mailer->IsHTML(true); 
           
		
                  

		               $emailestudio=$estudiolista['email'];
		               $emailcliente=$clientelista['email'];
		  
    
		if($mailer->Send()){
	
	       echo "<script>jQuery(function(){swal({   title: 'Envio Email',   text: 'E-Mail Enviado com Sucesso',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../agenda'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../agenda'    } });});</script>";    
   
	
     }else{
			 
			 	echo "<script>jQuery(function(){swal({   title: 'Envio Email',   text: 'Não foi possível, Verifique as Configurações de Email!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../agenda'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../agenda'    } });});</script>";
    			 
		 }

		
	 

		
?> 
    
    
</body>
</html>    