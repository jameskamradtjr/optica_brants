<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../sweetalert/js/sweetalert.min.js"></script>

  <title> </title>

</head>

<body>

  <?php

  if (!isset($_SESSION)) session_start();
  session_regenerate_id(true);

  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

    date_default_timezone_set('America/Sao_Paulo');


		//função para gravar os valores no mysql
  function moeda($get_valor) {
   $source = array('.', ','); 
   $replace = array('', '.');
	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
	return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql


if(isset($_POST['adicionar_produto']))
{

        //funcao converte a data br para a do mysql
 function inverteData($data){
  if(count(explode("/",$data)) > 1){
   return implode("-",array_reverse(explode("/",$data)));
 }elseif(count(explode("-",$data)) > 1){
  return implode("/",array_reverse(explode("-",$data)));
}
}
			 //funcao converte a data br para a do mysql



$nome_produto =  mysqli_real_escape_string($con,$_POST['nome_produto']);
$codigo_barras =  mysqli_real_escape_string($con,$_POST['codigo_barras']);

$valor_custo =  mysqli_real_escape_string($con,$_POST['valor_custo']);
$valor_custo = moeda($valor_custo);
$valor_venda =  mysqli_real_escape_string($con,$_POST['valor_venda']);
$valor_venda = moeda($valor_venda);
$disponivel_estoque =  mysqli_real_escape_string($con,$_POST['disponivel_estoque']);
$minimo_estoque =  mysqli_real_escape_string($con,$_POST['minimo_estoque']);
$maximo_estoque=  mysqli_real_escape_string($con,$_POST['maximo_estoque']);
$idempresa = $_SESSION['id_empresa'];

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';


$fornecedor_array = mysqli_real_escape_string($con,$_POST['fornecedor']);
list($fk_id_fornecedor,$fornecedor) = explode('|', $fornecedor_array);  

$categoria_produto_array =  mysqli_real_escape_string($con,$_POST['categoria_produto']);
list($idcategoria_produto,$categoria_produto,$valor_categoria) = explode('|', $categoria_produto_array);
$valor_categoria = moeda($valor_categoria); 

$grife=  mysqli_real_escape_string($con,$_POST['grife']);
$unidade=  mysqli_real_escape_string($con,$_POST['unidade']);



$cod_peca=  mysqli_real_escape_string($con,$_POST['cod_peca']);
$data_entrada=  mysqli_real_escape_string($con,$_POST['data_entrada']);


if(isset($_POST['desc_estoque']))
{

$descontar_estoque = '1';

}


if(isset($_SESSION['idadmin'])){


  $query_insert_prod = "INSERT INTO produtos (valor_cat_prod,fk_id_empresa,desc_prod,codigo_barras,id_cat_produto,categoria_prod,valor_custo,valor_venda,quant_est,min_est,max_est,FK_usuarios_admin_id,data_cadastro,fornecedor,fk_id_fornecedor,unidade,grife,descontar_estoque,codigo_peca,data_entrada) VALUES ('$valor_categoria','$idempresa','$nome_produto','$codigo_barras','$idcategoria_produto','$categoria_produto','$valor_custo','$valor_venda','$disponivel_estoque','$minimo_estoque','$maximo_estoque','$idadmin',now(),'$fornecedor','$fk_id_fornecedor','$unidade','$grife','$descontar_estoque','$cod_peca','$data_entrada')";
  $insert_prod = mysqli_query($con, $query_insert_prod);  
  $FK_id_prod = mysqli_insert_id($con);

  if($insert_prod){				

     include 'lista_nf/salvar_lista_nf.php'; 


  //imagem anexo
   if(!empty($_FILES['foto_prod']['name']))
   {
     $caminho_img = "docs_empresas/$idadmin/anexos/outros/foto_prod$FK_id_prod.jpg"; 
     move_uploaded_file($_FILES["foto_prod"]["tmp_name"],'../'.$caminho_img);

    // require('../componentes_terceiros/redimage/WideImage.php');  
   //  $image = WideImage::load($caminho_img);
   //  $nova_img = $image->resize(155, 151); 
   //  $nova_img->saveToFile($caminho_img);
     //atualiza o caminho da foto com o id unico
     $queryupdate = "UPDATE produtos SET  foto='$caminho_img' WHERE ID='$FK_id_prod'";
     $update = mysqli_query($con, $queryupdate);
   }

   //imagem anexo  



   echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Produto Cadastrado com Sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";

 }else{
  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível Cadastrar!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";
}


}else
{
 echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não Logado!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";
}

}else
{
 echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";
}







//--------------------------------------------------		

?>

</body>
</html>