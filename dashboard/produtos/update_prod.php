<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../sweetalert/js/sweetalert.min.js"></script>

  <title></title>

</head>

<body>

  <?php

  if (!isset($_SESSION)) session_start();
  session_regenerate_id(true);

  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

  //função para gravar os valores no mysql
  function moeda($get_valor) {
  $source = array('.', ','); 
  $replace = array('', '.');
  $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
  return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql


if( isset($_POST['salvar_produto']) and isset($_SESSION['idadmin']) )
{


  $id =  mysqli_real_escape_string($con,$_POST['editar_id_prod']);	
  $desc_prod =  mysqli_real_escape_string($con,$_POST['editar_nome_produto']);  
  $codigo_barras =  mysqli_real_escape_string($con,$_POST['editar_codigo_barras']);  
 
  $valor_custo =  mysqli_real_escape_string($con,$_POST['editar_valor_custo']); 
  $valor_custo = moeda($valor_custo); 
  $valor_venda =  mysqli_real_escape_string($con,$_POST['editar_valor_venda']); 
  $valor_venda = moeda($valor_venda);
  $quant_est =  mysqli_real_escape_string($con,$_POST['editar_disponivel_estoque']);   
  $min_est =  mysqli_real_escape_string($con,$_POST['editar_minimo_estoque']); 
  $max_est =  mysqli_real_escape_string($con,$_POST['editar_maximo_estoque']); 

  $idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';


$fornecedor_array = mysqli_real_escape_string($con,$_POST['editar_fornecedor']);
list($fk_id_fornecedor,$fornecedor) = explode('|', $fornecedor_array);  

$categoria_produto_array =  mysqli_real_escape_string($con,$_POST['editar_categoria_produto']);
list($idcategoria_produto,$categoria_produto,$valor_categoria) = explode('|', $categoria_produto_array); 
$valor_categoria = moeda($valor_categoria); 

$grife=  mysqli_real_escape_string($con,$_POST['editar_grife']);
$unidade=  mysqli_real_escape_string($con,$_POST['editar_unidade']);



$cod_peca=  mysqli_real_escape_string($con,$_POST['editar_cod_peca']);
$data_entrada=  mysqli_real_escape_string($con,$_POST['editar_data_entrada']);



if(isset($_POST['editar_desc_estoque']))
{

$descontar_estoque = '1';

}else{

$descontar_estoque = '0';

}





  if(isset($_SESSION['idadmin'])){

  $query_atualizar_produtos = "update produtos set  valor_cat_prod='$valor_categoria',grife='$grife',unidade='$unidade',codigo_peca='$cod_peca',descontar_estoque='$descontar_estoque',data_entrada='$data_entrada', id_cat_produto='$idcategoria_produto',fornecedor='$fornecedor',fk_id_fornecedor='$fk_id_fornecedor',desc_prod ='$desc_prod', codigo_barras='$codigo_barras',categoria_prod='$categoria_produto',valor_custo='$valor_custo',valor_venda='$valor_venda',quant_est='$quant_est',min_est='$min_est',max_est='$max_est' where id = '$id' ";
  $update_prod = mysqli_query($con, $query_atualizar_produtos);




  if($update_prod){


    include 'lista_nf/atualizar_lista_nf.php';


  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Atualizado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";

}else{
echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";
}


}else
{
 echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";
}

}

//--------------------------------------------------APAGAR	

if(isset($_POST['excluir_produto']) and isset($_SESSION['idadmin']) ){

$editar_id_prod = mysqli_real_escape_string($con,$_POST['editar_id_prod']);

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';



$sql_del_prod = "DELETE FROM produtos WHERE id='$editar_id_prod' ";
$del_prod = $con->query($sql_del_prod);


if($del_prod){
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Deletado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";
}else{
echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";
} 
}


if(!isset($_POST['excluir_produto']) and !isset($_POST['salvar_produto'])  ){

echo"<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi Possivel :( ',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_produtos'    } });});</script>";

} 




?>

</body>
</html>