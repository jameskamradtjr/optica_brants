<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../../sweetalert/js/sweetalert.min.js"></script>

  <title> </title>

</head>

<body>

  <?php

  if (!isset($_SESSION)) session_start();
  session_regenerate_id(true);

  include '../../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

    date_default_timezone_set('America/Sao_Paulo');


		//função para gravar os valores no mysql
  function moeda($get_valor) {
   $source = array('.', ','); 
   $replace = array('', '.');
	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
	return $valor; //retorna o valor formatado para gravar no banco
}
//função para gravar os valores no mysql


$id_produto_trans =  mysqli_real_escape_string($con,$_POST['id_produto_trans']);
 

$select_query_produtos = "SELECT SQL_CACHE * FROM produtos WHERE (id = '".$id_produto_trans."')  limit 1 ";
$select_go_query_produtos = $con->query($select_query_produtos);
$select_go_query_produtoslista = $select_go_query_produtos->fetch_assoc();

$quant_trans =  mysqli_real_escape_string($con,$_POST['quant_trans']);

$loja_destino =  mysqli_real_escape_string($con,$_POST['loja_destino']);
 list($idloja_destino,$loja_destino) = explode('|', $loja_destino_array);  
$prod_loja_destino_array=  mysqli_real_escape_string($con,$_POST['prod_loja_destino']);
 list($idprod_loja_destino,$prod_loja_destino) = explode('|', $prod_loja_destino_array);  

  

if(empty($prod_loja_destino_array)){


$nome_produto = $select_go_query_produtoslista['desc_prod'] ;
$codigo_barras = $select_go_query_produtoslista['codigo_barras'] ;

$valor_custo =  $select_go_query_produtoslista['valor_custo'] ;

$valor_venda =  $select_go_query_produtoslista['valor_venda'] ;

$disponivel_estoque = $select_go_query_produtoslista['quant_est']  ;
$minimo_estoque =$select_go_query_produtoslista['min_est']  ;
$maximo_estoque=$select_go_query_produtoslista['max_est']   ;
$idempresa = $idloja_destino ;

$idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';


$fk_id_fornecedor =$select_go_query_produtoslista['fk_id_fornecedor']  ;

$fornecedor = $select_go_query_produtoslista['fornecedor'] ;

$idcategoria_produto =$select_go_query_produtoslista['id_cat_produto']  ;

$categoria_produto = $select_go_query_produtoslista['categoria_prod'] ;

$valor_categoria = $select_go_query_produtoslista['valor_cat_prod'] ;


$grife=  $select_go_query_produtoslista['grife'] ;
$unidade=  $select_go_query_produtoslista['unidade'] ;



$cod_peca=  $select_go_query_produtoslista['codigo_peca'] ;
$data_entrada=  $select_go_query_produtoslista['data_entrada'] ;



$descontar_estoque = $select_go_query_produtoslista['descontar_estoque'] ;




if(isset($_SESSION['idadmin'])){


  $query_insert_prod = "INSERT INTO produtos (valor_cat_prod,fk_id_empresa,desc_prod,codigo_barras,id_cat_produto,categoria_prod,valor_custo,valor_venda,quant_est,min_est,max_est,FK_usuarios_admin_id,data_cadastro,fornecedor,fk_id_fornecedor,unidade,grife,descontar_estoque,codigo_peca,data_entrada) VALUES ('$valor_categoria','$idempresa','$nome_produto','$codigo_barras','$idcategoria_produto','$categoria_produto','$valor_custo','$valor_venda','$disponivel_estoque','$minimo_estoque','$maximo_estoque','$idadmin',now(),'$fornecedor','$fk_id_fornecedor','$unidade','$grife','$descontar_estoque','$cod_peca','$data_entrada')";
  $insert_prod = mysqli_query($con, $query_insert_prod);  
  $FK_id_prod = mysqli_insert_id($con);

  if($insert_prod){				

    


   echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Produto Transferido com Sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/cadastro_produtos'    } });});</script>";

 }else{
  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível Cadastrar!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/cadastro_produtos'    } });});</script>";
}


}else
{
 echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não Logado!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/cadastro_produtos'    } });});</script>";
}


}else{
  

  $id = $idprod_loja_destino ; 


  $select_query_produtos_dest = "SELECT SQL_CACHE * FROM produtos WHERE (id = '".$id_produto_trans."')  limit 1 ";
  $select_go_query_produtos_dest = $con->query($select_query_produtos_dest);
  $select_go_query_produtoslista_dest = $select_go_query_produtos_dest->fetch_assoc();

  $nova_quantidade =  $quant_trans + $select_go_query_produtoslista_dest['quant_est'];
  


  if(isset($_SESSION['idadmin'])){

  $query_atualizar_produtos = "update produtos set  quant_est='$nova_quantidade' where id = '$id' ";
  $update_prod = mysqli_query($con, $query_atualizar_produtos);




  if($update_prod){



  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Atualizado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/cadastro_produtos'    } });});</script>";

}else{
echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/cadastro_produtos'    } });});</script>";
}


}else
{
 echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../../view/cadastro_produtos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../../view/cadastro_produtos'    } });});</script>";
}

}

		

?>

</body>
</html>