<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../../sweetalert/js/sweetalert.min.js"></script>

  <title> </title>

</head>

<body>

<?php

//FORMA DE PAGAMENTO

$formapagamento = '';

$select_queryrelacionamento_pagamentos_os = "SELECT SQL_CACHE * FROM relacionamento_pagamentos_os WHERE fk_id_os = '$id_os' ";
$selectrelacionamento_pagamentos_os = $con->query($select_queryrelacionamento_pagamentos_os); 


while ($selectrelacionamento_pagamentos_oslista = $selectrelacionamento_pagamentos_os->fetch_assoc()): 

    $sql_contasareceber = "SELECT SQL_CACHE * FROM contasareceber WHERE id = '".$selectrelacionamento_pagamentos_oslista['fk_id_pagamentos']."' ";
    $contasareceber = $con->query($sql_contasareceber);
    $contasareceber_oslista = $contasareceber->fetch_assoc();
   


    $formapagamento .= '

    <tr class="details">
    <td>
    '.$contasareceber_oslista['forma_pagamento'].'
    </td>

    <td>
    '.number_format($contasareceber_oslista['valor_bruto'], 2, ',', '.').'
    </td>
    </tr>

    ';

endwhile;

//FORMA DE PAGAMENTO

//PRODUTOS

$produtos = '';

$select_relacionamento_produto_os = "SELECT SQL_CACHE * FROM relacionamento_produto_os WHERE fk_id_os = '$id_os' ";
$relacionamento_pagamentos_os = $con->query($select_relacionamento_produto_os); 


while ($relacionamento_pagamentos_oslista = $relacionamento_pagamentos_os->fetch_assoc()): 


    $produtos .= '

    <tr class="item">
    <td>
    '.$relacionamento_pagamentos_oslista['desc_prod'].'
    </td>

    <td>
    '.number_format($relacionamento_pagamentos_oslista['valor'], 2, ',', '.').'
    </td>
    </tr>

    ';

endwhile;

//PRODUTOS


//indicação

    $relacionamento_indicacao_os = "SELECT SQL_CACHE * FROM relacionamento_indicacao_os WHERE fk_id_os = '".$id_os."' ";
    $sql_relacionamento_indicacao_os= $con->query($relacionamento_indicacao_os);
    $sql_relacionamento_indicacao_osdados = $sql_relacionamento_indicacao_os->fetch_assoc();

    $desc_indicacao = $sql_relacionamento_indicacao_osdados['desc_indicacao'];


//indicação



//dados loja

    $sql_loja = "SELECT SQL_CACHE nome_fantasia,endereco,cidade,uf,id FROM dados_empresa WHERE id = '".$id_loja."' ";
    $sql_loja_res = $con->query($sql_loja);
    $sql_loja_res_dados = $sql_loja_res->fetch_assoc();

    $nome_loja = $sql_loja_res_dados['nome_fantasia'];
    $endereco = $sql_loja_res_dados['endereco'];
    $cidade = $sql_loja_res_dados['cidade'];
    $uf = $sql_loja_res_dados['uf'];

//dados loja 



$html_pdf_os = '





<div class="invoice-box">

<br>
<hr>
<br>
<tr class="top">
                 <td colspan="2">
                    <table>
                        <tr>
						
						
                            <td align="center" >
                            <span style="color:white;">____________________________________</span><strong>Nº da O.S: '.$id_os.'</strong>
                            </td>
                            
                           
                        </tr>
                    </table>
                </td>
            </tr>

            <br>
      

            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
            
            
                            <td class="title">
                              <span style="color:white"> ___</span>  '.date('d/m/Y').' <span style="color:white"> _______________________________________________________</span>
                            </td>
                            
                            <td>                                                            
                           '.date('d/m/Y',strtotime($data_combinado_entrega)).'
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
	
	         <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
						
						
                            <td class="title">
                             <span style="color:white"> ___</span> Data da OS  <span style="color:white"> ____________________________________________________</span>
                            </td>
                            
                            <td>                                                            
                             Data de Entrega 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
			
			      <br>
       

			 <tr class="top">
                 <td colspan="2">
                    <table>
                        <tr>
						
						
                            <td class="title" >
                             Nome: '.$cliente.'
                            </td>
							
                            
                           
                        </tr>
                        <br>
                        <br>

                           <tr>         
            
            
                            <td class="title" >
                             Celular: '.$celular.'<span style="color:white">_____________________________________________</span>   Fixo: '.$telefone.'
                            </td>
              
                            
                           
                        </tr>
                        <br>
                        <br>

                        <tr>
            
            
                            <td class="title" >
                             CPF: '.$cpf.'
                            </td>
              
                            
                           
                        </tr>
                         <br>
                       
						           
						
						
                       
                    </table>
                </td>
            </tr>
			<br>



<hr>
<br>


<table cellpadding="0" cellspacing="0">

<!--/LISTA PRODUTOS--> 
<tr class="heading">
<td>
Item
</td>

<td>
Valor
</td>
</tr>

'.$produtos.'
<!--/LISTA PRODUTOS--> 




<!--/ LISTA FORMA PAGAMENTO-->  
<tr class="heading">
<td>
Forma de Pagamento
</td>

<td>
Valor
</td>
</tr>

'.$formapagamento.'


<!--/ LISTA FORMA PAGAMENTO--> 


<!--/TOTAL LISTA PRODUTOS--> 
<tr class="total">
<td></td>

<td>
Pago: R$'.$pago.' / A Pagar R$'.$a_pagar.'
</td>
</tr>
<!--/TOTAL LISTA PRODUTOS-->




<!--/RODAPE-->          


<tr class="information">
<td colspan="2">
<table>
<tr>
<td>
Médico: '.$medico.'<span style="color:white">___________________________________________</span> Indicação: '.$desc_indicacao.'<br><br>
Vendedor: '.$vendedor.'<br><br>
Loja: '.$nome_loja.'<br><br>
Obs: '.$observacao.'<br><br>
<strong>Status:</strong> '.$status_loja.'<br><br>
<strong>Tipo:</strong> '.$tipo_os.'
</td>
<td>

</td>


</tr>
</table>
</td>
</tr>



          
			
<!--/RODAPE-->              

</table>
</div>

';


?>        


</body>
</html>