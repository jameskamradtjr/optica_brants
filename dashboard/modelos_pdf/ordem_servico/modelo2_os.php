<?php

$html_pdf_os = '<div class="page-wrapper">
           
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                          
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
							
							
                                <div class="container">
                                    <!-- BEGIN PAGE BREADCRUMBS -->
                                   
                                    <!-- END PAGE BREADCRUMBS -->
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        <div class="invoice-content-2 ">
                                            <div class="row invoice-head">
                                                <div class="col-md-7 col-xs-6">
                                                    <div class="invoice-logo">
                                                        <img src="../../assets/pages/img/logos/logo5.jpg" class="img-responsive" alt="" />
                                                        <h1 class="uppercase"><!--Um Campo--></h1>
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-xs-6">
                                                    <div class="company-address">
                                                        <span class="bold uppercase">Audrey Brants</span>
                                                        <br/> Asa Sul Comércio Local Sul 305 BL D 305 Loja 21
                                                        <br/> Brasília - DF
                                                        <br/>
                                                        <span class="bold"><!--Um Campo--></span> (61) 3443-9817
                                                        <br/>
                                                        <span class="bold"><!--Um Campo--></span> <!--Um Campo-->
                                                        <br/>
                                                        <span class="bold"><!--Um Campo--></span> www.audreybrants.com.br </div>
                                                </div>
                                            </div>
                                            <div class="row invoice-cust-add">
                                                <div class="col-xs-3">
                                                    <h2 class="invoice-title uppercase">Cliente</h2>
                                                    <p class="invoice-desc">James Alberto Kamradt Jr.</p>
                                                </div>
                                                <div class="col-xs-3">
                                                    <h2 class="invoice-title uppercase">Entrega</h2>
                                                    <p class="invoice-desc">22/07/2018</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <h2 class="invoice-title uppercase">Nº OS </h2>
                                                    <p class="invoice-desc inv-address">00161</p>
                                                </div>
                                            </div>
											
											 <!--DIV PREENCHIMENTO RECEITA-->
                  <div class="m-t col-xs-12">
                    <div class="table-responsive">
                      <div style="min-width: 400px">
                        <table style="width: 25%; margin-top: 35px;" class="table table-bordered table-responsive table-condensed receita-label pull-left">
                          <tbody>
                            <tr class="text-success">
                              <td rowspan="2">Longe</td>
                              <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
                            </tr>
                            <tr class="text-success">
                              <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
                            </tr>
                            <tr class="text-danger">
                              <td rowspan="2">Perto</td>
                              <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
                            </tr>
                            <tr class="text-danger">
                              <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
                            </tr>
                          </table>
                          <table style="width: 75%" class="table table-bordered table-responsive table-condensed receita">
                            <thead>
                              <tr>
                                <th>Esférico</th>
                                <th>Cilíndrico</th>
                                <th>Eixo</th>
                                <th>Altura</th>
                                <th>DNP</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr class="text-success">
                                <td><input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="nova_receita[esferico_od_longe]" type="text"></td>
                                <td><input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="nova_receita[cilindrico_od_longe]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_od_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="nova_receita[eixo_od_longe]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="nova_receita[altura_od_longe]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="nova_receita[dnp_od_longe]" type="text"></td>
                              </tr>
                              <tr class="text-success">
                                <td><input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="nova_receita[esferico_oe_longe]" type="text"></td>
                                <td><input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="nova_receita[cilindrico_oe_longe]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_oe_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="nova_receita[eixo_oe_longe]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="nova_receita[altura_oe_longe]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="nova_receita[dnp_oe_longe]" type="text"></td>
                              </tr>
                              <tr class="text-danger">
                                <td><input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_od_perto" data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off" name="nova_receita[esferico_od_perto]" type="text"></td>
                                <td><input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_od_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="nova_receita[cilindrico_od_perto]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_od_perto" autocomplete="off" data-m-dec="0" maxlength="12" name="nova_receita[eixo_od_perto]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="nova_receita[altura_od_perto]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="nova_receita[dnp_od_perto]" type="text"></td>
                              </tr>
                              <tr class="text-danger">
                                <td><input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_oe_perto" data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off" name="nova_receita[esferico_oe_perto]" type="text"></td>
                                <td><input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_oe_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="nova_receita[cilindrico_oe_perto]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_oe_perto" autocomplete="off" data-m-dec="0" maxlength="12" name="nova_receita[eixo_oe_perto]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="nova_receita[altura_oe_perto]" type="text"></td>
                                <td><input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="nova_receita[dnp_oe_perto]" type="text"></td>

                              </tr>
                              <tr>
                                <td>
                                  <div class="form-group full-width m-b-none text-left "><label for="nova_receita[adicao]" class="control-label">Adição</label><input class="input-sm form-control numeric-field text-right" id="valor-adicao" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="2" maxlength="12" name="nova_receita[adicao]" type="text"></div>
                                </td>
                                <td colspan="4"></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <!--DIV PREENCHIMENTO RECEITA-->
											
											
                                            <div class="row invoice-body">
                                                <div class="col-xs-12 table-responsive">
                                                    <table class="table table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th class="invoice-title uppercase">Pedido Laboratório</th>
                                                                <th class="invoice-title uppercase text-center">Laboratório</th>
                                                                <th class="invoice-title uppercase text-center">Entrega</th>
                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <h3>Observação</h3>
                                                                    <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet et dolore siat magna aliquam erat volutpat. </p>
                                                                </td>
                                                                <td class="text-center sbold">Hoya</td>
                                                                <td class="text-center sbold">22/07/2015</td>
                                                               
                                                            </tr>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row invoice-subtotal">
                                                <div class="col-xs-3">
                                                    <h2 class="invoice-title uppercase">Pago</h2>
                                                    <p class="invoice-desc">R$1.500</p>
                                                </div>
                                                <div class="col-xs-3">
                                                    <h2 class="invoice-title uppercase">A Pagar</h2>
                                                    <p class="invoice-desc">R$1.500</p>
                                                </div>
                                                <div class="col-xs-6">
                                                    <h2 class="invoice-title uppercase">Total</h2>
                                                    <p class="invoice-desc grand-total">R$3.000,00</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <a class="btn btn-lg green-haze hidden-print uppercase print-btn" onclick="javascript:window.print();">Imprimir</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                        <!-- BEGIN QUICK SIDEBAR -->
                       
                        
                        <!-- END QUICK SIDEBAR -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            
        </div>';


?>        