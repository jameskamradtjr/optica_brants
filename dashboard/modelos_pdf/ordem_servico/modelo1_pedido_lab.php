<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link href="../../sweetalert/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../../sweetalert/css/main.css" rel="stylesheet">
  <!-- Scroll Menu -->
  <link href="../../sweetalert/css/sweetalert.css" rel="stylesheet">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


  <!-- Custom functions file -->
  <script src="../../sweetalert/js/functions.js"></script>
  <!-- Sweet Alert Script -->
  <script src="../../sweetalert/js/sweetalert.min.js"></script>

  <title> </title>

</head>

<body>

<?php

//FORMA DE PAGAMENTO

$formapagamento = '';

$select_queryrelacionamento_pagamentos_os = "SELECT SQL_CACHE * FROM relacionamento_pagamentos_os WHERE fk_id_os = '$id_os' ";
$selectrelacionamento_pagamentos_os = $con->query($select_queryrelacionamento_pagamentos_os); 


while ($selectrelacionamento_pagamentos_oslista = $selectrelacionamento_pagamentos_os->fetch_assoc()): 

    $sql_contasareceber = "SELECT SQL_CACHE * FROM contasareceber WHERE id = '".$selectrelacionamento_pagamentos_oslista['fk_id_pagamentos']."' ";
    $contasareceber = $con->query($sql_contasareceber);
    $contasareceber_oslista = $contasareceber->fetch_assoc();
   


    $formapagamento .= '

    <tr class="details">
    <td>
    '.$contasareceber_oslista['forma_pagamento'].'
    </td>

    <td>
    '.$contasareceber_oslista['valor_bruto'].'
    </td>
    </tr>

    ';

endwhile;

//FORMA DE PAGAMENTO

//PRODUTOS

$produtos = '';

$select_relacionamento_produto_os = "SELECT SQL_CACHE * FROM relacionamento_produto_os WHERE fk_id_os = '$id_os' ";
$relacionamento_pagamentos_os = $con->query($select_relacionamento_produto_os); 


while ($relacionamento_pagamentos_oslista = $relacionamento_pagamentos_os->fetch_assoc()): 


    $produtos .= '

    <tr class="item">
    <td>
    '.$relacionamento_pagamentos_oslista['desc_prod'].'
    </td>

    <td>
    '.$relacionamento_pagamentos_oslista['valor'].'
    </td>
    </tr>

    ';

endwhile;

//PRODUTOS

//PEDIDO LAB
$pedido_lab= '';

    $select_relacionamento_pedidolaboratorio_os = "SELECT SQL_CACHE * FROM relacionamento_pedidolaboratorio_os WHERE fk_id_os = '$id_os' ";
    $selectrelacionamento_pedidolaboratorio_os = $con->query($select_relacionamento_pedidolaboratorio_os); 


while ($selectrelacionamento_pedidolaboratorio_oslista = $selectrelacionamento_pedidolaboratorio_os->fetch_assoc()): 

    $sql_pedido_laboratorio = "SELECT SQL_CACHE nome_laboratorio,status_laboratorio,id FROM pedido_laboratorio WHERE id= '".$selectrelacionamento_pedidolaboratorio_oslista['fk_id_pedido_laboratorio']."' ";
    $pedido_laboratorio = $con->query($sql_pedido_laboratorio);
    $pedido_laboratoriolista = $pedido_laboratorio->fetch_assoc();

$pedido_lab .= '

    <tr class="item">
    <td>
    Cód.:'.$pedido_laboratoriolista['id'].' /'.$pedido_laboratoriolista['nome_laboratorio'].'
    </td>

    <td>
    '.$pedido_laboratoriolista['status_laboratorio'].'
    </td>
    </tr>

    ';


 endwhile;
//PEDIDO LAB


//dados loja

    $sql_loja = "SELECT SQL_CACHE nome_fantasia,endereco,cidade,uf,id FROM dados_empresa WHERE id = '".$id_loja."' ";
    $sql_loja_res = $con->query($sql_loja);
    $sql_loja_res_dados = $sql_loja_res->fetch_assoc();

    $nome_loja = $sql_loja_res_dados['nome_fantasia'];
    $endereco = $sql_loja_res_dados['endereco'];
    $cidade = $sql_loja_res_dados['cidade'];
    $uf = $sql_loja_res_dados['uf'];

//dados loja 



$html_pdf_os = '



<div class="invoice-box">

<br>
<hr>
<br>

<!--/RECEITA MEDICA-->
<div>
<table style="height: 228px; width:800px;margin:0;">
<tbody>
<tr>
<td style="width: 82px;"></td>
<td style="width: 82px;"></td>
<td align="center"  style="width: 82px;">Esférico</td>
<td align="center" style="width: 83px;">Cilíndrico</td>
<td align="center" style="width: 83px;">Eixo</td>
<td align="center" style="width: 84px;">Altura</td>
<td align="center" style="width: 86px;">DNP</td>
</tr>
<tr>
<td style="width: 82px;">Longe</td>
<td align="center" style="width: 82px;">OD</td>
<td align="center" style="width: 82px;">'.$valor_esferico_od_longe.'</td>
<td align="center" style="width: 83px;">'.$valor_cilindrico_od_longe.'</td>
<td align="center" style="width: 83px;">'.$valor_eixo_od_longe.'</td>
<td align="center" style="width: 84px;">'.$valor_altura_od_longe.'</td>
<td align="center" style="width: 86px;">'.$valor_dnp_od_longe.'</td>
</tr>
<tr>
<td  style="width: 82px;"></td>
<td  align="center" style="width: 82px;">OE</td>
<td align="center" style="width: 82px;">'.$valor_esferico_oe_longe.'</td>
<td align="center" style="width: 83px;">'.$valor_cilindrico_oe_longe.'</td>
<td align="center" style="width: 83px;">'.$valor_eixo_oe_longe.'</td>
<td align="center" style="width: 84px;">'.$valor_altura_oe_longe.'</td>
<td align="center" style="width: 86px;">'.$valor_dnp_oe_longe.'</td>
</tr>
<tr>
<td  style="width: 82px;">Perto</td>
<td align="center"  style="width: 82px;">OD</td>
<td align="center" style="width: 82px;">'.$valor_esferico_od_perto.'</td>
<td align="center" style="width: 83px;">'.$valor_cilindrico_od_perto.'</td>
<td align="center" style="width: 83px;">'.$valor_eixo_od_perto.'</td>
<td align="center" style="width: 84px;">'.$valor_altura_od_perto.'</td>
<td align="center" style="width: 86px;">'.$valor_dnp_od_perto.'</td>
</tr>
<tr>
<td style="width: 82px;"></td>
<td align="center"  style="width: 82px;">OE</td>
<td align="center" style="width: 82px;">'.$valor_esferico_oe_perto.'</td>
<td align="center" style="width: 83px;">'.$valor_cilindrico_oe_perto.'</td>
<td align="center" style="width: 83px;">'.$valor_eixo_oe_perto.'</td>
<td align="center" style="width: 84px;">'.$valor_altura_oe_perto.'</td>
<td align="center" style="width: 86px;">'.$valor_dnp_oe_perto.'</td>
</tr>
<tr>
<td style="width: 82px;"></td>
<td align="center"  style="width: 82px;">Adição</td>
<td align="center" style="width: 82px;">'.$valor_adicao.'</td>
<td align="center" style="width: 83px;"></td>
<td align="center" style="width: 83px;"></td>
<td align="center" style="width: 84px;"></td>
<td align="center" style="width: 86px;"></td>
</tr>
</tbody>
</table>
</div>
<!--/RECEITA MEDICA-->


<hr>
<br>


<table cellpadding="0" cellspacing="0">



<!--/LISTA PRODUTOS--> 
<tr class="heading">
<td>
Item
</td>

<td>
Valor
</td>
</tr>

'.$produtos.'
<!--/LISTA PRODUTOS--> 

<!--/LISTA PEDIDO LAB--> 
<tr class="heading">
<td>
Laboratório
</td>

<td>
Status Lab
</td>
</tr>

'.$pedido_lab.'
<!--/LISTA PEDIDO LAB--> 






<!--/RODAPE-->          
<tr class="top">
<td colspan="2">
<table>
<tr>
<td class="title">
<img src="../../docs_empresas/'.$FK_usuarios_admin_id.'/logo/logo_sistema'.$FK_usuarios_admin_id.'.jpg" style="width:100%; max-width:100px;">
</td>

<td>
NR O.S : '.$id_os.'<br>
Criado:'.date('d/m/Y').' <br>

</td>
</tr>
</table>
</td>
</tr>

<tr class="information">
<td colspan="2">
<table>
<tr>
<td>
Endereço:'.$endereco.'<br>

'.$cidade.'/'.$uf.'
</td>

<td>
Loja:'.$nome_loja.'<br>
Vendedor: '.$vendedor.'<br>
Cliente: '.$cliente.'<br>
Médico: '.$medico.'<br>
</td>
</tr>
</table>
</td>
</tr>

<!--/RODAPE-->              

</table>
</div>


';


?>        


</body>
</html>