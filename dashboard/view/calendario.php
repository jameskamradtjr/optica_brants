<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente 
if(empty($_SESSION['user_email'])){header("Location: login");}; 
?>

<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Agenda</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../../assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
    <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
                        <!-- BEGIN HEADER TOP -->
                        <div class="page-header-top">
                            <div class="container">
                                <!-- BEGIN LOGO -->
                                <div class="page-logo">
                                    <a href="index.html">
                                        <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">  </a>
                                    </div>
                                    <!-- END LOGO -->
                                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                    <a href="javascript:;" class="menu-toggler"></a>
                                    <!-- END RESPONSIVE MENU TOGGLER -->
                                    <!-- BEGIN TOP NAVIGATION MENU -->
                                    <div class="top-menu">
                                        <ul class="nav navbar-nav pull-right">
                                         
                                          
                                         
                                         <?php  include '../notificacoes/quant_alertas.php';  ?>
                                         
                                         <!-- BEGIN NOTIFICATION DROPDOWN -->
                                         
                                     <!-- END NOTIFICATION DROPDOWN -->
                                     
                                     <?php  include '../notificacoes/quant_agendamentos.php';  ?>
                                     
                                     <!-- BEGIN TODO DROPDOWN -->
                                     
                                    
                                    
                                    
                                    
                                    <!-- BEGIN USER LOGIN DROPDOWN -->
                                    <li class="dropdown dropdown-user dropdown-dark">
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                         
                                         <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };  ?>
                                         
                                         <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                                         <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                                     </a>
                                     <ul class="dropdown-menu dropdown-menu-default">
                                        <li id="config_menu">
                                            <a href="configuracoes">
                                                Configurações</a>
                                            </li>
                                           
                                                <li class="divider"> </li>
                                                
                                                <li>
                                                    <a href="../login/logout">
                                                        <i class="icon-key"></i> Sair </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- END USER LOGIN DROPDOWN -->
                                            
                                            
                                        </ul>
                                    </div>
                                    <!-- END TOP NAVIGATION MENU -->
                                </div>
                            </div>
                            <!-- END HEADER TOP -->
                            <!-- BEGIN HEADER MENU -->
                            <div class="page-header-menu">
            <div class="container" >

              <!-- END HEADER SEARCH BOX -->
              <!-- BEGIN MEGA MENU -->
              <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
              <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
              <div class="hor-menu "  >
                  <ul class="nav navbar-nav" >
                    <li id="menu_visao_geral" class="menu-dropdown classic-menu-dropdown   ">
                      <a  href="visaogeral"> Visão Geral

                      </a>

                    </li>
                    <li id="menu_agenda" class="menu-dropdown active mega-menu-dropdown">
                      <a  href="calendario"> Agenda

                      </a>

                    </li>

                    <li id="menu_clientes" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                      <a  href="clientes"> Clientes

                      </a>

                    </li>
                    <li id="menu_os" class="menu-dropdown mega-menu-dropdown  ">
                     <a  href="ordem_servico"> Ordem de Serviço

                     </a>

                   </li>



                   <li id="menu_estoque" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="cadastro_produtos"> Estoque

                    </a>

                  </li>





                  <li id="menu_movimentacoes" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="movimentacoes"> Movimentações

                    </a>

                  </li>

                  <li id="menu_relatorios" class="menu-dropdown classic-menu-dropdown ">
                    <a href="relatorios"> Relatórios

                    </a>

                  </li>


                  <li id="menu_cadastros" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"> Cadastros
                      <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                     <li class=" ">
                      <a href="cadastro_fornecedores" class="nav-link  ">
                      Fornecedores</a>
                    </li>
                    <li class=" ">
                      <a href="cadastro_servicos" class="nav-link  ">
                       Categorias Produtos/Serviços

                     </a>
                   </li>
                   <li class=" ">
                    <a href="cadastro_laboratorio" class="nav-link  ">
                      Laboratórios

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_medicos" class="nav-link  ">
                      Médicos

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_indicacao" class="nav-link  ">
                      Indicações

                    </a>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
        <!-- END MEGA MENU -->
      </div>
    </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
    </div>
</div>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> Agenda </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        
                       <!-- BEGIN CONTENT -->
                       <div class="page-content-wrapper">
                        <!-- BEGIN CONTENT BODY -->
                        
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <div class="page-content">
                            <div class="container">
                             
                                <!-- BEGIN PAGE CONTENT INNER -->
                                <div id="div_todo_conteudo" class="page-content-inner">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light portlet-fit  calendar">
                                                <div class="portlet-title">
                                                 <div style="display:none" class="btn-group">
                                                    <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novoagendamento"> Novo Agendamento
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                
                                                
                                             
                                                <div id="calendar" class="has-toolbar"> </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PAGE CONTENT INNER -->
                            
                        </div>
                    </div>
                    <!-- END PAGE CONTENT BODY -->
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->         
                
                
                
                
                
                <!-- /.MODAL EDITAR CALENDARIO INICIO -->


                <div class="modal fade bs-modal-lg" id="modal_editaragendamento" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Editar Agendamento</h4>
                            </div>
                            <div class="modal-body"> 
                             
                                 
                                 <div id="respostaevento"></div>
                                 
                                 
                                 
                             </div>
                             <div class="modal-footer">
                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                               
                            </div>
                        </div>
                        <!-- /.modal-content -->
                        
                          
                    
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.MODAL EDITRAR AGENDAMENTO INICIO -->             
            
            
            
            
            
            <!-- /.MODAL NOVO CALENDARIO INICIO -->


            <div class="modal fade bs-modal-lg" id="modal_novoagendamento" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Novo Agendamento</h4>
                        </div>
                        <div class="modal-body"> 
                           <form action="../calendario/novo_agendamento" enctype="multipart/form-data" method="post"  role="form" >       
                             <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Descrição</label>
                                        <input  type="text" required id="title_agendamento" name="title_agendamento" class="form-control" >
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Data</label>
                                        <input  type="date" required id="datainicial_agendamento" name="datainicial_agendamento" class="form-control" >
                                        
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Hora Inicial</label>
                                        <input   type="time" id="horainicial_agendamento" name="horainicial_agendamento" class="form-control">
                                        
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Hora Final</label>
                                        <input   type="time" id="horefinal_agendamento" name="horefinal_agendamento" class="form-control">
                                        
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            
                            <div class="row">
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Cor</label>
                                    <input  type="color" required id="cor_agendamento" name="cor_agendamento" class="form-control" >
                                    
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        <?php  include '../calendario/verificarclientes.php';  ?> 
                        <?php  include '../calendario/verificarrepresentantes.php';  ?> 
                        
                        
                        <div class="row">
                            
                         
                            <div class="col-md-4">
                              <div class="form-group">
                                  <label class="control-label">Cliente</label>  
                                  <select  name="cliente_agendamento" id="single-append-text" class="optional form-control select2-allow-clear col-md-7 col-xs-12" onkeypress="return handleEnter(this, event)">
                                      <option value="" ></option> 
                                      <?php while ($clientelista = $cliente->fetch_assoc()):   ?> 
                                          
                                          <option value="<?php echo $clientelista['id'].'|'.$clientelista['nome']; ?>" ><?php echo $clientelista['nome']; ?></option>   
                                          
                                      <?php endwhile; ?>  
                                      
                                  </select>  
                                  
                                  
                              </div>  
                          </div>
                          
                          
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="control-label">Representante</label>  
                                  <select  name="representante_agendamento" id="single-append-text" class="optional form-control select2-allow-clear col-md-7 col-xs-12" onkeypress="return handleEnter(this, event)">
                                      <option value="" ></option> 
                                      <?php while ($representanteslista = $representantes->fetch_assoc()):   ?> 
                                          
                                          <option value="<?php echo $representanteslista['id'].'|'.$representanteslista['nome']; ?>" ><?php echo $representanteslista['nome']; ?></option>   
                                          
                                      <?php endwhile; ?>  
                                      
                                  </select>  
                                  
                                  
                              </div>  
                          </div>  
                          
                          
                          
                      </div>                                                             
                      
                      <br>
                      <hr>
                      <div class="row">
                       
                          <div class="col-md-12">
                            <div class="form-group">
                               <label class="control-label">Enviar Lembrete por Email?</label>            
                               <input type="checkbox" name="alerta" id="alerta" unchecked class="make-switch switch-large" data-label-icon="fa fa-youtube" data-on-text="<i class='fa fa-thumbs-up'></i>" data-off-text="<i class='fa fa-thumbs-down'></i>"> </div>
                           </div>
                           
                           
                           <div class="col-md-6">
                              <div class="form-group">
                               <label class="control-label">Status Contato</label>
                               <select type="text" id="status_atendimento" name="status_atendimento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                 <option value="" ></option>
                                 <option value="Aguardando Pagamento" >Aguardando Pagamento</option>
                                 <option value="Solicitar Assinatura">Solicitar Assinatura</option>   
                             </select>
                         </div>           
                     </div>
                     
                     
                     
                     
                 </div> 
                 
             </div>
             <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                <button type="submit" name="salvar_agendamento" class="btn green">Salvar</button>
            </div>
        </div>
        <!-- /.modal-content -->
        
    </form>           
    
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.MODAL NOVO AGENDAMENTO INICIO -->      



</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
</div>
</div>
<div class="page-wrapper-row">
    <div class="page-wrapper-bottom">
        
    </div>
</div>
</div>
        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="../../assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->


<script type="text/javascript">
  
 $(document).ready(function() {
        
      var nivelacesso =  "<?php echo $_SESSION['nivel']; ?>";

    
      if((nivelacesso == '3')){         
          
           $('#menu_visao_geral').hide();
          // $('#menu_agenda').hide();
          // $('#menu_clientes').hide();
      // $('#menu_os').hide();
      // $('#menu_estoque').hide();
       $('#menu_relatorios').hide();
       $('#menu_cadastros').hide();
       $('#menu_movimentacoes').hide();

       $('#config_menu').hide();
       
      };

       if((nivelacesso == '2')){         
          
           $('#menu_visao_geral').hide();
          // $('#menu_agenda').hide();
          // $('#menu_clientes').hide();
      // $('#menu_os').hide();
      // $('#menu_estoque').hide();
      // $('#menu_relatorios').hide();
      // $('#menu_cadastros').hide();
       $('#menu_movimentacoes').hide();

       $('#config_menu').hide();
       
      };



        
      });


</script>

</body>


<script>


  var AppCalendar = function() {

        return {
        //main function to initiate the module
        init: function() {
            this.initCalendar();
        },

        initCalendar: function() {

            if (!jQuery().fullCalendar) {
                return;
            }

            

            var h = {};

            if (App.isRTL()) {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today'
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today, prev,next'
                    };
                }
            } else {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'today,month,agendaWeek,agendaDay'
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            
            

            
            $('#calendar').fullCalendar({ //initialize the calendar
              ignoreTimezone: false,
              monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
              monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
              dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
              dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
              
              buttonText: {
                //  prev: "&nbsp;&#9668;&nbsp;",
                //  next: "&nbsp;&#9658;&nbsp;",
                //  prevYear: "&nbsp;&lt;&lt;&nbsp;",
                //  nextYear: "&nbsp;&gt;&gt;&nbsp;",
                today: "Hoje",
                month: "Mês",
                week: "Semana",
                day: "Dia"
            },
            header: h,
                defaultView: 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 30,
                editable: true,
                droppable: true,
                eventResize: function(event, delta, revertFunc) {

               //  alert(event.url + " was dropped on " + event.start.format('YYYY-MM-DD HH:mm:ss'));

               if (!confirm("Deseja realmente fazer essa alteração?")) {
                   revertFunc();
               }else{
                  
                   $.post("../calendario/atualizar_drop.php", {idagendamento: event.url,datainicial_agendamento:event.start.format('YYYY-MM-DD HH:mm:ss'),datafinal_agendamento:event.end.format('YYYY-MM-DD HH:mm:ss')},
                    function(data){
                     
                    }
                    , "html");     
                   
               };

           },
           eventDrop: function(event, delta, revertFunc) {

               //  alert(event.url + " was dropped on " + event.start.format('YYYY-MM-DD HH:mm:ss'));

               if (!confirm("Deseja realmente fazer essa alteração?")) {
                   revertFunc();
               }else{

                alert(event.url);
                  
                   $.post("../calendario/atualizar_drop.php", {idagendamento: event.url,datainicial_agendamento:event.start.format('YYYY-MM-DD HH:mm:ss'),datafinal_agendamento:event.end.format('YYYY-MM-DD HH:mm:ss')},
                    function(data){
                     
                    }
                    , "html");     
                   
               };

           },
           eventClick: function(event, jsEvent, view) {
               
               
               $.post("../calendario/respostaevento", {idagendamento: event.url},
                function(data){
                    $("#respostaevento").empty();
                      // Coloca as frases na DIV
                      $("#respostaevento").append(data);
                  }
                  , "html");    
               
               
               

               $('#modal_editaragendamento').modal();
               
                // alert('Event: ' + event.url);
               //  alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
               //  alert('View: ' + view.name);
               
               

               if (event.url) {
                 return false;
             }

         },
         
         events: [ {
            title: 'Click for Google',
            start: new Date('1900, 01, 01'),
            end: new Date('1900, 01, 01'),
            backgroundColor: App.getBrandColor('yellow'),
            url: 'http://google.com/',
        },
        
        <?php  include '../calendario/verificar_calendario.php';  ?>  
        
        <?php 
        
        
        while ($calendariolista = $calendario->fetch_assoc()): 
           
           
           
           ?>   
           {
            title: 'OS: <?php echo $calendariolista['id'];  ?>',                    
            start: new Date('<?php echo date("Y-m-d G:i", strtotime($calendariolista['data_combinado_entrega'])); ?>'),
            end: new Date('<?php echo date("Y-m-d G:i", strtotime($calendariolista['data_combinado_entrega'])); ?>'),
            color: '#2980b9',
            url: '<?php echo $calendariolista['id']; ?>|1', 
        },     
        
        
      <?php endwhile; ?> 

       <?php  include '../calendario/verificar_calendario_lab.php';  ?> 

      <?php 
        
        
        while ($calendariopedido_laboratoriolista = $calendariopedido_laboratorio->fetch_assoc()): 
           
           
           
           ?>   
           {
            title: 'Pedido Lab: <?php echo $calendariopedido_laboratoriolista['id'];  ?>',                    
            start: new Date('<?php echo date("Y-m-d G:i", strtotime($calendariopedido_laboratoriolista['data_previsao_entrega_laboratorio'].' 08:00:00')); ?>'),
            end: new Date('<?php echo date("Y-m-d G:i", strtotime($calendariopedido_laboratoriolista['data_previsao_entrega_laboratorio'].' 08:30:00')); ?>'),
            color: '#f1c40f',
            url: '<?php echo $calendariopedido_laboratoriolista['id']; ?>|2', 
        },     
        
        
      <?php endwhile; ?>  
    
    {
        title: 'Click for Google',
        start: new Date('1900, 01, 01'),
        end: new Date('1900, 01, 01'),
        backgroundColor: App.getBrandColor('yellow'),
        url: 'http://google.com/',
        allDay: false,
    }        
    
    ]
});

}

};

}();

jQuery(document).ready(function() {    
 AppCalendar.init(); 
});


</script>


</html>