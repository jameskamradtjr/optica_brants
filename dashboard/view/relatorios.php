<?php if (!isset($_SESSION)){ session_start();};// A sessao precisa ser iniciada em cada p�gina diferente 

if(empty($_SESSION['user_email'])){header("Location: login");}; 


?>
<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Relatórios</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />

  <link href="../../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

  <link href="../../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="favicon.ico" /> </head>
  <!-- END HEAD -->


 <!-- Pular input no enter -->
  <script type="text/javascript">
    function handleEnter(field, event) {
     var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
     if (keyCode == 13) {
      var i;
      for (i = 0; i < field.form.elements.length; i++)
       if (field == field.form.elements[i])
        break;
      i = (i + 1) % field.form.elements.length;
      field.form.elements[i].focus();
      return false;
    } else
    return true;
  }
</script>


  <body class="page-container-bg-solid">
    <div class="page-wrapper">
      <div class="page-wrapper-row">
        <div class="page-wrapper-top">
          <!-- BEGIN HEADER -->
          <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
              <div class="container">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                  <a href="index.html">
                    <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">  </a>
                  </div>
                  <!-- END LOGO -->
                  <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                  <a href="javascript:;" class="menu-toggler"></a>
                  <!-- END RESPONSIVE MENU TOGGLER -->
                  <!-- BEGIN TOP NAVIGATION MENU -->
                  <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">

                     
<?php  include '../notificacoes/quant_alertas.php';  ?>

                      <!-- BEGIN USER LOGIN DROPDOWN -->
                      <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                         <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };  ?>

                         <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                         <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                       </a>
                       <ul class="dropdown-menu dropdown-menu-default">
                        <li id="config_menu">
                          <a href="configuracoes">
                          Configurações</a>
                        </li>
                        <li class="divider"> </li>
                        
                        <li>
                          <a href="../login/logout">
                            <i class="icon-key"></i> Sair </a>
                          </li>
                        </ul>
                      </li>
                      <!-- END USER LOGIN DROPDOWN -->


                    </ul>
                  </div>
                  <!-- END TOP NAVIGATION MENU -->
                </div>
              </div>
              <!-- END HEADER TOP -->
              <!-- BEGIN HEADER MENU -->
              <div class="page-header-menu">
                <div class="container" >

                  <!-- END HEADER SEARCH BOX -->
                  <!-- BEGIN MEGA MENU -->
                  <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                  <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                  <div class="hor-menu "  >
                  <ul class="nav navbar-nav" >
                    <li id="menu_visao_geral" class="menu-dropdown classic-menu-dropdown   ">
                      <a  href="visaogeral"> Visão Geral

                      </a>

                    </li>
                    <li id="menu_agenda" class="menu-dropdown mega-menu-dropdown">
                      <a  href="calendario"> Agenda

                      </a>

                    </li>

                    <li id="menu_clientes" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                      <a  href="clientes"> Clientes

                      </a>

                    </li>
                    <li id="menu_os" class="menu-dropdown mega-menu-dropdown  ">
                     <a  href="ordem_servico"> Ordem de Serviço

                     </a>

                   </li>



                   <li id="menu_estoque" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="cadastro_produtos"> Estoque

                    </a>

                  </li>





                  <li id="menu_movimentacoes" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="movimentacoes"> Movimentações

                    </a>

                  </li>

                  <li id="menu_relatorios" class="menu-dropdown classic-menu-dropdown active">
                    <a href="relatorios"> Relatórios

                    </a>

                  </li>


                  <li id="menu_cadastros" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"> Cadastros
                      <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                     <li class=" ">
                      <a href="cadastro_fornecedores" class="nav-link  ">
                      Fornecedores</a>
                    </li>
                    <li class=" ">
                      <a href="cadastro_servicos" class="nav-link  ">
                       Categorias Produtos/Serviços

                     </a>
                   </li>
                   <li class=" ">
                    <a href="cadastro_laboratorio" class="nav-link  ">
                      Laboratórios

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_medicos" class="nav-link  ">
                      Médicos

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_indicacao" class="nav-link  ">
                      Indicações

                    </a>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
            <!-- END MEGA MENU -->
          </div>
        </div>
        <!-- END HEADER MENU -->
      </div>
      <!-- END HEADER -->
    </div>
  </div>
  <div id="div_todo_conteudo" class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
      <!-- BEGIN CONTAINER -->
      <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
          <!-- BEGIN CONTENT BODY -->
          <!-- BEGIN PAGE HEAD-->
          <div class="page-head">
            <div class="container">
              <!-- BEGIN PAGE TITLE -->
              <div class="page-title">
                <h1> Relatórios </h1>
              </div>
              <!-- END PAGE TITLE -->

            </div>
          </div>
          <!-- END PAGE HEAD-->
          <!-- BEGIN PAGE CONTENT BODY -->
          <div class="page-content">
            <div class="container">


              <div id="relatorio_os" class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#relatorio_os_collpase"> + Relatório De O.S </a>
                  </h4>
                </div>



                <div id="relatorio_os_collpase" class="panel-collapse collapse">
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Data Criação Inicial</label>
                          <input value="<?php echo date('Y-m-d') ?>"  type="date" required id="data_inicial_os" name="data_inicial_os" class="form-control" >

                        </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Data Criação Final</label>
                          <input value="<?php echo date('Y-m-d') ?>"  type="date" id="data_final_os" name="data_final_os" class="form-control">

                        </div>
                      </div>
                      <!--/span-->
                    </div>

                    <div class="row">

                     <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Tipo O.S</label>
                        <select required id="tipo_os" name="tipo_os" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                         <option value=""></option>
                         <option value="1|Óptica">Óptica</option>
                         <option value="2|Assistência">Assistência</option>
                         <option value="3|Médico">Médico</option>
                         <option value="4|Refazendo">Refazendo</option>


                       </select>

                     </div>
                   </div>

                   <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Status OS</label>
                      <select  type="text" id="status_loja" name="status_loja" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">  

                       <option value=""></option>
                       <option value="Montagem">Montagem</option>
                       <option value="Pronto">Pronto</option>
                       <option value="Entregue">Entregue</option>
                       <option value="Laboratório">Laboratório</option>
                       <option value="Aguardando Armação">Aguardando Armação</option>

                     </select>  

                   </div>
                 </div>

               </div>    


               <hr>

               <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="control-label">Nr OS</label>
                    <input   type="text" id="nr_os" name="nr_os" class="form-control">

                  </div>
                </div>
              </div>    



              <div class="modal-footer">

                <button id="verificar_os" class="btn green">Verificar</button>

              </div>


              <div id="div_table_os">


              </div>   






            </div>
          </div>
        </div>  



        <!-- /.MODAL OS INICIO --> 
        <div class="modal fade bs-modal-lg" id="modal_os" role="dialog" aria-hidden="true">
         <div class="modal-dialog modal-lg">
           <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Ordem de Serviço</h4>
            </div>
            <div class="modal-body"> 

              <div id="div_modal_os">

              </div>  

            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.MODAL OS INICIO --> 



      <div id="relatorio_pl" class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#relatorio_pedido_lab"> + Relatório Pedidos Laboratório </a>
          </h4>
        </div>

        
        <div id="relatorio_pedido_lab" class="panel-collapse collapse">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Data Inicial OS</label>
                  <input value="<?php echo date('Y-m-d') ?>"  type="date" required id="data_inicial_pedido_lab" name="data_inicial_pedido_lab" class="form-control" >

                </div>
              </div>
              <!--/span-->
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Data Final OS</label>
                  <input value="<?php echo date('Y-m-d') ?>"  type="date" id="data_final_pedido_lab" name="data_final_pedido_lab" class="form-control">

                </div>
              </div>
              <!--/span-->

              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Status Pedido</label>
                  <select  type="text" id="status_pedido_lab" name="status_pedido_lab" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">  

                    <option value=""></option>
                    <option value="Enviar">Enviar</option>
                    <option value="Enviado">Enviado</option>
                    <option value="Entregue">Entregue</option>
                    

                  </select>  

                </div>
              </div>

            </div>

            <hr>

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label class="control-label">Código Pedido</label>
                  <input   type="text" id="codigo_pedido_lab" name="codigo_pedido_lab" class="form-control">

                </div>
              </div>
            </div>    


            <div class="modal-footer">

              <button id="verificar_pedido_lab" class="btn green">Verificar</button>
              
            </div>


            <div id="div_table_pedido_lab">


            </div>   



          </div>
        </div>
      </div>  

      <!-- /.MODAL OS INICIO --> 
      <div class="modal fade bs-modal-lg" id="modal_pedido_lab" role="dialog" aria-hidden="true">
       <div class="modal-dialog modal-lg">
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Pedido Laboratório</h4>
          </div>
          <div class="modal-body"> 

           <form action="../relatorios/pedido_laboratorio/atualizar_pedido_lab.php" enctype="multipart/form-data" method="post"  role="form" >     

            <div id="div_pedidolab_os">

            </div>  

          </div>
          <div class="modal-footer">
            <button class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
            <button name="atualizar_pedido_lab" type="submit" class="btn green">Salvar</button>
            <button name="excluir_pedido_lab" type="submit" class="btn red">Excluir</button>
          </div>


        </form>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.MODAL OS INICIO --> 


  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#relatorio_indicacao"> + Relatório Indicação </a>
      </h4>
    </div>

    <form action="../relatorios/indicacao/exportar_indicacoes.php" enctype="multipart/form-data" method="post"  role="form" >
      <div id="relatorio_indicacao" class="panel-collapse collapse">
        <div class="panel-body">

          <?php   include '../indicacao/verificar_indicacao.php';  ?>

          <div class="row">

            <div class="col-md-12">

             <div class="form-group">

              <label>Indicação</label>
              <select id="indicacao" name="indicacao" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
               <option value=""></option>


               <?php while ($selectindicacaolista = $selectindicacao->fetch_assoc()): ?>

                 <option value="<?php echo $selectindicacaolista['id']?>"><?php echo $selectindicacaolista['descricao'] ?></option>

               <?php  endwhile; ?>

             </select>
           </div>
         </div>
       </div>

       <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">Data Inicial</label>
            <input  type="date" required id="datainicial_ind" name="datainicial_ind" class="form-control" >

          </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">Data Final</label>
            <input   type="date" id="datafinal_ind" name="datafinal_ind" class="form-control">

          </div>
        </div>
        <!--/span-->
      </div>


      <div class="modal-footer">

        <button type="submit" name="verificar_indicacao" class="btn green">Verificar</button>

      </form>                      
    </div>

  </div>
</div>
</div>  




<div id="relatorio_comissao" class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#relatorio_comissao_colap"> + Relatório De Comissão </a>
    </h4>
  </div>

<div id="relatorio_comissao_colap" class="panel-collapse collapse">

  <form action="../relatorios/comissao/exportar_comissao.php" enctype="multipart/form-data" method="post"  role="form" >
    
      <div class="panel-body">

        <?php  include '../perfil_cliente/verificar_representantes.php';  ?>

        <div class="row">

          <div class="col-md-12">
            <div class="form-group">

              <label>Vendedor</label>
              <select required id="vendedor" name="vendedor" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 

               <option value=""></option>

               <?php while ($representanteslista = $representantes->fetch_assoc()): ?>

                 <option value="<?php echo $representanteslista['id'] ?>"><?php echo $representanteslista['nome'] ?></option>

               <?php endwhile; ?>

             </select>
           </div>
         </div>
       </div> 

       <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">Data Inicial</label>
            <input  type="date" required id="datainicial_vd" name="datainicial_vd" class="form-control" >

          </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">Data Final</label>
            <input   type="date" id="datafinal_vd" name="datafinal_vd" class="form-control">

          </div>
        </div>
        <!--/span-->
      </div>


      <div class="modal-footer">

        <button type="submit" name="verificar_comissao" class="btn green">Verificar</button>
      </form>                              
    </div>
  </div>
</div>
</div>


<div id="relatorio_despesas"  class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#relatorio_contasapagar"> + Relatório Despesas </a>
    </h4>
  </div>


  <form action="../relatorios/despesa/exportar_despesas" enctype="multipart/form-data" method="post"  role="form" >
    <div id="relatorio_contasapagar" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">Data Inicial</label>
              <input  type="date" required id="datainicial_cp" name="datainicial_cp" class="form-control" >

            </div>
          </div>
          <!--/span-->
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">Data Final</label>
              <input   type="date" id="datafinal_cp" name="datafinal_cp" class="form-control">

            </div>
          </div>
          <!--/span-->
        </div>


        <div class="modal-footer">

          <button type="submit" name="verificar_despesa" class="btn green">Verificar</button>
        </form>								 
      </div>
    </div>
  </div>
</div>	

<div id="relatorio_receitas"  class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#relatorio_contasareceber"> + Relatório Receitas </a>
    </h4>
  </div>

  <form action="../relatorios/receita/exportar_receitas" enctype="multipart/form-data" method="post"  role="form" >
    <div id="relatorio_contasareceber" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">Data Inicial</label>
              <input  type="date" required id="datainicial_cr" name="datainicial_cp" class="form-control" >

            </div>
          </div>
          <!--/span-->
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">Data Final</label>
              <input   type="date" id="datafinal_cr" name="datafinal_cp" class="form-control">

            </div>
          </div>
          <!--/span-->
        </div>


        <div class="modal-footer">

          <button type="submit" name="verificar_receitas" class="btn green">Verificar</button>
        </form>						 
      </div>

    </div>
  </div>
</div>	





</div>
</div>
</div>	






</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
</div>
</div>
<div class="page-wrapper-row">
  <div class="page-wrapper-bottom">

  </div>
</div>
</div>
        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="../../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script src="../../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="../../assets/pages/scripts/components-select2.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>
<script src="js/jquery.table2excel.min.js"></script> 
<script src="../js/dist/jquery.mask.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->


<!--Scripts Model-->
<script src="../relatorios/ordem_servico/js/adicionar_produtos_lista_relatorio.js" type="text/javascript"></script>
<script src="../relatorios/ordem_servico/js/pagamento_relatorio_os.js" type="text/javascript"></script>
<script src="../relatorios/ordem_servico/js/desconto_pagamento.js" type="text/javascript"></script>
<script src="../ordem_servico/laboratorio/add_laboratorio.js" type="text/javascript"></script>
<script src="../ordem_servico/indicacao/add_indicacao.js" type="text/javascript"></script>

<script src="../ordem_servico/cad_novo_prod_os.js" type="text/javascript"></script>
<!--Scripts Model-->


<script type="text/javascript">
  
 $(document).ready(function() {
        
      var nivelacesso =  "<?php echo $_SESSION['nivel']; ?>";
      

      if((nivelacesso == '3')){
         
             $('#config_menu').hide();
             $('#div_todo_conteudo').hide();
        alert('Seu Login não da acesso a essa área!');
      
      };


      if((nivelacesso == '4')){         
          
           $('#config_menu').hide();
           $('#menu_visao_geral').hide();
          // $('#menu_agenda').hide();
          // $('#menu_clientes').hide();
       $('#menu_os').hide();
       $('#menu_estoque').hide();
      // $('#menu_relatorios').hide();
       $('#menu_cadastros').hide();
       $('#menu_movimentacoes').hide();

       $('#relatorio_os').hide();
       $('#relatorio_comissao').hide();
       $('#relatorio_receitas').hide();
       $('#relatorio_despesas').hide();



       
      };
        
        
        
        
      });

  
</script>


<!-- MASCARA CPF/RG -->
<script>
  
  $('#valor_pagamento').mask('#.##0,00', {reverse: true});
  $('#pago').mask('#.##0,00', {reverse: true});
  $('#a_pagar').mask('#.##0,00', {reverse: true});
  $('#pago_sinal').mask('#.##0,00', {reverse: true});
  $('#a_pagar_sinal').mask('#.##0,00', {reverse: true});
  $('#total_produtos').mask('#.##0,00', {reverse: true});
    $('#valor_novo_produto').mask('#.##0,00', {reverse: true});
    $('#desconto').mask('#.##0,00', {reverse: true});
    $('#desconto_sinal').mask('#.##0,00', {reverse: true});
    $('#desvolver').mask('#.##0,00', {reverse: true});


</script>
<!-- /MASCARA CPF/RG -->


<!-- ****************************************************************************************************************************************** -->

<!-- ****************************************************************************************************************************************** -->			

<script>

	$("#exportar_listaatualizada").click(function(e) {
   $("#sample_1").table2excel({
     exclude: ".noExl",
     name: "Excel Document Name",
     filename: "Lista Atualizada",
     fileext: ".xls",
     exclude_img: true,
     exclude_links: true,
     exclude_inputs: true
   });
 });

</script>		




<script>

  $(document).on('click', '#verificar_os', function(e) {
    e.preventDefault();


    var data_inicial_os = $("#data_inicial_os");  
    var data_inicial_osPost = data_inicial_os.val(); 

    var data_final_os = $("#data_final_os");  
    var data_final_osPost = data_final_os.val(); 
    
    var tipo_os = $("#tipo_os");  
    var tipo_osPost = tipo_os.val(); 
    
    var status_loja = $("#status_loja");  
    var status_lojaPost = status_loja.val(); 

    var nr_os = $("#nr_os");  
    var nr_osPost = nr_os.val();
    


    

    $.post("../relatorios/ordem_servico/filtro_os.php", {nr_os:nr_osPost,data_inicial_os:data_inicial_osPost,data_final_os:data_final_osPost,tipo_os:tipo_osPost,status_loja:status_lojaPost},
      function(data){

       $("#div_table_os").empty();
         // Coloca na DIV
         $("#div_table_os").append(data);  

       });
    

  });

</script>    


<script>

  $(document).on('click', '.editar_os_id', function(e) {
    e.preventDefault();


        var id_osoPost = $(this).closest('tr').find('td[data-id_os]').data('id_os');//data- variavel


        $.post("../relatorios/ordem_servico/ver_os.php", {id_os:id_osoPost},
          function(data){

           $("#div_modal_os").empty();
               // Coloca na DIV
               $("#div_modal_os").append(data);  

             });
        

      });

    </script>    


    <script>

      $(document).on('click', '#verificar_pedido_lab', function(e) {
        e.preventDefault();


        var data_inicial_os = $("#data_inicial_pedido_lab");  
        var data_inicial_osPost = data_inicial_os.val(); 

        var data_final_os = $("#data_final_pedido_lab");  
        var data_final_osPost = data_final_os.val(); 
        
        var codigo_pedido_lab = $("#codigo_pedido_lab");  
        var codigo_pedido_labPost = codigo_pedido_lab.val(); 
        
        var status_pedido_lab = $("#status_pedido_lab");  
        var status_pedido_labPost = status_pedido_lab.val();    


        

        $.post("../relatorios/pedido_laboratorio/filtro_pedido.php", {data_inicial_os:data_inicial_osPost,data_final_os:data_final_osPost,codigo_pedido_lab:codigo_pedido_labPost,status_pedido_lab:status_pedido_labPost},
          function(data){

           $("#div_table_pedido_lab").empty();
         // Coloca na DIV
         $("#div_table_pedido_lab").append(data);  

       });
        

      });

    </script>    


    <script>

      $(document).on('click', '.editar_pedidolab_id', function(e) {
        e.preventDefault();


        var id_ped_labPost = $(this).closest('tr').find('td[data-id_pedido_lab]').data('id_pedido_lab');//data- variavel


        $.post("../relatorios/pedido_laboratorio/ver_pedidos_lab.php", {id_ped_lab:id_ped_labPost},
          function(data){

           $("#div_pedidolab_os").empty();
               // Coloca na DIV
               $("#div_pedidolab_os").append(data);  

             });
        

      });

    </script>    

  </body>

  </html>