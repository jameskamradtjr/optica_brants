<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>

<html lang="pt-br">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Sistema de Gestão para Óptica</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="../../assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
      
        <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet">
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="../../favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="../../index.html">
                <img src="../../assets/pages/img/logo-big.png" alt="" /> </a>
        </div>
        <!-- END LOGO -->
      
      
       <?php  include '../login/logar.php'; ?>
      
      
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" name="logar" id="logar" action="login" method="post">
                <h3 class="form-title font-green">Acessar</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Informe seu login e senha de acesso. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Login</label>
                    <input style="text-transform: lowercase;" class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Login" name="username" /> </div>
                
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Loja</label>
                    <input required="" class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="ID Loja" name="loja" /> </div>

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Senha</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Senha" name="password" id="password" /> </div>
                <div class="form-actions">
                    <button name="botao_acessar" id="botao_acessar" type="submit" class="btn green uppercase">Entrar</button>
                    <label class="rememberme check mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" />Lembrar
                        <span></span>
                    </label>
                    <a href="javascript:;" id="forget-password" class="forget-password">Esqueceu a Senha?</a>
                </div>
               
                <div class="create-account">
                    <p>
                        <a href="javascript:;" id="register-btn" class="uppercase">Criar uma Conta</a>
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" name="esqueceu" id="esqueceu" action="../login/esqueceu_senha/enviar_conf_rec" method="post">
                <h3 class="font-green">Esqueceu a Senha ?</h3>
                <p> Informe seu e-mail para enviarmos informações de recuperação. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn green btn-outline">Voltar</button>
                    <button type="submit" class="btn btn-success uppercase pull-right">Enviar</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
            <!-- BEGIN REGISTRATION FORM -->
            <form class="register-form" action="../login/nova_empresa" method="post">
                <h3 class="font-green">Criar Conta</h3>
                
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Seu Nome ou da Empresa</label>
                    <input required class="form-control placeholder-no-fix" title="Campo Obrigatório" type="text" placeholder="Seu Nome ou da Empresa" name="nome" /> </div>
               <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Login</label>
                    <input required class="form-control placeholder-no-fix" title="Email Obrigatório" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" autocomplete="off" placeholder="Login" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Senha</label>
                    <input required class="form-control placeholder-no-fix" title="Campo Obrigatório" type="password" minlength="6" title="no minimo 6 digitos para a senha!" autocomplete="off" id="register_password" placeholder="Senha" name="password" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Confirmar Senha</label>
                    <input required class="form-control placeholder-no-fix" title="Confirmação Inválida" type="password" autocomplete="off" placeholder="Confirmar Senha" name="rpassword" /> </div>
              
               <p class="hint">  </p>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9"></label>
                    <select title="Campo Obrigatório" required name="plano" class="form-control">
                        
                    
                       <option value="3">Ilimitado</option> 
                    </select>
                </div>
                
               
                <div class="form-group margin-top-20 margin-bottom-20">
                    <label class="mt-checkbox mt-checkbox-outline">
                        <input required type="checkbox" name="tnc" /> Eu Aceito
                        <a href="javascript:;">Termos de Serviço </a> &
                        <a href="javascript:;">Privacidade </a>
                        <span></span>
                    </label>
                    <div id="register_tnc_error"> </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="register-back-btn" class="btn green btn-outline">Voltar</button>
                    <button disabled="" type="submit"  id="register-submit-btn" name="cadastrar" class="btn btn-success uppercase pull-right">Criar Conta</button>
                </div>
            </form>
            <!-- END REGISTRATION FORM -->
        </div>
        <div class="copyright"> 2018 © James Desenvolvimento de Sistemas. Todos os Direitos Reservados. </div>
        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../../assets/pages/scripts/login.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>	
      
       <script>
			
			$("#password").keyup(function(event){
    if(event.keyCode == 13){
        $("#botao_acessar").click();
    }
});
			
			  </script>
      
      
    </body>

</html>