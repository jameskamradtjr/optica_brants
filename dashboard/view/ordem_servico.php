<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente 

if(empty($_SESSION['user_email'])){header("Location: login");}; 


?>
<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Cadastros</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL STYLES -->



  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
 
  <!-- DATATABLE --> 
  <link href="../../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- DATATABLE -->


  <link rel="shortcut icon" href="favicon.ico" /> </head>
  <!-- END HEAD -->
  <!-- END HEAD -->

  <!-- Pular input no enter -->
  <script type="text/javascript">
    function handleEnter(field, event) {
     var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
     if (keyCode == 13) {
      var i;
      for (i = 0; i < field.form.elements.length; i++)
       if (field == field.form.elements[i])
        break;
      i = (i + 1) % field.form.elements.length;
      field.form.elements[i].focus();
      return false;
    } else
    return true;
  }
</script>


<body class="page-container-bg-solid">

  <div class="page-wrapper">
    <div class="page-wrapper-row">
      <div class="page-wrapper-top">
        <!-- BEGIN HEADER -->
        <div class="page-header">
          <!-- BEGIN HEADER TOP -->
          <div class="page-header-top">
            <div class="container">
              <!-- BEGIN LOGO -->
              <div class="page-logo">
                <a href="#">
                  <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">    </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler"></a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                  <ul class="nav navbar-nav pull-right">





                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                      <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                       <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };  ?>

                       <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                       <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-default">
                      <li id="config_menu">
                        <a href="configuracoes">
                        Configurações</a>
                      </li>

                      <li class="divider"> </li>

                      <li>
                        <a href="../login/logout">
                          <i class="icon-key"></i> Sair </a>
                        </li>

                      </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->


                  </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
              </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
              <div class="container" >

                <!-- END HEADER SEARCH BOX -->
                <!-- BEGIN MEGA MENU -->
                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                <div class="hor-menu "  >
                  <ul class="nav navbar-nav" >
                    <li id="menu_visao_geral" class="menu-dropdown classic-menu-dropdown   ">
                      <a  href="visaogeral"> Visão Geral

                      </a>

                    </li>
                    <li id="menu_agenda" class="menu-dropdown mega-menu-dropdown">
                      <a  href="calendario"> Agenda

                      </a>

                    </li>

                    <li id="menu_clientes" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                      <a  href="clientes"> Clientes

                      </a>

                    </li>
                    <li id="menu_os" class="menu-dropdown mega-menu-dropdown  active">
                     <a  href="ordem_servico"> Ordem de Serviço

                     </a>

                   </li>



                   <li id="menu_estoque" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="cadastro_produtos"> Estoque

                    </a>

                  </li>





                  <li id="menu_movimentacoes" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="movimentacoes"> Movimentações

                    </a>

                  </li>

                  <li id="menu_relatorios" class="menu-dropdown classic-menu-dropdown ">
                    <a href="relatorios"> Relatórios

                    </a>

                  </li>


                  <li id="menu_cadastros" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"> Cadastros
                      <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                     <li class=" ">
                      <a href="cadastro_fornecedores" class="nav-link  ">
                      Fornecedores</a>
                    </li>
                    <li class=" ">
                      <a href="cadastro_servicos" class="nav-link  ">
                       Categorias Produtos/Serviços

                     </a>
                   </li>
                   <li class=" ">
                    <a href="cadastro_laboratorio" class="nav-link  ">
                      Laboratórios

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_medicos" class="nav-link  ">
                      Médicos

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_indicacao" class="nav-link  ">
                      Indicações

                    </a>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
          <!-- END MEGA MENU -->
        </div>
      </div>
      <!-- END HEADER MENU -->
    </div>
    <!-- END HEADER -->
  </div>
</div>
<div id="div_todo_conteudo" class="page-wrapper-row full-height">
  <div class="page-wrapper-middle">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
              <h1> </h1>
            </div>
            <!-- END PAGE TITLE -->

          </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
          <div class="container">


            <!-- TAB GERAL INICIO -->	

            <div class="portlet light ">

              <div class="portlet-body">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#tabservicos" data-toggle="tab"> Lista O.S do Dia </a>
                  </li>
                  <li style="display:none">
                    <a href="#taboutros" data-toggle="tab"> Outros </a>
                  </li>


                </ul> 
                <div class="tab-content">


                  <!-- TAB INICIO -->	 							
                  <div class="tab-pane fade active in" id="tabservicos">

                   <br>


                   <!-- INICIO DATATABLE SERVICOS-->

                   <div class="portlet light ">
                     <br>
                     <div class="portlet-body">
                      <div class="table-toolbar">
                       <div class="row">
                        <div class="col-md-6">
                          <div class="btn-group"><a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novoordem_servico"> Nova O.S <i class="fa fa-plus"></i></a></div>
                        </div>
                        <div class="col-md-6">
                         <div class="btn-group pull-right">
                          <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas<i class="fa fa-angle-down"></i></button>
                          <ul class="dropdown-menu pull-right">
                           <li><a id="exportar_os"><i class="fa fa-file-excel-o"></i> Exportar Excel </a></li>
                         </ul>
                       </div>
                     </div>
                   </div>
                 </div>
                 <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_ordem_servico">
                   <thead>
                    <tr>
                     <td>  OS  </td> 

                     <td style="display:none"> </td> 
                     <td style="display:none"> </td> 

                     <td>  Data Gerado  </td>
                     <td>  Entrega  </td>
                     <td>  Cliente  </td>
                     <td style="display:none"> </td> 
                     <td style="display:none"> </td> 


                     <td>  Vendedor  </td>
                     <td style="display:none"> </td> 
                     <td>  Obs  </td>
                     <td>  Pago  </td>
                     <td>  A Pagar  </td>
                     <td>  Status Cliente  </td>
                     <td>  Status Loja  </td>
                     <td style="display:none"> </td> 
                     <td style="display:none"> </td> 
                     <td style="display:none"> </td> 
                     <td style="display:none"> </td> 
                     <td style="display:none"> </td> 
                     <td style="display:none"> </td> 

                     <td> Ações </td>
                   </tr>
                 </thead>
                 <tbody>
                  <?php include '../ordem_servico/verificar_lista_os_dia.php' ; ?>

                  <?php while ($select_ordem_servicolista = $select_go_ordem_servico->fetch_assoc()): ?> 
                    <tr class="odd gradeX">
                     <td data-id ="<?php echo $select_ordem_servicolista['id']; ?>"  >  <?php echo $select_ordem_servicolista['id']; ?>  </td>
                     <td  style="display:none" data-idadmin ="<?php echo $select_ordem_servicolista['FK_usuarios_admin_id']; ?>"  >  <?php echo $select_ordem_servicolista['FK_usuarios_admin_id']; ?>  </td>
                     <td style="display:none" data-id_loja ="<?php echo $select_ordem_servicolista['id_loja']; ?>"  >  <?php echo $select_ordem_servicolista['id_loja']; ?>  </td>
                     <td data-data_gerado ="<?php echo $select_ordem_servicolista['data_gerado']; ?>"  >  <?php echo date('d/m/Y',strtotime($select_ordem_servicolista['data_gerado'])); ?>  </td>
                     <td data-data_combinado_entrega ="<?php echo $select_ordem_servicolista['data_combinado_entrega']; ?>"  >  <?php echo $select_ordem_servicolista['data_combinado_entrega']; ?>  </td>
                     <td data-cliente ="<?php echo $select_ordem_servicolista['cliente']; ?>"  >  <?php echo $select_ordem_servicolista['cliente']; ?>  </td>
                     <td style="display:none" data-fk_id_cliente ="<?php echo $select_ordem_servicolista['fk_id_cliente']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_cliente']; ?>  </td>
                     <td style="display:none" data-nr_os ="<?php echo $select_ordem_servicolista['nr_os']; ?>"  >  <?php echo $select_ordem_servicolista['nr_os']; ?>  </td>
                     <td data-vendedor ="<?php echo $select_ordem_servicolista['vendedor']; ?>"  >  <?php echo $select_ordem_servicolista['vendedor']; ?>  </td>
                     <td style="display:none" data-fk_id_vendedor ="<?php echo $select_ordem_servicolista['fk_id_vendedor']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_vendedor']; ?>  </td>
                     <td data-observacao ="<?php echo $select_ordem_servicolista['observacao']; ?>"  >  <?php echo $select_ordem_servicolista['observacao']; ?>  </td>
                     <td data-pago ="<?php echo $select_ordem_servicolista['pago']; ?>"  >  <?php echo $select_ordem_servicolista['pago']; ?>  </td>
                     <td data-a_pagar ="<?php echo $select_ordem_servicolista['a_pagar']; ?>"  >  <?php echo $select_ordem_servicolista['a_pagar']; ?>  </td>
                     <td data-status_cliente ="<?php echo $select_ordem_servicolista['status_cliente']; ?>"  >  <?php echo $select_ordem_servicolista['status_cliente']; ?>  </td>
                     <td data-status_loja ="<?php echo $select_ordem_servicolista['status_loja']; ?>"  >  <?php echo $select_ordem_servicolista['status_loja']; ?>  </td>
                     <td style="display:none" data-fk_id_relacionamento_indicacao_id ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_indicacao_id']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_indicacao_id']; ?>  </td>
                     <td style="display:none" data-fk_id_relacionamento_anexopagamentos ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_anexopagamentos']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_anexopagamentos']; ?>  </td>
                     <td style="display:none" data-fk_id_relacionamento_pagamentos ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_pagamentos']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_pagamentos']; ?>  </td>
                     <td style="display:none" data-fk_id_relacionamento_tipo_os ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_tipo_os']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_tipo_os']; ?>  </td>
                     <td style="display:none" data-fk_id_relacionamento_pedido_laboratorio ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_pedido_laboratorio']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_pedido_laboratorio']; ?>  </td>
                     <td style="display:none" data-fk_id_relacionamento_produto ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_produto']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_produto']; ?>  </td>
                     <td >
                      <div class="btn-group">
                       <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações<i class="fa fa-angle-down"></i></button>
                       <ul class="dropdown-menu" role="menu">
                        <li><a style="display:none" class="cliqueparaeditarordem_servico"   data-toggle="modal" href="#modal_editarordem_servico"> <i class="icon-docs"></i> Ver O.S </a></li>
                        <li><a  target="_blank" href="<?php echo '../docs_empresas/'.$select_ordem_servicolista['FK_usuarios_admin_id'].'/anexos/os/'.$select_ordem_servicolista['id'].'.pdf'; ?>"> <i class="icon-docs"></i> Abrir PDF OS </a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php endwhile; ?>
            </tbody>
          </table>
        </div>
      </div>

      <!-- FIM DATATABLE RENDIMENTOS-->





    </div>

    <!-- TAB FIM -->				



    <!-- Modal Nova Os Inicio-->
    <div class="modal fade bs-modal-lg" id="modal_novoordem_servico"  role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Nova O.S</h4>
      </div>
      <div class="modal-body">
       <div class="panel-body">
        <form name="add_ordem_servico" id="add_ordem_servico" action="../ordem_servico/nova_os.php" enctype="multipart/form-data" method="post" class="horizontal-form">
         <div class="form-body">
          <h3 class="form-section">   </h3>
          <div class="row">



            <div class="panel-body">


              <div class="form-group">

                <label>Tipo O.S</label>
                <select required id="tipo_os" name="tipo_os" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 

                 <option value="1|Óptica">Óptica</option>
                 <option value="2|Assistência">Assistência</option>
                 <option value="3|Médico">Médico</option>


               </select>
             </div>


           </div>

           <!--/loja vendedor-->         
           <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#relac_cadcliente"> + Vendedor </a>
              </h4>
            </div>
            <div id="relac_cadcliente" class="panel-collapse collapse in">
              <div class="panel-body">

                <?php  include '../perfil_cliente/verificar_representantes.php';  ?>


                <div class="form-group">

                  <label>Vendedor</label>
                  <select required id="vendedor" name="vendedor" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 

                    <?php if( $_SESSION['tipo_login'] <> 1){ echo '<option value="'.$_SESSION['idcolab'].'|'.$_SESSION['user_email'].'">'.$_SESSION['user_email'].'</option>';}


                     else{echo '<option value=""></option>';


                       while ($representanteslista = $representantes->fetch_assoc()):

                         echo  '<option value="'.$representanteslista['id'].'|'.$representanteslista['nome'].'">'.$representanteslista['nome'].'</option>';
  
                       endwhile; 


                     };


                   ?>

                 </select>
               </div>


             </div>
           </div>
         </div>                                                                                         
         <!--/loja vendedor--> 

         <!--/cliente-->         
         <div class="panel panel-default">




          <div class="panel-heading">
            <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#cadcliente"> + Cliente </a>
            </h4>
          </div>
          <div id="cadcliente" class="panel-collapse collapse in">
            <div class="panel-body">


              <!-- Novo Cliente-->
              <div class="form-group">  


                <a  role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapsenovocliente" aria-expanded="false" aria-controls="collapseOne">
                  <p class="col-lg-12 text-left"><strong>+ Novo Cliente</strong></p>
                </a>
                <div id="collapsenovocliente" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">


                   <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Nome</label>
                        <input  type="text"  id="nome_cliente_novo" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                      </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Celular</label>
                        <input type="text" id="celular_cliente_novo" class="form-control" placeholder="WhatsApp com DDD" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                      </div>
                    </div>
                    <!--/span-->
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Fixo</label>
                        <input type="text" id="fixo_cliente_novo"  class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                      </div>
                    </div>
                    <!--/span-->

                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">CPF</label>
                        <input type="text" id="cpf_cliente_novo"  class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                      </div>
                    </div>
                  </div>



                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <a  id="cadastrar_cliente" class="btn yellow btn-sm"> <i class="fa fa-plus" aria-hidden="true"></i> Cadastrar  </a>
                      </div>                  
                    </div>
                  </div>                             



                </div>
              </div>


            </div>
            <!-- Novo Cliente-->

            <?php  include '../ordem_servico/verificar_lista_clientes.php';  ?>


            <div class="form-group">

              <label>Cliente</label>
              <select required id="cliente" name="cliente" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 

               <option value=""></option>

               <?php while ($clientelista = $cliente->fetch_assoc()): ?>

                 <option value="<?php echo $clientelista['id'].'|'.$clientelista['nome'] ?>"><?php echo $clientelista['nome'] ?></option>

               <?php endwhile; ?>

             </select>
           </div>


         </div>
       </div>
     </div>                                                                                         
     <!--/cliente--> 

     <!--/DADOS ORDEM DE SERVIÇO-->         
     <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#dadosos"> + Dados O.S </a>
        </h4>
      </div>

      <div id="dadosos" class="panel-collapse collapse in">
        <div class="panel-body">

         <input style="display:none" value="<?php echo $_SESSION['id_empresa'] ?>"  type="text" id="id_loja" name="id_loja" class="form-control" onkeypress="return handleEnter(this, event)">    



         <div style="display:none" class="col-md-6">
          <div class="form-group"> <label class="control-label">NR OS</label>   <input  type="text" id="nr_os" name="nr_os" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    </div>
        </div>


        <div class="col-md-6">
          <div class="form-group"> <label class="control-label">Data Gerado</label>   <input value="<?php echo date("Y-m-d"); ?>"  type="date" id="data_gerado" name="data_gerado" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    </div>
        </div>
        <div  class="col-md-6">
          <div class="form-group"> <label class="control-label">Data Entrega</label>   <input  value="<?php echo date("Y-m-d"); ?>" type="date" id="data_combinado_entrega" name="data_combinado_entrega" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    </div>
        </div>
        <div  class="col-md-6">
          <div class="form-group"> <label class="control-label">Hora Entrega</label>   

            <select name="hora_entrega" id="hora_entrega"  type="text" class="form-control" data-inputmask="'mask': '99:99'" onkeypress="return handleEnter(this, event)">
              <option value="07:00">07:00</option>  
              <option value="07:30">07:30</option>  
              <option value="08:00">08:00</option>  
              <option value="08:30">08:30</option>  
              <option value="09:00">09:00</option>  
              <option value="09:30">09:30</option>  
              <option value="10:00">10:00</option>  
              <option value="10:30">10:30</option>  
              <option value="11:00">11:00</option>  
              <option value="11:30">11:30</option>  
              <option value="12:00">12:00</option>  
              <option value="12:30">12:30</option>  
              <option value="13:00">13:00</option>  
              <option value="13:30">13:30</option>  
              <option value="14:00">14:00</option>  
              <option value="14:30">14:30</option>  
              <option value="15:00">15:00</option>  
              <option value="15:30">15:30</option>  
              <option value="16:00">16:00</option>  
              <option value="16:30">16:30</option>  
              <option value="17:00">17:00</option>  
              <option value="17:30">17:30</option>  
              <option value="18:00">18:00</option>  
              <option value="18:30">18:30</option>  
              <option value="19:00">19:00</option>  
              <option value="19:30">19:30</option>  
              <option value="20:00">20:00</option>  
              <option value="20:30">20:30</option>  

            </select> 

          </div>
        </div>


        <div class="col-md-6">
          <div class="form-group">
           <label class="control-label">Status Cliente</label>   

           <select  type="text" id="status_cliente" name="status_cliente" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">  

             <option value=""></option>
             <option value="Avisar Cliente">Avisar Cliente</option>
             <option value="Aguardando Buscar">Aguardando Buscar</option>
             <option value="Entregue">Entregue</option>
             <option value="Aguardando Receita">Aguardando Receita</option>
             <option value="Aguardando Armação">Aguardando Armação</option>

           </select>  

         </div>
       </div>
       <div class="col-md-6">
        <div class="form-group">
         <label class="control-label">Status Loja</label>   

         <select  type="text" id="status_loja" name="status_loja" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">  

           <option value=""></option>
           <option value="Montagem">Montagem</option>
           <option value="Pronto">Pronto</option>
           <option value="Entregue">Entregue</option>
           <option value="Laboratório">Laboratório</option>
           <option value="Aguardando Lente">Aguardando Lente</option>
           <option value="Em Fabricação">Em Fabricação</option>
           <option value="Pedir Laboratório">Pedir Laboratório</option>


         </select>  

       </div>
     </div>

     <div class="col-md-12">
      <div class="form-group"> <label class="control-label">Observações</label>   

        <textarea type="text" id="observacao" name="observacao" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)" rows="4" cols="50"></textarea>

      </div>
    </div>




  </div>
</div>
</div>                                                                                         
<!--/DADOS ORDEM DE SERVIÇO--> 

<!--/PRODUTOS-->         
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#add_produtos"> + Produtos e Serviços </a>
    </h4>
  </div>
  <div id="add_produtos" class="panel-collapse collapse">
    <div class="panel-body">

      <?php   include '../produtos/verificar_produtos.php';  ?>

      <div class="row"> 

       <!-- Novo produto-->
       <div class="form-group">  


        <a  role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapsenovoproduto" aria-expanded="false" aria-controls="collapseOne">
          <p class="col-lg-12 text-left">+ Novo</p>
        </a>
        <div id="collapsenovoproduto" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">

           <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Descrição</label>
                <input  type="text" id="descricao_novo_produto" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label">Valor R$</label>
                <input  type="text" id="valor_novo_produto" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

              </div>
            </div>  

            <div class="col-md-3">
              <div class="form-group">
                <label class="control-label">Quantidade</label>
                <input  type="text"  id="quant_novo_produto"  class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label class="control-label">Cód. Peça</label>
                <input  type="text"  id="cod_peca_novo_produto"  class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

              </div>
            </div>

          </div> 

          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <a  id="cadastrar_produto" class="btn yellow btn-sm"> <i class="fa fa-plus" aria-hidden="true"></i> Cadastrar  </a>
              </div>                  
            </div>
          </div>             

        </div>


      </div>


    </div>
    <!-- Novo produto-->



    <div class="col-md-6">
      <div class="form-group">

        <label>Produto</label>
        <select id="produto" name="produto" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



         <option value=""></option>

         <?php while ($select_prodslista = $select_prods->fetch_assoc()): ?>

           <option value="<?php echo $select_prodslista['id'].'|'.$select_prodslista['desc_prod'].'|'.$select_prodslista['valor_venda'] ?>"><?php echo 'Desc.:'.$select_prodslista['desc_prod'].' / Cod. Peça:'.$select_prodslista['codigo_peca'] ?></option>

         <?php endwhile; ?>

       </select>

     </div>
   </div>



   <div class="col-md-3">
    <div class="form-group"> 
      <label>Quantidade</label>   
      <input value="1"  type="number" id="quant_produto" name="quant_produto" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
    </div>
  </div>

  <br>

</div> 

<div class="row"> 
  <div class="col-md-3">
   <div class="form-group">
     <a onclick="AddTableRowProdutoOs()" name="adicionar_produto" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
   </div>                  
 </div>
</div>


<br>
<div class="row">     

  <div class="col-md-12">
   <!-- iniciando tabela area-->
   <div class="portlet box blue">
     <div class="portlet-title">
       <div class="caption">
         <i class="fa fa-product-hunt"></i>Produtos/Serviços </div>
         <div class="tools">
           <a href="javascript:;" class="collapse"> </a>

           <a href="javascript:;" class="reload"> </a>

         </div>
       </div>
       <div class="portlet-body">
         <div class="table-scrollable">
           <table id="tabela_produtosos" class="table table-striped table-hover">
             <thead>
               <tr>
                 <th> # </th>
                 <th> Desc. </th>
                 <th> Quant. </th>
                 <th> Valor Unit. </th>
                 <th> Total</th>                                   
                 <th>Ação</th>

               </tr>
             </thead>
             <tbody>

               <tr>

               </tr>


             </tbody>
           </table>
         </div>
       </div>
     </div>
     <!-- fim tabela area-->

     <div class="row">
      <div class="col-md-3">
        <div class="form-group">

          <div id="div_total_produtos">



          </div>

        </div>
      </div>
    </div>    


  </div>

</div>   

</div>
</div>
</div>                                                                                         
<!--/PRODUTOS-->

<!--/PAGAMENTO-->         
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#add_pagamento"> + Pagamento </a>
    </h4>
  </div>
  <div id="add_pagamento" class="panel-collapse collapse">
    <div class="panel-body">

      <?php   //include '';  ?>

      <div class="row"> 

        <div class="col-md-6">
          <div class="form-group">

            <label>Forma Pagamento</label>
            <select id="forma_pagamento" name="forma_pagamento" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 


             <option value="Dinheiro">Dinheiro</option>
             <option value="Cartão de Crédito">Cartão de Crédito</option>
             <option value="Cartão de Débito">Cartão de Débito</option>
             
             <option value="Cheque">Cheque</option>
             <option value="Transferência Bancária">Transferência Bancária</option>


             <?php //while ($select_prodslista = $select_prods->fetch_assoc()): ?>

             <option value="<?php //echo $select_prodslista['id'].'|'.$select_prodslista['desc_prod'].'|'.$select_prodslista['valor_venda'] ?>"><?php //echo $select_prodslista['desc_prod'] ?></option>

             <?php //endwhile; ?>

           </select>

         </div>
       </div>



       <div class="col-md-3">
        <div class="form-group"> 
          <label>Valor R$</label>   
          <input  type="text" id="valor_pagamento" name="valor_pagamento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group"> 
          <label>Parcelas</label>   
          <select name="parcela_pagamento" id="parcela_pagamento"  type="text" class="form-control" onkeypress="return handleEnter(this, event)">
            <option value="1">1x</option>  
            <option value="2">2x</option>  
            <option value="3">3x</option>  
            <option value="4">4x</option>  
            <option value="5">5x</option>  
            <option value="6">6x</option>  
            <option value="7">7x</option>  
            <option value="8">8x</option>  
            <option value="9">9x</option>
            <option value="10">10x</option> 
            <option value="11">11x</option>
            <option value="12">12x</option>       
            

          </select>   
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group"> 
          <label>Entra no Caixa</label>   
          <input value="<?php echo date("Y-m-d"); ?>" type="date" id="data_caixa" name="data_caixa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">

          <?php include '../configuracoes/conta/verificar_conta.php' ?>

          <label>Conta</label>
          <select id="conta_pagamento" name="conta_pagamento" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



           <option value=""></option>
           


           <?php while ($conta_principallista = $conta_principal->fetch_assoc()): ?>

             <option value="<?php echo $conta_principallista['id'].'|'.$conta_principallista['descricao'] ?>"><?php echo $conta_principallista['descricao'] ?></option>

           <?php endwhile; ?>

         </select>

       </div>
     </div>

     <div class="col-md-4">
      <div class="form-group">

        <?php include '../movimentacoes/verificar_categorias.php' ?>

        <label>Categoria</label>
        <select id="categoria_pagamento" name="categoria_pagamento" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



         <option value=""></option>
         


         <?php echo $categoriadespesa;  ?>

       </select>

     </div>
   </div>

   <br>

 </div> 

 <div class="row"> 
  <div class="col-md-3">
   <div class="form-group">
     <a onclick="AddTableRowPagamentoOs()" name="adicionar_pagamento" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
   </div>                  
 </div>
</div>


<br>
<div class="row">     

  <div class="col-md-12">
   <!-- iniciando tabela area-->
   <div class="portlet box blue">
     <div class="portlet-title">
       <div class="caption">
         <i class="fa fa-product-hunt"></i>Pagamento </div>
         <div class="tools">
           <a href="javascript:;" class="collapse"> </a>

           <a href="javascript:;" class="reload"> </a>

         </div>
       </div>
       <div class="portlet-body">
         <div class="table-scrollable">
           <table id="tabela_pagamentosos" class="table table-striped table-hover">
             <thead>
               <tr>
                 <th> # </th>
                 <th> Forma Pag. </th>
                 <th> Conta </th>
                 <th> Cat. </th>
                 <th> Parc. </th>
                 <th> Valor. </th>
                 <th> Data</th>                                   
                 <th> Ação</th>

               </tr>
             </thead>
             <tbody>

               <tr>

               </tr>


             </tbody>
           </table>
         </div>
       </div>
     </div>
     <!-- fim tabela area-->

     <div class="row">
      <div class="col-md-3">
        <div class="form-group">

          <div id="div_total_pagamentos">



          </div>

        </div>
      </div>
    </div>    


  </div>

</div>   

</div>
</div>
</div>                                                                                         
<!--/PAGAMENTO--> 









<!--/RECEITA-->         
<div id="div_receita" class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#receita_medica"> + Receita </a>
    </h4>
  </div>
  <div id="receita_medica" class="panel-collapse collapse">
    <div class="panel-body">


     <?php  include '../medicos/verificar_medicos.php';  ?>

     <div class="row"> 

       <!-- Novo Medico-->
       <div class="form-group">  


        <a  role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapsenovomedico" aria-expanded="false" aria-controls="collapseOne">
          <p class="col-lg-12 text-left">+ Cadastrar Médico</p>
        </a>
        <div id="collapsenovomedico" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
           <div class="row">   
            <div class="col-md-12">
              <div class="form-group">
               <input type="text" id="nome_medico_novo" class="form-control form-block" placeholder="Nome Médico" value="">
             </div>
           </div>
         </div>
         <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <a  id="cadastrar_medico" class="btn yellow btn-sm"> <i class="fa fa-plus" aria-hidden="true"></i> Cadastrar  </a>
            </div>                  
          </div>
        </div>                             



      </div>
    </div>


  </div>
  <!-- Novo Medico-->

  <div class="col-md-6">
    <div class="form-group">

      <label>Médico</label>
      <select id="medico_receita_os" name="medico_receita" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



       <option value=""></option>

       <?php while ($select_go_cad_medicolista = $select_go_cad_medico->fetch_assoc()): ?>

         <option value="<?php echo $select_go_cad_medicolista['id'].'|'.$select_go_cad_medicolista['nome'] ?>"><?php echo $select_go_cad_medicolista['nome'] ?></option>

       <?php endwhile; ?>

     </select>

   </div>
 </div>



 <div class="col-md-3">
  <div class="form-group"> 
    <label>Data</label>   
    <input  type="date" value="<?php echo date("Y-m-d"); ?>" id="validade_receita" name="validade_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
  </div>
</div>


<div class="col-md-3">
  <div class="form-group"> 
    <label>Anexar</label>   
    <input  type="file" id="anexo_receita" name="anexo_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
  </div>
</div>


<br>

<hr>

<div class="col-md-6">
  <div class="form-group">




    <label>Usar Receita Anexada do Histórico</label>
    <select id="receitas_anexadas" name="receitas_anexadas" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



     <option value=""></option>

     <?php include '../receitas_medicas/verificar_receitas.php'; ?>

     <?php while ($select_receita_medicalista = $select_go_receita_medica->fetch_assoc()): ?> 

       <option value="<?php echo $select_receita_medicalista['fk_id_medico'].'|'.$select_receita_medicalista['medico'].'|'.$select_receita_medicalista['validade'].'|'.$select_receita_medicalista['valor_adicao'].'|'.$select_receita_medicalista['valor_dnp_oe_perto'].'|'.$select_receita_medicalista['valor_altura_oe_perto'].'|'.$select_receita_medicalista['valor_eixo_oe_perto'].'|'.$select_receita_medicalista['valor_cilindrico_oe_perto'].'|'.$select_receita_medicalista['valor_esferico_oe_perto'].'|'.$select_receita_medicalista['valor_dnp_od_perto'].'|'.$select_receita_medicalista['valor_altura_od_perto'].'|'.$select_receita_medicalista['valor_eixo_od_perto'].'|'.$select_receita_medicalista['valor_cilindrico_od_perto'].'|'.$select_receita_medicalista['valor_esferico_od_perto'].'|'.$select_receita_medicalista['valor_dnp_oe_longe'].'|'.$select_receita_medicalista['valor_altura_oe_longe'].'|'.$select_receita_medicalista['valor_eixo_oe_longe'].'|'.$select_receita_medicalista['valor_cilindrico_oe_longe'].'|'.$select_receita_medicalista['valor_esferico_oe_longe'].'|'.$select_receita_medicalista['valor_dnp_od_longe'].'|'.$select_receita_medicalista['valor_altura_od_longe'].'|'.$select_receita_medicalista['valor_eixo_od_longe'].'|'.$select_receita_medicalista['valor_cilindrico_od_longe'].'|'.$select_receita_medicalista['valor_esferico_od_longe'].'|'.$select_receita_medicalista['id'].'|'.$select_receita_medicalista['anexo'] ?>"> ID: <?php echo $select_receita_medicalista['id'] ?> Médico: <?php echo $select_receita_medicalista['medico'] ?> Validade: <?php echo $select_receita_medicalista['validade'] ?></option>

     <?php endwhile; ?>

   </select>

 </div>
</div>

</div> 


<br>

<!--DIV PREENCHIMENTO RECEITA-->
<div class="m-t col-xs-12">
  <div class="table-responsive">
    <div style="min-width: 626px">
      <table style="width: 25%; margin-top: 35px;" class="table table-bordered table-responsive table-condensed receita-label pull-left">
        <tbody>
          <tr  style=" border-color:#2ecc71;border-style:solid ;" class="text-success">
            <td rowspan="2">Longe</td>
            <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
          </tr>
          <tr style=" border-color:#2ecc71;border-style:solid ;" class="text-success">
            <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
          </tr>
          <tr style=" border-color:#e74c3c;border-style:solid ;" class="text-danger">
            <td rowspan="2">Perto</td>
            <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
          </tr>
          <tr style=" border-color:#e74c3c;border-style:solid ;" class="text-danger">
            <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
          </tr>
        </table>
        <table style="width: 75%" class="table table-bordered table-responsive table-condensed receita">
          <thead>
            <tr>
              <th>Esférico</th>
              <th>Cilíndrico</th>
              <th>Eixo</th>
              <th>Altura</th>
              <th>DNP</th>
            </tr>
          </thead>
          <tbody>
            <tr style=" border-color:#2ecc71;border-style:solid ;" class="text-success">
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_od_longe" type="text"> </td>
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_od_longe" type="text"> </td>
              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_od_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_od_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_od_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_od_longe" id="valor_dnp_od_longe" type="text"></td>
            </tr>
            <tr style=" border-color:#2ecc71;border-style:solid ;" class="text-success">
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_oe_longe" type="text"></td>
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_oe_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_oe_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_oe_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_oe_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_oe_longe" id="valor_dnp_oe_longe" type="text"></td>
            </tr>
            <tr style=" border-color:#e74c3c;border-style:solid ;" class="text-danger">
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_od_perto" data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_od_perto" type="text"></td>
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_od_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_od_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_od_perto" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_od_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_od_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_od_perto" id="valor_dnp_od_perto" type="text"></td>
            </tr>
            <tr style=" border-color:#e74c3c;border-style:solid ;" class="text-danger">
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_oe_perto" data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_oe_perto" type="text"></td>
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_oe_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_oe_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_oe_perto" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_oe_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_oe_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_oe_perto" id="valor_dnp_oe_perto" type="text"></td>

            </tr>
            <tr>
              <td>
                <div class="form-group full-width m-b-none text-left "><label for="nova_receita[adicao]" class="control-label">Adição</label><input class="input-sm form-control numeric-field text-right" id="valor_adicao" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="2" maxlength="12" name="valor_adicao" type="text"></div>
              </td>
              <td colspan="4"></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!--DIV PREENCHIMENTO RECEITA-->


  <div class="row"> 

    <div class="col-md-12">
      <div class="form-group">

        <label>Observação</label>
        <textarea rows="4" cols="50" id="obs_receita" name="obs_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)"></textarea>

      </div>
    </div>







  </div> 



</div>
</div>
</div>                                                                                         
<!--/RECEITA--> 



<!--/PEDIDO LABORATORIO-->         
<div id="div_pedido_lab" class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#pedidolab"> + Pedido Laboratório </a>
    </h4>
  </div>
  <div id="pedidolab" class="panel-collapse collapse">
    <div class="panel-body">

      <div class="row"> 

        <div class="col-md-6">
          <div class="form-group">

            <?php   include '../laboratorios/verificar_laboratorios.php';  ?> 

            <label>Laboratório</label>
            <select id="laboratorio" name="laboratorio" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



             <option value=""></option>

             <?php while ($select_go_cad_laboratorioslista = $select_go_cad_laboratorios->fetch_assoc()): ?>

               <option value="<?php echo $select_go_cad_laboratorioslista['id'].'|'.$select_go_cad_laboratorioslista['nome'] ?>"><?php echo $select_go_cad_laboratorioslista['nome'] ?></option>

             <?php endwhile; ?>

           </select>

         </div>
       </div>



       <div class="col-md-3">
        <div class="form-group"> 
          <label>Status Laboratório</label>   
          <select  type="text" id="status_lab" name="status_lab" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">  

           <option value=""></option>
           <option value="Enviar">Enviar</option>
           <option value="Em Fabricação">Em Fabricação</option>
           <option value="Aguardando Lente">Aguardando Lente</option>
           <option value="Entregue Loja">Entregue Loja</option>


         </select>    
       </div>
     </div>


     <div class="col-md-3">
      <div class="form-group"> 
        <label>Data Entrega</label>   
        <input  type="date" id="data_entrega" value="<?php echo date("Y-m-d"); ?>" name="data_entrega" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
      </div>
    </div>

    <br>

  </div>

  <div class="row"> 

    <div class="col-md-12">
      <div class="form-group">

        <label>Observação</label>
        <textarea rows="4" cols="50" id="obs_laboratorio" name="obs_laboratorio" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)"></textarea>

      </div>
    </div>

  </div> 


  <div class="row"> 
    <div class="col-md-3">
     <div class="form-group">
       <a onclick="AddTableRowLaboratorioOs()" name="adicionar_laboratorio" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
     </div>                  
   </div>
 </div>


 <br>
 <div class="row">     

  <div class="col-md-12">
   <!-- iniciando tabela area-->
   <div class="portlet box blue">
     <div class="portlet-title">
       <div class="caption">
         <i class="fa fa-product-hunt"></i>Pedidos </div>
         <div class="tools">
           <a href="javascript:;" class="collapse"> </a>

           <a href="javascript:;" class="reload"> </a>

         </div>
       </div>
       <div class="portlet-body">
         <div class="table-scrollable">
           <table id="tabela_laboratorio" class="table table-striped table-hover">
             <thead>
               <tr>
                 <th> ID Lab. </th>
                 <th> Laboratório </th>
                 <th> Status </th>
                 <th> Entrega. </th>
                 <th> Obs</th>                                   
                 <th> Ação</th>

               </tr>
             </thead>
             <tbody>

               <tr>

               </tr>


             </tbody>
           </table>
         </div>
       </div>
     </div>
     <!-- fim tabela area-->

     <div class="row">
      <div class="col-md-3">
        <div class="form-group">

          <div id="div_total_laboratorios">



          </div>

        </div>
      </div>
    </div>    


  </div>

</div>


</div>
</div>
</div>                                                                                         
<!--/PEDIDO LABORATORIO--> 

<!--/Assistência-->         
<div id="div_assistencia" style="display:none" class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#assistencia"> + Assistência </a>
    </h4>
  </div>

  <div id="assistencia" class="panel-collapse collapse">
    <div class="panel-body">


      <div class="col-md-12">
        <div class="form-group"> <label class="control-label">Identificação</label>   
          <input  type="text" id="identificacao_assistencia" name="identificacao_assistencia" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
        </div>
      </div>

      <div class="col-md-12">
        <div class="form-group"> <label class="control-label">Observação</label>   

         <textarea type="text" id="obs_assistencia" name="obs_assistencia" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)" rows="4" cols="50"></textarea>   

       </div>
     </div>

     <div class="col-md-12">
      <div class="form-group"> <label class="control-label">Defeito Relatado</label> 
        <textarea type="text" id="defeito_assistencia" name="defeito_assistencia" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)" rows="4" cols="50"></textarea>  
      </div>
    </div>

    <div class="col-md-12">
      <div class="form-group"> <label class="control-label">Serviço a ser Realizado</label>   
       <textarea type="text" id="servico_assistencia" name="servico_assistencia" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)" rows="4" cols="50"></textarea>     
     </div>
   </div>
   
   
   



 </div>
</div>
</div>                                                                                         
<!--/Assistência--> 



<!--/indicação-->         
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#add_indicacao"> + Indicação </a>
    </h4>
  </div>
  <div id="add_indicacao" class="panel-collapse collapse">
    <div class="panel-body">

     <!-- Nova Indicação-->
     <div class="form-group">  


      <a  role="tab" id="headingind" data-toggle="collapse" data-parent="#accordion" href="#collapsenovaindicacao" aria-expanded="false" aria-controls="collapseOne">
        <p class="col-lg-12 text-left">+ Cadastrar Indicação</p>
      </a>
      <div id="collapsenovaindicacao" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingind">
        <div class="panel-body">
         <div class="row">    
           <div class="col-md-12">
            <div class="form-group">     
              <input type="text" id="nome_nova_indicacao" class="form-control form-block" placeholder="Indicação Descrição" value="">                             
            </div>                  
          </div>
        </div> 
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <a  id="cadastrar_indicacao" class="btn yellow btn-sm"> <i class="fa fa-plus" aria-hidden="true"></i> Cadastrar  </a>
            </div>                  
          </div>
        </div>                             



      </div>
    </div>


  </div>
  <!-- Nova Indicação-->

  <?php   include '../indicacao/verificar_indicacao.php';  ?>


  <div class="form-group">

    <label>Indicação</label>
    <select id="indicacao" name="indicacao" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
     <option value=""></option>


     <?php while ($selectindicacaolista = $selectindicacao->fetch_assoc()): ?>

       <option value="<?php echo $selectindicacaolista['id'].'|'.$selectindicacaolista['descricao'] ?>"><?php echo $selectindicacaolista['descricao'] ?></option>

     <?php  endwhile; ?>

   </select>

 </div>

 <div class="row"> 
  <div class="col-md-3">
   <div class="form-group">
     <a onclick="AddTableRowIndicacaoOs()" name="adicionar_indicacao" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
   </div>                  
 </div>
</div>


<br>
<div class="row">     

  <div class="col-md-12">
   <!-- iniciando tabela area-->
   <div class="portlet box blue">
     <div class="portlet-title">
       <div class="caption">
         <i class="fa fa-product-hunt"></i>Indicações </div>
         <div class="tools">
           <a href="javascript:;" class="collapse"> </a>

           <a href="javascript:;" class="reload"> </a>

         </div>
       </div>
       <div class="portlet-body">
         <div class="table-scrollable">
           <table id="tabela_indicacao" class="table table-striped table-hover">
             <thead>
               <tr>
                 <th> # </th>
                 <th> Desc. </th>

                 <th>Ação</th>

               </tr>
             </thead>
             <tbody>

               <tr>

               </tr>


             </tbody>
           </table>
         </div>
       </div>
     </div>
     <!-- fim tabela area-->




   </div>

 </div>  





</div>
</div>
</div>                                                                                         
<!--/indicação--> 







</div>
</div>
</div>
</div>
<div class="modal-footer">
  <a class="btn dark btn-outline" data-dismiss="modal">Fechar</a>
  <button type="submit" name="adicionar_ordem_servico" class="btn green">Adicionar</button>
</div>

</form>
</div>
</div>
</div>




<!-- Modal Nova Os Fim-->


<!-- Modal Editar OS Inicio-->

<div class="modal fade bs-modal-lg" id="modal_editarordem_servico"  role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-lg">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">O.S</h4>
  </div>
  <div class="modal-body">
    <form name="editar_ordem_servico" id="editar_ordem_servico" action="../ordem_servico/atualizar_os" enctype="multipart/form-data" method="post" class="horizontal-form">

     <div class="form-body">

       <div id="div_ver_os">

       </div>
       
       

     </div>

   </div>
   <div class="modal-footer"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
    <button type="submit" name="salvar_ordem_servico" class="btn green">Salvar</button>
    <button type="submit" name="excluir_ordem_servico" class="btn red">Excluir</button></div>
  </form>
</div>
</div>
</div>



<!-- Modal Editar OS Fim-->


<!-- tab outros inicio -->												
<div class="tab-pane fade" id="taboutros">
  <br>


</div>
<!-- tab outros fim -->







</div>
<div class="clearfix margin-bottom-20"> </div>


</div>
</div>  


<!-- TAB GERAL FIM -->	


</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
</div>
</div>
<div class="page-wrapper-row">
  <div class="page-wrapper-bottom">

  </div>
</div>
</div>

        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="../../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="../../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="../../assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<!-- jquery.inputmask data -->
<script src="../js/jquery.inputmask.bundle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js"></script>
<script src="../js/jquery.table2excel.min.js"></script> 

<!-- DATATABLE -->
<script src="../../assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>
<script src="../../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- DATATABLE -->



<!--Scripts Model-->
<script src="../ordem_servico/produtos/adicionar_produtos_lista.js" type="text/javascript"></script>
<script src="../ordem_servico/pagamentos/add_pagamentos.js" type="text/javascript"></script>
<script src="../ordem_servico/laboratorio/add_laboratorio.js" type="text/javascript"></script>
<script src="../ordem_servico/indicacao/add_indicacao.js" type="text/javascript"></script>
<script src="../ordem_servico/pagamentos/desconto_pagamento.js" type="text/javascript"></script>

<script src="../ordem_servico/cad_medico_novo_os.js" type="text/javascript"></script>
<script src="../ordem_servico/cad_nova_ind_os.js" type="text/javascript"></script>
<script src="../ordem_servico/cad_novo_prod_os.js" type="text/javascript"></script>
<script src="../ordem_servico/hide_tipo_os.js" type="text/javascript"></script>
<script src="../ordem_servico/mascaras.js" type="text/javascript"></script>
<script src="../ordem_servico/ver_os.js" type="text/javascript"></script>
<script src="../ordem_servico/cad_novo_cliente.js" type="text/javascript"></script>
<script src="../ordem_servico/validacao_campos_os.js" type="text/javascript"></script>



<script type="text/javascript">
  
 $(document).ready(function() {

    var nivelacesso =  "<?php echo $_SESSION['nivel']; ?>";

        

      if((nivelacesso == '3')){         
          
           $('#config_menu').hide();
           $('#menu_visao_geral').hide();
          // $('#menu_agenda').hide();
          // $('#menu_clientes').hide();
      // $('#menu_os').hide();
      // $('#menu_estoque').hide();
       $('#menu_relatorios').hide();
       $('#menu_cadastros').hide();
       $('#menu_movimentacoes').hide();
       
      };
      
   if( (nivelacesso == '4') ){
         
            $('#config_menu').hide();
            $('#div_todo_conteudo').hide();
        alert('Seu Login não da acesso a essa área!');

      };      
        
        
        
      });


</script>
<!--Scripts Model-->









<!-- ****************************************************************************************************************************************** -->     

<!-- jquery.inputmask data -->
<script>
 $(document).ready(function() {
  $(":input").inputmask();
});
</script>
<!-- /jquery.inputmask -->


<!-- ****************************************************************************************************************************************** -->     

<script>

  $("#exportar_os").click(function(e) {
    $("#table_ordem_servico").table2excel({
     exclude: ".noExl",
     name: "Excel Document Name",
     filename: "Lista OS",
     fileext: ".xls",
     exclude_img: true,
     exclude_links: true,
     exclude_inputs: true
   });
  });

</script>   


<!-- ****************************************************************************************************************************************** -->     



</body>

</html>