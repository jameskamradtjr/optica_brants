<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  

if(empty($_SESSION['user_email'])){header("Location: login");}; 



if (filter_var($_GET['id'], FILTER_VALIDATE_INT)) {

} else {
  header("Location: clientes");   
}



?>
<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Cliente</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL STYLES -->

  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="favicon.ico" /> </head>
  <!-- END HEAD -->

  <body class="page-container-bg-solid">



   <!-- Pular input no enter -->
   <script type="text/javascript">
    function handleEnter(field, event) {
     var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
     if (keyCode == 13) {
      var i;
      for (i = 0; i < field.form.elements.length; i++)
       if (field == field.form.elements[i])
        break;
      i = (i + 1) % field.form.elements.length;
      field.form.elements[i].focus();
      return false;
    } else
    return true;
  }
</script>


<!-- Adicionando Javascript -->
<script type="text/javascript" >

  function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('endereco').value=("");            
          //  document.getElementById('cidade').value=("");
          //  document.getElementById('estado').value=("");

        }

        function meu_callback(conteudo) {
          if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('endereco').value=(conteudo.logradouro+', '+conteudo.bairro);
					 // document.getElementById('estado').value=(conteudo.uf);
          //  document.getElementById('cidade').value=(conteudo.localidade);


        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
          }
        }

        function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('endereco').value="...";

               // document.getElementById('cidade').value="...";
               // document.getElementById('estado').value="...";
               

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = '//viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
              }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
          }
        };

      </script>



      <?php  include '../perfil_cliente/verificarperfilclientes.php';  ?>	


      <?php  include '../perfil_cliente/verificarmovimentacoesclientes.php';  ?>
      <?php  include '../perfil_cliente/verificar_representantes.php';  ?>
      <?php  include '../perfil_cliente/verificar_empresas.php';  ?>


      <div class="page-wrapper">
        <div class="page-wrapper-row">
          <div class="page-wrapper-top">
            <!-- BEGIN HEADER -->
            <div class="page-header">
              <!-- BEGIN HEADER TOP -->
              <div class="page-header-top">
                <div class="container">
                  <!-- BEGIN LOGO -->
                  <div class="page-logo">
                    <a href="index.html">
                      <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">
                    </a>
                  </div>
                  <!-- END LOGO -->
                  <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                  <a href="javascript:;" class="menu-toggler"></a>
                  <!-- END RESPONSIVE MENU TOGGLER -->
                  <!-- BEGIN TOP NAVIGATION MENU -->
                  <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">







                      <!-- BEGIN USER LOGIN DROPDOWN -->
                      <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                         <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };  ?>

                         <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                         <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                       </a>
                       <ul class="dropdown-menu dropdown-menu-default">
                        <li id="config_menu">
                          <a href="configuracoes">
                          Configurações</a>
                        </li>

                        <li class="divider"> </li>

                        <li>
                          <a href="../login/logout">
                            <i class="icon-key"></i> Sair </a>
                          </li>

                        </ul>
                      </li>
                      <!-- END USER LOGIN DROPDOWN -->


                    </ul>
                  </div>
                  <!-- END TOP NAVIGATION MENU -->
                </div>
              </div>
              <!-- END HEADER TOP -->
              <!-- BEGIN HEADER MENU -->
              <div class="page-header-menu">
                <div class="container" >

                  <!-- END HEADER SEARCH BOX -->
                  <!-- BEGIN MEGA MENU -->
                  <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                  <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                  <div class="hor-menu "  >
                  <ul class="nav navbar-nav" >
                    <li id="menu_visao_geral" class="menu-dropdown classic-menu-dropdown   ">
                      <a  href="visaogeral"> Visão Geral

                      </a>

                    </li>
                    <li id="menu_agenda" class="menu-dropdown mega-menu-dropdown">
                      <a  href="calendario"> Agenda

                      </a>

                    </li>

                    <li id="menu_clientes" class="menu-dropdown mega-menu-dropdown active mega-menu-full">
                      <a  href="clientes"> Clientes

                      </a>

                    </li>
                    <li id="menu_os" class="menu-dropdown mega-menu-dropdown  ">
                     <a  href="ordem_servico"> Ordem de Serviço

                     </a>

                   </li>



                   <li id="menu_estoque" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="cadastro_produtos"> Estoque

                    </a>

                  </li>





                  <li id="menu_movimentacoes" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="movimentacoes"> Movimentações

                    </a>

                  </li>

                  <li id="menu_relatorios" class="menu-dropdown classic-menu-dropdown ">
                    <a href="relatorios"> Relatórios

                    </a>

                  </li>


                  <li id="menu_cadastros" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"> Cadastros
                      <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                     <li class=" ">
                      <a href="cadastro_fornecedores" class="nav-link  ">
                      Fornecedores</a>
                    </li>
                    <li class=" ">
                      <a href="cadastro_servicos" class="nav-link  ">
                       Categorias Produtos/Serviços

                     </a>
                   </li>
                   <li class=" ">
                    <a href="cadastro_laboratorio" class="nav-link  ">
                      Laboratórios

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_medicos" class="nav-link  ">
                      Médicos

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_indicacao" class="nav-link  ">
                      Indicações

                    </a>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
            <!-- END MEGA MENU -->
          </div>
        </div>
        <!-- END HEADER MENU -->
      </div>
      <!-- END HEADER -->
    </div>
  </div>
  <div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
      <!-- BEGIN CONTAINER -->
      <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
          <!-- BEGIN CONTENT BODY -->
          <!-- BEGIN PAGE HEAD-->
          <div class="page-head">
            <div class="container">
              <!-- BEGIN PAGE TITLE -->
              <div class="page-title">
                <ul class="page-breadcrumb breadcrumb">

                  <li>
                    <a href="clientes">Lista Clientes</a>
                    <i class="fa fa-circle"></i>
                  </li>
                  <li>
                    <span>Perfil (<?php echo $clientelista['nome'] ?>)</span>
                  </li>
                </ul>
              </div>
              <!-- END PAGE TITLE -->

            </div>
          </div>
          <!-- END PAGE HEAD-->
          <!-- BEGIN PAGE CONTENT BODY -->
          <div class="page-content">
            <div class="container">

              <div style="overflow:visible" class="tabbable-custom nav-justified">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active">
                    <a href="#tab_perfilcliente" data-toggle="tab"> Perfil </a>
                  </li>
                  <li>
                    <a href="#tab_receitas" data-toggle="tab"> Receita Médica </a>
                  </li>
                  <li>
                    <a href="#tab_servicos" data-toggle="tab"> Ordem de Serviço </a>
                  </li>

                  <li style="display:none">
                    <a href="#tab_vendas" data-toggle="tab"> Vendas </a>
                  </li>

                </ul>
                <div class="tab-content">

                 <!-- PERFIL TAB INICIO -->
                 <div class="tab-pane active" id="tab_perfilcliente">


                   <div class="page-content-inner">
                    <div class="row">
                      <div class="col-md-12">
                        <!-- BEGIN PROFILE SIDEBAR -->
                        <div class="profile-sidebar">
                          <!-- PORTLET MAIN -->
                          <div class="portlet light profile-sidebar-portlet ">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">

                             <?php 


                             $idadmin = $_SESSION['idadmin'];
                             if (empty($clientelista['foto'])){ $foto='../img/user.jpg'; } else { $foto='../docs_empresas/'.$idadmin.'/fotos/clientes/'.$clientelista['id'].'.jpg'; };


                             ?>

                             <img src="<?php echo $foto ?>" class="img-responsive" alt=""> </div>
                             <!-- END SIDEBAR USERPIC -->
                             <!-- SIDEBAR USER TITLE -->
                             <div class="profile-usertitle">
                              <div class="profile-usertitle-name"> <?php echo $clientelista['nome'] ; ?> </div>
                              <div class="profile-usertitle-job">  </div>
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->
                            <div class="profile-userbuttons">
                              <a style="display:none" type="button" class="btn btn-circle red btn-sm" data-toggle="modal" href="#modal_novoagendamento">Alerta</a>
                              <a target="_blank" href="https://api.whatsapp.com/send?1=pt_BR&phone=55<?php echo $clientelista['celular']; ?>&text=Olá <?php echo $clientelista['nome']; ?>" type="button" class="btn btn-circle green btn-sm">Chamar no WhatsApp</a>
                            </div>
                            <!-- END SIDEBAR BUTTONS -->

                          </div>
                          <!-- END PORTLET MAIN -->
                          <!-- PORTLET MAIN -->
                          <div class="portlet light ">
                           <?php $fk_id_cliente = $clientelista['id']; ?>






                         </div>
                         <!-- END PORTLET MAIN -->
                       </div>
                       <!-- END BEGIN PROFILE SIDEBAR -->
                       <!-- BEGIN PROFILE CONTENT -->
                       <div class="profile-content">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="portlet light ">
                              <div class="portlet-title tabbable-line">

                                <ul class="nav nav-tabs">
                                  <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Dados</a>
                                  </li>
                                  <li>
                                    <a href="#tab_1_2" data-toggle="tab">Foto</a>
                                  </li>
                                  <li>
                                    <a href="#tab_1_3" data-toggle="tab">Banco</a>
                                  </li>
                                  <li>
                                    <a href="#tab_1_4" data-toggle="tab">Config.</a>
                                  </li>
                                  <li>
                                    <a href="#tab_1_5" data-toggle="tab">Anexos</a>
                                  </li>
                                </ul>
                              </div>
                              <div class="portlet-body">
                                <div class="tab-content">
                                  <!-- PERSONAL INFO TAB -->
                                  <div class="tab-pane active" id="tab_1_1">
                                    <form action="../clientes/atualizar_cliente" enctype="multipart/form-data" method="post"  role="form" >

                                     <input type="hidden" id="id_cliente_edicao" name="id_cliente_edicao" value="<?php echo $clientelista['id']; ?>">

                                     <div style="display:none" class="form-group">
                                      <label class="control-label">Pessoa</label>                                                                      

                                      <select  name="pessoa" id="pessoa" class="optional form-control col-md-7 col-xs-12" onkeypress="return handleEnter(this, event)">
                                        <option value="<?php echo $clientelista['pessoa'] ?>" ><?php echo $clientelista['pessoa']; ?></option> 
                                        <option value="Física" >Física</option>
                                        <option value="Jurídica" >Jurídica</option>		



                                      </select>  
                                    </div>

                                    <br>
                                    <div style="display:none" class="form-group">
                                      <label class="control-label">Status Atendimento</label>                                                                      

                                      <select  name="status_atendimento" id="status_atendimento" class="optional form-control col-md-7 col-xs-12" onkeypress="return handleEnter(this, event)">
                                        <option value="<?php echo $clientelista['status_atendimento'] ?>" ><?php echo $clientelista['status_atendimento']; ?></option> 
                                        <option value="Primeiro Contato">Primeiro Contato</option>
                                        <option value="Aguardando Pagamento" >Aguardando Pagamento</option>
                                        <option value="Cliente">Cliente</option>
                                        <option value="Afastado">Afastado</option>	
                                        <option value="Ofertar">Ofertar</option>		
                                        <option value="Entrar em Contato">Entrar em Contato</option>	
                                        <option value="Enviar Contrato">Enviar Contrato</option>	
                                        <option value="Solicitar Assinatura">Solicitar Assinatura</option> 	
                                        <option value="Comprar Bitcoin">Comprar Bitcoin</option> 
                                        <option value="Vender Bitcoin">Vender Bitcoin</option>
                                        <option value="Reaplicar">Reaplicar</option>
                                        <option value="Fazer Depósito">Fazer Depósito</option>
                                        <option value="Stand By">Stand By</option>



                                      </select>  
                                    </div>
                                    <br>
                                    <br>

                                    <div class="form-group">
                                      <label class="control-label">Nome</label>
                                      <input type="text" name="nome" id="nome" required value="<?php echo $clientelista['nome']; ?>" placeholder="" class="form-control" onkeypress="return handleEnter(this, event)"  /> </div>
                                      <div class="form-group">
                                        <label class="control-label">Data Nascimento</label>
                                        <input onkeypress="return handleEnter(this, event)" type="text" placeholder="" data-inputmask="'mask': '99/99/9999'" name="data_nascimento" id="data_nascimento" value="<?php $data_nascimento = implode("/",array_reverse(explode("-",$clientelista['data_nascimento']))); echo $data_nascimento ?>"  class="form-control" /> </div>
                                        <div class="form-group">
                                          <label class="control-label">E-Mail</label>
                                          <input type="text" placeholder="" name="email" id="email" value="<?php echo $clientelista['email']; ?>"  class="form-control" onkeypress="return handleEnter(this, event)" /> </div>
                                          <div class="form-group">
                                            <label class="control-label">Telefone Fixo</label>
                                            <input type="text" placeholder="" name="telefone" id="telefone" value="<?php echo $clientelista['telefone']; ?>" class="form-control" onkeypress="return handleEnter(this, event)" /> </div>
                                            <div class="form-group">
                                              <label class="control-label">Celular</label>
                                              <input type="text" placeholder="" name="celular" id="celular" value="<?php echo $clientelista['celular']; ?>" class="form-control" onkeypress="return handleEnter(this, event)" /> </div>
                                              <div class="form-group">
                                                <label class="control-label">CPF</label>
                                                <input type="text" placeholder="" name="cpf"  id="cpf" value="<?php echo $clientelista['cpf']; ?>" class="form-control" onkeypress="return handleEnter(this, event)" /> </div>
                                                <div style="display:none" class="form-group">
                                                  <label class="control-label">CNPJ</label>
                                                  <input type="text" placeholder="" name="cnpj"  id="cnpj" value="<?php echo $clientelista['cnpj']; ?>" class="form-control" onkeypress="return handleEnter(this, event)" /> </div>
                                                  <div style="display:none" class="form-group">
                                                    <label class="control-label">RG</label>
                                                    <input type="text" placeholder="" name="rg"  id="rg" value="<?php echo $clientelista['rg']; ?>" class="form-control" onkeypress="return handleEnter(this, event)" /> </div>


                                                    <div style="display:none" class="form-group">
                                                      <label class="control-label">Estado</label>
                                                      <?php		include '../cidade_estados/popularestados.php';   ?>	

                                                      <select  name="estado" id="estado" class="optional form-control col-md-7 col-xs-12" onkeypress="return handleEnter(this, event)">
                                                        <option value="<?php echo $clientelista['uf'].'|'.$clientelista['cod_estados']; ?>" ><?php echo $clientelista['uf']; ?></option> 
                                                        <?php while ($uf = $estados->fetch_assoc()):   ?>	

                                                          <option value="<?php echo $uf['sigla'].'|'.$uf['cod_estados']; ?>" ><?php echo $uf['sigla']; ?></option>   

                                                        <?php endwhile; ?>	

                                                      </select>  
                                                    </div>

                                                    <div style="display:none" class="form-group">


                                                      <label class="control-label">Cidade</label>

                                                      <select  name="cidade" id="cidade" class="optional form-control col-md-7 col-xs-12" onkeypress="return handleEnter(this, event)">

                                                        <option value="<?php echo $clientelista['cidade'].'|'.$clientelista['cod_cidades']; ?>"> <?php echo $clientelista['cidade']; ?>  </option> 
                                                        <option value="">-- Escolha um estado --</option> 



                                                      </select>        


                                                    </div>

                                                    <div style="display:none" class="form-group">
                                                      <label class="control-label">CEP</label>
                                                      <input onblur="pesquisacep(this.value);" type="text" placeholder="" name="cep" id="cep" value="<?php echo $clientelista['cep']; ?>" class="form-control" onkeypress="return handleEnter(this, event)" /> </div>
                                                      <div class="form-group">
                                                        <label class="control-label">Endereço</label>
                                                        <input type="text" placeholder="" name="endereco" id="endereco" value="<?php echo $clientelista['endereco']; ?>" class="form-control" onkeypress="return handleEnter(this, event)"/> </div>
                                                        <div class="form-group">
                                                          <label class="control-label">Observações</label>
                                                          <textarea class="form-control"  name="observacoes" id="observacoes" rows="3" placeholder="" onkeypress="return handleEnter(this, event)"><?php echo $clientelista['observacoes']; ?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                          <label class="control-label">Como Conheceu</label>
                                                          <textarea class="form-control"  name="como_conheceu" id="como_conheceu" rows="3" placeholder="" onkeypress="return handleEnter(this, event)"><?php echo $clientelista['como_conheceu']; ?></textarea>
                                                        </div>

                                                        <div class="margiv-top-10">

                                                         <button class="btn green" type="submit" name="salvar_alteracoes_cliente"> Salvar Alterações </button>   

                                                       </div>
                                                     </form>
                                                   </div>
                                                   <!-- END PERSONAL INFO TAB -->
                                                   <!-- CHANGE AVATAR TAB -->
                                                   <div class="tab-pane" id="tab_1_2">

                                                     <form action="../clientes/atualizar_foto" enctype="multipart/form-data" method="post"  role="form" >
                                                      <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                            <div>
                                                              <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Selecione Imagem </span>
                                                                <span class="fileinput-exists"> Outra? </span>
                                                                <input type="file" name="foto"> </span>
                                                                <input type="hidden" id="id_cliente_edicao" name="id_cliente_edicao" value="<?php echo $clientelista['id']; ?>">
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                                              </div>
                                                            </div>
                                                            <div class="clearfix margin-top-10">
                                                              <span class="label label-danger">NOTA! </span>
                                                              <span>IMAGENS NO FORMATO JPEG! </span>
                                                            </div>
                                                          </div>
                                                          <div class="margin-top-10">
                                                            <button type="submit" name="salvar_foto" class="btn green"> Salvar Alterações </button>

                                                          </div>
                                                        </form>
                                                      </div>
                                                      <!-- END CHANGE AVATAR TAB -->

                                                      <?php  include '../perfil_cliente/verificarcontasbancariasclientes.php';  ?>
                                                      <?php  include '../clientes/verificarbanco.php';  ?>	

                                                      <!-- CONTA BANCARIATAB -->
                                                      <div class="tab-pane" id="tab_1_3">
                                                       <form action="../clientes/atualizar_dadosbancarios" enctype="multipart/form-data" method="post"  role="form" >

                                                        <input type="hidden" id="id_cliente_edicao" name="id_cliente_edicao" value="<?php echo $clientelista['id']; ?>">

                                                        <div class="form-group">

                                                          <label>Banco</label>
                                                          <select id="single-append-text" name="banco" required class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                                                           <option value="<?php echo $conta_bancaria_clientelista['fk_id_banco'].'|'.$conta_bancaria_clientelista['banco'] ?>"><?php echo $conta_bancaria_clientelista['banco']; ?></option>
                                                           <?php while ($bancoslista = $bancos->fetch_assoc()): ?>

                                                             <option value="<?php echo $bancoslista['id'].'|'.$bancoslista['code'].' - '.$bancoslista['title'] ?>"><?php echo $bancoslista['code'].' - '.$bancoslista['title'] ?></option>
                                                           <?php endwhile; ?>
                                                         </select>
                                                       </div>

                                                       <div class="form-group">
                                                        <label class="control-label">Agência</label>
                                                        <input type="text" name="agencia" id="agencia" value="<?php echo $conta_bancaria_clientelista['agencia']; ?>" class="form-control" /> </div>
                                                        <div class="form-group">
                                                          <label class="control-label">Conta</label>
                                                          <input type="text" name="conta" id="conta" value="<?php echo $conta_bancaria_clientelista['conta']; ?>" class="form-control" /> </div>
                                                          <div class="form-group">
                                                            <label class="control-label">Titular</label>
                                                            <input type="text" name="titular" id="titular" value="<?php echo $conta_bancaria_clientelista['titular']; ?>" class="form-control" /> </div>
                                                            <div class="form-group">
                                                              <label class="control-label">CPF</label>
                                                              <input type="text" name="cpf_titular" id="cpf_titular" value="<?php echo $conta_bancaria_clientelista['cpf_titular']; ?>" class="form-control" /> </div>
                                                              <div class="margin-top-10">
                                                                <button type="submit" name="atualizar_banco" class="btn green"> Salvar Alterações </button>

                                                              </div>
                                                            </form>
                                                          </div>
                                                          <!-- END CONTA BANCARIA TAB -->
                                                          <!-- PRIVACY SETTINGS TAB -->
                                                          <div class="tab-pane" id="tab_1_4">

                                                            <!-- TAB CLIENTES INICIO -->	

                                                            <div class="portlet light ">


                                                            </div>  

                                                            <!-- TAB INTERNA CONFIGURAÇÕES FIM -->	




                                                          </div>

                                                          <!-- COMPROVANTES -->
                                                          <div class="tab-pane" id="tab_1_5">
                                                           <form action="../comprovantes/novo_comprovante" name="form_comprovantes" enctype="multipart/form-data" method="post"  role="form" >

                                                            <input type="hidden" id="id_cliente_comprovante" name="id_cliente_comprovante" value="<?php echo $clientelista['id']; ?>">

                                                            <div class="form-group">
                                                              <label class="control-label">Descrição</label>
                                                              <input type="text" name="descricao_comprovante" id="descricao_comprovante" value="" class="form-control" /> </div>


                                                              <div class="form-group">
                                                                <label class="control-label">Arquivo</label>
                                                                <input type="file" name="comprovante" id="comprovante" class="form-control" /> </div>

                                                                <div class="margin-top-10">
                                                                  <button type="submit" name="salvar_comprovante" class="btn green"> Salvar Anexo </button>                                                                                  
                                                                </div>

                                                              </form>
                                                              <br>
                                                              <br>
                                                              <hr>

                                                              <?php  include '../comprovantes/verificar_comprovantes.php';  ?>	             
                                                              <table id="tabela_comprovante" class="table table-hover">
                                                                <thead>
                                                                  <tr>
                                                                    <th> # </th>
                                                                    <th> Descrição </th>
                                                                    <th> Data </th>

                                                                    <th> Ações </th>
                                                                  </tr>
                                                                </thead>
                                                                <tbody>

                                                                 <?php while ($comprovanteslista = $comprovantes->fetch_assoc()):   ?>	

                                                                   <tr>
                                                                    <td> <?php echo $comprovanteslista['id']; ?> </td>
                                                                    <td> <?php echo $comprovanteslista['descricao']; ?> </td>
                                                                    <td> <?php echo $comprovanteslista['data']; ?> </td>

                                                                    <td>
                                                                     <a target="_blank" href="<?php echo $comprovanteslista['anexo_comprovante']; ?>" class="label label-sm label-success"> Abrir </a>
                                                                     <br>
                                                                     <form action="../comprovantes/excluir_comprovante" name="form_comprovantes" enctype="multipart/form-data" method="post"  role="form" >

                                                                      <input type="hidden" id="id_comprovante" name="id_comprovante" value="<?php echo $comprovanteslista['id']; ?>">
                                                                      <input type="hidden" id="id_cliente_comprovante" name="id_cliente_comprovante" value="<?php echo $clientelista['id']; ?>">

                                                                      <button type="submit" name="excluir_comprovante" class="label label-sm label-danger"> Excluir Anexo </button> 

                                                                    </form>





                                                                  </td>
                                                                </tr>


                                                              <?php endwhile; ?>	


                                                            </tbody>
                                                          </table>

                                                        </div>
                                                        <!-- COMPROVANTES -->


                                                        <!-- END PRIVACY SETTINGS TAB -->
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- END PROFILE CONTENT -->
                                          </div>
                                        </div>
                                      </div>
                                      <!-- END PAGE CONTENT INNER -->



                                    </div>
                                    <!-- PERFIL FIM -->








                                    <!-- RECEITAS INICIO -->

                                    <div class="tab-pane" id="tab_receitas">
                                      <!-- INICIO DATATABLE RECEITAS-->

                                      <div class="portlet light ">
                                       <br>
                                       <div class="portlet-body">
                                        <div class="table-toolbar">
                                         <div class="row">
                                          <div  class="col-md-6">
                                            <div style="display:none" class="btn-group"><a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novoreceita_medica"> Novo Receita Médica <i class="fa fa-plus"></i></a></div>
                                          </div>
                                          <div class="col-md-6">
                                           <div class="btn-group pull-right">
                                            <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas<i class="fa fa-angle-down"></i></button>
                                            <ul class="dropdown-menu pull-right">
                                             <li><a href="javascript:;"><i class="fa fa-file-excel-o"></i> Exportar Excel </a></li>
                                           </ul>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                   <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_receita_medica">
                                     <thead>
                                      <tr>

                                       <td>  Médico  </td>
                                       <td>  Data  </td>
                                       <td>  Observacao  </td>

                                       <td> Ações </td>
                                     </tr>
                                   </thead>
                                   <tbody>
                                    <?php include '../receitas_medicas/verificar_receitas.php'; ?>

                                    <?php while ($select_receita_medicalista = $select_go_receita_medica->fetch_assoc()): ?> 
                                      <tr class="odd gradeX">
                                        <td style="display:none" data-id_receita ="<?php echo $select_receita_medicalista['id']; ?>"  >  <?php echo $select_receita_medicalista['id']; ?>  </td>
                                        <td style="display:none" data-fk_id_medico ="<?php echo $select_receita_medicalista['fk_id_medico']; ?>"  >  <?php echo $select_receita_medicalista['fk_id_medico']; ?>  </td>
                                        <td  data-medico ="<?php echo $select_receita_medicalista['medico']; ?>"  >  <?php echo $select_receita_medicalista['medico']; ?>  </td>
                                        <td  data-validade ="<?php echo $select_receita_medicalista['validade']; ?>"  >  <?php echo $select_receita_medicalista['validade']; ?>  </td>
                                        <td  data-observacao ="<?php echo $select_receita_medicalista['observacao']; ?>"  >  <?php echo $select_receita_medicalista['observacao']; ?>  </td>
                                        <td style="display:none" data-valor_adicao ="<?php echo $select_receita_medicalista['valor_adicao']; ?>"  >  <?php echo $select_receita_medicalista['valor_adicao']; ?>  </td>
                                        <td style="display:none" data-valor_dnp_oe_perto ="<?php echo $select_receita_medicalista['valor_dnp_oe_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_dnp_oe_perto']; ?>  </td>
                                        <td style="display:none" data-valor_altura_oe_perto ="<?php echo $select_receita_medicalista['valor_altura_oe_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_altura_oe_perto']; ?>  </td>
                                        <td style="display:none" data-valor_eixo_oe_perto ="<?php echo $select_receita_medicalista['valor_eixo_oe_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_eixo_oe_perto']; ?>  </td>
                                        <td style="display:none" data-valor_cilindrico_oe_perto ="<?php echo $select_receita_medicalista['valor_cilindrico_oe_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_cilindrico_oe_perto']; ?>  </td>
                                        <td style="display:none" data-valor_esferico_oe_perto ="<?php echo $select_receita_medicalista['valor_esferico_oe_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_esferico_oe_perto']; ?>  </td>
                                        <td style="display:none" data-valor_dnp_od_perto ="<?php echo $select_receita_medicalista['valor_dnp_od_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_dnp_od_perto']; ?>  </td>
                                        <td style="display:none" data-valor_altura_od_perto ="<?php echo $select_receita_medicalista['valor_altura_od_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_altura_od_perto']; ?>  </td>
                                        <td style="display:none" data-valor_eixo_od_perto ="<?php echo $select_receita_medicalista['valor_eixo_od_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_eixo_od_perto']; ?>  </td>
                                        <td style="display:none" data-valor_cilindrico_od_perto ="<?php echo $select_receita_medicalista['valor_cilindrico_od_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_cilindrico_od_perto']; ?>  </td>
                                        <td style="display:none" data-valor_esferico_od_perto ="<?php echo $select_receita_medicalista['valor_esferico_od_perto']; ?>"  >  <?php echo $select_receita_medicalista['valor_esferico_od_perto']; ?>  </td>
                                        <td style="display:none" data-valor_dnp_oe_longe ="<?php echo $select_receita_medicalista['valor_dnp_oe_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_dnp_oe_longe']; ?>  </td>
                                        <td style="display:none" data-valor_altura_oe_longe ="<?php echo $select_receita_medicalista['valor_altura_oe_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_altura_oe_longe']; ?>  </td>
                                        <td style="display:none" data-valor_eixo_oe_longe ="<?php echo $select_receita_medicalista['valor_eixo_oe_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_eixo_oe_longe']; ?>  </td>
                                        <td style="display:none" data-valor_cilindrico_oe_longe ="<?php echo $select_receita_medicalista['valor_cilindrico_oe_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_cilindrico_oe_longe']; ?>  </td>
                                        <td style="display:none" data-valor_esferico_oe_longe ="<?php echo $select_receita_medicalista['valor_esferico_oe_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_esferico_oe_longe']; ?>  </td>
                                        <td style="display:none" data-valor_dnp_od_longe ="<?php echo $select_receita_medicalista['valor_dnp_od_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_dnp_od_longe']; ?>  </td>
                                        <td style="display:none" data-valor_altura_od_longe ="<?php echo $select_receita_medicalista['valor_altura_od_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_altura_od_longe']; ?>  </td>
                                        <td style="display:none" data- ="<?php echo $select_receita_medicalista['']; ?>"  >  <?php echo $select_receita_medicalista['']; ?>  </td>
                                        <td style="display:none" data-valor_eixo_od_longe ="<?php echo $select_receita_medicalista['valor_eixo_od_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_eixo_od_longe']; ?>  </td>
                                        <td style="display:none" data-valor_cilindrico_od_longe ="<?php echo $select_receita_medicalista['valor_cilindrico_od_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_cilindrico_od_longe']; ?>  </td>
                                        <td style="display:none" data-valor_esferico_od_longe ="<?php echo $select_receita_medicalista['valor_esferico_od_longe']; ?>"  >  <?php echo $select_receita_medicalista['valor_esferico_od_longe']; ?>  </td>
                                        <td style="display:none" data-fk_id_cliente ="<?php echo $select_receita_medicalista['fk_id_cliente']; ?>"  >  <?php echo $select_receita_medicalista['fk_id_cliente']; ?>  </td>
                                        <td style="display:none" data-FK_usuarios_admin_id ="<?php echo $select_receita_medicalista['FK_usuarios_admin_id']; ?>"  >  <?php echo $select_receita_medicalista['FK_usuarios_admin_id']; ?>  </td>
                                        <td style="display:none" data-fk_id_empresa ="<?php echo $select_receita_medicalista['fk_id_empresa']; ?>"  >  <?php echo $select_receita_medicalista['fk_id_empresa']; ?>  </td>
                                        <td >
                                          <div class="btn-group">
                                           <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações<i class="fa fa-angle-down"></i></button>
                                           <ul class="dropdown-menu" role="menu">
                                            <li><a class="cliqueparaeditarreceita_medica"  data-toggle="modal" href="#modal_editarreceita_medica"> <i class="icon-docs"></i> Editar </a></li>
                                            <li><a target="_blank" href="../<?php echo $select_receita_medicalista['anexo']; ?>"> <i class="icon-docs"></i> Abrir Anexo </a></li>
                                          </ul>
                                        </div>
                                      </td>
                                    </tr>
                                  <?php endwhile; ?>
                                </tbody>
                              </table>
                            </div>
                          </div>

                          <!-- FIM DATATABLE RECEITAS-->

                          <!-- INICIO EDITAR RECEITA -->

                          <div class="modal fade bs-modal-lg" id="modal_editarreceita_medica"  role="dialog" aria-hidden="true">
                           <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                             <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                              <h4 class="modal-title">Editar Receita</h4>
                            </div>
                            <div class="modal-body">
                              <form name="editar_receita_medica" id="editar_receita_medica" action="#" enctype="multipart/form-data" method="post" class="horizontal-form">
                               <div class="form-body">
                                <h3 class="form-section">   </h3>

                                <div class="panel-body">

                                  <div class="row">

                                   <input style="display:none" type="text" id="id_receita" name="id_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                   <?php  include '../medicos/verificar_medicos.php';  ?>

                                   <div class="row"> 

                                    <div class="col-md-6">
                                      <div class="form-group">

                                        <label>Médico</label>
                                        <select id="editar_medico_receita" name="editar_medico_receita" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



                                         <option value=""></option>

                                         <?php while ($select_go_cad_medicolista = $select_go_cad_medico->fetch_assoc()): ?>

                                           <option value="<?php echo $select_go_cad_medicolista['id'].'|'.$select_go_cad_medicolista['nome'] ?>"><?php echo $select_go_cad_medicolista['nome'] ?></option>

                                         <?php endwhile; ?>

                                       </select>

                                     </div>
                                   </div>



                                   <div class="col-md-3">
                                    <div class="form-group"> 
                                      <label>Data</label>   
                                      <input  type="date" value="<?php echo date("Y-m-d"); ?>" id="editar_validade_receita" name="editar_validade_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
                                    </div>
                                  </div>


                                  <div class="col-md-3">
                                    <div class="form-group"> 
                                      <label>Anexar</label>   
                                      <input  type="file" id="anexo_receita" name="anexo_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
                                    </div>
                                  </div>


                                  <br>

                                  <hr>



                                </div> 


                                <br>

                                <!--DIV PREENCHIMENTO RECEITA-->
                                <div class="m-t col-xs-12">
                                  <div class="table-responsive">
                                    <div style="min-width: 626px">
                                      <table style="width: 25%; margin-top: 35px;" class="table table-bordered table-responsive table-condensed receita-label pull-left">
                                        <tbody>
                                          <tr class="text-success">
                                            <td rowspan="2">Longe</td>
                                            <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
                                          </tr>
                                          <tr class="text-success">
                                            <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
                                          </tr>
                                          <tr class="text-danger">
                                            <td rowspan="2">Perto</td>
                                            <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
                                          </tr>
                                          <tr class="text-danger">
                                            <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
                                          </tr>
                                        </table>
                                        <table style="width: 75%" class="table table-bordered table-responsive table-condensed receita">
                                          <thead>
                                            <tr>
                                              <th>Esférico</th>
                                              <th>Cilíndrico</th>
                                              <th>Eixo</th>
                                              <th>Altura</th>
                                              <th>DNP</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr class="text-success">
                                              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="editar_valor_esferico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="editar_valor_esferico_od_longe" type="text"> </td>
                                              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="editar_valor_cilindrico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="editar_valor_cilindrico_od_longe" type="text"> </td>
                                              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="editar_valor_eixo_od_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="editar_valor_eixo_od_longe" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="editar_valor_altura_od_longe" id="editar_valor_altura_od_longe" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="editar_valor_dnp_od_longe" id="editar_valor_dnp_od_longe" type="text"></td>
                                            </tr>
                                            <tr class="text-success">
                                              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="editar_valor_esferico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="editar_valor_esferico_oe_longe" type="text"></td>
                                              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="editar_valor_cilindrico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="editar_valor_cilindrico_oe_longe" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="editar_valor_eixo_oe_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="editar_valor_eixo_oe_longe" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="editar_valor_altura_oe_longe" id="editar_valor_altura_oe_longe" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="editar_valor_dnp_oe_longe" id="editar_valor_dnp_oe_longe" type="text"></td>
                                            </tr>
                                            <tr class="text-danger">
                                              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="editar_valor_esferico_od_perto" data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off"  name="editar_valor_esferico_od_perto" type="text"></td>
                                              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="editar_valor_cilindrico_od_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="editar_valor_cilindrico_od_perto" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right"  autocomplete="off" data-m-dec="0" maxlength="12" name="editar_valor_eixo_od_perto" id="editar_valor_eixo_od_perto" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="editar_valor_altura_od_perto" id="editar_valor_altura_od_perto" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="editar_valor_dnp_od_perto" id="editar_valor_dnp_od_perto" type="text"></td>
                                            </tr>
                                            <tr class="text-danger">
                                              <td> <input class="input-sm text-right input-mask-receita-field form-control"  data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off" name="editar_valor_esferico_oe_perto" id="editar_valor_esferico_oe_perto" type="text"></td>
                                              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="editar_valor_cilindrico_oe_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="editar_valor_cilindrico_oe_perto" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="editar_valor_eixo_oe_perto" autocomplete="off" data-m-dec="0" maxlength="12" name="editar_valor_eixo_oe_perto" id="editar_valor_eixo_oe_perto" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="editar_valor_altura_oe_perto" id="editar_valor_altura_oe_perto" type="text"></td>
                                              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="editar_valor_dnp_oe_perto" id="editar_valor_dnp_oe_perto" type="text"></td>

                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="form-group full-width m-b-none text-left "><label for="nova_receita[adicao]" class="control-label">Adição</label><input class="input-sm form-control numeric-field text-right" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="2" maxlength="12" name="editar_valor_adicao" id="editar_valor_adicao" type="text"></div>
                                              </td>
                                              <td colspan="4"></td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                  <!--DIV PREENCHIMENTO RECEITA-->


                                  <div class="row"> 

                                    <div class="col-md-12">
                                      <div class="form-group">

                                        <label>Observação</label>
                                        <textarea rows="4" cols="50" id="editar_obs_receita" name="editar_obs_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                        </textarea>

                                      </div>
                                    </div>







                                  </div> 

                                </div>

                              </div>
                            </div>
                          </div>
                          <div class="modal-footer"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                            <button style="display:none" type="submit" name="salvar_receita_medica" class="btn green">Salvar</button>
                            <button style="display:none" type="submit" name="excluir_receita_medica" class="btn red">Excluir</button></div></form>
                          </div>
                        </div>
                      </div>



                      <!-- FIM EDITAR RECEITA -->



                    </div>


                    <!-- NOVA RECEIT MEDICA INICIO -->


                    <div class="modal fade bs-modal-lg" id="modal_novoreceita_medica"  role="dialog" aria-hidden="true">
                     <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                       <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Novo</h4>
                      </div>
                      <div class="modal-body">
                        <form name="add_receita_medica" id="add_receita_medica" action="#" enctype="multipart/form-data" method="post" class="horizontal-form">
                         <div class="panel-body">

                          <div class="row">


                           <?php  include '../medicos/verificar_medicos.php';  ?>

                           <div class="row"> 

                            <div class="col-md-6">
                              <div class="form-group">

                                <label>Médico</label>
                                <select id="medico_receita" name="medico_receita" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



                                 <option value=""></option>

                                 <?php while ($select_go_cad_medicolista = $select_go_cad_medico->fetch_assoc()): ?>

                                   <option value="<?php echo $select_go_cad_medicolista['id'].'|'.$select_go_cad_medicolista['nome'] ?>"><?php echo $select_go_cad_medicolista['nome'] ?></option>

                                 <?php endwhile; ?>

                               </select>

                             </div>
                           </div>



                           <div class="col-md-3">
                            <div class="form-group"> 
                              <label>Data</label>   
                              <input  type="date" value="<?php echo date("Y-m-d"); ?>" id="validade_receita" name="validade_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
                            </div>
                          </div>


                          <div class="col-md-3">
                            <div class="form-group"> 
                              <label>Anexar</label>   
                              <input  type="file" id="anexo_receita" name="anexo_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
                            </div>
                          </div>


                          <br>

                          <hr>



                        </div> 


                        <br>

                        <!--DIV PREENCHIMENTO RECEITA-->
                        <div class="m-t col-xs-12">
                          <div class="table-responsive">
                            <div style="min-width: 626px">
                              <table style="width: 25%; margin-top: 35px;" class="table table-bordered table-responsive table-condensed receita-label pull-left">
                                <tbody>
                                  <tr class="text-success">
                                    <td rowspan="2">Longe</td>
                                    <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
                                  </tr>
                                  <tr class="text-success">
                                    <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
                                  </tr>
                                  <tr class="text-danger">
                                    <td rowspan="2">Perto</td>
                                    <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
                                  </tr>
                                  <tr class="text-danger">
                                    <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
                                  </tr>
                                </table>
                                <table style="width: 75%" class="table table-bordered table-responsive table-condensed receita">
                                  <thead>
                                    <tr>
                                      <th>Esférico</th>
                                      <th>Cilíndrico</th>
                                      <th>Eixo</th>
                                      <th>Altura</th>
                                      <th>DNP</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr class="text-success">
                                      <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_od_longe" type="text"> </td>
                                      <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_od_longe" type="text"> </td>
                                      <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_od_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_od_longe" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_od_longe" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_od_longe" type="text"></td>
                                    </tr>
                                    <tr class="text-success">
                                      <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_oe_longe" type="text"></td>
                                      <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_oe_longe" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_oe_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_oe_longe" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_oe_longe" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_oe_longe" type="text"></td>
                                    </tr>
                                    <tr class="text-danger">
                                      <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_od_perto" data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_od_perto" type="text"></td>
                                      <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_od_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_od_perto" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_od_perto" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_od_perto" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_od_perto" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_od_perto" type="text"></td>
                                    </tr>
                                    <tr class="text-danger">
                                      <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_oe_perto" data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_oe_perto" type="text"></td>
                                      <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_oe_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_oe_perto" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_oe_perto" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_oe_perto" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_oe_perto" type="text"></td>
                                      <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_oe_perto" type="text"></td>

                                    </tr>
                                    <tr>
                                      <td>
                                        <div class="form-group full-width m-b-none text-left "><label for="nova_receita[adicao]" class="control-label">Adição</label><input class="input-sm form-control numeric-field text-right" id="valor_adicao" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="2" maxlength="12" name="valor_adicao" type="text"></div>
                                      </td>
                                      <td colspan="4"></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                          <!--DIV PREENCHIMENTO RECEITA-->


                          <div class="row"> 

                            <div class="col-md-12">
                              <div class="form-group">

                                <label>Observação</label>
                                <textarea rows="4" cols="50" id="obs_receita" name="obs_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </textarea>

                              </div>
                            </div>







                          </div> 

                        </div>

                      </div>
                    </div>
                    <div class="modal-footer"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                      <button type="submit" name="adicionar_receita_medica" class="btn green">Adicionar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>


            <!-- NOVA RECEITA MEDICA FIM -->






            <!-- RECEITAS FIM -->





            <!-- SERVICOS INICIO -->										

            <div style="overflow:visible" class="tab-pane" id="tab_servicos">

             <!-- INICIO DATATABLE SERVICOS-->

             <div class="portlet light ">
               <br>
               <div class="portlet-body">
                <div class="table-toolbar">
                 <div class="row">
                  <div class="col-md-6">
                    <div style="display:none" class="btn-group"><a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novoordem_servico"> Nova O.S <i class="fa fa-plus"></i></a></div>
                  </div>
                  <div class="col-md-6">
                   <div class="btn-group pull-right">
                    <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas<i class="fa fa-angle-down"></i></button>
                    <ul class="dropdown-menu pull-right">
                     <li><a href="javascript:;"><i class="fa fa-file-excel-o"></i> Exportar Excel </a></li>
                   </ul>
                 </div>
               </div>
             </div>
           </div>
           <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_ordem_servico">
             <thead>
              <tr>
               <td>  OS  </td>                                
               <td>  Data Gerado  </td>
               <td>  Entrega  </td>
               <td>  Cliente  </td>

               <td>  Vendedor  </td>
               <td>  Obs  </td>
               <td>  Pago  </td>
               <td>  A Pagar  </td>
               <td>  Status Cliente  </td>
               <td>  Status Loja  </td>

               <td> Ações </td>
             </tr>
           </thead>
           <tbody>
            <?php include '../ordem_servico/verificar_lista_os_cliente.php' ; ?>

            <?php while ($select_ordem_servicolista = $select_go_ordem_servico->fetch_assoc()): ?> 
              <tr class="odd gradeX">
               <td data-id ="<?php echo $select_ordem_servicolista['id']; ?>"  >  <?php echo $select_ordem_servicolista['id']; ?>  </td>
               <td  style="display:none" data-idadmin ="<?php echo $select_ordem_servicolista['FK_usuarios_admin_id']; ?>"  >  <?php echo $select_ordem_servicolista['FK_usuarios_admin_id']; ?>  </td>
               <td style="display:none" data-id_loja ="<?php echo $select_ordem_servicolista['id_loja']; ?>"  >  <?php echo $select_ordem_servicolista['id_loja']; ?>  </td>
               <td data-data_gerado ="<?php echo $select_ordem_servicolista['data_gerado']; ?>"  >  <?php echo $select_ordem_servicolista['data_gerado']; ?>  </td>
               <td data-data_combinado_entrega ="<?php echo $select_ordem_servicolista['data_combinado_entrega']; ?>"  >  <?php echo $select_ordem_servicolista['data_combinado_entrega']; ?>  </td>
               <td data-cliente ="<?php echo $select_ordem_servicolista['cliente']; ?>"  >  <?php echo $select_ordem_servicolista['cliente']; ?>  </td>
               <td style="display:none" data-fk_id_cliente ="<?php echo $select_ordem_servicolista['fk_id_cliente']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_cliente']; ?>  </td>
               <td style="display:none" data-nr_os ="<?php echo $select_ordem_servicolista['nr_os']; ?>"  >  <?php echo $select_ordem_servicolista['nr_os']; ?>  </td>
               <td data-vendedor ="<?php echo $select_ordem_servicolista['vendedor']; ?>"  >  <?php echo $select_ordem_servicolista['vendedor']; ?>  </td>
               <td style="display:none" data-fk_id_vendedor ="<?php echo $select_ordem_servicolista['fk_id_vendedor']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_vendedor']; ?>  </td>
               <td data-observacao ="<?php echo $select_ordem_servicolista['observacao']; ?>"  >  <?php echo $select_ordem_servicolista['observacao']; ?>  </td>
               <td data-pago ="<?php echo $select_ordem_servicolista['pago']; ?>"  >  <?php echo $select_ordem_servicolista['pago']; ?>  </td>
               <td data-a_pagar ="<?php echo $select_ordem_servicolista['a_pagar']; ?>"  >  <?php echo $select_ordem_servicolista['a_pagar']; ?>  </td>
               <td data-status_cliente ="<?php echo $select_ordem_servicolista['status_cliente']; ?>"  >  <?php echo $select_ordem_servicolista['status_cliente']; ?>  </td>
               <td data-status_loja ="<?php echo $select_ordem_servicolista['status_loja']; ?>"  >  <?php echo $select_ordem_servicolista['status_loja']; ?>  </td>
               <td style="display:none" data-fk_id_relacionamento_indicacao_id ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_indicacao_id']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_indicacao_id']; ?>  </td>
               <td style="display:none" data-fk_id_relacionamento_anexopagamentos ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_anexopagamentos']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_anexopagamentos']; ?>  </td>
               <td style="display:none" data-fk_id_relacionamento_pagamentos ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_pagamentos']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_pagamentos']; ?>  </td>
               <td style="display:none" data-fk_id_relacionamento_tipo_os ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_tipo_os']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_tipo_os']; ?>  </td>
               <td style="display:none" data-fk_id_relacionamento_pedido_laboratorio ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_pedido_laboratorio']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_pedido_laboratorio']; ?>  </td>
               <td style="display:none" data-fk_id_relacionamento_produto ="<?php echo $select_ordem_servicolista['fk_id_relacionamento_produto']; ?>"  >  <?php echo $select_ordem_servicolista['fk_id_relacionamento_produto']; ?>  </td>
               <td >
                <div class="btn-group">
                 <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações<i class="fa fa-angle-down"></i></button>
                 <ul class="dropdown-menu" role="menu">
                  <li><a class="cliqueparaeditarordem_servico"   data-toggle="modal" href="#modal_editarordem_servico"> <i class="icon-docs"></i> Ver O.S </a></li>
                  <li><a  target="_blank" href="<?php echo '../docs_empresas/'.$select_ordem_servicolista['FK_usuarios_admin_id'].'/anexos/os/'.$select_ordem_servicolista['id'].'.pdf'; ?>"> <i class="icon-docs"></i> Abrir PDF OS </a></li>
                </ul>
              </div>
            </td>
          </tr>
        <?php endwhile; ?>
      </tbody>
    </table>
  </div>
</div>

<!-- FIM DATATABLE RENDIMENTOS-->
</div>






<!-- Modal Nova Os Inicio-->
<div class="modal fade bs-modal-lg" id="modal_novoordem_servico"  role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-lg">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Nova O.S</h4>
  </div>
  <div class="modal-body">
   <div class="panel-body">
    <form name="add_ordem_servico" id="add_ordem_servico" action="../ordem_servico/nova_os.php" enctype="multipart/form-data" method="post" class="horizontal-form">
     <div class="form-body">
      <h3 class="form-section">   </h3>
      <div class="row">



        <div class="panel-body">


          <div class="form-group">

            <label>Tipo O.S</label>
            <select required id="tipo_os" name="tipo_os" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 

             <option value="1|Óptica">Óptica</option>
             <option value="2|Assistência">Assistência</option>
             <option value="3|Médico">Médico</option>


           </select>
         </div>


       </div>

       <!--/DADOS ORDEM DE SERVIÇO-->         
       <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#dadosos"> + Dados O.S </a>
          </h4>
        </div>

        <div id="dadosos" class="panel-collapse collapse in">
          <div class="panel-body">

           <input style="display:none" value="<?php echo $_SESSION['id_empresa'] ?>"  type="text" id="id_loja" name="id_loja" class="form-control" onkeypress="return handleEnter(this, event)">    

           <input style="display:none" value="<?php echo $clientelista['id'].'|'.$clientelista['nome'] ?>" type="text" id="cliente" name="cliente" class="form-control" onkeypress="return handleEnter(this, event)">    

          

           <div style="display:none" class="col-md-6">
            <div class="form-group"> <label class="control-label">NR OS</label>   <input  type="text" id="nr_os" name="nr_os" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    </div>
          </div>


          <div style="display:none" class="col-md-6">
            <div class="form-group"> <label class="control-label">Data Gerado</label>   <input value="<?php echo date("Y-m-d"); ?>"  type="date" id="data_gerado" name="data_gerado" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    </div>
          </div>
          <div  class="col-md-6">
            <div class="form-group"> <label class="control-label">Data Entrega</label>   <input  value="<?php echo date("Y-m-d"); ?>" type="date" id="data_combinado_entrega" name="data_combinado_entrega" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    </div>
          </div>
          <div  class="col-md-6">
            <div class="form-group"> <label class="control-label">Hora Entrega</label>   

              <select name="hora_entrega" id="hora_entrega"  type="text" class="form-control" data-inputmask="'mask': '99:99'" onkeypress="return handleEnter(this, event)">
                <option value="07:00">07:00</option>  
                <option value="07:30">07:30</option>  
                <option value="08:00">08:00</option>  
                <option value="08:30">08:30</option>  
                <option value="09:00">09:00</option>  
                <option value="09:30">09:30</option>  
                <option value="10:00">10:00</option>  
                <option value="10:30">10:30</option>  
                <option value="11:00">11:00</option>  
                <option value="11:30">11:30</option>  
                <option value="12:00">12:00</option>  
                <option value="12:30">12:30</option>  
                <option value="13:00">13:00</option>  
                <option value="13:30">13:30</option>  
                <option value="14:00">14:00</option>  
                <option value="14:30">14:30</option>  
                <option value="15:00">15:00</option>  
                <option value="15:30">15:30</option>  
                <option value="16:00">16:00</option>  
                <option value="16:30">16:30</option>  
                <option value="17:00">17:00</option>  
                <option value="17:30">17:30</option>  
                <option value="18:00">18:00</option>  
                <option value="18:30">18:30</option>  
                <option value="19:00">19:00</option>  
                <option value="19:30">19:30</option>  
                <option value="20:00">20:00</option>  
                <option value="20:30">20:30</option>  

              </select> 

            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group"> <label class="control-label">Obs</label>   <input  type="text" id="observacao" name="observacao" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
             <label class="control-label">Status Cliente</label>   

             <select  type="text" id="status_cliente" name="status_cliente" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">  

               <option value=""></option>
               <option value="Avisar Cliente">Avisar Cliente</option>
               <option value="Aguardando Buscar">Aguardando Buscar</option>
               <option value="Entregue">Entregue</option>
               <option value="Aguardando Receita">Aguardando Receita</option>
               <option value="Aguardando Armação">Aguardando Armação</option>

             </select>  

           </div>
         </div>
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label">Status Loja</label>   

           <select  type="text" id="status_loja" name="status_loja" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">  

             <option value=""></option>
             <option value="Montagem">Montagem</option>
             <option value="Pronto">Pronto</option>
             <option value="Entregue">Entregue</option>
             <option value="Laboratório">Laboratório</option>
             <option value="Aguardando Lente">Aguardando Lente</option>
             <option value="Em Fabricação">Em Fabricação</option>
             <option value="Pedir Laboratório">Pedir Laboratório</option>


           </select>  

         </div>
       </div>



     </div>
   </div>
 </div>                                                                                         
 <!--/DADOS ORDEM DE SERVIÇO--> 

 <!--/PRODUTOS-->         
 <div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#add_produtos"> + Produtos e Serviços </a>
    </h4>
  </div>
  <div id="add_produtos" class="panel-collapse collapse">
    <div class="panel-body">

      <?php   include '../produtos/verificar_produtos.php';  ?>

      <div class="row"> 

       <!-- Novo Medico-->
       <div class="form-group">  


        <a  role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapsenovoproduto" aria-expanded="false" aria-controls="collapseOne">
          <p class="col-lg-12 text-left">+ Novo</p>
        </a>
        <div id="collapsenovoproduto" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">

           <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Descrição</label>
                <input  type="text"  id="descricao_novo_produto" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label">Valor R$</label>
                <input  type="text" id="valor_novo_produto" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

              </div>
            </div>  

            <div class="col-md-3">
              <div class="form-group">
                <label class="control-label">Quantidade</label>
                <input  type="text"  id="quant_novo_produto"  class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label class="control-label">Cód. Peça</label>
                <input  type="text"  id="cod_peca_novo_produto"  class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

              </div>
            </div>

          </div> 

          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <a  id="cadastrar_produto" class="btn yellow btn-sm"> <i class="fa fa-plus" aria-hidden="true"></i> Cadastrar  </a>
              </div>                  
            </div>
          </div>             

        </div>


      </div>


    </div>
    <!-- Novo Medico-->



    <div class="col-md-6">
      <div class="form-group">

        <label>Produto</label>
        <select id="produto" name="produto" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



         <option value=""></option>

         <?php while ($select_prodslista = $select_prods->fetch_assoc()): ?>

           <option value="<?php echo $select_prodslista['id'].'|'.$select_prodslista['desc_prod'].'|'.$select_prodslista['valor_venda'] ?>"><?php echo 'Desc.:'.$select_prodslista['desc_prod'].' / Cod. Peça:'.$select_prodslista['codigo_peca'] ?></option>

         <?php endwhile; ?>

       </select>

     </div>
   </div>



   <div class="col-md-3">
    <div class="form-group"> 
      <label>Quantidade</label>   
      <input value="1"  type="number" id="quant_produto" name="quant_produto" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
    </div>
  </div>

  <br>

</div> 

<div class="row"> 
  <div class="col-md-3">
   <div class="form-group">
     <a onclick="AddTableRowProdutoOs()" name="adicionar_produto" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
   </div>                  
 </div>
</div>


<br>
<div class="row">     

  <div class="col-md-12">
   <!-- iniciando tabela area-->
   <div class="portlet box blue">
     <div class="portlet-title">
       <div class="caption">
         <i class="fa fa-product-hunt"></i>Produtos/Serviços </div>
         <div class="tools">
           <a href="javascript:;" class="collapse"> </a>

           <a href="javascript:;" class="reload"> </a>

         </div>
       </div>
       <div class="portlet-body">
         <div class="table-scrollable">
           <table id="tabela_produtosos" class="table table-striped table-hover">
             <thead>
               <tr>
                 <th> # </th>
                 <th> Desc. </th>
                 <th> Quant. </th>
                 <th> Valor Unit. </th>
                 <th> Total</th>                                   
                 <th>Ação</th>

               </tr>
             </thead>
             <tbody>

               <tr>

               </tr>


             </tbody>
           </table>
         </div>
       </div>
     </div>
     <!-- fim tabela area-->

     <div class="row">
      <div class="col-md-3">
        <div class="form-group">

          <div id="div_total_produtos">



          </div>

        </div>
      </div>
    </div>    


  </div>

</div>   

</div>
</div>
</div>                                                                                         
<!--/PRODUTOS-->

<!--/PAGAMENTO-->         
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#add_pagamento"> + Pagamento </a>
    </h4>
  </div>
  <div id="add_pagamento" class="panel-collapse collapse">
    <div class="panel-body">

      <?php   //include '';  ?>

      <div class="row"> 

        <div class="col-md-6">
          <div class="form-group">

            <label>Forma Pagamento</label>
            <select id="forma_pagamento" name="forma_pagamento" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



             <option value="Cartão de Crédito">Cartão de Crédito</option>
             <option value="Cartão de Débito">Cartão de Débito</option>
             <option value="Dinheiro">Dinheiro</option>
             <option value="Cheque">Cheque</option>
             <option value="Transferência Bancária">Transferência Bancária</option>


             <?php //while ($select_prodslista = $select_prods->fetch_assoc()): ?>

             <option value="<?php //echo $select_prodslista['id'].'|'.$select_prodslista['desc_prod'].'|'.$select_prodslista['valor_venda'] ?>"><?php //echo $select_prodslista['desc_prod'] ?></option>

             <?php //endwhile; ?>

           </select>

         </div>
       </div>



       <div class="col-md-3">
        <div class="form-group"> 
          <label>Valor R$</label>   
          <input  type="text" id="valor_pagamento" name="valor_pagamento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group"> 
          <label>Parcelas</label>   
          <select name="parcela_pagamento" id="parcela_pagamento"  type="text" class="form-control" onkeypress="return handleEnter(this, event)">
            <option value="1">1x</option>  
            <option value="2">2x</option>  
            <option value="3">3x</option>  
            <option value="4">4x</option>  
            <option value="5">5x</option>  
            <option value="6">6x</option>  
            <option value="7">7x</option>  
            <option value="8">8x</option>  
            <option value="9">9x</option>
            <option value="10">10x</option> 
            <option value="11">11x</option>
            <option value="12">12x</option>       
            

          </select>   
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group"> 
          <label>Entra no Caixa</label>   
          <input value="<?php echo date("Y-m-d"); ?>" type="date" id="data_caixa" name="data_caixa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">

          <?php include '../configuracoes/conta/verificar_conta.php' ?>

          <label>Conta</label>
          <select id="conta_pagamento" name="conta_pagamento" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



           <option value=""></option>
           


           <?php while ($conta_principallista = $conta_principal->fetch_assoc()): ?>

             <option value="<?php echo $conta_principallista['id'].'|'.$conta_principallista['descricao'] ?>"><?php echo $conta_principallista['descricao'] ?></option>

           <?php endwhile; ?>

         </select>

       </div>
     </div>

     <div class="col-md-4">
      <div class="form-group">

        <?php include '../movimentacoes/verificar_categorias.php' ?>

        <label>Categoria</label>
        <select id="categoria_pagamento" name="categoria_pagamento" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



         <option value=""></option>
         


         <?php echo $categoriadespesa;  ?>

       </select>

     </div>
   </div>

   <br>

 </div> 

 <div class="row"> 
  <div class="col-md-3">
   <div class="form-group">
     <a onclick="AddTableRowPagamentoOs()" name="adicionar_pagamento" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
   </div>                  
 </div>
</div>


<br>
<div class="row">     

  <div class="col-md-12">
   <!-- iniciando tabela area-->
   <div class="portlet box blue">
     <div class="portlet-title">
       <div class="caption">
         <i class="fa fa-product-hunt"></i>Pagamento </div>
         <div class="tools">
           <a href="javascript:;" class="collapse"> </a>

           <a href="javascript:;" class="reload"> </a>

         </div>
       </div>
       <div class="portlet-body">
         <div class="table-scrollable">
           <table id="tabela_pagamentosos" class="table table-striped table-hover">
             <thead>
               <tr>
                 <th> # </th>
                 <th> Forma Pag. </th>
                 <th> Conta </th>
                 <th> Cat. </th>
                 <th> Parc. </th>
                 <th> Valor. </th>
                 <th> Data</th>                                   
                 <th> Ação</th>

               </tr>
             </thead>
             <tbody>

               <tr>

               </tr>


             </tbody>
           </table>
         </div>
       </div>
     </div>
     <!-- fim tabela area-->

     <div class="row">
      <div class="col-md-3">
        <div class="form-group">

          <div id="div_total_pagamentos">



          </div>

        </div>
      </div>
    </div>    


  </div>

</div>   

</div>
</div>
</div>                                                                                         
<!--/PAGAMENTO--> 









<!--/RECEITA-->         
<div id="div_receita" class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#receita_medica"> + Receita </a>
    </h4>
  </div>
  <div id="receita_medica" class="panel-collapse collapse">
    <div class="panel-body">


     <?php  include '../medicos/verificar_medicos.php';  ?>

     <div class="row"> 

       <!-- Novo Medico-->
       <div class="form-group">  


        <a  role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapsenovomedico" aria-expanded="false" aria-controls="collapseOne">
          <p class="col-lg-12 text-left">+ Cadastrar Médico</p>
        </a>
        <div id="collapsenovomedico" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
           <div class="row">   
            <div class="col-md-12">
              <div class="form-group">
               <input type="text" id="nome_medico_novo" class="form-control form-block" placeholder="Nome Médico" value="">
             </div>
           </div>
         </div>
         <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <a  id="cadastrar_medico" class="btn yellow btn-sm"> <i class="fa fa-plus" aria-hidden="true"></i> Cadastrar  </a>
            </div>                  
          </div>
        </div>                             



      </div>
    </div>


  </div>
  <!-- Novo Medico-->

  <div class="col-md-6">
    <div class="form-group">

      <label>Médico</label>
      <select id="medico_receita_os" name="medico_receita" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



       <option value=""></option>

       <?php while ($select_go_cad_medicolista = $select_go_cad_medico->fetch_assoc()): ?>

         <option value="<?php echo $select_go_cad_medicolista['id'].'|'.$select_go_cad_medicolista['nome'] ?>"><?php echo $select_go_cad_medicolista['nome'] ?></option>

       <?php endwhile; ?>

     </select>

   </div>
 </div>



 <div class="col-md-3">
  <div class="form-group"> 
    <label>Data</label>   
    <input  type="date" value="<?php echo date("Y-m-d"); ?>" id="validade_receita" name="validade_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
  </div>
</div>


<div class="col-md-3">
  <div class="form-group"> 
    <label>Anexar</label>   
    <input  type="file" id="anexo_receita" name="anexo_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
  </div>
</div>


<br>

<hr>

<div class="col-md-6">
  <div class="form-group">




    <label>Usar Receita Anexada do Histórico</label>
    <select id="receitas_anexadas" name="receitas_anexadas" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



     <option value=""></option>

     <?php include '../receitas_medicas/verificar_receitas.php'; ?>

     <?php while ($select_receita_medicalista = $select_go_receita_medica->fetch_assoc()): ?> 

       <option value="<?php echo $select_receita_medicalista['fk_id_medico'].'|'.$select_receita_medicalista['medico'].'|'.$select_receita_medicalista['validade'].'|'.$select_receita_medicalista['valor_adicao'].'|'.$select_receita_medicalista['valor_dnp_oe_perto'].'|'.$select_receita_medicalista['valor_altura_oe_perto'].'|'.$select_receita_medicalista['valor_eixo_oe_perto'].'|'.$select_receita_medicalista['valor_cilindrico_oe_perto'].'|'.$select_receita_medicalista['valor_esferico_oe_perto'].'|'.$select_receita_medicalista['valor_dnp_od_perto'].'|'.$select_receita_medicalista['valor_altura_od_perto'].'|'.$select_receita_medicalista['valor_eixo_od_perto'].'|'.$select_receita_medicalista['valor_cilindrico_od_perto'].'|'.$select_receita_medicalista['valor_esferico_od_perto'].'|'.$select_receita_medicalista['valor_dnp_oe_longe'].'|'.$select_receita_medicalista['valor_altura_oe_longe'].'|'.$select_receita_medicalista['valor_eixo_oe_longe'].'|'.$select_receita_medicalista['valor_cilindrico_oe_longe'].'|'.$select_receita_medicalista['valor_esferico_oe_longe'].'|'.$select_receita_medicalista['valor_dnp_od_longe'].'|'.$select_receita_medicalista['valor_altura_od_longe'].'|'.$select_receita_medicalista['valor_eixo_od_longe'].'|'.$select_receita_medicalista['valor_cilindrico_od_longe'].'|'.$select_receita_medicalista['valor_esferico_od_longe'].'|'.$select_receita_medicalista['id'].'|'.$select_receita_medicalista['anexo'] ?>"> ID: <?php echo $select_receita_medicalista['id'] ?> Médico: <?php echo $select_receita_medicalista['medico'] ?> Validade: <?php echo $select_receita_medicalista['validade'] ?></option>

     <?php endwhile; ?>

   </select>

 </div>
</div>

</div> 


<br>

<!--DIV PREENCHIMENTO RECEITA-->
<div class="m-t col-xs-12">
  <div class="table-responsive">
    <div style="min-width: 626px">
      <table style="width: 25%; margin-top: 35px;" class="table table-bordered table-responsive table-condensed receita-label pull-left">
        <tbody>
          <tr  style=" border-color:#2ecc71;border-style:solid ;" class="text-success">
            <td rowspan="2">Longe</td>
            <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
          </tr>
          <tr style=" border-color:#2ecc71;border-style:solid ;" class="text-success">
            <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
          </tr>
          <tr style=" border-color:#e74c3c;border-style:solid ;" class="text-danger">
            <td rowspan="2">Perto</td>
            <td><i class="fa fa-fw fa-eye fa-lg"></i> OD</td>
          </tr>
          <tr style=" border-color:#e74c3c;border-style:solid ;" class="text-danger">
            <td><i class="fa fa-fw fa-eye fa-lg"></i> OE</td>
          </tr>
        </table>
        <table style="width: 75%" class="table table-bordered table-responsive table-condensed receita">
          <thead>
            <tr>
              <th>Esférico</th>
              <th>Cilíndrico</th>
              <th>Eixo</th>
              <th>Altura</th>
              <th>DNP</th>
            </tr>
          </thead>
          <tbody>
            <tr style=" border-color:#2ecc71;border-style:solid ;" class="text-success">
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_od_longe" type="text"> </td>
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_od_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_od_longe" type="text"> </td>
              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_od_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_od_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_od_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_od_longe" id="valor_dnp_od_longe" type="text"></td>
            </tr>
            <tr style=" border-color:#2ecc71;border-style:solid ;" class="text-success">
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_oe_longe" type="text"></td>
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_oe_longe" data-bind-keyup="editGrauV2|processaAdicaoV2" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_oe_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_oe_longe" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_oe_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_oe_longe" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_oe_longe" id="valor_dnp_oe_longe" type="text"></td>
            </tr>
            <tr style=" border-color:#e74c3c;border-style:solid ;" class="text-danger">
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_od_perto" data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_od_perto" type="text"></td>
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_od_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_od_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_od_perto" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_od_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_od_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_od_perto" id="valor_dnp_od_perto" type="text"></td>
            </tr>
            <tr style=" border-color:#e74c3c;border-style:solid ;" class="text-danger">
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-esferico_oe_perto" data-bind-change="limpaAdicao" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_esferico_oe_perto" type="text"></td>
              <td> <input class="input-sm text-right input-mask-receita-field form-control" id="valor-cilindrico_oe_perto" data-bind-blur="formataValorReceita" autocomplete="off" name="valor_cilindrico_oe_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" &deg;" class="input-sm form-control numeric-field text-right" id="valor-eixo_oe_perto" autocomplete="off" data-m-dec="0" maxlength="12" name="valor_eixo_oe_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" class="input-sm form-control numeric-field text-right" maxlength="7" autocomplete="off" data-m-dec="2" name="valor_altura_oe_perto" type="text"></td>
              <td> <input data-p-sign="s" data-a-sign=" mm" maxlength="7" class="input-sm form-control numeric-field text-right" autocomplete="off" data-m-dec="2" name="valor_dnp_oe_perto" id="valor_dnp_oe_perto" type="text"></td>

            </tr>
            <tr>
              <td>
                <div class="form-group full-width m-b-none text-left "><label for="nova_receita[adicao]" class="control-label">Adição</label><input class="input-sm form-control numeric-field text-right" id="valor_adicao" data-bind-keyup="processaAdicaoV2" autocomplete="off" data-m-dec="2" maxlength="12" name="valor_adicao" type="text"></div>
              </td>
              <td colspan="4"></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!--DIV PREENCHIMENTO RECEITA-->


  <div class="row"> 

    <div class="col-md-12">
      <div class="form-group">

        <label>Observação</label>
        <textarea rows="4" cols="50" id="obs_receita" name="obs_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

        </textarea>

      </div>
    </div>







  </div> 



</div>
</div>
</div>                                                                                         
<!--/RECEITA--> 



<!--/PEDIDO LABORATORIO-->         
<div id="div_pedido_lab" class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#pedidolab"> + Pedido Laboratório </a>
    </h4>
  </div>
  <div id="pedidolab" class="panel-collapse collapse">
    <div class="panel-body">

      <div class="row"> 

        <div class="col-md-6">
          <div class="form-group">

            <?php   include '../laboratorios/verificar_laboratorios.php';  ?> 

            <label>Laboratório</label>
            <select id="laboratorio" name="laboratorio" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 



             <option value=""></option>

             <?php while ($select_go_cad_laboratorioslista = $select_go_cad_laboratorios->fetch_assoc()): ?>

               <option value="<?php echo $select_go_cad_laboratorioslista['id'].'|'.$select_go_cad_laboratorioslista['nome'] ?>"><?php echo $select_go_cad_laboratorioslista['nome'] ?></option>

             <?php endwhile; ?>

           </select>

         </div>
       </div>



       <div class="col-md-3">
        <div class="form-group"> 
          <label>Status Laboratório</label>   
          <select  type="text" id="status_lab" name="status_lab" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">  

           <option value=""></option>
           <option value="Enviar">Enviar</option>
           <option value="Em Fabricação">Em Fabricação</option>
           <option value="Aguardando Lente">Aguardando Lente</option>
           <option value="Entregue Loja">Entregue Loja</option>


         </select>    
       </div>
     </div>


     <div class="col-md-3">
      <div class="form-group"> 
        <label>Data Entrega</label>   
        <input  type="date" id="data_entrega" value="<?php echo date("Y-m-d"); ?>" name="data_entrega" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
      </div>
    </div>

    <br>

  </div>

  <div class="row"> 

    <div class="col-md-12">
      <div class="form-group">

        <label>Observação</label>
        <textarea rows="4" cols="50" id="obs_laboratorio" name="obs_laboratorio" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

        </textarea>

      </div>
    </div>

  </div> 


  <div class="row"> 
    <div class="col-md-3">
     <div class="form-group">
       <a onclick="AddTableRowLaboratorioOs()" name="adicionar_laboratorio" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
     </div>                  
   </div>
 </div>


 <br>
 <div class="row">     

  <div class="col-md-12">
   <!-- iniciando tabela area-->
   <div class="portlet box blue">
     <div class="portlet-title">
       <div class="caption">
         <i class="fa fa-product-hunt"></i>Pedidos </div>
         <div class="tools">
           <a href="javascript:;" class="collapse"> </a>

           <a href="javascript:;" class="reload"> </a>

         </div>
       </div>
       <div class="portlet-body">
         <div class="table-scrollable">
           <table id="tabela_laboratorio" class="table table-striped table-hover">
             <thead>
               <tr>
                 <th> ID Lab. </th>
                 <th> Laboratório </th>
                 <th> Status </th>
                 <th> Entrega. </th>
                 <th> Obs</th>                                   
                 <th> Ação</th>

               </tr>
             </thead>
             <tbody>

               <tr>

               </tr>


             </tbody>
           </table>
         </div>
       </div>
     </div>
     <!-- fim tabela area-->

     <div class="row">
      <div class="col-md-3">
        <div class="form-group">

          <div id="div_total_laboratorios">



          </div>

        </div>
      </div>
    </div>    


  </div>

</div>


</div>
</div>
</div>                                                                                         
<!--/PEDIDO LABORATORIO--> 

<!--/Assistência-->         
<div id="div_assistencia" style="display:none" class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#assistencia"> + Assistência </a>
    </h4>
  </div>

  <div id="assistencia" class="panel-collapse collapse">
    <div class="panel-body">


      <div class="col-md-12">
        <div class="form-group"> <label class="control-label">Identificação</label>   
          <input  type="text" id="identificacao_assistencia" name="identificacao_assistencia" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">    
        </div>
      </div>

      <div class="col-md-12">
        <div class="form-group"> <label class="control-label">Observação</label>   

         <textarea type="text" id="obs_assistencia" name="obs_assistencia" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)" rows="4" cols="50">

         </textarea>   

       </div>
     </div>

     <div class="col-md-12">
      <div class="form-group"> <label class="control-label">Defeito Relatado</label> 
        <textarea type="text" id="defeito_assistencia" name="defeito_assistencia" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)" rows="4" cols="50">

        </textarea>  
      </div>
    </div>

    <div class="col-md-12">
      <div class="form-group"> <label class="control-label">Serviço a ser Realizado</label>   
       <textarea type="text" id="servico_assistencia" name="servico_assistencia" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)" rows="4" cols="50">

       </textarea>     
     </div>
   </div>
   
   
   



 </div>
</div>
</div>                                                                                         
<!--/Assistência--> 

<!--/loja vendedor-->         
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#relac_cadcliente"> + Vendedor </a>
    </h4>
  </div>
  <div id="relac_cadcliente" class="panel-collapse collapse">
    <div class="panel-body">

      <?php  include '../perfil_cliente/verificar_representantes.php';  ?>


      <div class="form-group">

        <label>Vendedor</label>
        <select required id="vendedor" name="vendedor" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 

         <option value=""></option>

         <?php while ($representanteslista = $representantes->fetch_assoc()): ?>

           <option value="<?php echo $representanteslista['id'].'|'.$representanteslista['nome'] ?>"><?php echo $representanteslista['nome'] ?></option>

         <?php endwhile; ?>

       </select>
     </div>


   </div>
 </div>
</div>                                                                                         
<!--/loja vendedor--> 

<!--/indicação-->         
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#add_indicacao"> + Indicação </a>
    </h4>
  </div>
  <div id="add_indicacao" class="panel-collapse collapse">
    <div class="panel-body">

     <!-- Nova Indicação-->
     <div class="form-group">  


      <a  role="tab" id="headingind" data-toggle="collapse" data-parent="#accordion" href="#collapsenovaindicacao" aria-expanded="false" aria-controls="collapseOne">
        <p class="col-lg-12 text-left">+ Cadastrar Indicação</p>
      </a>
      <div id="collapsenovaindicacao" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingind">
        <div class="panel-body">
         <div class="row">    
           <div class="col-md-12">
            <div class="form-group">     
              <input type="text" id="nome_nova_indicacao" class="form-control form-block" placeholder="Indicação Descrição" value="">                             
            </div>                  
          </div>
        </div> 
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <a  id="cadastrar_indicacao" class="btn yellow btn-sm"> <i class="fa fa-plus" aria-hidden="true"></i> Cadastrar  </a>
            </div>                  
          </div>
        </div>                             



      </div>
    </div>


  </div>
  <!-- Nova Indicação-->

  <?php   include '../indicacao/verificar_indicacao.php';  ?>


  <div class="form-group">

    <label>Indicação</label>
    <select id="indicacao" name="indicacao" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
     <option value=""></option>


     <?php while ($selectindicacaolista = $selectindicacao->fetch_assoc()): ?>

       <option value="<?php echo $selectindicacaolista['id'].'|'.$selectindicacaolista['descricao'] ?>"><?php echo $selectindicacaolista['descricao'] ?></option>

     <?php  endwhile; ?>

   </select>

 </div>

 <div class="row"> 
  <div class="col-md-3">
   <div class="form-group">
     <a onclick="AddTableRowIndicacaoOs()" name="adicionar_indicacao" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
   </div>                  
 </div>
</div>


<br>
<div class="row">     

  <div class="col-md-12">
   <!-- iniciando tabela area-->
   <div class="portlet box blue">
     <div class="portlet-title">
       <div class="caption">
         <i class="fa fa-product-hunt"></i>Indicações </div>
         <div class="tools">
           <a href="javascript:;" class="collapse"> </a>

           <a href="javascript:;" class="reload"> </a>

         </div>
       </div>
       <div class="portlet-body">
         <div class="table-scrollable">
           <table id="tabela_indicacao" class="table table-striped table-hover">
             <thead>
               <tr>
                 <th> # </th>
                 <th> Desc. </th>

                 <th>Ação</th>

               </tr>
             </thead>
             <tbody>

               <tr>

               </tr>


             </tbody>
           </table>
         </div>
       </div>
     </div>
     <!-- fim tabela area-->




   </div>

 </div>  





</div>
</div>
</div>                                                                                         
<!--/indicação--> 







</div>
</div>
</div>
</div>
<div class="modal-footer">
  <a class="btn dark btn-outline" data-dismiss="modal">Fechar</a>
  <button type="submit" name="adicionar_ordem_servico" class="btn green">Adicionar</button>
</div>

</form>
</div>
</div>
</div>




<!-- Modal Nova Os Fim-->


<!-- Modal Editar OS Inicio-->

<div class="modal fade bs-modal-lg" id="modal_editarordem_servico"  role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-lg">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">O.S</h4>
  </div>
  <div class="modal-body">
    <form name="editar_ordem_servico" id="editar_ordem_servico" action="../ordem_servico/atualizar_os" enctype="multipart/form-data" method="post" class="horizontal-form">

     <div class="form-body">

       <div id="div_ver_os">

       </div>
       
       

     </div>

   </div>
   <div class="modal-footer"><button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
    <button type="submit" name="salvar_ordem_servico" class="btn green">Salvar</button>
    <button type="submit" name="excluir_ordem_servico" class="btn red">Excluir</button></div>
  </form>
</div>
</div>
</div>



<!-- Modal Editar OS Fim-->












<!-- SERVICOS FIM -->    











<!-- INICIO VENDAS TAB -->                  

<div class="tab-pane" id="tab_vendas">

 <!-- INICIO DATATABLE VENDAS-->

 <!-- FIM DATATABLE VENDAS -->
</div>                
<!-- FIM VENDAS TAB -->



</div>
</div>




</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
</div>
</div>
<div class="page-wrapper-row">
  <div class="page-wrapper-bottom">

  </div>
</div>
</div>
        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->

<script src="../../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="../../assets/pages/scripts/components-select2.js" type="text/javascript"></script>

<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="../../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>

<!-- jquery.inputmask data -->
<script src="../js/jquery.inputmask.bundle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js"></script>
<script src="../js/jquery.table2excel.min.js"></script> 
<script src="../js/dist/jquery.mask.min.js" type="text/javascript"></script>

<!--Scripts Model-->
<script src="../ordem_servico/produtos/adicionar_produtos_lista.js" type="text/javascript"></script>
<script src="../ordem_servico/pagamentos/add_pagamentos.js" type="text/javascript"></script>
<script src="../ordem_servico/laboratorio/add_laboratorio.js" type="text/javascript"></script>
<script src="../ordem_servico/indicacao/add_indicacao.js" type="text/javascript"></script>
<script src="../ordem_servico/pagamentos/desconto_pagamento.js" type="text/javascript"></script>

<script src="../ordem_servico/cad_medico_novo_os.js" type="text/javascript"></script>
<script src="../ordem_servico/cad_nova_ind_os.js" type="text/javascript"></script>
<script src="../ordem_servico/cad_novo_prod_os.js" type="text/javascript"></script>
<script src="../ordem_servico/hide_tipo_os.js" type="text/javascript"></script>
<script src="../ordem_servico/mascaras.js" type="text/javascript"></script>
<script src="../ordem_servico/ver_os.js" type="text/javascript"></script>
<script src="../ordem_servico/validacao_campos_os.js" type="text/javascript"></script>

<!--Scripts Model-->






<!-- ****************************************************************************************************************************************** -->			

<!-- jquery.inputmask data -->
<script>
 $(document).ready(function() {
  $(":input").inputmask();
});
</script>
<!-- /jquery.inputmask -->

<!-- ****************************************************************************************************************************************** -->			



<!-- popular cidades -->

<script>


  $(function() {

   $('#estado').change(function(){

    var cod_estados = $("#estado");
    var cod_estadosPost = cod_estados.val(); 

    $("#cod_cidades").empty();

		//var telefone = $("#telefone");
		//var telefonePost = telefone.val(); 	
		$.post("cidade_estados/popularcidades", {cod_estados: cod_estadosPost},
      function(data){
        $("#cidade").empty();
       // Coloca as frases na DIV
       $("#cidade").append(data);




     }
     , "html");	 

	});

 });




</script>

<!-- popular cidades -->



<!-- EXCEL EXPORTAR -->

<script>

	$("#exportar_contratos").click(function(e) {
   $("#aporte_perfil").table2excel({
     exclude: ".noExl",
     name: "Excel Document Name",
     filename: "Lista ",
     fileext: ".xls",
     exclude_img: true,
     exclude_links: true,
     exclude_inputs: true
   });
 });
	
	
  $("#exportar_aportes").click(function(e) {
   $("#rend_perfil").table2excel({
     exclude: ".noExl",
     name: "Excel Document Name",
     filename: "Lista",
     fileext: ".xls",
     exclude_img: true,
     exclude_links: true,
     exclude_inputs: true
   });
 });	

</script>				  
<!-- EXCEL EXPORTAR -->




<!-- edicao RECEITA-->


<script> 

  $(function() {$('#table_receita_medica tbody').on('click', '.cliqueparaeditarreceita_medica', function () { 

    var id_receita = $(this).closest('tr').find('td[data-id_receita]').data('id_receita');  
    document.getElementById('id_receita').value = id_receita ; 

    var fk_id_medico = $(this).closest('tr').find('td[data-fk_id_medico]').data('fk_id_medico');  
    var medico = $(this).closest('tr').find('td[data-medico]').data('medico');  
    document.getElementById('editar_medico_receita').value = fk_id_medico+'|'+medico ;   

    var validade = $(this).closest('tr').find('td[data-validade]').data('validade');  
    document.getElementById('editar_validade_receita').value = validade ;   

    var observacao = $(this).closest('tr').find('td[data-observacao]').data('observacao');  
    document.getElementById('editar_obs_receita').value = observacao ;   

    var valor_adicao = $(this).closest('tr').find('td[data-valor_adicao]').data('valor_adicao');  
    document.getElementById('editar_valor_adicao').value = valor_adicao ;   

    var valor_dnp_oe_perto = $(this).closest('tr').find('td[data-valor_dnp_oe_perto]').data('valor_dnp_oe_perto');  
    document.getElementById('editar_valor_dnp_oe_perto').value = valor_dnp_oe_perto ;   

    var valor_altura_oe_perto = $(this).closest('tr').find('td[data-valor_altura_oe_perto]').data('valor_altura_oe_perto');  
    document.getElementById('editar_valor_altura_oe_perto').value = valor_altura_oe_perto ;   

    var valor_eixo_oe_perto = $(this).closest('tr').find('td[data-valor_eixo_oe_perto]').data('valor_eixo_oe_perto');  
    document.getElementById('editar_valor_eixo_oe_perto').value = valor_eixo_oe_perto ;   

    var valor_cilindrico_oe_perto = $(this).closest('tr').find('td[data-valor_cilindrico_oe_perto]').data('valor_cilindrico_oe_perto');  
    document.getElementById('editar_valor_cilindrico_oe_perto').value = valor_cilindrico_oe_perto ;   

    var valor_esferico_oe_perto = $(this).closest('tr').find('td[data-valor_esferico_oe_perto]').data('valor_esferico_oe_perto');  
    document.getElementById('editar_valor_esferico_oe_perto').value = valor_esferico_oe_perto ;   

    var valor_dnp_od_perto = $(this).closest('tr').find('td[data-valor_dnp_od_perto]').data('valor_dnp_od_perto');  
    document.getElementById('editar_valor_dnp_od_perto').value = valor_dnp_od_perto ;   

    var valor_altura_od_perto = $(this).closest('tr').find('td[data-valor_altura_od_perto]').data('valor_altura_od_perto');  
    document.getElementById('editar_valor_altura_od_perto').value = valor_altura_od_perto ;   

    var valor_eixo_od_perto = $(this).closest('tr').find('td[data-valor_eixo_od_perto]').data('valor_eixo_od_perto');  
    document.getElementById('editar_valor_eixo_od_perto').value = valor_eixo_od_perto ;   

    var valor_cilindrico_od_perto = $(this).closest('tr').find('td[data-valor_cilindrico_od_perto]').data('valor_cilindrico_od_perto');  
    document.getElementById('editar_valor_cilindrico_od_perto').value = valor_cilindrico_od_perto ;   

    var valor_esferico_od_perto = $(this).closest('tr').find('td[data-valor_esferico_od_perto]').data('valor_esferico_od_perto');  
    document.getElementById('editar_valor_esferico_od_perto').value = valor_esferico_od_perto ;   

    var valor_dnp_oe_longe = $(this).closest('tr').find('td[data-valor_dnp_oe_longe]').data('valor_dnp_oe_longe');  
    document.getElementById('editar_valor_dnp_oe_longe').value = valor_dnp_oe_longe ;   

    var valor_altura_oe_longe = $(this).closest('tr').find('td[data-valor_altura_oe_longe]').data('valor_altura_oe_longe');  
    document.getElementById('editar_valor_altura_oe_longe').value = valor_altura_oe_longe ;   

    var valor_eixo_oe_longe = $(this).closest('tr').find('td[data-valor_eixo_oe_longe]').data('valor_eixo_oe_longe');  
    document.getElementById('editar_valor_eixo_oe_longe').value = valor_eixo_oe_longe ;   

    var valor_cilindrico_oe_longe = $(this).closest('tr').find('td[data-valor_cilindrico_oe_longe]').data('valor_cilindrico_oe_longe');  
    document.getElementById('editar_valor_cilindrico_oe_longe').value = valor_cilindrico_oe_longe ;   

    var valor_esferico_oe_longe = $(this).closest('tr').find('td[data-valor_esferico_oe_longe]').data('valor_esferico_oe_longe');  
    document.getElementById('editar_valor_esferico_oe_longe').value = valor_esferico_oe_longe ;   

    var valor_dnp_od_longe = $(this).closest('tr').find('td[data-valor_dnp_od_longe]').data('valor_dnp_od_longe');  
    document.getElementById('editar_valor_dnp_od_longe').value = valor_dnp_od_longe ;   

    var valor_altura_od_longe = $(this).closest('tr').find('td[data-valor_altura_od_longe]').data('valor_altura_od_longe');  
    document.getElementById('editar_valor_altura_od_longe').value = valor_altura_od_longe ;   

    var valor_eixo_od_longe = $(this).closest('tr').find('td[data-valor_eixo_od_longe]').data('valor_eixo_od_longe');  
    document.getElementById('editar_valor_eixo_od_longe').value = valor_eixo_od_longe ;   

    var valor_cilindrico_od_longe = $(this).closest('tr').find('td[data-valor_cilindrico_od_longe]').data('valor_cilindrico_od_longe');  
    document.getElementById('editar_valor_cilindrico_od_longe').value = valor_cilindrico_od_longe ;   

    var valor_esferico_od_longe = $(this).closest('tr').find('td[data-valor_esferico_od_longe]').data('valor_esferico_od_longe');  
    document.getElementById('editar_valor_esferico_od_longe').value = valor_esferico_od_longe ;   

    var fk_id_cliente = $(this).closest('tr').find('td[data-fk_id_cliente]').data('fk_id_cliente');  
    document.getElementById('editar_fk_id_cliente').value = fk_id_cliente ;   

    var FK_usuarios_admin_id = $(this).closest('tr').find('td[data-FK_usuarios_admin_id]').data('FK_usuarios_admin_id');  
    document.getElementById('editar_FK_usuarios_admin_id').value = FK_usuarios_admin_id ;   

    var fk_id_empresa = $(this).closest('tr').find('td[data-fk_id_empresa]').data('fk_id_empresa');  
    document.getElementById('editar_fk_id_empresa').value = fk_id_empresa ;   

  });

});

</script> 




</body>

</html>