<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente 

if(empty($_SESSION['user_email'])){header("Location: login");}; 

if($_SESSION['tipo_login'] <> '1'){header("Location: clientes");};


?>
<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Configurações</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <!-- ASSETS DATATABLE-->
  <link href="../../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  
  <link rel="shortcut icon" href="favicon.ico" /> </head>
  <!-- END HEAD -->

  <body class="page-container-bg-solid">
    <div class="page-wrapper">
      <div class="page-wrapper-row">
        <div class="page-wrapper-top">
          <!-- BEGIN HEADER -->
          <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
              <div class="container">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                  <a href="#">
                    <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">   </a>
                  </div>
                  <!-- END LOGO -->
                  <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                  <a href="javascript:;" class="menu-toggler"></a>
                  <!-- END RESPONSIVE MENU TOGGLER -->
                  <!-- BEGIN TOP NAVIGATION MENU -->
                  <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">

                      
                      
                      
                      
                      
                      <!-- BEGIN USER LOGIN DROPDOWN -->
                      <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                         <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };  ?>

                         <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                         <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                       </a>
                       <ul class="dropdown-menu dropdown-menu-default">
                        <li id="config_menu">
                          <a href="configuracoes">
                          Configurações</a>
                        </li>
                        <li class="divider"> </li>
                        
                        <li>
                          <a href="../login/logout">
                            <i class="icon-key"></i> Sair </a>
                          </li>


                        </ul>
                      </li>
                      <!-- END USER LOGIN DROPDOWN -->


                    </ul>
                  </div>
                  <!-- END TOP NAVIGATION MENU -->
                </div>
              </div>
              <!-- END HEADER TOP -->
              <!-- BEGIN HEADER MENU -->
              <div class="page-header-menu">
                <div class="container" >

                  <!-- END HEADER SEARCH BOX -->
                  <!-- BEGIN MEGA MENU -->
                  <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                  <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                  <div class="hor-menu "  >
                  <ul class="nav navbar-nav" >
                    <li id="menu_visao_geral" class="menu-dropdown classic-menu-dropdown   ">
                      <a  href="visaogeral"> Visão Geral

                      </a>

                    </li>
                    <li id="menu_agenda" class="menu-dropdown mega-menu-dropdown">
                      <a  href="calendario"> Agenda

                      </a>

                    </li>

                    <li id="menu_clientes" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                      <a  href="clientes"> Clientes

                      </a>

                    </li>
                    <li id="menu_os" class="menu-dropdown mega-menu-dropdown  ">
                     <a  href="ordem_servico"> Ordem de Serviço

                     </a>

                   </li>



                   <li id="menu_estoque" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="cadastro_produtos"> Estoque

                    </a>

                  </li>





                  <li id="menu_movimentacoes" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="movimentacoes"> Movimentações

                    </a>

                  </li>

                  <li id="menu_relatorios" class="menu-dropdown classic-menu-dropdown ">
                    <a href="relatorios"> Relatórios

                    </a>

                  </li>


                  <li id="menu_cadastros" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"> Cadastros
                      <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                     <li class=" ">
                      <a href="cadastro_fornecedores" class="nav-link  ">
                      Fornecedores</a>
                    </li>
                    <li class=" ">
                      <a href="cadastro_servicos" class="nav-link  ">
                       Categorias Produtos/Serviços

                     </a>
                   </li>
                   <li class=" ">
                    <a href="cadastro_laboratorio" class="nav-link  ">
                      Laboratórios

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_medicos" class="nav-link  ">
                      Médicos

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_indicacao" class="nav-link  ">
                      Indicações

                    </a>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
            <!-- END MEGA MENU -->
          </div>
        </div>
        <!-- END HEADER MENU -->
      </div>
      <!-- END HEADER -->
    </div>
  </div>
  <div id="div_todo_conteudo" class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
      <!-- BEGIN CONTAINER -->
      <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
          <!-- BEGIN CONTENT BODY -->
          <!-- BEGIN PAGE HEAD-->
          <div class="page-head">
            <div class="container">
              <!-- BEGIN PAGE TITLE -->
              <div class="page-title">
                <h1> Configurações </h1>
              </div>
              <!-- END PAGE TITLE -->
              
            </div>
          </div>
          <!-- END PAGE HEAD-->
          <!-- BEGIN PAGE CONTENT BODY -->
          <div class="page-content">
            <div class="container">


              <!-- TAB CONFIGURAÇÕES INICIO -->	

              <div class="portlet light ">

                <div class="portlet-body">
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a href="#tabcadastros" data-toggle="tab"> Configurações Gerais </a>
                    </li>
                    <li>
                      <a href="#tabusuarios" data-toggle="tab"> Usuários </a>
                    </li>
                    <li>
                      <a href="#tabcategorias" data-toggle="tab"> Categorias </a>
                    </li>
                    <li>
                      <a href="#tabcontas" data-toggle="tab"> Contas </a>
                    </li>
                    
                    <li>
                      <a href="#tabempresa" data-toggle="tab"> Dados Empresa </a>
                    </li>
                    
                    <li>
                      <a href="#tablogeventos" data-toggle="tab">Log Eventos </a>
                    </li>
                    
                  </ul>
                  <div class="tab-content">



                   <div class="tab-pane fade active in" id="tabcadastros">

                    <br>

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#config_email"> + Configuração Email de Envio </a>
                        </h4>
                      </div>
                      <div id="config_email" class="panel-collapse collapse">


                       <div class="panel-body">

                         <?php  include '../configuracoes/email/verificaremail.php';  ?>									

                         <form action="../configuracoes/email/emailenvio" enctype="multipart/form-data" method="post" class="horizontal-form">			


                           <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Email</label>
                                <input  type="text" value="<?php echo $check_emaillista['username']; ?>" required id="email" name="email" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                              </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">From</label>
                                <input   type="text" id="from_email" value="<?php echo $check_emaillista['nome']; ?>" name="from_email" class="form-control">

                              </div>
                            </div>
                            <!--/span-->
                          </div>

                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="control-label">Senha</label>
                                <input  type="password" required  value="<?php echo $check_emaillista['password']; ?>" id="senha" name="senha" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                              </div>
                            </div>
                            <!--/span-->

                            <!--/span-->
                          </div>


                          <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                            <button type="submit" name="adicionar_email" class="btn green">Salvar</button>
                          </div>
                        </div>
                      </div>
                    </div>	


                  </form>				


                </div>



                <!-- TAB usuarios INICIO -->	
                <div class="tab-pane fade" id="tabusuarios">
                  <!-- tabela usuarios INICIO -->	
                  <div class="portlet light ">

                    <div class="portlet-body">
                      <div class="table-toolbar">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="btn-group">
                              <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novousuario"> Novo Usúario
                                <i class="fa fa-plus"></i>
                              </a>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="btn-group pull-right">
                              <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas
                                <i class="fa fa-angle-down"></i>
                              </button>
                              <ul class="dropdown-menu pull-right">


                                <li>
                                  <a href="javascript:;">
                                    <i class="fa fa-file-excel-o"></i> Exportar para Excel </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="novousuario">
                          <thead>
                            <tr>
                              <th>
                                ID
                              </th>
                              <th> Nome </th>
                              <th> Login </th>
                              <th> Função </th>
                              <th> Ações </th>

                            </tr>
                          </thead>
                          <tbody>

                            <?php  include '../configuracoes/usuarios/verificar_usuarios.php';  ?>	

                            <?php while ($usuarioslista = $usuarios->fetch_assoc()): ?>

                              <tr class="odd gradeX">
                                <td data-idusuario="<?php echo $usuarioslista['id']; ?>"  data-relatorio_diario="<?php echo $usuarioslista['relatorio_diario']; ?>"  data-usuario_nome="<?php echo $usuarioslista['nome']; ?>" > <?php echo $usuarioslista['id']; ?> </td> 
                                <td> <?php echo $usuarioslista['nome']; ?> </td>
                                <td data-usuario_comissao="<?php echo $usuarioslista['comissao']; ?>" data-usuario_documento="<?php echo $usuarioslista['documento']; ?>" data-usuario_contato="<?php echo $usuarioslista['contato']; ?>"  >
                                  <a href="mailto:<?php echo $usuarioslista['login']; ?>"> <?php echo $usuarioslista['login']; ?> </a>
                                </td>
                                <td data-usuario_banco="<?php echo $usuarioslista['id_banco'].'|'.$usuarioslista['banco']; ?>" data-usuario_conta="<?php echo $usuarioslista['conta']; ?>" data-usuario_id_acesso="<?php echo $usuarioslista['id_acesso']; ?>" >
                                  <span class="label label-sm label-info"> <?php echo $usuarioslista['funcao']; ?> </span>
                                </td>

                                <td data-usuario_agencia="<?php echo $usuarioslista['agencia']; ?>" data-usuario_obs="<?php echo $usuarioslista['obs']; ?>" data-usuario_funcao="<?php echo $usuarioslista['id_acesso'].'|'.$usuarioslista['funcao']; ?>" data-usuario_fk_id_empresa="<?php echo $usuarioslista['fk_id_empresa']; ?>" >
                                  <div class="btn-group">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                      <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                      <li>
                                        <a class="cliqueparaeditarusuario" data-toggle="modal" href="#modal_editar_usuario">
                                          <i class="icon-docs"></i> Editar </a>
                                        </li>




                                      </ul>
                                    </div>
                                  </td>
                                </tr>

                              <?php endwhile; ?> 

                            </tbody>
                          </table>
                        </div>
                      </div>                                          


                      <!-- tabela usuarios FIM -->	

                      <!-- modal novo usuarios inicio -->	
                      <?php  include '../clientes/verificarbanco.php';  ?>	
                      <div class="modal fade bs-modal-lg" id="modal_novousuario" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                              <h4 class="modal-title">Cadastrar Novo Usuário</h4>
                            </div>
                            <div class="modal-body"> 

                             <!-- BEGIN FORM-->
                             <form action="../configuracoes/usuarios/add_usuario" enctype="multipart/form-data" method="post" class="horizontal-form">
                              <div class="form-body">
                                <h3 class="form-section">   </h3>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="control-label">Nome</label>
                                      <input  type="text" required id="nome_usuario" name="nome_usuario" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                    </div>
                                  </div>
                                  <!--/span-->
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="control-label">Contato</label>
                                      <input type="text" id="contato_usuario" name="contato_usuario" class="form-control" placeholder="WhatsApp com DDD" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                    </div>
                                  </div>
                                  <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="control-label">Documento</label>
                                      <input type="text" id="documento_usuario"  name="documento_usuario" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                    </div>
                                  </div>
                                  <!--/span-->
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="control-label">Comissão %</label>
                                      <input type="text" id="comissao_usuario" name="comissao_usuario" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                                    </div>
                                  </div>

                                </div>		
                                <div class="row">		
                                 <div class="col-md-6">   
                                   <div class="form-group">
                                    <label class="control-label">Função</label>
                                    <select type="text" id="funcao_usuario" name="funcao_usuario" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                      <option value="1|Diretoria">Diretoria</option>
                              <option value="2|Gerência">Gerência</option>
                              <option value="3|Vendedor">Vendedor</option>
                              <option value="4|Mediador de Pedidos">Mediador de Pedidos </option>
                                      
                                      
                                    </select>
                                    
                                  </div>
                                </div>	
                                <div class="col-md-6"> 
                                 <div class="form-group">
                                  <?php  include '../perfil_cliente/verificar_empresas.php';  ?>	   
                                  <label>Empresa</label>
                                  <select required id="single-append-text" name="empresa" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                                    <option value="" ></option>	
                                    <?php while ($dados_empresalista = $dados_empresa->fetch_assoc()): ?>

                                     <option value="<?php echo $dados_empresalista['id'].'|'.$dados_empresalista['razao_social']; ?>"><?php echo $dados_empresalista['nome_fantasia'] ?></option>
                                   <?php endwhile; ?>
                                 </select>
                               </div>
                             </div>
                           </div>
                           <!--/row-->
                           <div class="row">

                            <div class="col-md-6">   
                             <div class="form-group">
                              <label class="control-label">Login (E-Mail)</label>
                              <input required type="email" id="login_usuario" name="login_usuario" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                            </div>
                          </div>	

                          <div class="col-md-6">   
                           <div class="form-group">
                            <label class="control-label">Senha</label>
                            <input required type="password" id="senha_usuario" name="senha_usuario" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>	


                      </div>

                      <div class="row">
                        


                          </div>	

                          <br>
                          <br>
                          <div class="row">
                            <div class="form-group">
                              <label class="control-label col-md-3">Receber Relatório Diário</label>
                              <div class="col-md-2">

                               <input type="checkbox" name="notificacao" id="notificacao" unchecked class="make-switch switch-large" data-label-icon="fa fa-youtube" data-on-text="<i class='fa fa-thumbs-up'></i>" data-off-text="<i class='fa fa-thumbs-down'></i>"> </div>
                             </div>
                           </div>	



                           <!--/row-->
                           <h3 class="form-section">Dados Bancários</h3>

                           <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Banco</label>
                                <select id="single-append-text" name="banco_usuario"  class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                                  <?php while ($bancoslista = $bancos->fetch_assoc()): ?>
                                   <option value=""></option>
                                   <option value="<?php echo $bancoslista['id'].'|'.$bancoslista['code'].' - '.$bancoslista['title'] ?>"><?php echo $bancoslista['code'].' - '.$bancoslista['title'] ?></option>
                                 <?php endwhile; ?>
                               </select>
                             </div>
                           </div>
                           <div class="col-md-6">
                            <div class="form-group">
                              <label>Agência</label>
                              <input id="agencia_usuario" name="agencia_usuario"  type="text" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
                            </div>
                            <!--/span-->
                          </div>
                          <!--/row-->
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Conta</label>
                                <input id="conta_usuario" name="conta_usuario"  type="text" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
                              </div>
                              
                            </div>
                          </div>
                          
                          
                          
                          <!--/obs-->         
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#obs_cadusuario"> + Observações </a>
                              </h4>
                            </div>
                            <div id="obs_cadusuario" class="panel-collapse collapse">
                              <div class="panel-body">
                                <div class="form-group">
                                 <label class="control-label">Observações</label>
                                 <textarea rows="4" cols="50" type="text" id="observacoes_usuario" name="observacoes_usuario" class="form-control" placeholder=""></textarea>
                               </div>
                             </div>
                           </div>
                         </div>		


                         <!--/obs--> 						
                       </div>     
                       <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                        <button type="submit" name="adicionar_usuarios" class="btn green">Adicionar</button>
                      </div>
                    </form>						
                    
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>



              <!-- modal novo usuarios FIM -->									






              <!-- EDITAR MODAL USUAURIO INICIO--------------------------------------------------------------------------------->		

              <?php  include '../clientes/verificarbanco.php';  ?>	
              <div class="modal fade bs-modal-lg" id="modal_editar_usuario" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title">Editar Usuário</h4>
                    </div>
                    <div class="modal-body"> 

                     <!-- BEGIN FORM-->
                     <form action="../configuracoes/usuarios/atualizar_usuarios" enctype="multipart/form-data" method="post" class="horizontal-form">
                      <div class="form-body">

                        <input type="hidden" id="id_usuario_edicao" name="id_usuario_edicao" >
                        
                        <h3 class="form-section">   </h3>
                        
                        <div class="row">


                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Foto</label>
                              <input type="file" name="foto"> </span>     
                            </div>
                          </div>
                          
                        </div>	
                        
                        
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Nome</label>
                              <input  type="text" required id="editar_nome_usuario" name="editar_nome_usuario" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                              
                            </div>
                          </div>
                          <!--/span-->
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Contato</label>
                              <input type="text" id="editar_contato_usuario" name="editar_contato_usuario" class="form-control" placeholder="WhatsApp com DDD" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                              
                            </div>
                          </div>
                          <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Documento</label>
                              <input type="text" id="editar_documento_usuario"  name="editar_documento_usuario" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                              
                            </div>
                          </div>
                          <!--/span-->
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Comissão %</label>
                              <input type="text" id="editar_comissao_usuario" name="editar_comissao_usuario" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                              
                            </div>
                          </div>
                          
                        </div>		
                        <div class="row">		
                         <div class="col-md-6">   
                           <div class="form-group">
                            <label class="control-label">Função</label>
                            <select required type="text" id="editar_funcao_usuario" name="editar_funcao_usuario" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                              <option value="1|Diretoria">Diretoria</option>
                              <option value="2|Gerência">Gerência</option>
                              <option value="3|Vendedor">Vendedor</option>
                              <option value="4|Mediador de Pedidos">Mediador de Pedidos </option>


                            </select>

                          </div>
                        </div>	
                        <div class="col-md-6"> 
                         <div class="form-group">
                          <?php  include '../perfil_cliente/verificar_empresas.php';  ?>	   
                          <label>Empresa</label>
                          <select required id="usuario_editar_empresa" name="editar_empresa" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 

                            <?php while ($dados_empresalista = $dados_empresa->fetch_assoc()): ?>

                             <option value="<?php echo $dados_empresalista['id']; ?>"><?php echo $dados_empresalista['nome_fantasia'] ?></option>
                           <?php endwhile; ?>
                         </select>
                       </div>
                     </div>
                   </div>
                   <!--/row-->


                   


                 <br>
                 <br>
                 <div class="row">
                  <div class="form-group">
                    <label class="control-label col-md-3">Receber Relatório Diário</label>
                    <div  class="col-md-2">

                     <input type="checkbox" name="editar_notificacao" id="editar_notificacao" class="make-switch switch-large" data-label-icon="fa fa-youtube" data-on-text="<i class='fa fa-thumbs-up'></i>" data-off-text="<i class='fa fa-thumbs-down'></i>"> </div>
                   </div>
                 </div>	




               </div>

               <!--/row-->
               <h3 class="form-section">Dados Bancários</h3>

               <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Banco</label>
                    <select id="editar_banco_usuario" name="editar_banco_usuario"  class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                      <?php while ($bancoslista = $bancos->fetch_assoc()): ?>
                       <option value=""></option>
                       <option value="<?php echo $bancoslista['id'].'|'.$bancoslista['code'].' - '.$bancoslista['title'] ?>"><?php echo $bancoslista['code'].' - '.$bancoslista['title'] ?></option>
                     <?php endwhile; ?>
                   </select>
                 </div>
               </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label>Agência</label>
                  <input id="editar_agencia_usuario" name="editar_agencia_usuario"  type="text" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
                </div>
                <!--/span-->
              </div>
              <!--/row-->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Conta</label>
                    <input id="editar_conta_usuario" name="editar_conta_usuario"  type="text" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
                  </div>
                  
                </div>
                
                
                
                
                
                
                <!--/obs-->         
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#obs_cadusuarioeditar_"> + Observações </a>
                    </h4>
                  </div>
                  <div id="obs_cadusuarioeditar_" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="form-group">
                       <label class="control-label">Observações</label>
                       <textarea rows="4" cols="50" type="text" id="editar_observacoes_usuario" name="editar_observacoes_usuario" class="form-control" placeholder=""></textarea>
                     </div>
                   </div>
                 </div>
               </div>																							
               <!--/obs--> 						
             </div>     
             <div class="modal-footer">
              <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
              <button type="submit" name="editar_usuarios" class="btn green">Salvar</button>
              <button type="submit" name="excluir_usuario" class="btn red">Excluir</button>
            </div>
          </form>						

        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>



    <!-- EDITAR MODAL USUAURIO FIM--------------------------------------------------------------------------------->		








  </div>

  <!-- TAB usuarios FIM -->								
  <!-- TAB CATEGROAIS INICIO -->										
  <?php  include '../configuracoes/categorias/verificar_categorias.php';  ?>		

  <div class="tab-pane fade" id="tabcategorias">

    <div class="portlet-body">
     <div class="col-md-6">
      <div class="btn-group">
        <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novacategoria"> Nova Categoria
          <i class="fa fa-plus"></i>
        </a>
      </div>
    </div>																					 
  </div> 

  <br>
  <br>
  <br>
  <br>

  <div class="portlet-body">
    <?php echo $categoria_despesa;  ?>  
  </div>		



</div>
<!-- TAB CATEGROAIS FIM -->									

<!-- MODAL NOVA CATEGROIAS INCIO -->	
<?php  include '../clientes/verificarbanco.php';  ?>	
<div class="modal fade bs-modal-lg" id="modal_novacategoria" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Nova Categoria</h4>
      </div>
      <div class="modal-body"> 


       <!-- TAB NOVA CATEGORIA INTERNA INICIO -->	

       <div class="portlet light ">

        <div class="portlet-body">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#tab_novacategoriapai" data-toggle="tab"> Nova Categoria Pai </a>
            </li>
            <li>
              <a href="#tab_novacategoriafilho" data-toggle="tab"> Nova Categoria Filho </a>
            </li>
            
          </ul>
          <div class="tab-content">



           <div class="tab-pane fade active in" id="tab_novacategoriapai">

            <br>


            <div class="modal-body"> 

             <!-- BEGIN FORM-->
             <form action="../configuracoes/categorias/novacategoria_pai" enctype="multipart/form-data" method="post" class="horizontal-form">
              <div class="form-body">
                <h3 class="form-section">   </h3>




                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Categoria Pai</label>
                      <input  type="text" required id="categoria_pai" name="categoria_pai" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                    </div>
                  </div>

                </div>






              </div>



            </div>     
            <div class="modal-footer">
              <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
              <button type="submit" name="adicionar_novacatpai" class="btn green">Adicionar</button>
            </div>
          </form>	


        </div>




        <div class="tab-pane fade" id="tab_novacategoriafilho">

         <div class="modal-body"> 

           <!-- BEGIN FORM-->
           <form action="../configuracoes/categorias/novacategoria_filho" enctype="multipart/form-data" method="post" class="horizontal-form">
            <div class="form-body">
              <h3 class="form-section">   </h3>
              <?php  include '../configuracoes/categorias/select_categoriapai.php';  ?>
              <div class="row">
               <div class="col-md-6">
                <div class="form-group">
                  <label>Categoria Pai</label>
                  <select  required id="categoria_pai" name="categoria_pai" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                    <?php while ($categoria_pai_despesalista = $categoria_pai_despesa->fetch_assoc()): ?>
                     <option value=""></option>
                     <option value="<?php echo $categoria_pai_despesalista['id'] ?>"><?php echo $categoria_pai_despesalista['descricao'] ?></option>
                   <?php endwhile; ?>
                 </select>
               </div>
             </div>
           </div>


           <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Categoria Filho</label>
                <input  type="text" required id="categoria_filho" name="categoria_filho" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

              </div>
            </div>

          </div>






        </div>



      </div>     
      <div class="modal-footer">
        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
        <button type="submit" name="adicionar_catfilho" class="btn green">Adicionar</button>
      </div>
    </form>	
  </div>



</div>
<div class="clearfix margin-bottom-20"> </div>


</div>
</div>  


<!-- TAB NOVA CATEGORIA INTERNA FIM -->	



</div>     



</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>



<!-- MODAL NOVA CATEGROIAS FIM -->				








<!-- TAB CONTAS INICIO -->								
<div class="tab-pane fade" id="tabcontas">
  <div class="portlet light ">

    <div class="portlet-body">
      <div class="table-toolbar">
        <div class="row">
          <div class="col-md-6">
            <div class="btn-group">
              <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novaconta"> Nova Conta
                <i class="fa fa-plus"></i>
              </a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="btn-group pull-right">
              <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas
                <i class="fa fa-angle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right">

                <li>
                  <a href="javascript:;">
                    <i class="fa fa-file-excel-o"></i> Exportar Excel </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="novaconta">
          <thead>
            <tr>
              <th>
                ID
              </th>
              <th> Descrição </th>
              <th> Banco </th>
              <th> Ações</th>
              
            </tr>
          </thead>
          <tbody>


            <?php  include '../configuracoes/conta/verificar_conta.php';  ?>		
            
            <?php while ($conta_principallista = $conta_principal->fetch_assoc()): ?>

              <tr class="odd gradeX">
                <td data-idconta="<?php echo $conta_principallista['id']; ?>" > <?php  echo $conta_principallista['id']; ?> </td> 
                <td data-descricao_conta="<?php echo $conta_principallista['descricao']; ?>" data-valor_conta="<?php echo number_format($conta_principallista['saldo_inicial'], 2, ',', '.');  ?>"> <?php echo $conta_principallista['descricao']; ?> </td>
                <td data-banco_conta="<?php echo $conta_principallista['fk_id_banco'].'|'.$conta_principallista['banco']; ?>">
                  <?php echo $conta_principallista['banco']; ?> 
                </td>
                
                
                <td data-saldo_inicial_conta="<?php echo $conta_principallista['saldo_inicial']; ?>">
                  <div class="btn-group">
                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                      <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li>
                        <a class="cliqueparaeditarconta"  data-toggle="modal" href="#modal_editarconta">
                          <i class="icon-docs"></i> Editar </a>
                        </li>
                        
                        
                        
                        
                      </ul>
                    </div>
                  </td>
                </tr>
                
              <?php endwhile; ?> 
              
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <!-- TAB CONTAS INICIO -->				



    <!-- MODAL NOVA CONTA INCIO -->	
    <?php  include '../clientes/verificarbanco.php';  ?>	
    <div class="modal fade bs-modal-lg" id="modal_novaconta" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Nova Conta</h4>
          </div>
          <div class="modal-body"> 

           <!-- BEGIN FORM-->
           <form action="../configuracoes/conta/adicionar_conta" enctype="multipart/form-data" method="post" class="horizontal-form">
            <div class="form-body">
              <h3 class="form-section">   </h3>
              
              <div class="row">
               <div class="col-md-6">
                <div class="form-group">
                  <label>Banco</label>
                  <select id="banco_conta" name="banco_conta" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                    <?php while ($bancoslista = $bancos->fetch_assoc()): ?>
                     <option value=""></option>
                     <option value="<?php echo $bancoslista['id'].'|'.$bancoslista['code'].' - '.$bancoslista['title'] ?>"><?php echo $bancoslista['code'].' - '.$bancoslista['title'] ?></option>
                   <?php endwhile; ?>
                 </select>
               </div>
             </div>
           </div>


           <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Descrição Conta</label>
                <input  type="text" required id="descricao_conta" name="descricao_conta" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Valor(R$)</label>
                <input onKeyPress="return(MascaraMoeda(this,'.',',',event))" type="text" id="valor_inicial" name="valor_inicial" class="form-control" placeholder="00,00">
                
              </div>
            </div>
            <!--/span-->
          </div>
          
          
          
          
          
          
        </div>
        
        
        
      </div>     
      <div class="modal-footer">
        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
        <button type="submit" name="adicionar_conta" class="btn green">Adicionar</button>
      </div>
    </form>						

  </div>
  <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>



<!-- MODAL NOVA CONTA FIM -->					



<!-- MODAL EDITAR CONTA INCIO -->	
<?php  include '../clientes/verificarbanco.php';  ?>	
<div class="modal fade bs-modal-lg" id="modal_editarconta" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Editar Conta</h4>
      </div>
      <div class="modal-body"> 

       <!-- BEGIN FORM-->
       <form action="../configuracoes/conta/atualizar_conta" enctype="multipart/form-data" method="post" class="horizontal-form">
        <div class="form-body">
          <h3 class="form-section">   </h3>
          <input type="hidden" id="idconta" name="idconta">
          
          <div class="row">
           <div class="col-md-6">
            <div class="form-group">
              <label>Banco</label>
              <select id="editar_banco_conta" name="editar_banco_conta" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                <?php while ($bancoslista = $bancos->fetch_assoc()): ?>
                 <option value=""></option>
                 <option value="<?php echo $bancoslista['id'].'|'.$bancoslista['code'].' - '.$bancoslista['title'] ?>"><?php echo $bancoslista['code'].' - '.$bancoslista['title']?></option>
               <?php endwhile; ?>
             </select>
           </div>
         </div>
       </div>


       <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">Descrição Conta</label>
            <input  type="text" required id="editar_descricao_conta" name="editar_descricao_conta" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
            
          </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">Valor(R$)</label>
            <input   onKeyPress="return(MascaraMoeda(this,'.',',',event))" type="text" id="editar_valor_inicial" name="editar_valor_inicial" class="form-control" placeholder="00,00">
            
          </div>
        </div>
        <!--/span-->
      </div>
      
      
      
      
      
      
    </div>
    
    
    
  </div>     
  <div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
    <button type="submit" name="atualizar_conta" class="btn green">Adicionar</button>
  </div>
</form>						

</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>



<!-- MODAL NOVA CONTA FIM -->											





<!-- TAB EMPRESAS INICIO -->					
<div class="tab-pane fade" id="tabempresa">

  <div class="portlet light ">

    <div class="portlet-body">
      <div class="table-toolbar">
        <div class="row">
          <div class="col-md-6">
            <div class="btn-group">
              <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novaempresa"> Nova Empresa
                <i class="fa fa-plus"></i>
              </a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="btn-group pull-right">
              <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas
                <i class="fa fa-angle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right">
                <li>
                  <a href="javascript:;">
                    <i class="fa fa-print"></i> Print </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                        <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="novaempresa">
              <thead>
                <tr>
                  <th>
                    ID
                  </th>
                  <th> Razão Social </th>
                  <th> CNPJ </th>
                  <th> Ações </th>
                  
                </tr>
              </thead>
              <tbody>

                <?php  include '../configuracoes/empresa/verificar_empresas.php';  ?>	
                <?php  include '../configuracoes/contratos/verificar_modelos_contratos.php';  ?>		
                
                <?php while ($empresaslista = $empresas->fetch_assoc()): ?>

                  <tr class="odd gradeX">
                    <td data-empresarazao_social="<?php echo $empresaslista['razao_social']; ?>" data-empresaid="<?php echo $empresaslista['id']; ?>" data-empresabanco="<?php echo $empresaslista['id_banco'].'|'.$empresaslista['banco']; ?>"  > <?php echo $empresaslista['id']; ?> </td> 
                    <td data-empresanome_fantasia="<?php echo $empresaslista['nome_fantasia']; ?>" data-empresacnpj="<?php echo $empresaslista['cnpj']; ?>" data-empresacontrato_modelo_id="<?php echo $empresaslista['contrato_modelo_id']; ?>"> <?php echo $empresaslista['razao_social']; ?> </td>
                    <td data-empresaendereco="<?php echo $empresaslista['endereco']; ?>" data-empresacidade="<?php echo $empresaslista['cidade']; ?>" data-empresauf="<?php echo $empresaslista['uf']; ?>">
                      <?php echo $empresaslista['cnpj']; ?> 
                    </td>
                    
                    
                    <td data-empresaconta="<?php echo $empresaslista['conta']; ?>" data-empresaagencia="<?php echo $empresaslista['agencia']; ?>" data-empresacontrato_modelo_id_juridico="<?php echo $empresaslista['contrato_modelo_id_juridico']; ?>" data-empresalogo_anexo="<?php echo $empresaslista['logo_anexo']; ?>" >
                      <div class="btn-group">
                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                          <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                          <li>
                            <a class="cliqueparaeditarempresa" data-toggle="modal" href="#modal_editarempresa" href="javascript:;">
                              <i class="icon-docs"></i> Editar </a>
                            </li>
                            
                            
                            
                            
                          </ul>
                        </div>
                      </td>
                    </tr>
                    
                  <?php endwhile; ?> 
                  
                </tbody>
              </table>
            </div>
          </div>  
          
          
        </div>
        
        <!-- TAB EMPRESAS FIM -->		
        
        
        <!-- MODAL NOVA EMPRESA INCIO -->	
        <?php  include '../clientes/verificarbanco.php';  ?>	
        <div class="modal fade bs-modal-lg" id="modal_novaempresa" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Cadastrar Nova Empresa</h4>
              </div>
              <div class="modal-body"> 

               <!-- BEGIN FORM-->
               <form action="../configuracoes/empresa/add_empresa" enctype="multipart/form-data" method="post" class="horizontal-form">
                <div class="form-body">
                  <h3 class="form-section">   </h3>

                  <div class="row">


                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Foto</label>
                        <input type="file" name="foto"> </span>     
                      </div>
                    </div>
                    
                  </div>  

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Razão Social</label>
                        <input  type="text" required id="razaosocial_empresa" name="razaosocial_empresa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                        
                      </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Nome Fantasia</label>
                        <input type="text" id="nomefantasia_empresa" name="nomefantasia_empresa" class="form-control">
                        
                      </div>
                    </div>
                    <!--/span-->
                  </div>
                  <!--/row-->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">CNPJ</label>
                        <input type="text" id="cnpj_empresa" required name="cnpj_empresa" class="form-control" placeholder="" >
                        
                      </div>
                    </div>
                    <!--/span-->
                    
                    
                  </div>
                  <!--/row-->
                  <div class="row">

                    <div class="col-md-2">   
                     <div class="form-group">
                      <label class="control-label">UF</label>
                      <input type="text" id="uf_empresa" name="uf_empresa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                    </div>
                  </div>	

                  <div class="col-md-6">   
                   <div class="form-group">
                    <label class="control-label">Cidade</label>
                    <input type="text" id="cidade_empresa" name="cidade_empresa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                  </div>
                </div>	




              </div>
              <!--/row-->
              <h3 class="form-section">Dados Bancários</h3>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Banco</label>
                    <select id="single-append-text" name="banco_empresa" required class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                      <?php while ($bancoslista = $bancos->fetch_assoc()): ?>
                       <option value=""></option>
                       <option value="<?php echo $bancoslista['id'].'|'.$bancoslista['code'].' - '.$bancoslista['title'] ?>"><?php echo $bancoslista['code'].' - '.$bancoslista['title'] ?></option>
                     <?php endwhile; ?>
                   </select>
                 </div>
               </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label>Agência</label>
                  <input id="agencia_empresa" name="agencia_empresa" required type="text" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
                </div>
                <!--/span-->
              </div>
              <!--/row-->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Conta</label>
                    <input id="conta_empresa" name="conta_empresa" required type="text" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
                  </div>

                </div>
              </div>



              <!--/obs-->         
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#colapendereco_empresa"> + Endereço </a>
                  </h4>
                </div>
                <div id="colapendereco_empresa" class="panel-collapse collapse">
                  <div class="panel-body">
                    <div class="form-group">
                     <label class="control-label">Endereço</label>
                     <textarea rows="4" cols="50" type="text" id="endereco_empresa" name="endereco_empresa" class="form-control" placeholder=""></textarea>
                   </div>
                 </div>
               </div>
             </div>																							
             <!--/obs--> 						
           </div>     
           <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="submit" name="adicionar_empresa" class="btn green">Adicionar</button>
          </div>
        </form>						

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>



  <!-- MODAL NOVA EMPRESA FIM -->				




  <!-- MODAL EDITAR EMPRESA INCIO -->	
  <?php  include '../clientes/verificarbanco.php';  ?>	
  <div class="modal fade bs-modal-lg" id="modal_editarempresa" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">Editar Empresa</h4>
        </div>
        <div class="modal-body"> 

         <!-- BEGIN FORM-->
         <form action="../#" enctype="multipart/form-data" method="post" class="horizontal-form">
          <div class="form-body">
            <h3 class="form-section">   </h3>
            
            <input type="hidden" id="id_empresa" name="id_empresa" >


            <div class="row">


              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Foto</label>
                  <input type="file" name="foto"> </span>     
                </div>
              </div>
              
            </div>  
            
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Razão Social</label>
                  <input  type="text" required id="editar_razaosocial_empresa" name="editar_razaosocial_empresa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                  
                </div>
              </div>
              <!--/span-->
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Nome Fantasia</label>
                  <input type="text" id="editar_nomefantasia_empresa" name="editar_nomefantasia_empresa" class="form-control">
                  
                </div>
              </div>
              <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">CNPJ</label>
                  <input type="text" id="editar_cnpj_empresa" required name="editar_cnpj_empresa" class="form-control" placeholder="" >
                  
                </div>
              </div>
              <!--/span-->
              
              
            </div>
            <!--/row-->
            <div class="row">

              <div class="col-md-2">   
               <div class="form-group">
                <label class="control-label">UF</label>
                <input type="text" id="editar_uf_empresa" name="editar_uf_empresa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

              </div>
            </div>	

            <div class="col-md-6">   
             <div class="form-group">
              <label class="control-label">Cidade</label>
              <input type="text" id="editar_cidade_empresa" name="editar_cidade_empresa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

            </div>
          </div>	




        </div>
        <!--/row-->
        <h3 class="form-section">Dados Bancários</h3>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Banco</label>
              <select id="editar_single-append-text" name="editar_banco_empresa" required class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                <?php while ($bancoslista = $bancos->fetch_assoc()): ?>
                 <option value=""></option>
                 <option value="<?php echo $bancoslista['id'].'|'.$bancoslista['code'].' - '.$bancoslista['title'] ?>"><?php echo $bancoslista['code'].' - '.$bancoslista['title'] ?></option>
               <?php endwhile; ?>
             </select>
           </div>
         </div>
         <div class="col-md-6">
          <div class="form-group">
            <label>Agência</label>
            <input id="editar_agencia_empresa" name="editar_agencia_empresa" required type="text" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
          </div>
          <!--/span-->
        </div>
        <!--/row-->
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Conta</label>
              <input id="editar_conta_empresa" name="editar_conta_empresa" required type="text" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
            </div>

          </div>
        </div>



        <!--/obs-->         
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#colapendereco_editarempresa"> + Endereço </a>
            </h4>
          </div>
          <div id="colapendereco_editarempresa" class="panel-collapse collapse">
            <div class="panel-body">
              <div class="form-group">
               <label class="control-label">Endereço</label>
               <textarea rows="4" cols="50" type="text" id="editar_endereco_empresa" name="editar_endereco_empresa" class="form-control" placeholder=""></textarea>
             </div>
           </div>
         </div>
       </div>																							
       <!--/obs--> 						
     </div>     
     <div class="modal-footer">
      <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
      <button type="submit" name="editar_empresa" class="btn green">Salvar</button>
    </div>
  </form>						

</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>



<!-- MODAL EDITAR EMPRESA FIM -->			





<div class="tab-pane fade" id="tablogeventos">
  <div class="portlet light ">

    <div class="portlet-body">
      <div class="table-toolbar">

      </div>
      
      <?php  include '../configuracoes/log_eventos/verificar_log.php';  ?>	
      
      <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_logeventos">
        <thead>
          <tr>

           <th> ID  </th>
           <th> Login </th>
           <th> Horário </th>
           <th> Ação </th>
           <th> Tabela </th>



         </tr>
       </thead>
       <tbody>



        <?php while ($logslista = $logs->fetch_assoc()): ?>

          <tr class="odd gradeX">
            <td> <?php  echo $logslista['id']; ?> </td> 
            
            <td>
              <?php echo $logslista['login']; ?> 
            </td>
            <td>
              <?php echo $logslista['hora']; ?> 
            </td>
            <td>
              <?php echo $logslista['acao']; ?> 
            </td>
            <td>
              <?php echo $logslista['tabela']; ?> 
            </td>
            
            
          </tr>
          
        <?php endwhile; ?> 
        
      </tbody>
    </table>
  </div>
</div>
</div>	


</div>
<div class="clearfix margin-bottom-20"> </div>


</div>
</div>  


<!-- TAB CONFIGURAÇÕES FIM -->	




</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
</div>
</div>
<div class="page-wrapper-row">
  <div class="page-wrapper-bottom">

  </div>
</div>
</div>
        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="../../assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>

<script type="text/javascript">
  
 $(document).ready(function() {
        
      var nivelacesso =  "<?php echo $_SESSION['nivel']; ?>";

      if((nivelacesso == '3') || (nivelacesso == '4') || (nivelacesso == '2')){
         
            $('#config_menu').hide();
            $('#div_todo_conteudo').hide();
        alert('Seu Login não da acesso a essa área!');

      };
        
        
        
        
      });


  
</script>


<!-- ****************************************************************************************************************************************** -->
<!-- MASCARA MOEDA -->

<script language="javascript">  

 function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){  
   var sep = 0;  
   var key = '';  
   var i = j = 0;  
   var len = len2 = 0;  
   var strCheck = '0123456789';  
   var aux = aux2 = '';  
   var whichCode = (window.Event) ? e.which : e.keyCode;  
   if (whichCode == 13 || whichCode == 8) return true;  
     key = String.fromCharCode(whichCode); // Valor para o código da Chave  
     if (strCheck.indexOf(key) == -1) return false; // Chave inválida  
     len = objTextBox.value.length;  
     for(i = 0; i < len; i++)  
       if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;  
     aux = '';  
     for(; i < len; i++)  
       if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);  
     aux += key;  
     len = aux.length;  
     if (len == 0) objTextBox.value = '';  
     if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;  
     if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;  
     if (len > 2) {  
       aux2 = '';  
       for (j = 0, i = len - 3; i >= 0; i--) {  
         if (j == 3) {  
           aux2 += SeparadorMilesimo;  
           j = 0;  
         }  
         aux2 += aux.charAt(i);  
         j++;  
       }  
       objTextBox.value = '';  
       len2 = aux2.length;  
       for (i = len2 - 1; i >= 0; i--)  
         objTextBox.value += aux2.charAt(i);  
       objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);  
     }  
     return false;  
   }  
 </script>

 <!-- ****************************************************************************************************************************************** -->


 <!-- ****************************************************************************************************************************************** -->			


 <!-- dados usuario para edição  -->
 <script>



   $(function() {

     $('#novousuario tbody').on('click', '.cliqueparaeditarusuario', function () {

       var usuario_nome = $(this).closest('tr').find('td[data-usuario_nome]').data('usuario_nome');
       document.getElementById('editar_nome_usuario').value = usuario_nome ;

       var idusuario = $(this).closest('tr').find('td[data-idusuario]').data('idusuario');
       document.getElementById('id_usuario_edicao').value = idusuario ;

       var usuario_comissao = $(this).closest('tr').find('td[data-usuario_comissao]').data('usuario_comissao');
       document.getElementById('editar_comissao_usuario').value = usuario_comissao ;

       var usuario_documento = $(this).closest('tr').find('td[data-usuario_documento]').data('usuario_documento');
       document.getElementById('editar_documento_usuario').value = usuario_documento ;

       var usuario_contato  = $(this).closest('tr').find('td[data-usuario_contato]').data('usuario_contato');
       document.getElementById('editar_contato_usuario').value = usuario_contato  ;

       var usuario_banco = $(this).closest('tr').find('td[data-usuario_banco]').data('usuario_banco');
       document.getElementById('editar_banco_usuario').value = usuario_banco ;

       var usuario_conta = $(this).closest('tr').find('td[data-usuario_conta]').data('usuario_conta');
       document.getElementById('editar_conta_usuario').value = usuario_conta ;

       var usuario_id_acesso = $(this).closest('tr').find('td[data-usuario_id_acesso]').data('usuario_id_acesso');
       document.getElementById('editar_nivel_acesso').value = usuario_id_acesso ;

       var usuario_fk_id_empresa = $(this).closest('tr').find('td[data-usuario_fk_id_empresa]').data('usuario_fk_id_empresa');
					//document.getElementById('editar_empresa').value = usuario_fk_id_empresa ;

					alert(usuario_fk_id_empresa);
					$('#usuario_editar_empresa').val(usuario_fk_id_empresa).trigger('change');
					
					var usuario_funcao = $(this).closest('tr').find('td[data-usuario_funcao]').data('usuario_funcao');
					document.getElementById('editar_funcao_usuario').value = usuario_funcao ;
					
					var usuario_obs = $(this).closest('tr').find('td[data-usuario_obs]').data('usuario_obs');
					document.getElementById('editar_observacoes_usuario').value = usuario_obs ;
					
					var usuario_agencia = $(this).closest('tr').find('td[data-usuario_agencia]').data('usuario_agencia');
					document.getElementById('editar_agencia_usuario').value = usuario_agencia ;
					
					var relatorio_diario = $(this).closest('tr').find('td[data-relatorio_diario]').data('relatorio_diario');		
					
					if(relatorio_diario == '1'){document.getElementById('editar_notificacao').checked = true;};


					
					
					$("#editar_notificacao").bootstrapSwitch('state', relatorio_diario);
					

					

				});
   });
 </script>


 <!-- dados rendimento para edição  -->							

 <!-- ****************************************************************************************************************************************** -->	



 <!-- ****************************************************************************************************************************************** -->			


 <!-- dados conta para edição  -->
 <script>



   $(function() {


     $('#novaconta tbody').on('click', '.cliqueparaeditarconta', function () {


       var banco_conta = $(this).closest('tr').find('td[data-banco_conta]').data('banco_conta');

       document.getElementById('editar_banco_conta').value = banco_conta ;

       var descricao_conta = $(this).closest('tr').find('td[data-descricao_conta]').data('descricao_conta');
       document.getElementById('editar_descricao_conta').value = descricao_conta ;

       var valor_conta = $(this).closest('tr').find('td[data-valor_conta]').data('valor_conta');
       document.getElementById('editar_valor_inicial').value = valor_conta ;

       var idconta = $(this).closest('tr').find('td[data-idconta]').data('idconta');
       document.getElementById('idconta').value = idconta ;





     });
   });
 </script>


 <!-- dados rendimento para edição  -->							

 <!-- ****************************************************************************************************************************************** -->			


 <!-- ****************************************************************************************************************************************** -->			


 <!-- dados conta para edição  -->
 <script>



   $(function() {


     $('#novaempresa tbody').on('click', '.cliqueparaeditarempresa', function () {




       var empresaid = $(this).closest('tr').find('td[data-empresaid]').data('empresaid');
       document.getElementById('id_empresa').value = empresaid ;

       var empresarazao_social = $(this).closest('tr').find('td[data-empresarazao_social]').data('empresarazao_social');
       document.getElementById('editar_razaosocial_empresa').value = empresarazao_social ;

       var empresanome_fantasia = $(this).closest('tr').find('td[data-empresanome_fantasia]').data('empresanome_fantasia');
       document.getElementById('editar_nomefantasia_empresa').value = empresanome_fantasia ;

       var empresacnpj = $(this).closest('tr').find('td[data-empresacnpj]').data('empresacnpj');
       document.getElementById('editar_cnpj_empresa').value = empresacnpj ;

       var empresauf = $(this).closest('tr').find('td[data-empresauf]').data('empresauf');
       document.getElementById('editar_uf_empresa').value = empresauf ;

       var empresacidade  = $(this).closest('tr').find('td[data-empresacidade]').data('empresacidade');
       document.getElementById('editar_cidade_empresa').value = empresacidade ;

       var empresabanco = $(this).closest('tr').find('td[data-empresabanco]').data('empresabanco');
       document.getElementById('editar_banco_empresa').value = empresabanco ;

       var empresaconta = $(this).closest('tr').find('td[data-empresaconta]').data('empresaconta');
       document.getElementById('editar_conta_empresa').value = empresaconta ;

       var empresaendereco = $(this).closest('tr').find('td[data-]').data('empresaendereco');
       document.getElementById('editar_endereco_empresa').value = empresaendereco ;

       var empresaagencia = $(this).closest('tr').find('td[data-empresaagencia]').data('empresaagencia');
       document.getElementById('editar_agencia_empresa ').value = empresaagencia ;




     });
   });
 </script>


 <!-- dados rendimento para edição  -->							

 <!-- ****************************************************************************************************************************************** -->		




</body>

</html>