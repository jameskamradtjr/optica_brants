<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente 

if(empty($_SESSION['user_email'])){header("Location: login");}; 


?>
<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Cadastros</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->

    <!-- GRAFICO LEVEL PLUGINS -->

    <link href="../../assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

    <!-- GRAFICO LEVEL PLUGINS -->

    <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">

        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
                        <!-- BEGIN HEADER TOP -->
                        <div class="page-header-top">
                            <div class="container">
                                <!-- BEGIN LOGO -->
                                <div class="page-logo">
                                    <a href="#">
                                        <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">    </a>
                                    </div>
                                    <!-- END LOGO -->
                                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                    <a href="javascript:;" class="menu-toggler"></a>
                                    <!-- END RESPONSIVE MENU TOGGLER -->
                                    <!-- BEGIN TOP NAVIGATION MENU -->
                                    <div class="top-menu">
                                        <ul class="nav navbar-nav pull-right">

                                         
                                         


                                    <!-- BEGIN USER LOGIN DROPDOWN -->
                                    <li class="dropdown dropdown-user dropdown-dark">
                      <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                       <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };  ?>

                       <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                       <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-default">
                      <li id="config_menu">
                        <a href="configuracoes">
                        Configurações</a>
                      </li>

                        <li class="divider"> </li>
                                                
                                                <li>
                                                    <a href="../login/logout">
                                                        <i class="icon-key"></i> Sair </a>
                                                    </li>

                    </ul>
                  </li>
                  <!-- END USER LOGIN DROPDOWN -->


                </ul>
              </div>
              <!-- END TOP NAVIGATION MENU -->
            </div>
          </div>
          <!-- END HEADER TOP -->
          <!-- BEGIN HEADER MENU -->
          <div class="page-header-menu">
            <div class="container" >

              <!-- END HEADER SEARCH BOX -->
              <!-- BEGIN MEGA MENU -->
              <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
              <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
              <div class="hor-menu "  >
                <ul class="nav navbar-nav" >
                  <li class="menu-dropdown classic-menu-dropdown  ">
                    <a href="visaogeral"> Visão Geral

                    </a>

                  </li>
                  <li class="menu-dropdown mega-menu-dropdown">
                    <a href="calendario"> Agenda

                    </a>

                  </li>

                  <li class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                    <a href="clientes"> Clientes

                    </a>

                  </li>
                  <li class="menu-dropdown mega-menu-dropdown  ">
                   <a href="ordem_servico"> Ordem de Serviço

                   </a>

                 </li>



                 <li class="menu-dropdown mega-menu-dropdown  ">
                  <a href="cadastro_produtos"> Estoque

                  </a>

                </li>





                <li class="menu-dropdown mega-menu-dropdown  ">
                  <a href="movimentacoes"> Movimentações

                  </a>

                </li>

                <li class="menu-dropdown classic-menu-dropdown ">
                  <a href="relatorios"> Relatórios

                  </a>

                </li>


                <li class="menu-dropdown classic-menu-dropdown ">
                  <a href="javascript:;"> Cadastros
                    <span class="arrow"></span>
                  </a>
                  <ul class="dropdown-menu pull-left">

                   <li class=" ">
                    <a href="cadastro_fornecedores" class="nav-link  ">
                    Fornecedores</a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_servicos" class="nav-link  ">
                     Categorias Produtos/Serviços

                   </a>
                 </li>
                 <li class=" ">
                  <a href="cadastro_laboratorio" class="nav-link  ">
                    Laboratórios

                  </a>
                </li>
                <li class=" ">
                  <a href="cadastro_medicos" class="nav-link  ">
                    Médicos

                  </a>
                </li>
                <li class=" ">
                  <a href="cadastro_indicacao" class="nav-link  ">
                    Indicações

                  </a>
                </li>
              </ul>
            </li>


          </ul>
        </div>
        <!-- END MEGA MENU -->
      </div>
    </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
    </div>
</div>
<div id="div_todo_conteudo" class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> </h1>
                        </div>
                        <!-- END PAGE TITLE -->

                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">


                      <!-- TAB GERAL INICIO -->	

                      <div class="portlet light ">

                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tabservicos" data-toggle="tab"> Serviços </a>
                                </li>
                                <li>
                                    <a href="#taboutros" data-toggle="tab"> Outros </a>
                                </li>


                            </ul> 
                            <div class="tab-content">


                                <!-- TAB INICIO -->	 							
                                <div class="tab-pane fade active in" id="tabservicos">

                                 <br>

                                 
                                 <!-- DATATABLE --> 


                                 <!-- DATATABLE -->	 						

                                 

                                 <!-- MODAL NOVO INCIO -->


                                 <!-- MODAL NOVO FIM -->			



                                 <!-- MODAL EDITAR INCIO -->	


                                 <!-- MODAL EDITAR FIM -->


                             </div>

                             <!-- TAB FIM -->				                 




                             <!-- tab outros inicio -->												
                             <div class="tab-pane fade" id="taboutros">
                                <br>


                            </div>
                            <!-- tab outros fim -->







                        </div>
                        <div class="clearfix margin-bottom-20"> </div>


                    </div>
                </div>  


                <!-- TAB GERAL FIM -->	


            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
</div>
</div>
<div class="page-wrapper-row">
    <div class="page-wrapper-bottom">

    </div>
</div>
</div>

        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->




<!-- ****************************************************************************************************************************************** -->			


<!-- dados para edição  -->
<script>



   
</script>


<!-- dados para edição  -->							

<!-- ****************************************************************************************************************************************** -->				

</body>

</html>