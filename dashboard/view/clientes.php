<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente 
if(empty($_SESSION['user_email'])){header("Location: login");}; 
?>
<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Clientes</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- CSS da Barra de Progresso Inicia Pagina -->
  <link href="loading_page/loading_page.css" rel="stylesheet" type="text/css" />

  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->

  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="../../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- END PAGE LEVEL PLUGINS -->

  <link rel="shortcut icon" href="favicon.ico" /> </head>
  <!-- Pular input no enter -->
  <script type="text/javascript">
    function handleEnter(field, event) {
     var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
     if (keyCode == 13) {
      var i;
      for (i = 0; i < field.form.elements.length; i++)
       if (field == field.form.elements[i])
        break;
      i = (i + 1) % field.form.elements.length;
      field.form.elements[i].focus();
      return false;
    } else
    return true;
  }
</script>


<!-- END HEAD -->

<body class="page-container-bg-solid">


 <!-- Loading Porcentagem--> 
 <div class="preloader-wrap">
  <div class="percentage" id="precent"></div>
  <div class="loader">
    <div class="trackbar">
     <div class="loadbar"></div>
   </div>
   <div class="glow"></div>
 </div>
</div>
<!-- Loading Porcentagem--> 

<div class="page-wrapper">
  <div class="page-wrapper-row">
    <div class="page-wrapper-top">
      <!-- BEGIN HEADER -->
      <div class="page-header">
        <!-- BEGIN HEADER TOP -->
        <div class="page-header-top">
          <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
              <a href="#">
                <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">
              </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">

             
             <ul class="nav navbar-nav pull-right">



            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown dropdown-user dropdown-dark">
                      <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                       <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };  ?>

                       <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                       <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-default">
                      <li id="config_menu">
                        <a href="configuracoes">
                        Configurações</a>
                      </li>

                        <li class="divider"> </li>
                                                
                                                <li>
                                                    <a href="../login/logout">
                                                        <i class="icon-key"></i> Sair </a>
                                                    </li>

                    </ul>
                  </li>
                  <!-- END USER LOGIN DROPDOWN -->




                </ul>
              </div>
              <!-- END TOP NAVIGATION MENU -->
            </div>
          </div>
          <!-- END HEADER TOP -->
          <!-- BEGIN HEADER MENU -->
          <div class="page-header-menu">
            <div class="container" >

              <!-- END HEADER SEARCH BOX -->
              <!-- BEGIN MEGA MENU -->
              <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
              <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
              <div class="hor-menu "  >
                  <ul class="nav navbar-nav" >
                    <li id="menu_visao_geral" class="menu-dropdown classic-menu-dropdown   ">
                      <a  href="visaogeral"> Visão Geral

                      </a>

                    </li>
                    <li id="menu_agenda" class="menu-dropdown mega-menu-dropdown">
                      <a  href="calendario"> Agenda

                      </a>

                    </li>

                    <li id="menu_clientes" class="menu-dropdown mega-menu-dropdown active mega-menu-full">
                      <a  href="clientes"> Clientes

                      </a>

                    </li>
                    <li id="menu_os" class="menu-dropdown mega-menu-dropdown  ">
                     <a  href="ordem_servico"> Ordem de Serviço

                     </a>

                   </li>



                   <li id="menu_estoque" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="cadastro_produtos"> Estoque

                    </a>

                  </li>





                  <li id="menu_movimentacoes" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="movimentacoes"> Movimentações

                    </a>

                  </li>

                  <li id="menu_relatorios" class="menu-dropdown classic-menu-dropdown ">
                    <a href="relatorios"> Relatórios

                    </a>

                  </li>


                  <li id="menu_cadastros" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"> Cadastros
                      <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                     <li class=" ">
                      <a href="cadastro_fornecedores" class="nav-link  ">
                      Fornecedores</a>
                    </li>
                    <li class=" ">
                      <a href="cadastro_servicos" class="nav-link  ">
                       Categorias Produtos/Serviços

                     </a>
                   </li>
                   <li class=" ">
                    <a href="cadastro_laboratorio" class="nav-link  ">
                      Laboratórios

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_medicos" class="nav-link  ">
                      Médicos

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_indicacao" class="nav-link  ">
                      Indicações

                    </a>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
        <!-- END MEGA MENU -->
      </div>
    </div>
      <!-- END HEADER MENU -->
    </div>
    <!-- END HEADER -->
  </div>
</div>
<div id="div_todo_conteudo" class="page-wrapper-row full-height">
  <div class="page-wrapper-middle">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
              <h1> Clientes </h1>
            </div>
            <!-- END PAGE TITLE -->

          </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
          <div class="container">

           <div class="portlet light ">

            <div class="portlet-body">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tabclientes" data-toggle="tab"> Clientes </a>
                </li>
                <li style="display:none">
                  <a href="#taboutros" data-toggle="tab"> Outros </a>
                </li>

              </ul>
              <div class="tab-content">

               <!-- TAB CLIENTES INICIO -->	

               <div class="tab-pane fade active in" id="tabclientes">

                <br>
                <br>

                <!-- INICIO DATATABLE-->
                <div class="portlet light ">

                  <div class="portlet-body">
                    <div class="table-toolbar">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="btn-group">
                            <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novocliente"> Novo Cliente
                              <i class="fa fa-plus"></i>
                            </a>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="btn-group pull-right">
                            <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas
                              <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">


                              <li>
                                <a id="exportar_clientes"href="javascript:;">
                                  <i class="fa fa-file-excel-o"></i> Exportar para Excel </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                          <tr>
                            <th>
                             ID
                           </th>
                           <th> Nome </th>
                           <th> Email </th>
                           <th> Status </th>
                           <th> Celular </th>
                           <th> Ações </th>
                         </tr>
                       </thead>
                       <tbody>



                        <?php  include '../clientes/verificarclientes.php';  ?>	
                        <?php  include '../clientes/verificarbanco.php';  ?>	

                        <?php while ($clientelista = $cliente->fetch_assoc()): ?>

                          <tr class="odd gradeX">
                            <td>
                              <?php echo $clientelista['id']; ?>
                            </td>
                            <td data-nome_cliente="<?php echo $clientelista['nome'] ?>" data-idcliente="<?php echo $clientelista['id'] ?>" > <?php echo $clientelista['nome']; ?> </td>
                            <td data-email_cliente="<?php echo $clientelista['email'] ?>">
                              <a href="mailto:<?php echo $clientelista['email']; ?>"> <?php echo $clientelista['email']; ?> </a>
                            </td>
                            <td data-mensagememail_cliente="<?php echo $clientelista['status_atendimento'] ?>">
                              <span class="label label-sm label-info"> <?php echo $clientelista['status_atendimento']; ?> </span>
                            </td>
                            <td class="center"> <?php echo $clientelista['celular']; ?> </td>
                            <td>
                              <div class="btn-group">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                  <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                  <li>
                                    <a href="perfil_cliente?id=<?php echo $clientelista['id']; ?>">
                                      <i class="icon-docs"></i> Abrir Cadastro </a>
                                    </li>
                                    <li>
                                      <a class="cliqueparaeditarmensagemcliente" data-toggle="modal" href="#enviar_lembrete_cliente">
                                        <i class="icon-tag"></i> Enviar Lembrete </a>
                                      </li>
                                      <li class="divider">

                                      </li>




                                    </ul>
                                  </div>
                                </td>
                              </tr>
                            <?php endwhile; ?>  

                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- FIM DATATABLE-->

                    <!-- /.MODAL ENVIAR LEMBRETE AGENDAMENTO -->			
                    <div class="modal fade bs-modal-lg" id="enviar_lembrete_cliente" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Enviar Lembrete Cliente</h4>
                          </div>
                          <div class="modal-body"> 


                           <!-- TAB CLIENTES INICIO -->	

                           <div class="portlet light ">

                            <div class="portlet-body">
                              <ul class="nav nav-tabs">
                                <li class="active">
                                  <a href="#tabenviar_lembrete_cliente_email" data-toggle="tab"> Email </a>
                                </li>
                                <li>
                                  <a href="#tabenviar_lembrete_cliente_sms" data-toggle="tab"> SMS </a>
                                </li>

                              </ul>
                              <div class="tab-content">



                               <div class="tab-pane fade active in" id="tabenviar_lembrete_cliente_email">

                                <br>

                                <form action="../notificacoes/tela_clientes/enviar_email_lembrete_agendamento" enctype="multipart/form-data" method="post" class="horizontal-form">

                                 <div class="form-body">

                                  <div class="row">

                                    <input type="hidden" name="FK_cliente_admin_id" id="FK_cliente_admin_id">


                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">Cliente</label>
                                        <input type="text"  id="nome_cliente_lembrete" name="nome_cliente_lembrete_agendamento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                      </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="control-label">E-Mail</label>
                                        <input type="email" required id="email_cliente_lembrete" name="email_cliente_lembrete_agendamento" class="form-control" onkeypress="return handleEnter(this, event)">

                                      </div>
                                    </div>
                                    <!--/span-->
                                  </div>

                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="control-label">Assunto</label>
                                        <input type="text" value="" required id="assunto_email_lembrete" name="assunto_email_lembrete_agendamento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                      </div>
                                    </div>
                                  </div>																																		 


                                  <div class="row">
                                    <div class="col-md-12">	 
                                     <div class="form-group">
                                       <label class="control-label">Mensagem</label>
                                       <select type="text" id="status_atendimento" required name="status_atendimento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                         <option value="" ></option>
                                         <option value="Aguardando Pagamento" >Aguardando Pagamento</option>
                                         <option value="Solicitar Assinatura">Solicitar Assinatura</option> 
                                         <option value="Enviar seu Saldo">Enviar seu Saldo</option> 
                                       </select>
                                     </div>
                                   </div>


                                 </div>	 



                               </div>

                               <div class="modal-footer">
                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                <button type="submit" name="enviar_email_lembrete_agendamento" class="btn green">Enviar</button>
                              </div>

                            </form>




                          </div>




                          <div class="tab-pane fade" id="tabenviar_lembrete_cliente_sms">
                            <p> Em breve. </p>
                          </div>



                        </div>
                        <div class="clearfix margin-bottom-20"> </div>


                      </div>
                    </div>  


                    <!-- TAB CORPO  FIM -->




                  </div>

                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.MODAL ENVIAR LEMBRETE AGENDAMENTO -->	





            <!-- /.MODAL NOVO CLIENTE INICIO -->
            <div class="modal fade" id="modal_novocliente" tabindex="" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Novo Cliente</h4>
                  </div>

                  <div class="modal-body"> 

                   <!-- BEGIN FORM-->
                   <form action="../clientes/cadastrarcliente" enctype="multipart/form-data" method="post" class="horizontal-form">
                    <div class="form-body">
                      <h3 class="form-section">Info Pessoais</h3>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Nome</label>
                            <input  type="text" required id="nome" name="nome" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Celular</label>
                            <input type="text" id="celular" name="celular" class="form-control" placeholder="WhatsApp com DDD" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                          </div>
                        </div>
                        <!--/span-->
                      </div>
                      <!--/row-->
                      <div style="display:none" class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">CPF</label>
                            <input type="text" id="cpf" name="cpf" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                          </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">RG</label>
                            <input type="text" id="rg" name="rg" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>

                          </div>
                        </div>
                      </div>
                      <!--/row-->
                      <div class="row">

                        <div class="col-md-6">   
                         <div class="form-group">
                          <label class="control-label">E-Mail</label>
                          <input type="email" id="email" name="email" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                        </div>
                      </div>	

                      <div style="display:none" class="col-md-6">   
                       <div class="form-group">
                        <label class="control-label">Status Contato</label>
                        <select type="text" id="status_atendimento" name="status_atendimento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          <option value=""></option>

                         <option value="Urgência">Urgência</option>
                         <option value="Aguardando Promoção">Aguardando Promoção</option>	
                         <option value="Interessado Produto">Interessado Produto</option>		
                        	

                       </select>

                     </div>
                   </div>	

                 </div>
                 <!--/row-->




               </div>




               <!--/obs-->         
               <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#obs_cadcliente"> + Observações </a>
                  </h4>
                </div>
                <div id="obs_cadcliente" class="panel-collapse collapse">
                  <div class="panel-body">
                    <div class="form-group">
                     <label class="control-label">Observações</label>
                     <textarea rows="4" cols="50" type="text" id="observacoes" name="observacoes" class="form-control" placeholder=""></textarea>
                   </div>
                 </div>
               </div>
             </div>																							
             <!--/obs--> 		




           </div>                                                                   


           <div class="modal-footer">
            <a class="btn dark btn-outline" data-dismiss="modal">Fechar</a>
            <button type="submit" name="cadastrar_cliente" class="btn green"><i class="fa fa-plus"></i> Adicionar</button>
          </div>

        </form>
        <!-- END FORM-->					 

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.MODAL NOVO CLIENTE INICIO -->						






</div>

<!-- TAB CLIENTES FIM -->	


<div class="tab-pane fade" id="taboutros">


  

      <!-- /.MODAL ENVIAR LEMBRETE AGENDAMENTO -->			
      <div class="modal fade bs-modal-lg" id="enviar_lembrete_agenda" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Enviar Lembrete</h4>
            </div>
            <div class="modal-body"> 


             <!-- TAB CLIENTES INICIO -->	

             <div class="portlet light ">

              <div class="portlet-body">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#tabenviar_lembrete_agenda_email" data-toggle="tab"> Email </a>
                  </li>
                  <li>
                    <a href="#tabenviar_lembrete_agenda_sms" data-toggle="tab"> SMS </a>
                  </li>

                </ul>
                <div class="tab-content">



                 <div class="tab-pane fade active in" id="tabenviar_lembrete_agenda_email">

                  <br>

                  <form action="../notificacoes/tela_clientes/enviar_email_lembrete_agendamento" enctype="multipart/form-data" method="post" class="horizontal-form">

                   <div class="form-body">

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Cliente</label>
                          <input type="text"  id="nome_cliente_lembrete_agendamento" name="nome_cliente_lembrete_agendamento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                        </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">E-Mail</label>
                          <input type="email" required id="email_cliente_lembrete_agendamento" name="email_cliente_lembrete_agendamento" class="form-control" onkeypress="return handleEnter(this, event)">

                        </div>
                      </div>
                      <!--/span-->
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="control-label">Assunto</label>
                          <input type="text" value="Lembrete Consultoria Bitcoin" required id="assunto_email_lembrete_agendamento" name="assunto_email_lembrete_agendamento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                        </div>
                      </div>
                    </div>																																		 


                    <div class="row">
                      <div class="col-md-12">	 
                       <div class="form-group">
                         <label class="control-label">Mensagem</label>
                         <select type="text" id="status_atendimento" required name="status_atendimento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                           <option value="" ></option>
                           <option value="Aguardando Pagamento" >Aguardando Pagamento</option>
                           <option value="Solicitar Assinatura">Solicitar Assinatura</option> 	
                         </select>
                       </div>
                     </div>


                   </div>	 



                 </div>

                 <div class="modal-footer">
                  <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                  <button type="submit" name="enviar_email_lembrete_agendamento" class="btn green">Enviar</button>
                </div>

              </form>




            </div>




            <div class="tab-pane fade" id="tabenviar_lembrete_agenda_sms">
              <p> Em breve. </p>
            </div>



          </div>
          <div class="clearfix margin-bottom-20"> </div>


        </div>
      </div>  


      <!-- TAB CORPO  FIM -->




    </div>

  </div>
  <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.MODAL ENVIAR LEMBRETE AGENDAMENTO -->	





</div>



</div>
<div class="clearfix margin-bottom-20"> </div>


</div>
</div>  




</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
</div>
</div>
<div class="page-wrapper-row">
  <div class="page-wrapper-bottom">

  </div>
</div>
</div>
        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="../../assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<!-- mascara cpf rg -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js"></script>
<script src="../js/jquery.table2excel.min.js"></script> 
<!-- END PAGE LEVEL SCRIPTS -->

<script src="loading_page/loading_page.js" type="text/javascript"></script>


<!-- ****************************************************************************************************************************************** -->
<!-- MASCARA MOEDA -->

<script language="javascript">  

 function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){  
   var sep = 0;  
   var key = '';  
   var i = j = 0;  
   var len = len2 = 0;  
   var strCheck = '0123456789';  
   var aux = aux2 = '';  
   var whichCode = (window.Event) ? e.which : e.keyCode;  
   if (whichCode == 13 || whichCode == 8) return true;  
     key = String.fromCharCode(whichCode); // Valor para o código da Chave  
     if (strCheck.indexOf(key) == -1) return false; // Chave inválida  
     len = objTextBox.value.length;  
     for(i = 0; i < len; i++)  
       if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;  
     aux = '';  
     for(; i < len; i++)  
       if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);  
     aux += key;  
     len = aux.length;  
     if (len == 0) objTextBox.value = '';  
     if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;  
     if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;  
     if (len > 2) {  
       aux2 = '';  
       for (j = 0, i = len - 3; i >= 0; i--) {  
         if (j == 3) {  
           aux2 += SeparadorMilesimo;  
           j = 0;  
         }  
         aux2 += aux.charAt(i);  
         j++;  
       }  
       objTextBox.value = '';  
       len2 = aux2.length;  
       for (i = len2 - 1; i >= 0; i--)  
         objTextBox.value += aux2.charAt(i);  
       objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);  
     }  
     return false;  
   }  
 </script>

 <!-- ****************************************************************************************************************************************** -->


<!-- MASCARA CPF/RG -->
<script>
  $('#cpf').mask('000.000.000-00', {reverse: true}); 
  
  
  
</script>
<!-- /MASCARA CPF/RG -->			
<!-- ****************************************************************************************************************************************** -->			

<script>

	$("#exportar_clientes").click(function(e) {
    $("#sample_1").table2excel({
     exclude: ".noExl",
     name: "Excel Document Name",
     filename: "Lista Clientes",
     fileext: ".xls",
     exclude_img: true,
     exclude_links: true,
     exclude_inputs: true
   });
  });

</script>		


<!-- ****************************************************************************************************************************************** -->			

<script>

 $(function() {

   $('.cliqueparaeditarmensagemagenda').click(function(){




     var email_cliente_agendamensagem  = $(this).closest('tr').find('td[data-email_cliente_agendamensagem]').data('email_cliente_agendamensagem');
     document.getElementById('email_cliente_lembrete_agendamento').value = email_cliente_agendamensagem  ;					





     var  nome_cliente_agendamensagem = $(this).closest('tr').find('td[data-nome_cliente_agendamensagem]').data('nome_cliente_agendamensagem');
     document.getElementById('nome_cliente_lembrete_agendamento').value = nome_cliente_agendamensagem ;		



   });
 });
 
 
 
</script>		


<!-- ****************************************************************************************************************************************** -->		



<!-- ****************************************************************************************************************************************** -->			

<script>

 $(function() {

  $('#sample_1 tbody').on('click', '.cliqueparaeditarmensagemcliente', function () {




   var nome_cliente  = $(this).closest('tr').find('td[data-nome_cliente]').data('nome_cliente');								
   document.getElementById('nome_cliente_lembrete').value = nome_cliente ;		
   

   var FK_cliente_admin_id  = $(this).closest('tr').find('td[data-idcliente]').data('idcliente');
   document.getElementById('FK_cliente_admin_id').value = FK_cliente_admin_id  ;					
   
   
   
   var  email_cliente = $(this).closest('tr').find('td[data-email_cliente]').data('email_cliente');
   document.getElementById('email_cliente_lembrete').value =   email_cliente ;		
   
   var  mensagememail_cliente = $(this).closest('tr').find('td[data-mensagememail_cliente]').data('mensagememail_cliente');
   document.getElementById('status_atendimento').value =   mensagememail_cliente ;			
   
   

   
 });
});
 
 
 
</script>		


<!-- ****************************************************************************************************************************************** -->		
<script>   
$(document).ready(function() {
        
      var nivelacesso =  "<?php echo $_SESSION['nivel']; ?>";

    
 
      if( (nivelacesso == '3') ){
         
            $('#config_menu').hide();
            $('.cliqueparaeditarproduto').hide();
            $('.cliqueparatransferirproduto').hide();
            $('#menu_visao_geral').hide();
          // $('#menu_agenda').hide();
          // $('#menu_clientes').hide();
      // $('#menu_os').hide();
      // $('#menu_estoque').hide();
       $('#menu_relatorios').hide();
       $('#menu_cadastros').hide();
       $('#menu_movimentacoes').hide();
      
      };
        
    
    if((nivelacesso == '4')){         
           
           $('#config_menu').hide();
           $('#menu_visao_geral').hide();
          // $('#menu_agenda').hide();
          // $('#menu_clientes').hide();
       $('#menu_os').hide();
       $('#menu_estoque').hide();
      // $('#menu_relatorios').hide();
       $('#menu_cadastros').hide();
       $('#menu_movimentacoes').hide();
       
      };    
        
        
      });

</script>
</body>

</html>