<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente 

if(empty($_SESSION['user_email'])){header("Location: login");}; 


?>
<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Movimentações</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <!-- ASSETS DATATABLE-->
    <link href="../../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    
    
    
    <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

 <!-- Pular input no enter -->
  <script type="text/javascript">
    function handleEnter(field, event) {
     var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
     if (keyCode == 13) {
      var i;
      for (i = 0; i < field.form.elements.length; i++)
       if (field == field.form.elements[i])
        break;
      i = (i + 1) % field.form.elements.length;
      field.form.elements[i].focus();
      return false;
    } else
    return true;
  }
</script>


    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
                        <!-- BEGIN HEADER TOP -->
                        <div class="page-header-top">
                            <div class="container">
                                <!-- BEGIN LOGO -->
                                <div class="page-logo">
                                    <a href="index.html">
                                        <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">   </a>
                                    </div>
                                    <!-- END LOGO -->
                                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                    <a href="javascript:;" class="menu-toggler"></a>
                                    <!-- END RESPONSIVE MENU TOGGLER -->
                                    <!-- BEGIN TOP NAVIGATION MENU -->
                                    <div class="top-menu">
                                        <ul class="nav navbar-nav pull-right">
                                           
                                         
                                    
                                    
                                    <!-- BEGIN USER LOGIN DROPDOWN -->
                                    <li class="dropdown dropdown-user dropdown-dark">
                      <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                       <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };  ?>

                       <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                       <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-default">
                      <li id="config_menu">
                        <a href="configuracoes">
                        Configurações</a>
                      </li>

                        <li class="divider"> </li>
                                                
                                                <li>
                                                    <a href="../login/logout">
                                                        <i class="icon-key"></i> Sair </a>
                                                    </li>

                    </ul>
                  </li>
                  <!-- END USER LOGIN DROPDOWN -->


                </ul>
              </div>
              <!-- END TOP NAVIGATION MENU -->
            </div>
          </div>
          <!-- END HEADER TOP -->
          <!-- BEGIN HEADER MENU -->
          <div class="page-header-menu">
            <div class="container" >

              <!-- END HEADER SEARCH BOX -->
              <!-- BEGIN MEGA MENU -->
              <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
              <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
              <div class="hor-menu "  >
                  <ul class="nav navbar-nav" >
                    <li id="menu_visao_geral" class="menu-dropdown classic-menu-dropdown   ">
                      <a  href="visaogeral"> Visão Geral

                      </a>

                    </li>
                    <li id="menu_agenda" class="menu-dropdown mega-menu-dropdown">
                      <a  href="calendario"> Agenda

                      </a>

                    </li>

                    <li id="menu_clientes" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                      <a  href="clientes"> Clientes

                      </a>

                    </li>
                    <li id="menu_os" class="menu-dropdown mega-menu-dropdown  ">
                     <a  href="ordem_servico"> Ordem de Serviço

                     </a>

                   </li>



                   <li id="menu_estoque" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="cadastro_produtos"> Estoque

                    </a>

                  </li>





                  <li id="menu_movimentacoes" class="menu-dropdown mega-menu-dropdown  active">
                    <a href="movimentacoes"> Movimentações

                    </a>

                  </li>

                  <li id="menu_relatorios" class="menu-dropdown classic-menu-dropdown ">
                    <a href="relatorios"> Relatórios

                    </a>

                  </li>


                  <li id="menu_cadastros" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"> Cadastros
                      <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                     <li class=" ">
                      <a href="cadastro_fornecedores" class="nav-link  ">
                      Fornecedores</a>
                    </li>
                    <li class=" ">
                      <a href="cadastro_servicos" class="nav-link  ">
                       Categorias Produtos/Serviços

                     </a>
                   </li>
                   <li class=" ">
                    <a href="cadastro_laboratorio" class="nav-link  ">
                      Laboratórios

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_medicos" class="nav-link  ">
                      Médicos

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_indicacao" class="nav-link  ">
                      Indicações

                    </a>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
        <!-- END MEGA MENU -->
      </div>
    </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
    </div>
</div>
<div id="div_todo_conteudo" class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> Financeiro </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        
                      <!-- TAB MOVIMENTACOES INICIO -->	

                      <div class="portlet light ">
                        
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tabpagamentos" data-toggle="tab"> Pagamentos </a>
                                </li>
                                <li>
                                    <a href="#tabrecebimentos" data-toggle="tab"> Recebimentos </a>
                                </li>
                                
                            </ul>
                            <div class="tab-content">
                                
                             
                             
                             <div class="tab-pane fade active in" id="tabpagamentos">
                              
                              <br>
                              
                              <!-- INICIO DATATABLE PAGAMENTO-->
                              <div class="portlet light ">
                               
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novopagamento"> Novo Pagamento
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="btn-group pull-right">
                                                    <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas
                                                        <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right" >
                                                        
                                                        <li>
                                                            <a id="exportar_pagamentos" href="javascript:;">
                                                                <i class="fa fa-file-excel-o"></i> Exportar Excel </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="novopagamento">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        ID
                                                    </th>
                                                    <th> Data </th>
                                                    <th> Descrição </th>
                                                    <th> Status </th>
                                                    <th> Valor </th>
                                                    <th> Categoria </th>
                                                    <th> Ações </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                <?php  include '../movimentacoes/verificar_despesas.php';  ?>
                                                
                                                <?php while ($dados_contasapagarlista = $dados_contasapagar->fetch_assoc()): ?> 
                                                   
                                                    <tr class="odd gradeX">
                                                        <td data-relacionamento_despesa_edicao="<?php echo $dados_contasapagarlista['relacionamento'] ?>" data-id_despesa="<?php echo $dados_contasapagarlista['id'] ?>" data-conta_despesa="<?php echo $dados_contasapagarlista['fk_conta_id'].'|'.$dados_contasapagarlista['conta'] ?>" class="center" >
                                                           <?php echo $dados_contasapagarlista['id']; ?>			
                                                       </td>
                                                       <td  data-vencimento_despesa="<?php echo $dados_contasapagarlista['data_vencimento'] ?>" data-categoria_despesa="<?php echo $dados_contasapagarlista['grupo_categoria'].'|'.$dados_contasapagarlista['FK_categoria_id'].'|'.$dados_contasapagarlista['categoria'] ?>" class="center"> 
                                                        <?php echo date("d/m/Y", strtotime($dados_contasapagarlista['data_vencimento'])); ?>		
                                                    </td>
                                                    <td data-descricao_despesa="<?php echo $dados_contasapagarlista['descricao'] ?>" class="center">
                                                      <?php echo $dados_contasapagarlista['descricao']; ?>		 
                                                  </td>
                                                  <td data-status_despesa="<?php echo $dados_contasapagarlista['status'] ?>" class="center">
                                                    <span class="label label-sm label-info" >   <?php echo $dados_contasapagarlista['status']; ?> </span>
                                                </td>
                                                <td class="center" data-valor_despesa="<?php echo abs(number_format($dados_contasapagarlista['valor_bruto'], 2, ',', '.')); ?>" data-parcelas_despesas="<?php echo $dados_contasapagarlista['parcelas'] ?>">   <?php echo $dados_contasapagarlista['valor_bruto']; ?>		 </td>
                                                <td class="center">   <?php echo $dados_contasapagarlista['categoria']; ?>		 </td>
                                                <td class="center" >
                                                  
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a class="cliqueparaeditardespesa"  data-toggle="modal" href="#modal_editarpagamento">
                                                                    <i class="icon-docs"></i> Editar </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:;">
                                                                        <i class="icon-tag"></i> Enviar Lembrete de Pagamento </a>
                                                                    </li>
                                                                    
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    
                                                <?php endwhile; ?>
                                                
                                            </tbody>
                                        </table>
                                        
                                        
                                    </div>
                                </div>
                                <!-- FIM DATATABLE PAGAMENTO-->
                                
                                
                                
                                <!-- /.MODAL NOVO PAGAMENTO INICIO -->                                                                                                                            

                                <div class="modal fade" id="modal_novopagamento" tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Novo Pagamento</h4>
                                            </div>
                                            <div class="modal-body"> 
                                               
                                               <!-- BEGIN FORM-->
                                               <form action="../movimentacoes/contasapagar/nova_despesa" name="nova_despesa" id="nova_despesa" enctype="multipart/form-data" method="post" class="horizontal-form">
                                                <div class="form-body">
                                                   
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Descrição</label>
                                                                <input  type="text" required id="descricao_despesa" name="descricao_despesa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                                                
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">
                                                      <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Valor(R$)</label>
                                                            <input onKeyPress="return(MascaraMoeda(this,'.',',',event))" type="text" id="valor_despesa" required name="valor_despesa" class="form-control" placeholder="" >
                                                            
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Data</label>
                                                            <input type="date" id="data_despesa" name="data_despesa" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                  <?php  include '../movimentacoes/verificar_conta.php';  ?>		
                                                  <div class="col-md-6">   
                                                     <div class="form-group">
                                                        <label class="control-label">Conta</label>
                                                        <select type="text" id="conta_despesa" name="conta_despesa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                                         <option value=""></option>
                                                         <?php while ($conta_principallista = $conta_principal->fetch_assoc()): ?>
                                                             
                                                           <option value="<?php echo $conta_principallista['id'].'|'.$conta_principallista['descricao'] ?>"><?php echo $conta_principallista['descricao']?></option>
                                                       <?php endwhile; ?>
                                                       
                                                       
                                                   </select>
                                                   
                                               </div>
                                           </div>		
                                           <?php  include '../movimentacoes/verificar_categorias.php';  ?>
                                           <div class="col-md-6">   
                                             <div class="form-group">
                                                <label class="control-label">Categoria</label>
                                                <select type="text" id="categoria_despesa" name="categoria_despesa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                                    <option value=""></option>
                                                    <?php echo $categoriadespesa;  ?>
                                                    
                                                </select>
                                                
                                            </div>
                                        </div>	
                                        
                                    </div>
                                    
                                    
                                    
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Parcelas</label>
                                                <input value="1" id="parcelas_despesa" name="parcelas_despesa" required type="number" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
                                            </div>
                                            
                                            <div class="col-md-4">
                                               <div class="form-group">
                                                <label class="control-label">Status</label>
                                                <select type="text" id="status_despesa" name="status_despesa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                                    <option value="Pago">Pago</option>
                                                    <option value="A Pagar">A Pagar</option>
                                                    
                                                </select>
                                                
                                            </div> 
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                
                                
                            </div> 
                            <div class="modal-footer">
                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                <button type="submit" name="salvar_pagamento" class="btn green">Salvar</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </form>				
                    
                </div>
                <!-- /.modal-dialog -->
            </div>
            
            <!-- /.MODAL NOVO PAGAMENTO FIM -->		
            
            
            
            <!-- /.MODAL EDITAR PAGAMENTO INICIO -->                                                                                                                            

            <div class="modal fade" id="modal_editarpagamento" tabindex="-1" role="basic" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Editar Pagamento</h4>
                        </div>
                        <div class="modal-body"> 
                           
                           <!-- BEGIN FORM-->
                           <form action="../movimentacoes/contasapagar/editarcontasapagar" name="editar_despesa" id="editar_despesa" enctype="multipart/form-data" method="post" class="horizontal-form">
                            <div class="form-body">
                              <input type="hidden" required id="ideditardespesa" name="ideditardespesa" value=""> 
                              <input type="hidden" required id="relacionamento_despesa_edicao" name="relacionamento_despesa_edicao" value=""> 
                              <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Descrição</label>
                                        <input  type="text" required id="editar_descricao_despesa" name="editar_descricao_despesa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <!--/row-->
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Valor(R$)</label>
                                    <input onKeyPress="return(MascaraMoeda(this,'.',',',event))" type="text" id="editar_valor_despesa" required name="editar_valor_despesa" class="form-control" placeholder="" >
                                    
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Data</label>
                                    <input type="date" id="editar_data_despesa" name="editar_data_despesa" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                        <div class="row">
                           
                         
                           
                           <?php  include '../movimentacoes/verificar_conta.php';  ?>		
                           <div class="col-md-6">   
                             <div class="form-group">
                                <label class="control-label">Conta</label>
                                <select type="text" id="editar_conta_despesa" name="editar_conta_despesa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                 <option value=""></option>
                                 <?php while ($conta_principallista = $conta_principal->fetch_assoc()): ?>
                                     
                                   <option value="<?php echo $conta_principallista['id'].'|'.$conta_principallista['descricao'] ?>"><?php echo $conta_principallista['descricao']?></option>
                               <?php endwhile; ?>
                               
                               
                           </select>
                           
                       </div>
                   </div>		
                   
                   
                   <?php  include '../movimentacoes/verificar_categorias.php';  ?>	
                   <div class="col-md-6">   
                     <div class="form-group">
                        <label class="control-label">Categoria</label>
                        <select type="text" id="editar_categoria_despesa" name="editar_categoria_despesa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                            <option value=""></option>
                            <?php echo $categoriadespesa;  ?>
                            
                        </select>
                        
                    </div>
                </div>	
                
            </div>
            
            
            
            <!--/row-->
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Parcelas</label>
                        <input disabled id="editar_parcelas_despesa" name="editar_parcelas_despesa" required type="number" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
                    </div>
                    <div class="col-md-6">   
                     <div class="form-group">
                        <label class="control-label">Status</label>
                        <select type="text" id="editar_status_despesa" name="editar_status_despesa" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                            <option value="Pago">Pago</option>
                            <option value="A Pagar">A Pagar</option>
                            
                        </select>
                        
                    </div>
                </div>	
                
            </div>
        </div>
        
        
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
        <button type="submit" name="editar_salvar_pagamento" class="btn green">Salvar</button>
        <button type="submit" name="excluir_contasapagar_edicao" class="btn red">Excluir</button>  
    </div>
</div>
<!-- /.modal-content -->
</form>				
</div>
<!-- /.modal-dialog -->
</div>

<!-- /.MODAL EDITAR PAGAMENTO FIM -->						




</div>




<div class="tab-pane fade" id="tabrecebimentos">
  <!-- INICIO DATATABLE RECEBIMENTO-->
  <div class="portlet light ">
   
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novorecebimento"> Novo Recebimento
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-print"></i> Print </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="novorecebimento">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th> Data </th>
                                <th> Descrição </th>
                                <th> Status </th>
                                <th>Valor</th>
                                <th> Categoria </th>
                                <th> Ações </th>
                            </tr>
                        </thead>
                        <tbody>
                            
                           <?php  include '../movimentacoes/verificar_receita.php';  ?>
                           
                           <?php while ($dados_contasarececeberlista = $dados_contasarececeber->fetch_assoc()): ?> 
                               
                            <tr class="odd gradeX">
                                <td data-idreceita="<?php echo $dados_contasarececeberlista['id'] ?>" >
                                  <?php echo $dados_contasarececeberlista['id']; ?>
                              </td>
                              <td data-relacionamento_receita_edicao="<?php echo $dados_contasarececeberlista['relacionamento'] ?>" data-vencimento_receita="<?php echo $dados_contasarececeberlista['data_vencimento'] ?>" > <?php echo $dados_contasarececeberlista['data_vencimento']; ?> </td>
                              <td data-descricao_receita="<?php echo $dados_contasarececeberlista['descricao'] ?>" >
                                 <?php echo $dados_contasarececeberlista['descricao']; ?> 
                             </td>
                             <td data-status_receita="<?php echo $dados_contasarececeberlista['status'] ?>" data-parcelas_receita="<?php echo $dados_contasarececeberlista['parcelas'] ?>"  >
                                <span class="label label-sm label-info"> <?php echo $dados_contasarececeberlista['status']; ?> </span>
                            </td>
                            <td data-valor_receita="<?php echo number_format($dados_contasarececeberlista['valor_bruto'], 2, ',', '.'); ?>" class="center"> <?php echo number_format($dados_contasarececeberlista['valor_bruto'], 2, ',', '.'); ?>		 </td>
                            
                            <td data-categoria_receita="<?php echo $dados_contasarececeberlista['grupo_categoria'].'|'.$dados_contasarececeberlista['FK_categoria_id'].'|'.$dados_contasarececeberlista['categoria'] ?>" data-cliente_receita_edicao="<?php echo $dados_contasarececeberlista['fk_id_cliente'].'|'.$dados_contasarececeberlista['cliente'] ?>" data-conta_receita="<?php echo $dados_contasarececeberlista['fk_conta_id'].'|'.$dados_contasarececeberlista['conta'] ?>"  class="center"> <?php echo $dados_contasarececeberlista['categoria']; ?> </td>
                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a class="cliqueparaeditarreceita" data-toggle="modal" href="#modal_editarrecebimento">
                                                <i class="icon-docs"></i> Editar </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="icon-tag"></i> Enviar Lembrete de Pagamento </a>
                                                </li>
                                                
                                                
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                
                            <?php endwhile; ?>     
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- FIM DATATABLE RECEBIMENTO-->
            
            <!-- /.MODAL NOVO RECEBIMENTO INICIO -->                                                                                                                            

            <div class="modal fade" id="modal_novorecebimento" tabindex="-1" role="basic" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Novo Recebimento</h4>
                        </div>
                        <div class="modal-body"> 
                           
                           <!-- BEGIN FORM-->
                           <form action="../movimentacoes/contasareceber/nova_receita" name="form_novareceita" id="form_novareceita" enctype="multipart/form-data" method="post" class="horizontal-form">
                            <div class="form-body">
                               
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Descrição</label>
                                            <input  type="text" required id="descricao_receita" name="descricao_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                <!--/row-->
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Valor(R$)</label>
                                        <input onKeyPress="return(MascaraMoeda(this,'.',',',event))" type="text" id="valor_receita" required name="valor_receita" class="form-control" placeholder="" >
                                        
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Data</label>
                                        <input type="date" id="data_receita" name="data_receita" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <div class="row">
                               
                                <?php  include '../movimentacoes/verificar_conta.php';  ?>		
                                <div class="col-md-6">   
                                 <div class="form-group">
                                    <label class="control-label">Conta</label>
                                    <select type="text" id="conta_receita" name="conta_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                     <option value=""></option>
                                     <?php while ($conta_principallista = $conta_principal->fetch_assoc()): ?>
                                         
                                       <option value="<?php echo $conta_principallista['id'].'|'.$conta_principallista['descricao'] ?>"><?php echo $conta_principallista['descricao']?></option>
                                   <?php endwhile; ?>
                                   
                                   
                               </select>
                               
                           </div>
                       </div>			
                       <?php  include '../movimentacoes/verificar_categorias.php';  ?>	
                       <div class="col-md-6">   
                         <div class="form-group">
                            <label class="control-label">Categoria</label>
                            <select type="text" id="categoria_receita" name="categoria_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                <option value=""></option>
                                <?php echo $categoriadespesa;  ?>
                                
                            </select>
                            
                        </div>
                    </div>	
                    
                </div>
                
                
                
                <!--/row-->
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Parcelas</label>
                            <input value="1" id="parcelas_receita" name="parcelas_receita" required type="number" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
                        </div>
                        
                        <div class="col-md-4">
                           <div class="form-group">
                            <label class="control-label">Status</label>
                            <select type="text" id="status_receita" name="status_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                <option value="Pago">Pago</option>
                                <option value="A Pagar">A Pagar</option>
                                
                            </select>
                            
                        </div> 
                    </div>
                    
                </div>
                <?php  include '../calendario/verificarclientes.php';  ?>
                <div class="row">		
                 <div class="col-md-4">
                  <div class="form-group">
                      <label class="control-label">Cliente</label>	
                      <select  name="contasareceber_cliente" id="contasareceber_cliente" class="optional form-control col-md-7 col-xs-12" onkeypress="return handleEnter(this, event)">
                          <option value="" ></option> 
                          <?php while ($clientelista = $cliente->fetch_assoc()):   ?>	
                              
                              <option value="<?php echo $clientelista['id'].'|'.$clientelista['nome']; ?>" ><?php echo $clientelista['nome']; ?></option>   
                              
                          <?php endwhile; ?>	
                          
                      </select>  
                      
                      
                  </div>	
              </div>
          </div>	
          
          
          
      </div>
      
      
  </div> 
  <div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
    <button type="submit" name="salvar_receita" class="btn green">Salvar</button>
</div>
</div>
<!-- /.modal-content -->
</form>			

</div>
<!-- /.modal-dialog -->
</div>

<!-- /.MODAL NOVO RECEBIMENTO FIM -->		

<!-- /.MODAL EDITAR RECEBIMENTO INICIO -->                                                                                                                            

<div class="modal fade" id="modal_editarrecebimento" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Editar Recebimento</h4>
            </div>
            <div class="modal-body"> 
             
               <!-- BEGIN FORM-->
               <form action="../movimentacoes/contasareceber/editarcontasareceber" name="form_editar_recebimento" id="form_editar_recebimento" enctype="multipart/form-data" method="post" class="horizontal-form">
                <div class="form-body">
                    <input type="hidden" required id="ideditarreceita" name="ideditarreceita" value="">  
                    <input type="hidden" required id="relacionamento_receita_edicao" name="relacionamento_receita_edicao" value=""> 	
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Descrição</label>
                                <input  type="text" required id="editar_descricao_receita" name="editar_descricao_receita" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                
                            </div>
                        </div>
                        
                    </div>
                    <!--/row-->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Valor(R$)</label>
                            <input onKeyPress="return(MascaraMoeda(this,'.',',',event))" type="text" id="editar_valor_recebimento" required name="editar_valor_recebimento" class="form-control" placeholder="" >
                            
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Data</label>
                            <input type="date" id="editar_data_recebimento" name="editar_data_recebimento" class="form-control" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                            
                        </div>
                    </div>
                </div>
                <!--/row-->
                <div class="row">
                   
                 
                   
                   <?php  include '../movimentacoes/verificar_conta.php';  ?>		
                   <div class="col-md-6">   
                     <div class="form-group">
                        <label class="control-label">Conta</label>
                        <select type="text" id="editar_conta_recebimento" name="editar_conta_recebimento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                         <option value=""></option>
                         <?php while ($conta_principallista = $conta_principal->fetch_assoc()): ?>
                             
                           <option value="<?php echo $conta_principallista['id'].'|'.$conta_principallista['descricao'] ?>"><?php echo $conta_principallista['descricao']?></option>
                       <?php endwhile; ?>
                       
                       
                   </select>
                   
               </div>
           </div>		
           <?php  include '../movimentacoes/verificar_categorias.php';  ?>	
           <div class="col-md-6">   
             <div class="form-group">
                <label class="control-label">Categoria</label>
                <select type="text" id="editar_categoria_recebimento" name="editar_categoria_recebimento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                    <option value=""></option>
                    <?php echo $categoriadespesa;  ?>
                    
                </select>
                
            </div>
        </div>	
        
    </div>



    <!--/row-->
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label>Parcelas</label>
                <input id="editar_parcelas_recebimento" disabled name="editar_parcelas_recebimento" required type="number" class="form-control" onkeypress="return handleEnter(this, event)"> </div>
            </div>
            
            <div class="col-md-4">
               <div class="form-group">
                <label class="control-label">Status</label>
                <select type="text" id="editar_status_recebimento" name="editar_status_recebimento" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                    <option value="Pago">Pago</option>
                    <option value="A Pagar">A Pagar</option>
                    
                </select>
                
            </div> 
        </div>
    </div>
    <?php  include '../calendario/verificarclientes.php';  ?>		
    <div class="row">		
     <div class="col-md-4">
      <div class="form-group">
          <label class="control-label">Cliente</label>	
          <select  name="contasareceber_cliente_edicao" id="contasareceber_cliente_edicao" class="optional form-control col-md-7 col-xs-12" onkeypress="return handleEnter(this, event)">
              <option value="" ></option> 
              <?php while ($clientelista = $cliente->fetch_assoc()):   ?>	
                  
                  <option value="<?php echo $clientelista['id'].'|'.$clientelista['nome']; ?>" ><?php echo $clientelista['nome']; ?></option>   
                  
              <?php endwhile; ?>	
              
          </select>  
          
          
      </div>	
  </div>
</div>	


</div>


</div> 
<div class="modal-footer">
    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
    <button type="submit" name="editar_salvar_recebimento" class="btn green">Salvar</button>
    <button type="submit" name="excluir_contasareceber_edicao" class="btn red">Excluir</button>
</div>
</div>
<!-- /.modal-content -->
</form>				
</div>
<!-- /.modal-dialog -->
</div>

<!-- /.MODAL EDITAR RECEBIMENTO FIM -->								




</div>



</div>
<div class="clearfix margin-bottom-20"> </div>


</div>
</div>  


<!-- TAB MOVIMENTACOES FIM -->	                    



</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
</div>
</div>
<div class="page-wrapper-row">
    <div class="page-wrapper-bottom">
        
    </div>
</div>
</div>
        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>
<script src="js/jquery.table2excel.min.js"></script> 


<script type="text/javascript">
  
 $(document).ready(function() {
        
      var nivelacesso =  "<?php echo $_SESSION['nivel']; ?>";

      if((nivelacesso == '3') || (nivelacesso == '4') || (nivelacesso == '2')){
         
            $('#config_menu').hide();
            $('#div_todo_conteudo').hide();
        alert('Seu Login não da acesso a essa área!');

      };
        
        
        
        
      });

  
</script>

<!-- END PAGE LEVEL SCRIPTS -->
<!-- ASSETS DATATABLE-->




<!-- dados despesa para edição  -->
<script>
   

   
   $(function() {
       
    
      
       
      $('#novopagamento tbody').on('click', '.cliqueparaeditardespesa', function () {
        
        
          

         var id_despesa = $(this).closest('tr').find('td[data-id_despesa]').data('id_despesa');
         document.getElementById('ideditardespesa').value = id_despesa ;
         
         var vencimento_despesa  = $(this).closest('tr').find('td[data-vencimento_despesa]').data('vencimento_despesa');
         document.getElementById('editar_data_despesa').value = vencimento_despesa ;
         
         var descricao_despesa = $(this).closest('tr').find('td[data-descricao_despesa]').data('descricao_despesa');
         document.getElementById('editar_descricao_despesa').value = descricao_despesa ;
         
         var status_despesa = $(this).closest('tr').find('td[data-status_despesa]').data('status_despesa');
         document.getElementById('editar_status_despesa').value = status_despesa ;
         
         var valor_despesa = $(this).closest('tr').find('td[data-valor_despesa]').data('valor_despesa');
         document.getElementById('editar_valor_despesa').value = valor_despesa ;
         
         var conta_despesa = $(this).closest('tr').find('td[data-conta_despesa]').data('conta_despesa');
         document.getElementById('editar_conta_despesa').value = conta_despesa ;
         
         var categoria_despesa = $(this).closest('tr').find('td[data-categoria_despesa]').data('categoria_despesa');
         document.getElementById('editar_categoria_despesa').value = categoria_despesa ;
         
         var parcelas_despesas = $(this).closest('tr').find('td[data-parcelas_despesas]').data('parcelas_despesas');
         document.getElementById('editar_parcelas_despesa').value = parcelas_despesas;
         
         var relacionamento_despesa_edicao = $(this).closest('tr').find('td[data-relacionamento_despesa_edicao]').data('relacionamento_despesa_edicao');
         document.getElementById('relacionamento_despesa_edicao').value = relacionamento_despesa_edicao;	
         

         
         
     });
  });
</script>


<!-- dados despesa para edição  -->							

<!-- ****************************************************************************************************************************************** -->	

<!-- dados rendimento para edição  -->
<script>
   

   
   $(function() {
       
    
      
      $('#novorecebimento tbody').on('click', '.cliqueparaeditarreceita', function () {
        

          

         var status_receita  = $(this).closest('tr').find('td[data-status_receita]').data('status_receita');
         document.getElementById('editar_status_recebimento').value = status_receita ;					
         
         var parcelas_receita = $(this).closest('tr').find('td[data-parcelas_receita]').data('parcelas_receita');
         document.getElementById('editar_parcelas_recebimento').value = parcelas_receita ;
         
         var categoria_receita = $(this).closest('tr').find('td[data-categoria_receita]').data('categoria_receita');
         document.getElementById('editar_categoria_recebimento').value = categoria_receita ;
         
         var vencimento_receita = $(this).closest('tr').find('td[data-vencimento_receita]').data('vencimento_receita');					
         document.getElementById('editar_data_recebimento').value = vencimento_receita ;
         
         var valor_receita = $(this).closest('tr').find('td[data-valor_receita]').data('valor_receita');
         document.getElementById('editar_valor_recebimento').value = valor_receita ;
         
         var descricao_receita = $(this).closest('tr').find('td[data-descricao_receita]').data('descricao_receita');				
         document.getElementById('editar_descricao_receita').value = descricao_receita ;
         
         var idreceita = $(this).closest('tr').find('td[data-idreceita]').data('idreceita');
         document.getElementById('ideditarreceita').value = idreceita ;
         
         
         var relacionamento_receita_edicao = $(this).closest('tr').find('td[data-relacionamento_receita_edicao]').data('relacionamento_receita_edicao');
         document.getElementById('relacionamento_receita_edicao').value = relacionamento_receita_edicao ;	
         
         var editar_conta_recebimento = $(this).closest('tr').find('td[data-conta_receita]').data('conta_receita');
         document.getElementById('editar_conta_recebimento').value = editar_conta_recebimento ;
         
         var cliente_receita_edicao = $(this).closest('tr').find('td[data-cliente_receita_edicao]').data('cliente_receita_edicao');
         document.getElementById('contasareceber_cliente_edicao').value = cliente_receita_edicao ;	

         
     });
  });
</script>


<!-- dados rendimento para edição  -->			

<!-- ****************************************************************************************************************************************** -->
<!-- MASCARA MOEDA -->

<script language="javascript">  

 function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){  
     var sep = 0;  
     var key = '';  
     var i = j = 0;  
     var len = len2 = 0;  
     var strCheck = '0123456789';  
     var aux = aux2 = '';  
     var whichCode = (window.Event) ? e.which : e.keyCode;  
     if (whichCode == 13 || whichCode == 8) return true;  
     key = String.fromCharCode(whichCode); // Valor para o código da Chave  
     if (strCheck.indexOf(key) == -1) return false; // Chave inválida  
     len = objTextBox.value.length;  
     for(i = 0; i < len; i++)  
         if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;  
     aux = '';  
     for(; i < len; i++)  
         if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);  
     aux += key;  
     len = aux.length;  
     if (len == 0) objTextBox.value = '';  
     if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;  
     if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;  
     if (len > 2) {  
         aux2 = '';  
         for (j = 0, i = len - 3; i >= 0; i--) {  
             if (j == 3) {  
                 aux2 += SeparadorMilesimo;  
                 j = 0;  
             }  
             aux2 += aux.charAt(i);  
             j++;  
         }  
         objTextBox.value = '';  
         len2 = aux2.length;  
         for (i = len2 - 1; i >= 0; i--)  
             objTextBox.value += aux2.charAt(i);  
         objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);  
     }  
     return false;  
 }  
</script>

<!-- ****************************************************************************************************************************************** -->						


<!-- ****************************************************************************************************************************************** -->			

<script>

	$("#exportar_pagamentos").click(function(e) {
       $("#novopagamento").table2excel({
         exclude: ".noExl",
         name: "Excel Document Name",
         filename: "Lista Pagamentos",
         fileext: ".xls",
         exclude_img: true,
         exclude_links: true,
         exclude_inputs: true
     });
   });
   
</script>							



</body>

</html>