<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente 

if(empty($_SESSION['user_email'])){header("Location: login");}; 
if($_SESSION['tipo_login'] <> '1'){header("Location: clientes");}; 

?>
<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Cadastros</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

  <!-- END GLOBAL MANDATORY STYLES -->

  <link href="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->

  <link href="../../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />


  <!-- GRAFICO LEVEL PLUGINS -->

  <link href="../../assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

  <!-- GRAFICO LEVEL PLUGINS -->

  <link rel="shortcut icon" href="favicon.ico" /> </head>
  <!-- END HEAD -->

   <!-- Pular input no enter -->
  <script type="text/javascript">
    function handleEnter(field, event) {
     var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
     if (keyCode == 13) {
      var i;
      for (i = 0; i < field.form.elements.length; i++)
       if (field == field.form.elements[i])
        break;
      i = (i + 1) % field.form.elements.length;
      field.form.elements[i].focus();
      return false;
    } else
    return true;
  }
</script>

  <body class="page-container-bg-solid">

    <div class="page-wrapper">
      <div class="page-wrapper-row">
        <div class="page-wrapper-top">
          <!-- BEGIN HEADER -->
          <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
              <div class="container">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                  <a href="#">
                    <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">  </a>
                  </div>
                  <!-- END LOGO -->
                  <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                  <a href="javascript:;" class="menu-toggler"></a>
                  <!-- END RESPONSIVE MENU TOGGLER -->
                  <!-- BEGIN TOP NAVIGATION MENU -->
                  <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">








                      <!-- BEGIN USER LOGIN DROPDOWN -->
                      <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                         <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };  ?>

                         <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                         <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                       </a>
                       <ul class="dropdown-menu dropdown-menu-default">
                        <li id="config_menu">
                          <a href="configuracoes">
                          Configurações</a>
                        </li>

                        <li class="divider"> </li>
                        
                        <li>
                          <a href="../login/logout">
                            <i class="icon-key"></i> Sair </a>
                          </li>

                        </ul>
                      </li>
                      <!-- END USER LOGIN DROPDOWN -->


                    </ul>
                  </div>
                  <!-- END TOP NAVIGATION MENU -->
                </div>
              </div>
              <!-- END HEADER TOP -->
              <!-- BEGIN HEADER MENU -->
              <div class="page-header-menu">
                <div class="container" >

                  <!-- END HEADER SEARCH BOX -->
                  <!-- BEGIN MEGA MENU -->
                  <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                  <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                  <div class="hor-menu "  >
                  <ul class="nav navbar-nav" >
                    <li id="menu_visao_geral" class="menu-dropdown classic-menu-dropdown   ">
                      <a  href="visaogeral"> Visão Geral

                      </a>

                    </li>
                    <li id="menu_agenda" class="menu-dropdown mega-menu-dropdown">
                      <a  href="calendario"> Agenda

                      </a>

                    </li>

                    <li id="menu_clientes" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                      <a  href="clientes"> Clientes

                      </a>

                    </li>
                    <li id="menu_os" class="menu-dropdown mega-menu-dropdown  ">
                     <a  href="ordem_servico"> Ordem de Serviço

                     </a>

                   </li>



                   <li id="menu_estoque" class="menu-dropdown mega-menu-dropdown active ">
                    <a href="cadastro_produtos"> Estoque

                    </a>

                  </li>





                  <li id="menu_movimentacoes" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="movimentacoes"> Movimentações

                    </a>

                  </li>

                  <li id="menu_relatorios" class="menu-dropdown classic-menu-dropdown ">
                    <a href="relatorios"> Relatórios

                    </a>

                  </li>


                  <li id="menu_cadastros" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"> Cadastros
                      <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                     <li class=" ">
                      <a href="cadastro_fornecedores" class="nav-link  ">
                      Fornecedores</a>
                    </li>
                    <li class=" ">
                      <a href="cadastro_servicos" class="nav-link  ">
                       Categorias Produtos/Serviços

                     </a>
                   </li>
                   <li class=" ">
                    <a href="cadastro_laboratorio" class="nav-link  ">
                      Laboratórios

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_medicos" class="nav-link  ">
                      Médicos

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_indicacao" class="nav-link  ">
                      Indicações

                    </a>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
            <!-- END MEGA MENU -->
          </div>
        </div>
        <!-- END HEADER MENU -->
      </div>
      <!-- END HEADER -->
    </div>
  </div>
  <div id="div_todo_conteudo" class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
      <!-- BEGIN CONTAINER -->
      <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
          <!-- BEGIN CONTENT BODY -->
          <!-- BEGIN PAGE HEAD-->
          <div class="page-head">
            <div class="container">
              <!-- BEGIN PAGE TITLE -->
              <div class="page-title">
                <h1> </h1>
              </div>
              <!-- END PAGE TITLE -->

            </div>
          </div>
          <!-- END PAGE HEAD-->
          <!-- BEGIN PAGE CONTENT BODY -->
          <div class="page-content">
            <div class="container">


              <!-- TAB GERAL INICIO -->	

              <div class="portlet light ">

                <div class="portlet-body">
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a href="#tabprodutos" data-toggle="tab"> Lista </a>
                    </li>
                    <li style="display:none">
                      <a href="#taboutros" data-toggle="tab"> Outros </a>
                    </li>


                  </ul> 
                  <div class="tab-content">


                    <!-- TAB FUNDIDOS FIM -->	 							
                    <div class="tab-pane fade active in" id="tabprodutos">

                     <br>

                     <div class="portlet light ">

                      <div class="portlet-body">
                        <div class="table-toolbar">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="btn-group">
                                <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#modal_novoproduto"> Novo
                                  <i class="fa fa-plus"></i>
                                </a>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="btn-group pull-right">
                                <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Ferramentas
                                  <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">

                                  <li>
                                    <a id="exportar_prods">
                                      <i class="fa fa-file-excel-o"></i> Exportar Excel </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                          <table class="table table-striped table-bordered table-hover table-checkable order-column" id="produtos">
                            <thead>
                              <tr>
                                <th> ID </th>
                                <th> Foto </th>
                                <th> Código </th>
                                <th> Descrição </th>
                                <th> Estoque </th>
                                <th> Ações</th>

                              </tr>
                            </thead>
                            <tbody>


                              <?php include '../produtos/verificar_produtos.php'; ?>

                              <?php while ($select_prodslista = $select_prods->fetch_assoc()): ?>

                                <tr class="odd gradeX">


                                  <td data-descontar_estoque ="<?php echo $select_prodslista['descontar_estoque']; ?>" data-grife ="<?php echo $select_prodslista['grife']; ?>" data-unidade ="<?php echo $select_prodslista['unidade']; ?>" data-id_prod ="<?php echo $select_prodslista['id']; ?>" data-codigo_barras ="<?php echo $select_prodslista['codigo_barras']; ?>" data-categoria_prod ="<?php echo $select_prodslista['id_cat_produto'].'|'.$select_prodslista['categoria_prod'].'|'.number_format($select_prodslista['valor_cat_prod'], 2, ',', '.'); ?>" >  <?php echo $select_prodslista['id']; ?>  </td> 
                                  <td align="center"> <img style="width: 125px; height: 75px;" src="<?php if(!empty($select_prodslista['foto'])){ echo '../'.$select_prodslista['foto'];}else{ echo 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image';} ?>" /> </td>
                                  <td align="center"><?php echo $select_prodslista['codigo_peca']; ?></td>
                                  <td data-desc_prod ="<?php echo $select_prodslista['desc_prod']; ?>" data-valor_custo ="<?php echo number_format($select_prodslista['valor_custo'], 2, ',', '.'); ?>" data-valor_venda ="<?php echo number_format($select_prodslista['valor_venda'], 2, ',', '.'); ?>">  <?php echo $select_prodslista['desc_prod']; ?>  </td>
                                  <td align="center" data-foto="<?php echo $select_prodslista['foto']; ?>" data-data_entrada="<?php echo $select_prodslista['data_entrada']; ?>" data-codigo_peca ="<?php echo $select_prodslista['codigo_peca']; ?>" data-fornecedor ="<?php echo $select_prodslista['fk_id_fornecedor'].'|'.$select_prodslista['fornecedor']; ?>" data-quant_est ="<?php echo $select_prodslista['quant_est']; ?>" data-min_est ="<?php echo $select_prodslista['min_est']; ?>" data-max_est ="<?php echo $select_prodslista['max_est']; ?>" >  <?php echo $select_prodslista['quant_est']; ?>  </td>


                                  <td align="center" >
                                    <div class="btn-group">
                                      <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                        <i class="fa fa-angle-down"></i>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <li>
                                          <a class="cliqueparaeditarproduto"  data-toggle="modal" href="#modal_editarproduto">
                                            <i class="icon-docs"></i> Editar </a>
                                          </li>

                                          <li>
                                            <a class="cliqueparatransferirproduto"  data-toggle="modal" href="#modal_transproduto">
                                              <i class="icon-docs"></i> Transferência de Loja </a>
                                            </li>




                                          </ul>
                                        </div>
                                      </td>


                                    </tr>

                                  <?php endwhile; ?>




                                </tbody>
                              </table>
                            </div>
                          </div>

                          <!-- TAB FUNDIDOS FIM -->	 		

                          <!-- /.MODAL ENVIAR LEMBRETE AGENDAMENTO -->      
                          <div class="modal fade bs-modal-lg" id="modal_transproduto" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                  <h4 class="modal-title">Transferência Loja</h4>
                                </div>
                                <div class="modal-body"> 


                                 <!-- TAB CLIENTES INICIO --> 

                                 <div class="portlet light ">

                                  <div class="portlet-body">


                                    <form action="../produtos/transf_produto/transf_prod.php" enctype="multipart/form-data" method="post" class="horizontal-form">

                                     <div class="form-body">

                                      <div class="row">


                                        <input type="hidden" required  id="id_produto_trans" name="id_produto_trans" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label class="control-label">Produto </label>
                                            <input type="text" required readonly="" id="produto_trans" name="produto_trans" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                          </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-3">
                                          <div class="form-group">
                                            <label class="control-label">Quantidade</label>
                                            <input type="number" required id="quant_trans" name="quant_trans" class="form-control" onkeypress="return handleEnter(this, event)">

                                          </div>
                                        </div>
                                        <!--/span-->
                                      </div>


                                      <hr>


                                      <?php include '../configuracoes/empresa/verificar_empresas.php' ?> 
                                      <div class="row">
                                        <div class="col-md-12">  
                                         <div class="form-group">
                                           <label class="control-label">Loja Destino</label>
                                           <select type="text" id="loja_destino" required name="loja_destino" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">
                                             <option value="" ></option>

                                             <?php while ($empresaslista = $empresas->fetch_assoc()): ?>

                                               <option value="<?php echo $empresaslista['id'].'|'.$empresaslista['razao_social'] ?>"><?php echo $empresaslista['razao_social'] ?></option>

                                             <?php endwhile; ?>

                                           </select>
                                         </div>
                                       </div>

                                       
                                        <div class="col-md-12">  
                                          <div class="form-group">
                                            <label class="control-label">Produto Loja Destino</label>
                                            <select type="text" id="prod_loja_destino" required name="prod_loja_destino" class="form-control select2-allow-clear" placeholder="" onkeypress="return handleEnter(this, event)">
                                              <option value="" ></option>


                                            </select>
                                          </div>
                                        </div>

                                      </div>  

                                     



                                  </div>

                                  <div class="modal-footer">
                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                    <button type="submit" name="trans_prd" class="btn green">Transferir</button>
                                  </div>

                                </form>




                              </div>
                            </div>  


                            <!-- TAB CORPO  FIM -->




                          </div>

                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.MODAL TRANSFERENCIA -->  






                    <!-- MODAL NOVO FUNDIDO INCIO -->	

                    <div class="modal fade bs-modal-lg" id="modal_novoproduto"  role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Novo Produto/Serviço</h4>
                          </div>
                          <div class="modal-body"> 

                           <!-- BEGIN FORM-->
                           <form action="../produtos/novo_prod.php" enctype="multipart/form-data" method="post" class="horizontal-form">
                            <div class="form-body">
                              <h3 class="form-section">   </h3>




                              <div class="row">

                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="control-label">Nome Produto/Serviço</label>
                                    <input  type="text" required id="nome_produto" name="nome_produto" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                  </div>
                                </div>
                                <!--/span-->


                              </div>
                              <!--/span-->

                              <div class="row">

                                  <div class="col-md-3">
                                <div class="form-group">
                                  <label class="control-label">Código Peça</label>
                                  <input  type="text"  id="cod_peca" name="cod_peca" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </div>
                              </div>

                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label class="control-label">Código Barras</label>
                                    <input  type="text" id="codigo_barras" name="codigo_barras" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                  </div>
                                </div>

                                <?php include '../produtos/verificar_categoria_produtos.php' ?> 
                                <div class="col-md-4">
                                  <div class="form-group">
                                    <label class="control-label">Categoria</label>
                                    <select id="categoria_produto" name="categoria_produto" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                                     <option value=""></option>
                                     <?php while ($select_go_cad_catlista = $select_go_cad_cat->fetch_assoc()): ?>

                                       <option value="<?php echo $select_go_cad_catlista['id'].'|'.$select_go_cad_catlista['descricao'].'|'.number_format($select_go_cad_catlista['valor_cobrado'], 2, ',', '.') ?>"><?php echo $select_go_cad_catlista['descricao'] ?></option>
                                     <?php endwhile; ?>
                                   </select>
                                 </div>
                               </div>
                             </div>  

                             <div class="row">

                              <div class="col-md-4">
                                <div class="form-group">
                                  <label class="control-label">Valor Custo R$</label>
                                  <input  type="text" id="valor_custo" name="valor_custo" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </div>
                              </div>

                              <div class="col-md-4">
                                <div class="form-group">
                                  <label class="control-label">Valor Venda R$</label>
                                  <input  type="text" required id="valor_venda" name="valor_venda" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </div>
                              </div>
                            </div>

                            <div class="row">

                              <div class="col-md-4">
                                <div class="form-group">
                                  <label class="control-label">Disponível Estoque</label>
                                  <input  type="text" required  id="disponivel_estoque" name="disponivel_estoque" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </div>
                              </div>

                              <div class="col-md-4">
                                <div class="form-group">
                                  <label class="control-label">Minimo Estoque</label>
                                  <input  type="text"  id="minimo_estoque" name="minimo_estoque" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </div>
                              </div>

                              <div class="col-md-4">
                                <div class="form-group">
                                  <label class="control-label">Máximo Estoque</label>
                                  <input  type="text"  id="maximo_estoque" name="maximo_estoque" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </div>
                              </div>

                            </div>

                            <div class="row">

                              <div class="col-md-4">
                                <div class="form-group">
                                  <label class="control-label">Data Entrada</label>
                                  <input  type="date"  id="data_entrada" name="data_entrada" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </div>
                              </div>

                            

                              <div class="col-md-4">
                                <div class="form-group">
                                  <label class="control-label">Unidade</label>
                                  <input  type="text"  id="unidade" name="unidade" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </div>
                              </div>

                            </div>

                            <div class="row">

                              <div class="col-md-4">
                                <div class="form-group">
                                  <label class="control-label">Grife</label>
                                  <input  type="text"  id="grife" name="grife" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                </div>
                              </div>




                              <?php include '../produtos/verificar_fornecedor.php' ?> 
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label class="control-label">Fornecedor</label>
                                  <select id="fornecedor" name="fornecedor"  class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                                   <option value=""></option>
                                   <?php while ($select_go_cad_fornecedorlista = $select_go_cad_fornecedor->fetch_assoc()): ?>

                                     <option value="<?php echo $select_go_cad_fornecedorlista['id'].'|'.$select_go_cad_fornecedorlista['nome_fantasia'] ?>"><?php echo $select_go_cad_fornecedorlista['nome_fantasia'] ?></option>
                                   <?php endwhile; ?>
                                 </select>
                               </div>
                             </div>


                           </div>


                           <div class="row">
                             <div class="col-md-4">
                              <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                      <span class="btn default btn-file">
                                        <span class="fileinput-new"> Selecione Imagem </span>
                                        <span class="fileinput-exists"> Outra? </span>
                                        <input type="file" name="foto_prod"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                      </div>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                      <span class="label label-danger">NOTA! </span>
                                      <span>IMAGENS NO FORMATO JPEG! </span>
                                    </div>
                                  </div>

                                </div>

                              </div>   

                              <br>

                              <div class="row">   
                                <div class="col-md-6">
                                 <div class="form-group">

                                  <div class="input-group">
                                    <div class="icheck-list">
                                      <label>

                                        <input type="checkbox" id="desc_estoque" name="desc_estoque" class="icheck"> Descontar Estoque? </label>


                                      </div>
                                    </div>
                                  </div>
                                </div> 
                              </div>   




                              <br>
                              <hr>
                              <br>

                              <div class="row">

                                <div class="col-md-4">
                                  <div class="form-group">
                                    <label class="control-label">Nr. NF</label>
                                    <input  type="text"  id="nr_nf" name="nr_nf" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                                  </div>
                                </div>

                              </div>


                              <div class="row">
                                <div class="col-md-3">
                                 <div class="form-group">
                                   <a onclick="AddTableRowProdutoNF()" name="adicionar_produto" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                 </div>                  
                               </div>
                             </div>
                           </div>


                           <div class="row">     

                            <div class="col-md-12">
                             <!-- iniciando tabela area-->
                             <div class="portlet box blue">
                               <div class="portlet-title">
                                 <div class="caption">
                                  <i class="fa fa-product-hunt"></i>Lista NF </div>
                                  <div class="tools">
                                   <a href="javascript:;" class="collapse"> </a>

                                   <a href="javascript:;" class="reload"> </a>

                                 </div>
                               </div>
                               <div class="portlet-body">
                                 <div class="table-scrollable">
                                   <table id="tabela_produtosnf" class="table table-striped table-hover">
                                     <thead>
                                       <tr>
                                         <th> # </th>
                                         <th> Nr NF </th>


                                         <th>Ação</th>

                                       </tr>
                                     </thead>
                                     <tbody>

                                       <tr>

                                       </tr>


                                     </tbody>
                                   </table>
                                 </div>
                               </div>
                             </div>
                             <!-- fim tabela area-->

                             <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">

                                  <div id="div_lista_nf">



                                  </div>

                                </div>
                              </div>
                            </div>    


                          </div>

                        </div> 






                      </div>




                      <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                        <button type="submit" name="adicionar_produto" class="btn green">Adicionar</button>
                      </div>
                    </form>						

                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>



              <!-- MODAL NOVO PRODUTO  FIM -->			



              <!-- MODAL NOVO PRODUTO INCIO -->	

              <div class="modal fade bs-modal-lg" id="modal_editarproduto" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title">Editar Produto</h4>
                    </div>
                    <div class="modal-body"> 

                     <!-- BEGIN FORM-->
                     <form action="../produtos/update_prod.php" enctype="multipart/form-data" method="post" class="horizontal-form">
                      <div class="form-body">
                        <h3 class="form-section">   </h3>


                        <input  type="text" style="display:none" required id="editar_id_prod" name="editar_id_prod" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">





                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Nome Produto</label>
                              <input  type="text" required id="editar_nome_produto" name="editar_nome_produto" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                            </div>
                          </div>
                          <!--/span-->


                        </div>
                        <!--/span-->

                        <div class="row">

                           <div class="col-md-3">
                          <div class="form-group">
                            <label class="control-label">Código Peça</label>
                            <input  type="text"  id="editar_cod_peca" name="editar_cod_peca" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label class="control-label">Código Barras</label>
                              <input  type="text" id="editar_codigo_barras" name="editar_codigo_barras" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                            </div>
                          </div>

                          <?php include '../produtos/verificar_categoria_produtos.php' ?> 
                          <div class="col-md-4">
                            <div class="form-group">
                              <label class="control-label">Categoria</label>
                              <select id="editar_categoria_produto" name="editar_categoria_produto" class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                               <option value=""></option>
                               <?php while ($select_go_cad_catlista = $select_go_cad_cat->fetch_assoc()): ?>

                                 <option value="<?php echo $select_go_cad_catlista['id'].'|'.$select_go_cad_catlista['descricao'].'|'.number_format($select_go_cad_catlista['valor_cobrado'], 2, ',', '.') ?>"><?php echo $select_go_cad_catlista['descricao'] ?></option>
                               <?php endwhile; ?>
                             </select>
                           </div>
                         </div>
                       </div>  

                       <div class="row">

                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label">Valor Custo R$</label>
                            <input  type="text" id="editar_valor_custo" name="editar_valor_custo" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label">Valor Venda R$</label>
                            <input  type="text" required id="editar_valor_venda" name="editar_valor_venda" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>
                      </div>

                      <div class="row">

                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label">Disponível Estoque</label>
                            <input  type="text"  id="editar_disponivel_estoque" name="editar_disponivel_estoque" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label">Minimo Estoque</label>
                            <input  type="text"  id="editar_minimo_estoque" name="editar_minimo_estoque" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label">Máximo Estoque</label>
                            <input  type="text"  id="editar_maximo_estoque" name="editar_maximo_estoque" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>

                      </div>

                      <div class="row">

                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label">Data Entrada</label>
                            <input  type="date"  id="editar_data_entrada" name="editar_data_entrada" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>

                       

                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label">Unidade</label>
                            <input  type="text"  id="editar_unidade" name="editar_unidade" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>

                      </div>

                      <div class="row">

                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label">Grife</label>
                            <input  type="text"  id="editar_grife" name="editar_grife" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                          </div>
                        </div>




                        <?php include '../produtos/verificar_fornecedor.php' ?> 
                        <div class="col-md-4">
                          <div class="form-group">
                            <label class="control-label">Fornecedor</label>
                            <select id="editar_fornecedor" name="editar_fornecedor"  class="form-control select2-allow-clear" onkeypress="return handleEnter(this, event)"> 
                             <option value=""></option>
                             <?php while ($select_go_cad_fornecedorlista = $select_go_cad_fornecedor->fetch_assoc()): ?>

                               <option value="<?php echo $select_go_cad_fornecedorlista['id'].'|'.$select_go_cad_fornecedorlista['nome_fantasia'] ?>"><?php echo $select_go_cad_fornecedorlista['nome_fantasia'] ?></option>
                             <?php endwhile; ?>
                           </select>
                         </div>
                       </div>


                     </div>




                     <div class="row">
                       <div class="col-md-4">
                        <div class="form-group">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                              <img id="foto_produto_edicao" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                              <div>
                                <span class="btn default btn-file">
                                  <span class="fileinput-new"> Alterar Imagem </span>
                                  <span class="fileinput-exists"> Outra? </span>
                                  <input type="file" name="editar_foto_prod"> </span>
                                  <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                </div>
                              </div>
                              <div class="clearfix margin-top-10">
                                <span class="label label-danger">NOTA! </span>
                                <span>IMAGENS NO FORMATO JPEG! </span>
                              </div>
                            </div>

                          </div>

                        </div>   

                        <br>

                        <div class="row">   
                          <div class="col-md-6">
                           <div class="form-group">

                            <div class="input-group">
                              <div class="icheck-list">
                                <label>

                                  <input type="checkbox" id="editar_desc_estoque" name="editar_desc_estoque" class="icheck"> Descontar Estoque? </label>


                                </div>
                              </div>
                            </div>
                          </div> 
                        </div>   




                        <br>
                        <hr>
                        <br>

                        <div class="row">

                          <div class="col-md-4">
                            <div class="form-group">
                              <label class="control-label">Nr. NF</label>
                              <input  type="text" id="editar_nr_nf" name="editar_nr_nf" class="form-control" placeholder="" onkeypress="return handleEnter(this, event)">

                            </div>
                          </div>

                        </div>


                        <div class="row">
                          <div class="col-md-3">
                           <div class="form-group">
                             <a onclick="AddTableRowProdutoNF_Editar()" name="adicionar_produto" class="btn blue">  Adicionar  <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                           </div>                  
                         </div>
                       </div>
                     </div>


                     <div class="row">     

                      <div class="col-md-12">
                       <!-- iniciando tabela area-->
                       <div class="portlet box blue">
                         <div class="portlet-title">
                           <div class="caption">
                            <i class="fa fa-product-hunt"></i>Lista NF </div>
                            <div class="tools">
                             <a href="javascript:;" class="collapse"> </a>

                             <a href="javascript:;" class="reload"> </a>

                           </div>
                         </div>
                         <div class="portlet-body">
                           <div class="table-scrollable">

                            <div id="div_editar_lista_nfs"> </div>

                          </div>
                        </div>
                      </div>
                      <!-- fim tabela area-->

                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">

                            <div id="div_lista_nf">



                            </div>

                          </div>
                        </div>
                      </div>    


                    </div>

                  </div> 






                </div>






                <div class="modal-footer">
                  <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                  <button type="submit" name="salvar_produto" class="btn green">Salvar</button>
                  <button type="submit" name="excluir_produto" class="btn red">Excluir</button>
                </div>
              </form>						

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>



        <!-- MODAL EDITAR PRODUTO  FIM -->


      </div>

      <!-- TAB PRODUTO FIM -->				                 




      <!-- tab outros inicio -->												
      <div class="tab-pane fade" id="taboutros">
        <br>












      </div>
      <!-- tab outros fim -->







    </div>
    <div class="clearfix margin-bottom-20"> </div>


  </div>
</div>  


<!-- TAB GERAL FIM -->	


</div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
</div>
</div>
<div class="page-wrapper-row">
  <div class="page-wrapper-bottom">

  </div>
</div>
</div>

        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>

<script src="../../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="../../assets/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="../js/jquery.table2excel.min.js"></script> 
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="../js/mask_dinheiro/dist/jquery.mask.min.js" type="text/javascript"></script>
<script src="../produtos/lista_nf/adicionar_nf_produtos_lista.js" type="text/javascript"></script>
<script src="../produtos/lista_nf/adicionar_nf_produtos_lista_editar.js" type="text/javascript"></script>
<script src="../produtos/js/setar_valor_categoria.js" type="text/javascript"></script>
<script src="../produtos/transf_produto/produtos_loja_destino.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- DATATABLE -->
<script src="../../assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script>
<script src="../../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- DATATABLE -->

<script type="text/javascript">
  
 $(document).ready(function() {
        
      var nivelacesso =  "<?php echo $_SESSION['nivel']; ?>";

      if( (nivelacesso == '3') ){
         
            $('#config_menu').hide();
            $('.cliqueparaeditarproduto').hide();
            $('.cliqueparatransferirproduto').hide();
            $('#menu_visao_geral').hide();
          // $('#menu_agenda').hide();
          // $('#menu_clientes').hide();
      // $('#menu_os').hide();
      // $('#menu_estoque').hide();
       $('#menu_relatorios').hide();
       $('#menu_cadastros').hide();
       $('#menu_movimentacoes').hide();
      
      };
        
    
   if( (nivelacesso == '4') ){
         
            $('#config_menu').hide();
            $('#div_todo_conteudo').hide();
        alert('Seu Login não da acesso a essa área!');

      };      
        
        
      });

</script>


<!-- ****************************************************************************************************************************************** -->			


<!-- dados fundido para edição  -->
<script>




 $(function() {

   $('#produtos tbody').on('click', '.cliqueparaeditarproduto', function () {




     var id_prod = $(this).closest('tr').find('td[data-id_prod]').data('id_prod');
     document.getElementById('editar_id_prod').value = id_prod ;

     var desc_prod = $(this).closest('tr').find('td[data-desc_prod]').data('desc_prod');
     document.getElementById('editar_nome_produto').value = desc_prod ;

     var quant_est = $(this).closest('tr').find('td[data-quant_est]').data('quant_est');
     document.getElementById('editar_disponivel_estoque').value = quant_est ;

     var codigo_barras = $(this).closest('tr').find('td[data-codigo_barras]').data('codigo_barras');
     document.getElementById('editar_codigo_barras').value = codigo_barras ;

     var categoria_prod = $(this).closest('tr').find('td[data-categoria_prod]').data('categoria_prod');
     document.getElementById('editar_categoria_produto').value = categoria_prod ;

     var valor_custo = $(this).closest('tr').find('td[data-valor_custo]').data('valor_custo');
     document.getElementById('editar_valor_custo').value = valor_custo ;

     var valor_venda = $(this).closest('tr').find('td[data-valor_venda]').data('valor_venda');
     document.getElementById('editar_valor_venda').value = valor_venda ;

     var min_est = $(this).closest('tr').find('td[data-min_est]').data('min_est');
     document.getElementById('editar_minimo_estoque').value = min_est ;

     var max_est = $(this).closest('tr').find('td[data-max_est]').data('max_est');
     document.getElementById('editar_maximo_estoque').value = max_est ;

     var fornecedor = $(this).closest('tr').find('td[data-fornecedor]').data('fornecedor');
     document.getElementById('editar_fornecedor').value = fornecedor ;

     var unidade  = $(this).closest('tr').find('td[data-unidade]').data('unidade');
     document.getElementById('editar_unidade').value = unidade ;

     var grife = $(this).closest('tr').find('td[data-grife]').data('grife');
     document.getElementById('editar_grife').value = grife ;

     var data_entrada = $(this).closest('tr').find('td[data-data_entrada]').data('data_entrada');
     document.getElementById('editar_data_entrada').value = data_entrada ;
     
     var codigo_peca = $(this).closest('tr').find('td[data-codigo_peca]').data('codigo_peca');
     document.getElementById('editar_cod_peca').value = codigo_peca ;
     

     var descontar_estoque = $(this).closest('tr').find('td[data-descontar_estoque]').data('descontar_estoque');
     
     if (descontar_estoque === 1){

       $('#editar_desc_estoque')[0].checked = true;

     }


     $.post("../produtos/lista_nf/verificar_lista_nfs.php", {id_prodpost: id_prod},
       function(data){
        $("#div_editar_lista_nfs").empty();
              // Coloca as frases na DIV
              $("#div_editar_lista_nfs").append(data);
            }
            , "html");
     

     var foto = $(this).closest('tr').find('td[data-foto]').data('foto');
     $("#foto_produto_edicao").attr("src","../"+foto);

   });
 });
</script>


<!-- dados fundido para edição  -->							

<!-- ****************************************************************************************************************************************** -->			


<!-- ****************************************************************************************************************************************** -->     


<!-- dados produto trans para edição  -->
<script>




 $(function() {

   $('#produtos tbody').on('click', '.cliqueparatransferirproduto', function () {


     var id_prod = $(this).closest('tr').find('td[data-id_prod]').data('id_prod');
     document.getElementById('id_produto_trans').value = id_prod ;

     var desc_prod = $(this).closest('tr').find('td[data-desc_prod]').data('desc_prod');
     document.getElementById('produto_trans').value = desc_prod ;

     
 });

 });   
</script>


<!-- dados fundido para edição  -->             

<!-- ****************************************************************************************************************************************** -->

<script>
  $(function() {

   $('#valor_venda').mask('#.##0,00', {reverse: true});
   $('#valor_custo').mask('#.##0,00', {reverse: true});  
   $('#editar_valor_custo').mask('#.##0,00', {reverse: true});
   $('#editar_valor_venda').mask('#.##0,00', {reverse: true});   

 });


</script>


<!-- ****************************************************************************************************************************************** -->     

<script>

  $("#exportar_prods").click(function(e) {
    $("#produtos").table2excel({
     exclude: ".noExl",
     name: "Excel Document Name",
     filename: "Lista Produtos",
     fileext: ".xls",
     exclude_img: true,
     exclude_links: true,
     exclude_inputs: true
   });
  });

</script>   


<!-- ****************************************************************************************************************************************** -->     




</body>

</html>