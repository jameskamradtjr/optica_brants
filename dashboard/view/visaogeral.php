<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente 

if(empty($_SESSION['user_email'])){header("Location: login");}; 


?>
<!DOCTYPE html>

<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Visão Geral</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <link href="../../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />

  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="../../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="../../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="../../assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="../../assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="../../assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->

  <!-- GRAFICO LEVEL PLUGINS -->

  <link href="../../assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

  <!-- GRAFICO LEVEL PLUGINS -->

  <link rel="shortcut icon" href="favicon.ico" /> </head>
  <!-- END HEAD -->

  <body class="page-container-bg-solid">
    <div class="page-wrapper">
      <div class="page-wrapper-row">
        <div class="page-wrapper-top">
          <!-- BEGIN HEADER -->
          <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
              <div class="container">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                  <a href="#">
                   <img src="<?php  if (file_exists("../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg")){ echo "../docs_empresas/".$_SESSION['idadmin']."/logo/logo_sistema".$_SESSION['idadmin'].".jpg"; } else { echo '../../assets/layouts/layout3/img/logo-default.jpg'; }; ?>" alt="logo" class="logo-default">    </a>
                 </div>
                 <!-- END LOGO -->
                 <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                 <a href="javascript:;" class="menu-toggler"></a>
                 <!-- END RESPONSIVE MENU TOGGLER -->
                 <!-- BEGIN TOP NAVIGATION MENU -->
                 <div class="top-menu">
                  <ul class="nav navbar-nav pull-right">


                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                      <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                       <?php  if (empty($_SESSION['foto'])){ $foto='../img/user.jpg'; } else { $foto=$_SESSION['foto']; };	?>

                       <img alt="" class="img-circle" src=" <?php echo '../'.$foto ?>">
                       <span class="username username-hide-mobile"><?php echo $_SESSION['nome']; ?></span>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-default">
                      <li id="config_menu">
                        <a href="configuracoes">
                        Configurações</a>
                      </li>

                      <li class="divider"> </li>
                      
                      <li>
                        <a href="../login/logout">
                          <i class="icon-key"></i> Sair </a>
                        </li>

                      </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->


                  </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
              </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
              <div class="container" >

                <!-- END HEADER SEARCH BOX -->
                <!-- BEGIN MEGA MENU -->
                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                <div class="hor-menu "  >
                  <ul class="nav navbar-nav" >
                    <li id="menu_visao_geral" class="menu-dropdown classic-menu-dropdown active  ">
                      <a  href="visaogeral"> Visão Geral

                      </a>

                    </li>
                    <li id="menu_agenda" class="menu-dropdown mega-menu-dropdown">
                      <a  href="calendario"> Agenda

                      </a>

                    </li>

                    <li id="menu_clientes" class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                      <a  href="clientes"> Clientes

                      </a>

                    </li>
                    <li id="menu_os" class="menu-dropdown mega-menu-dropdown  ">
                     <a  href="ordem_servico"> Ordem de Serviço

                     </a>

                   </li>



                   <li id="menu_estoque" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="cadastro_produtos"> Estoque

                    </a>

                  </li>





                  <li id="menu_movimentacoes" class="menu-dropdown mega-menu-dropdown  ">
                    <a href="movimentacoes"> Movimentações

                    </a>

                  </li>

                  <li id="menu_relatorios" class="menu-dropdown classic-menu-dropdown ">
                    <a href="relatorios"> Relatórios

                    </a>

                  </li>


                  <li id="menu_cadastros" class="menu-dropdown classic-menu-dropdown ">
                    <a href="javascript:;"> Cadastros
                      <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                     <li class=" ">
                      <a href="cadastro_fornecedores" class="nav-link  ">
                      Fornecedores</a>
                    </li>
                    <li class=" ">
                      <a href="cadastro_servicos" class="nav-link  ">
                       Categorias Produtos/Serviços

                     </a>
                   </li>
                   <li class=" ">
                    <a href="cadastro_laboratorio" class="nav-link  ">
                      Laboratórios

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_medicos" class="nav-link  ">
                      Médicos

                    </a>
                  </li>
                  <li class=" ">
                    <a href="cadastro_indicacao" class="nav-link  ">
                      Indicações

                    </a>
                  </li>
                </ul>
              </li>


            </ul>
          </div>
          <!-- END MEGA MENU -->
        </div>
      </div>
      <!-- END HEADER MENU -->
    </div>
    <!-- END HEADER -->
  </div>
</div>
<div class="page-wrapper-row full-height">
  <div class="page-wrapper-middle">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
          <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
              <h1>Visão Geral

              </h1>
            </div>
            <!-- END PAGE TITLE -->

          </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
          <div class="container">
            <!-- BEGIN PAGE BREADCRUMBS -->
            <ul class="page-breadcrumb breadcrumb">
              <li>
                <a href="#">Home</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
                <a href="#">Pages</a>
                <i class="fa fa-circle"></i>
              </li>
              <li>
                <span>Visão Geral</span>
              </li>
            </ul>
            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div id="div_todo_conteudo" class="page-content-inner">

             <?php  include '../visao_geral/somar_servicos.php';  ?> 

             <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
                <div class="dashboard-stat blue">
                  <div class="visual">
                    <i class="fa fa-money fa-icon-medium"></i>
                  </div>
                  <div class="details">
                    <div class="number"> R$<?php echo $contasarecebercaixa;  ?>  </div>
                    <div class="desc"> Entradas </div>
                  </div>
                  <a class="more" href="movimentacoes"> Ver Mais
                    <i class="m-icon-swapright m-icon-white"></i>
                  </a>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat red">
                  <div class="visual">
                    <i class="fa fa-money fa-icon-medium"></i>
                  </div>
                  <div class="details">
                    <div class="number"> R$<?php echo $contasapagarcaixa;  ?> </div>
                    <div class="desc"> Saídas </div>
                  </div>
                  <a class="more" href="movimentacoes"> Ver Mais
                    <i class="m-icon-swapright m-icon-white"></i>
                  </a>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat green">
                  <div class="visual">
                    <i class="fa fa-money"></i>
                  </div>
                  <div class="details">
                    <div class="number"> R$<?php echo $saldohoje;  ?> </div>
                    <div class="desc"> Total </div>
                  </div>
                  <a class="more" href="movimentacoes"> Ver Mais
                    <i class="m-icon-swapright m-icon-white"></i>
                  </a>
                </div>
              </div>

            </div>

            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
                <div class="dashboard-stat blue">
                  <div class="visual">
                    <i class="fa fa-money fa-icon-medium"></i>
                  </div>
                  <div class="details">
                    <div class="number"> R$0 </div>
                    <div class="desc"> Vendas </div>
                  </div>
                  <a class="more" href="movimentacoes"> Ver Mais
                    <i class="m-icon-swapright m-icon-white"></i>
                  </a>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat yellow">
                  <div class="visual">
                    <i class="fa fa-money"></i>
                  </div>
                  <div class="details">
                    <div class="number"> R$<?php echo $asscaixa;  ?> </div>
                    <div class="desc"> Assistência </div>
                  </div>
                  <a class="more" href="movimentacoes"> Ver Mais
                    <i class="m-icon-swapright m-icon-white"></i>
                  </a>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat green">
                  <div class="visual">
                    <i class="fa fa-money fa-icon-medium"></i>
                  </div>
                  <div class="details">
                    <div class="number"> R$<?php echo $opcaixa;  ?> </div>
                    <div class="desc"> Óptica </div>
                  </div>
                  <a class="more" href="movimentacoes"> Ver Mais
                    <i class="m-icon-swapright m-icon-white"></i>
                  </a>
                </div>
              </div>

            </div>


          </div> 


        </div> 

      </div>                                                                                                                                                                                      </div>


    </div> 


  </div> 

</div> 
</div>  

        <!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="../../assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="../../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="../../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/pages/scripts/ecommerce-dashboard.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->


<script type="text/javascript">
  
 $(document).ready(function() {
        
      var nivelacesso =  "<?php echo $_SESSION['nivel']; ?>";

      if((nivelacesso == '3') || (nivelacesso == '4') || (nivelacesso == '2')){
         
            $('#config_menu').hide();
            $('#div_todo_conteudo').hide();
        alert('Seu Login não da acesso a essa área!');

      };
        
        
        
        
      });


  
</script>



</body>

</html>