<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom functions file -->
    <script src="../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../sweetalert/js/sweetalert.min.js"></script>

    <title>GestãoInk </title>

  </head>

  <body>


<?php


  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    
  


		


   if(isset($_POST['excluir_mov']))
		  {

 	      $idmov = isset($_POST['idmov']) ? $_POST['idmov'] : '';
		    $idcliente = isset($_POST['id_cliente_mov']) ? $_POST['id_cliente_mov'] : '';

		
	    	$sqldeletarmov= "delete from movimentacoes_cliente where id = '$idmov' ";
		    $deletarmov = mysqli_query($con, $sqldeletarmov);
		
		
		      if($deletarmov)
			  {

					echo "<script>jQuery(function(){swal({   title: 'Deletar',   text: 'Movimentação apagada com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/perfil_cliente?id=$idcliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente?id=$idcliente'    } });});</script>";

					
              }else{

                     echo "<script>jQuery(function(){swal({   title: 'Edição',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/perfil_cliente?id=$idcliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente?id=$idcliente'    } });});</script>";
                   }
	 

	     }

?>

</body>
</html>