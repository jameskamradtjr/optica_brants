
<?php

$html = '<p align="center" >
  <strong>  CONTRATO DE INTERMEDIAÇÃO</strong>
</p>
<p align="center" >
  <strong>  DE</strong>
</p>
<p align="center" >
  <strong>  ATIVOS FINANCEIROS</strong>
</p>
<br/>
<p align="justify" >
    IDENTIFICAÇÃO DAS PARTES:
</p>
<p align="justify">
    STM OPERAÇÕES &amp; INVESTIMENTOS LTDA., inscrita no CNPJ/MF sob nº
    27.418.083/0001-90, com sede na Estrada do Capuava, 4421, Paisagem Renoir,
    Cotia, SP, doravante designada simplesmente Intermediadora, e do outro
    lado, '.$nome.', com sede na '.$endereco.', '.$cidade.' - '.$uf.', inscrita no CNPJ/MF sob nº '.$cpf.', neste ato denominada
    simplesmente Investidor (a), tem entre si, justo e contratado o que segue:
</p>
<p align="justify">
    01. DO OBJETO
</p>
<p align="justify">
    1.1.: Este Contrato de Intermediação de Operações no Mercado Financeiro tem
    por fim regular os direitos e obrigações das partes, relativamente a
    qualquer operação, isolada ou conjunta, efetuada no mercado financeiro pela
    Intermediadora.
</p>
<p align="justify">
    1.2.: A Empresa de Intermediações de Operações executará por conta
    operações nos mercados a vista e de obtenção de lucro no Mercado financeiro
    na negociação de fundos ativos BITCOINS, DASHCOIN, DOGECOIN, LITECOIN entre
    outros.
</p>
<p align="justify">
    02. DAS OBRIGAÇÕES DA INTERMEDIADORA
</p>
<p align="justify">
    2.1.: SERÁ EFETUADO prestação de serviços de TRADING (TRANSAÇÕES DE COMPRA
    E VENDA) do Ativo Financeiro BITCOIN entre outras, utilizando-se de
    recursos (DINHEIRO EM ESPÉCIE ou TRANSAÇÃO BANCARIA) aportados pelo
</p>
<p align="justify">
    INVESTIDOR, para fins de obtenção de lucros para este.
</p>
<ol start="2">
    <li >
        <p align="justify">
            2.: Obrigando-se a prestar ao INVESTIDOR (A) os seguintes serviços:
            •
        </p>
    </li>
</ol>
<p align="justify" >
    Negociação online, da moeda virtual BITCOIN e outras.
</p>
<ul>
    <li >
        <p >
            Gerenciamento dos recursos financeiros disponibilizados e aportados
            pelo INVESTIDOR (A), a fim de obter lucros de TRADING.
        </p>
    </li>
    <li >
        <p align="justify" >
            Após o período de 5 (cinco) a 7 (sete) dias do vencimento dos 30
            (trinta) dias de firmado o contrato, depositar o equivalente
            podendo chegar até 31% do valor mensalmente.
        </p>
    </li>
    <li >
        <p align="justify" >
            Solicitação de saques para conta bancária do INVESTIDOR (A), assim
            que por esta for requerido.
        </p>
    </li>
    <li >
        <p align="justify" >
            Consultoria para a declaração dos lucros a constar do imposto de
            renda do INVESTIDOR (A).
        </p>
    </li>
</ul>
<p align="justify" >
    2.3. É obrigação da Empresa de Intermediações de Operações, além das já
    implícitas neste instrumento: Zelar pela eficiência e efetividade das
    ferramentas e equipamentos para realização das operações de TRADING.
</p>
<ol>
    <li >
        <p align="justify" >
            Fornecer ao INVESTIDOR (A) o mais amplo suporte;
        </p>
    </li>
</ol>
<ol start="2">
    <li >
        <p align="justify" >
            Informar ao INVESTIDOR (A), com antecedência mínima de três (3)
            dias, acerca de eventuais interrupções necessárias para ajustes
            técnicos ou manutenções que demandem período superior a 06 (seis)
            horas de duração e que possam causar prejuízo à operacionalidade,
            salvo em casos de URGÊNCIA, aqui entendido como aquele que coloca
            em risco em regular funcionamento e obtenção dos lucros.
        </p>
    </li>
</ol>
<ol start="3">
    <li >
        <p align="justify" >
            Utilizar de expertise e know-how para obtenção de lucros em favor
            do INVESTIDOR (A).
        </p>
    </li>
</ol>
<ol start="4">
    <li >
        <p align="justify" >
            A STM Operaçoes & investimentos responsabiliza-se pelo valor
            aportado, tento que o restituir integralmente sempre que solicitado
            pelo investidor, ou também, na hipótese de o SISTEMA encontra-se
            inoperante por período superior a 5 a 10 (dias) corridos.
        </p>
    </li>
</ol>
<ol start="5">
    <li >
        <p align="justify" >
            A Empresa terá o prazo até 30 (dias) corridos para restituir, de
            forma integral e em uma única vez, os valores aportados pelo
            INVESTIDOR (A), em caso de solicitação do cancelamento das
            atividades neste descritos, sem as incidências de lucros neste
            período.
        </p>
    </li>
</ol>
<ol start="6">
    <li >
        <p align="justify" >
            Manter o sigilo das operações e demais informações do Investidor.
        </p>
    </li>
</ol>
<p align="justify" >
    03. DO PRAZO DE VIGÊNCIA.
</p>
<p align="justify" >
    3.1.: O prazo de vigência do presente instrumento particular são de '.$vigencia.' meses a contar da data de assinatura deste.
</p>
<p align="justify"  >
    3.2.: O rendimento começa da data da assinatura do contrato, tendo dois (2)
    dias para efetuar a transferência. Não sendo feito a transferência dentro
    do prazo, o rendimento só começara após a comprovação do aporte.
</p>
<p align="justify"  >
    04. DO INVESTIMENTO.
</p>
<p align="justify"  >
    4.1.: O INVESTIDOR (A), nesta oportunidade, de livre espontânea vontade
    realiza um aporte no valor integral de R$ '.$valor_aporte.' ('.$valor_extenso.'),
    mediante a transferência eletrônica para a conta bancária informada pela
    STM operações &amp; Investimentos e retirada da carteira virtual MERCADO
    BITCOIN, fins de realizações de TRADING (TRANSAÇÕES DE COMPRA E VENDA) do
    fundo ativo.
</p>
<p align="justify"  >
    4.2.: Em caso de solicitação do valor aportado pelo INVESTIDOR (A), ou STM
    Operaçoes&amp;investimentos terá o prazo até 30 (trinta) corridos para
    restituí-lo, de forma única e integral, mediante transferência eletrônica
    para conta bancária informada, sem as incidências de lucros neste período.
</p>
<p  align="justify" >
    4.3.: O INVESTIDOR (A), a qualquer momento, poderá aportar mais capitais a
    fim de aumentar o lucro auferido com a transação TRADING. Entretanto,
    deverá ser realizado um adendo a este instrumento, especificando as novas
    condições.
</p>
<p  align="justify" >
    - DADOS PARA APORTE DO VALOR DO INVESTIMENTO
</p>
<p align="justify" >
    Agência: 0918
</p>
<p align="justify"  >
    Conta C/c: 13-000634-9
</p>
<p align="justify"  >
    Banco: SANTANDER 033
</p>
<p align="justify"  >
    05. DO LUCRO E DO RESGATE.
</p>
<p align="justify"  >
    5.1.: Fica aqui estipulado que a STM operações &amp; investimentos
    creditara na conta corrente em nome do INVESTIDOR (A) titular do contrato,
    mensalmente podendo chegar em até 31% (trinta e um por cento) do valor
    investido, como lucro do valor aportado inicialmente.
</p>
<p align="justify" >
    5.2.: A STM operações &amp; investimentos deverá efetuar o valor
    equivalente ao lucro obtido pelo valor inicialmente apurado, entre 5 a 7
    dias seguinte úteis após do término do presente.
</p>
<p align="justify"  >
    5.3.: O não pagamento após os 7 (dias) úteis do valor mencionado acima No
    item 5.1., fará que a STM operações &amp; investimentos incorra em juros
    demora de 2% ao mês, e correção monetária pela variação do IGP-M/FGV,
    mediante a cobrança extrajudiciais ou judiciais que se fizerem necessárias,
    independentemente de notificação ou constituição em mora.
</p>
<p align="justify"  >
    5.4.: Os recursos financeiros encaminhados pelo INVESTIDOR à STM Operações
    &amp; investimentos somente serão considerados liberados para aplicação
    após a confirmação por parte da STM operações &amp; investimentos a efetiva
    disponibilidade dos mesmos tendo cliente 02 (dois) dias após firmar
    contrato. Não ocorrendo o aporte mencionado no item 4.1., o contrato estará
    automaticamente rescindido.
</p>
<ol start="6">
    <li >
        <p  align="justify" >
            DAS OBRIGAÇÕES DO INVESTIDOR
        </p>
    </li>
</ol>
<ol>
    <li >
        <p  align="justify" >
            O INVESTIDOR assume integralmente as responsabilidades civis e
            criminais pela veracidade dos dados e informações por ele prestados
            à STM Operações &amp; Investimentos, tais como a origem dos valores
            aplicados, isentando a corretora de quaisquer responsabilidades em
            virtude da ilicitude do aporte financeiro.
        </p>
    </li>
    <li >
        <p  align="justify" >
            Não entregar senhas ou assinaturas eletrônicas a terceiros, a fim
            de preservar a segurança das transações e informações.
        </p>
    </li>
    <li >
        <p align="justify"  >
            Declara estar ciente e aceitar que os investimentos nos mercados de
            renda variável são caracterizados como de risco, podendo a qualquer
            momento o contrato ser rescindo ante a impossibilidade da
            manutenção da taxa de lucro;
        </p>
    </li>
    <li >
        <p align="justify"  >
            As contas criadas no site serão movimentadas exclusivamente pela
            STM Operações &amp;Investimentos. Podendo o investidor apenas
            acessar para visualizar as movimentações.
        </p>
    </li>
</ol>
<ol start="7">
    <li >
        <p align="justify"  >
            DA RESCISÃO DO CONTRATO
        </p>
    </li>
</ol>
<ol>
    <li >
        <p align="justify"  >
            As partes declaram cientes de que, em caso de descumprimento de
            qualquer obrigação por qualquer das partes, o contrato estará
            rescindido, devendo ser devolvido os valores investidos, dentro do
            prazo de até 30 (trinta) dias.
        </p>
    </li>
    <li >
        <p align="justify"  >
            A STM Operações &amp; investimentos poderá a qualquer tempo,
            rescindir o contrato ora firmado, inclusive quando o mercado não
            possibilitar a manutenção da taxa de lucro, podendo notificar por
            qualquer meio eletrônico o INVESTIDOR, devendo devolver o valor
            investido em até 30 (trinta) dias após a data da notificação, sem
            as incidências de lucros neste período.
        </p>
    </li>
</ol>
<ol start="8">
    <li >
        <p align="justify"  >
            DO FALECIMENTO E DA SUCESSÃO.
        </p>
    </li>
</ol>
<p align="justify"  >
    8.1.: O presente instrumento obriga os herdeiros e/ou sucessores das partes
    contratantes, a qualquer título.
</p>
<ol start="8">
    <li >
        <p align="justify"  >
            2.: Em caso de falecimento ou de incapacidade declarada
            judicialmente do sócio administrador da STM Operações &amp;
            investimentos, resta imediatamente aqui declarado que caberá ao
            sócio remanescente a responsabilidade pela restituição do valor
            integral aportado pelo INVESTIDOR (A), no prazo limite de 30 dias
            corridos, dando-se por encerrado o presente contrato.
        </p>
    </li>
</ol>
<p align="justify"  >
    8.3. Em caso de falecimento ou de incapacidade declarada judicialmente do
    sócio administrador da INVESTIDORA, caberá ao sócio remanescente a
    responsabilidade pela manutenção ou não do contrato, com a solicitação do
    resgate do valor inicial aportado.
</p>
<p align="justify"  >
    09. DAS DISPOSIÇÕES FINAIS
</p>
<p align="justify"  >
    9.1.: O presente instrumento particular é regulamentado pela legislação
    civil em vigência, não podendo ser caracterizado, em hipótese alguma,
    vínculo empregatício ou associativo.
</p>
<ol start="9">
    <li >
        <p align="justify"  >
            2.: As partes não poderão ceder ou transferir os direitos e
            obrigações previstos neste contrato para terceiros sem a prévia
            anuência da outra parte.
        </p>
    </li>
    <ol start="3">
        <li >
            <p  align="justify" >
                E, por estarem juntos e contratados firmam o presente, que vai
                assinado em duas vias de igual teor e forma, que assinam para
                que produzam seus jurídicos legais efeitos para o mesmo fim.
            </p>
        </li>
        <li >
            <p  align="justify" >
                O foro eleito para dirimir dúvidas e processar ações derivadas
                deste negocio jurídico é o da Comarca de Embu das Artes, SP,
                com renuncia expressa das partes a qualquer outro foro, por
                mais especial ou privilegiado que seja ou que venham a ser,
                independentemente do domicilio ou residência atuais ou futuras
                dos contratantes.
            </p>
        </li>
    </ol>
</ol>
<p align="justify"  >
    Por estarem as partes justas e contratadas, assinam o presente instrumento
    em 02 (duas) vias de igual teor, na presença de 02 (duas) testemunhas.
</p>
<p  align="justify" >
    <img
        src="https://lh5.googleusercontent.com/XfJ_rb-HkcRRPmW6_-jOP3usiSJ0pnWiW_kNWCdbGcr9G3Z-l2CuYzr7XmuoNTzn6cDwkQYMLZN2MJ-_ilwoAsq-bNKR8qX2HEplkyQayNQzETOGM-fajG4Xukxz0FeTqXUDsRCDkMKm8pGusw"
        width="32"
        height="38"
    />
</p>
<br/>
<br/>
<p  align="justify" >
    São Paulo, '.$dataextenso.'.
</p>
<br/>
<br/>
<br/>
<br/>
<p >
    _____________________________ _____________________________
</p>
<p align="justify"  >
    STM Operações &amp; Investimentos Elshaday Manutenção e Reformas Prediais
    LTDA
</p>
<p align="justify"  >
    Intermediadora Investidor (a)
</p>
<br/>
<p align="justify"  >
    Testemunhas:
</p>
<br/>
<br/>
<p >
    _____________________________ ____________________________
</p>
<div>
    <br/>
</div>	';


?>