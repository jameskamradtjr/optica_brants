
<?php

$html = '<p align="center">
														  <h3 align="center">	<strong>CONTRATO DE INTERMEDIAÇÃO </strong> </h3>
                                </p>
                                <br/>
                                <br/>
                                <p>
     
                                </p>
                                <p align="justify">
                                    '.$razao_social.', inscrita no CNPJ sob nº '$cnpj', com
                                    sede '$endereco_empresa'
                                    – '.$cidade_empresa.' – '.$uf_empresa.', doravante designada simplesmente Intermediadora, e do outro
                                    lado, '.$nome.',  residente na '.$endereco.', CEP '.$cep.', '.$cidade.' - '.$uf.',
                                    inscrito (a) no RG nº '.$rg.', CPF nº '.$cpf.',  denominado (a)
                                    simplesmente Investidor (a), tem entre si, justo e contratado o que segue:
                                      
                                </p>
                                <p >
     
                                </p>
                                <ol>
                                    <li>
                                        <h4>
                                         <strong>   DO OBJETO  DO CONTRATO </strong>
                                        </h4>
                                    </li>
                                </ol>
                                <br/>
                                <p align="justify" >
                                    Cláusula Primeira: Este Contrato de Intermediação de Operações no Mercado
                                    Financeiro tem por fim regular os direitos e obrigações das partes,
                                    relativamente a qualquer operação, isolada ou conjunta, efetuada no mercado
                                    financeiro pela Intermediadora.  
                                </p>
                                <p align="justify" >
                                    Cláusula Segunda: A INTERMEDIADORA executará operações, a vista e de
                                    obtenção de lucro no Mercado financeiro na negociação de fundos ativos.  
                                </p>
                                <ol start="2">
                                    <li >                                       
																			<h4>
                                         <strong>   DAS OBRIGAÇÕES DA INTERMEDIADORA  </strong>
                                        </h4>
                                    </li>
                                </ol>
                                <br/>
                                <p align="justify" >
                                    Cláusula Primeira: Será efetuada a prestação de serviços de TRADING
                                    (TRANSAÇÕES DE COMPRA E VENDA) do Ativo Financeiro BITCOIN entre outras,
                                    utilizando-se de recursos (TRANSAÇÃO BANCARIA) aportados pelo INVESTIDOR,
                                    para fins de obtenção de lucros para este.  
                                </p>
                                <p align="justify" >
                                    Cláusula Segunda: Obrigando-se a prestar ao INVESTIDOR (A) os seguintes
                                    serviços:  
                                </p>
                                <p align="justify" >
                                    Depois de 30 (trinta) dias de firmado o contrato, observada as 72 (Setenta
                                    e Duas) horas úteis para liquidez do ativo, depositar de 10% à 31% do valor
                                    INVESTIDO, mensalmente, até o término do presente contrato, na conta
                                    bancária informada pelo investidor.
                                </p>
                                <p >
                                     
                                </p>
                                <p align="justify" >
                                    Cláusula Terceira: A porcentagem acima descrita, somente será garantida se
                                    o mercado oferecer a volatilidade necessária dentro dos 30 (Trinta Dias) de
                                    operação. Sendo Obrigação da INTERMEDIADORA informar ao INVESTIDOR a
                                    variação da porcentagem, com  5 ( Cinco ) dias de antecedência do
                                    vencimento do rendimento.
                                </p>
                                <br/>
                                <p align="justify" >
                                    Cláusula Quarta: É obrigação da INTERMEDIADORA, além das já implícitas
                                    neste instrumento: Zelar pela eficiência e efetividade das ferramentas e
                                    equipamentos para realização das operações de TRADING.  
                                </p>
                                <br/>
                                <p align="justify" >
                                    Cláusula Quinta: A INTERMEDIADORA, não poderá fornecer informações contidas
                                    nesse documento a terceiros.
                                </p>
                                <p align="justify" >
                                    Cláusula Sexta: A INTERMEDIADORA, deverá informar o (a) INVESTIDOR (a) com
                                    antecedência mínima de 05 ( cinco dias ), acerca de eventuais interrupções
                                    necessárias para ajustes técnicos ou manutenções que demandem período
                                    superior a 6 (seis) horas de duração e, que possam causar prejuízo a
                                    operacionalidade, salvo em caso de urgência, aqui entendido como aquele que
                                    coloca em risco em regular funcionamento e obtenção dos lucros.
                                </p>
                                <p align="justify" >
                                    Cláusula Sétima: A INTERMEDIADORA fica isenta de comunicar com
                                    antecedência, conforme item acima, em caso fortuito ou de força maior.
                                </p>
                                <p align="justify" >
                                    Cláusula Oitava: A INTERMEDIADORA obriga-se ao final desse contrato
                                    restituir de forma única e integral o valor aportado pelo INVESTIDOR (A).
                                </p>
                                <ol start="3">
                                    <li >
                                       
																			
																			<h4>
                                         <strong>   DAS OBRIGAÇÕES DO INVESTIDOR    </strong>
                                        </h4>
																			
                                    </li>
                                </ol>
                                <br/>
                                <p align="justify" >
                                    Cláusula Primeira: O INVESTIDOR (A) assume integralmente as
                                    responsabilidades civis e criminais pela veracidade dos dados e informações
                                    por ele prestados à INTERMEDIADORA, tais como a origem dos valores
                                    aplicados, isentando a INTERMEDIADORA de quaisquer responsabilidades em
                                    virtude da ilicitude do aporte.  
                                </p>
                                <p align="justify" >
                                    Cláusula Segunda: O INVESTIDOR (a) concorda que a INTERMEDIADORA não será
                                    responsável por quaisquer prejuízos decorrentes da não execução das
                                    respectivas ordens de trading, nem de lucros que o INVESTIDOR (a) deixe de
                                    obter em casos de, falha no sistema, interrupção dos serviços das
                                    corretoras de fundo ativo, em caso fortuito ou força maior.
                                </p>
                                <p align="justify" >
                                    Cláusula Terceira: O INVESTIDOR (A) declara estar ciente e aceitar que os
                                    investimentos nos mercados de renda variável são caracterizados como de
                                    risco, podendo a qualquer momento o contrato ser rescindo por parte da
                                    INTERMEDIADORA ante a impossibilidade da manutenção da taxa de lucro,
                                    garantido a devolução dos aportes principais.
                                </p>
                                <ol start="4">
                                    <li >
                                        
																				<h4>
                                         <strong>   DO PRAZO DE VIGÊNCIA    </strong>
                                        </h4>
																			
                                    </li>
                                </ol>
                                <br/>
                                <p align="justify" >
                                    Cláusula Primeira: O prazo de vigência do presente instrumento particular
                                    são de '.$vigencia.' meses a contar da data de assinatura deste.  
                                </p>
                                <ol start="5">
                                    <li >
                                       
																			
																					<h4>
                                         <strong>   DO INVESTIMENTO    </strong>
                                        </h4>
																			
                                    </li>
                                </ol>
                                <br/>
                                <p align="justify" >
                                    Cláusula Primeira: O INVESTIDOR (A), nesta oportunidade, de livre
                                    espontânea vontade realiza um aporte no valor integral de R$ '.$valor_aporte.'
                                    ('.$valor_extenso.' ), mediante a transferência eletrônica para a conta
                                    bancária informada pela INTERMEDIADORA.
                                </p>
                                <p align="justify" >
                                    Cláusula Segunda: O INVESTIDOR (A), a qualquer momento, poderá aportar mais
                                    capitais a fim de aumentar o lucro auferido com a transação TRADING.
                                    Entretanto, deverá ser realizado um adendo a este instrumento,
                                    especificando as novas condições.  
                                </p>
                                <br/>
                                <p >
                                    - DADOS PARA APORTE DO VALOR DO INVESTIMENTO  
                                </p>
                                <p >
                                    Banco: ITAU  
                                </p>
                                <p >
                                    Agência: 9892
                                </p>
                                <p >
                                    Conta Corrente: 28471-3
                                </p>
                                <p >
                                    CNPJ: 28.047.535/0001-37
                                </p>
                                <p >
                                    KEMPEN INVESTIMENTOS LTDA
                                </p>
                                <ol start="6">
                                    <li >
                                        																			
																			<h4>
                                         <strong>   DO LUCRO E DO RESGATE    </strong>
                                        </h4>
																			
                                    </li>
                                </ol>
                                <br/>
                                <p align="justify" >
                                    Cláusula Primeira: Fica aqui estipulado que a INTERMEDIADORA creditará na
                                    conta bancária informada pelo INVESTIDOR (A), mensalmente, até o término do
                                    presente contrato, até 31% (Trinta e Um Por Cento) do valor investido, como
                                    lucro do valor aportado inicialmente.   
                                </p>
                                <p align="justify" >
                                    Cláusula Segunda: A INTERMEDIADORA deverá efetuar o valor equivalente ao
                                    lucro obtido pelo valor inicialmente apurado, em até 72 (Setenta e Duas)
                                    horas úteis, após os 30 (Trinta) dias da assinatura do presente
                                    instrumento.
                                </p>
                                <p align="justify" >
                                    Cláusula Terceira: O não pagamento em 30 (trinta) dias após o vencimento,
                                    do valor mencionado na cláusula acima, fará com que a INTERMEDIADORA
                                    incorra em juros de 2% ao mês, e correção monetária pela variação do IGP-M.
                                     
                                </p>
                                <ol start="7">
                                    <li >
                                        																			
																				<h4>
                                         <strong>   DA RESCISÃO DO CONTRATO      </strong>
                                        </h4>
																			
                                    </li>
                                </ol>
                                <br/>
                                <p align="justify" >
                                    Cláusula Primeira: As partes declaram cientes de que, em caso de
                                    descumprimento de qualquer obrigação por qualquer das partes, o contrato
                                    estará rescindido, devendo ser devolvido os valores aportados, dentro do
                                    prazo de até 30 (Trinta) dias úteis, inclusive quando o mercado não
                                    possibilitar a manutenção da taxa de lucro, podendo notificar o INVESTIDOR
                                    por qualquer meio eletrônico, devendo restituir o valor aportado em até 30
                                    (trinta) dias úteis, após a data da notificação, sem as incidências de
                                    lucros neste período.
                                </p>
                                <p align="justify" >
                                    Cláusula Segunda: A Empresa terá o prazo até 30 (Dias) úteis, para
                                    restituir, de forma integral e em uma única vez, os valores aportados pelo
                                    INVESTIDOR (A), em caso de solicitação do cancelamento das atividades neste
                                    descritos, sem as incidências de lucros neste período.
                                </p>
                                <ol start="8">
                                    <li >
                                       
																				<h4>
                                         <strong>   DAS DISPOSIÇÕES FINAIS      </strong>
                                        </h4>
																			
																			
                                    </li>
                                </ol>
                                <p >
                                     
                                </p>
                                <p align="justify" >
                                    Cláusula Primeira: O presente instrumento particular é regulamentado pela
                                    legislação civil em vigência, não podendo ser caracterizado, em hipótese
                                    alguma, vínculo empregatício ou associativo.  
                                </p>
                                <p align="justify" >
                                    Cláusula Segunda: As partes não poderão ceder ou transferir os direitos e
                                    obrigações previstos neste contrato para terceiros sem a prévia anuência da
                                    outra parte.  
                                </p>
                                <p align="justify" >
                                    Cláusula Terceira: E, por estarem juntos e contratados firmam o presente,
                                    assinado em duas vias de igual teor e forma, para que produzam seus efeitos
                                    jurídicos legais.
                                </p>
                                <p align="justify" >
                                    Cláusula Quarta: O foro eleito para dirimir dúvidas e processar ações
                                    derivadas deste negocio jurídico é o da Comarca de Cotia, SP, com renuncia
                                    expressa das partes a qualquer outro foro, por mais especial ou
                                    privilegiado que seja , independentemente do domicilio ou residência atuais
                                    ou futuras dos contratantes.  
                                </p>
                                <p align="justify" >
                                    Por estarem  justas e contratadas, as partes assinam o presente instrumento
                                    em 02 (duas) vias de igual teor, na presença de 02 (duas)  testemunhas.   
                                </p>
                                <p >
                                    <img
                                        src="https://lh4.googleusercontent.com/IHaWU3q1ttjxjysCekxFbsmnOd0jXLd2TpIN9wcuMyX3FLRUGjrWwTaP6m29n1fpW7AnOAT7TgngejjANMyfusBUmGgDXZy2RLEfx-U1uDn8T40FJbAqJVMwjcYymWKt5_vxw7kYKBpyCp618w"
                                        width="32"
                                        height="38"
                                     />
     
                                     </p>
                                     <p >
                                     </p>
                                     <p >
                                         '.$cidade.', '.$dataextenso.'.  
                                     </p>
                                     <p >
                                          
                                     </p>
                                     <p >
                                          
                                     </p>
                                     <br/>
                                     <br/>
                                     <p >
                                         ___________________________              _____________________________
                                     </p>
                                     <p >
                                                                                    '.$nome.'
                                         '.$razao_social.'
                                     </p>
                                     <br/>
                                     <p >
                                           Intermediadora                                   Investidor (a)
                                     </p>
                                     <br/>
                                     <br/>
                                     <p >
                                           Testemunhas:
                                     </p>
                                     <p >
                                     </p>
                                     <p >
     
                                     </p>
                                     <p >
                                         ___________________________             _____________________________
                                                                                                                     
                                     </p>
                                     <div>
                                         <br/>
                                     </div>	';


?>