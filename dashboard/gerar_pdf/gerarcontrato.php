<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


    <!-- Custom functions file -->
    <script src="../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../sweetalert/js/sweetalert.min.js"></script>

    <title>Gestão Investimentos</title>

  </head>

  <body>

<?php
		
		
function valorPorExtenso($valor=0) {
	$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
	$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");

	$c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
	$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
	$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
	$u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");

	$z=0;

	$valor = number_format($valor, 2, ".", ".");
	$inteiro = explode(".", $valor);
	for($i=0;$i<count($inteiro);$i++)
		for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
			$inteiro[$i] = "0".$inteiro[$i];

	// $fim identifica onde que deve se dar junção de centenas por "e" ou por "," 😉
	$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
	for ($i=0;$i<count($inteiro);$i++) {
		$valor = $inteiro[$i];
		$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
		$rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
		$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";
	
		$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
		$t = count($inteiro)-1-$i;
		$r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
		if ($valor == "000")$z++; elseif ($z > 0) $z--;
		if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t]; 
		if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
	}

	return($rt ? $rt : "zero");
}		


  include '../config_db.php';

date_default_timezone_set('America/Sao_Paulo');  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}          
		  
		  
	     	  

			  
			  //funcao converte a data br para a do mysql
			  function inverteData($data){
              if(count(explode("/",$data)) > 1){
                 return implode("-",array_reverse(explode("/",$data)));
                   }elseif(count(explode("-",$data)) > 1){
                      return implode("/",array_reverse(explode("-",$data)));
                      }
                   }
			   //funcao converte a data br para a do mysql 
        
		
    
          $idaporte= isset($_POST['idaporte']) ? $_POST['idaporte'] : '';
          $idcliente= isset($_POST['idcliente']) ? $_POST['idcliente'] : '';
		      $rg= isset($_POST['rg']) ? $_POST['rg'] : '';
		      $cpf= isset($_POST['cpf']) ? $_POST['cpf'] : '';
	      	$nome= isset($_POST['nome']) ? $_POST['nome'] : '';
		      $endereco= isset($_POST['endereco']) ? $_POST['endereco'] : '';
		      $cep= isset($_POST['cep']) ? $_POST['cep'] : '';
		      $cidade= isset($_POST['cidade']) ? $_POST['cidade'] : '';
		      $uf= isset($_POST['uf']) ? $_POST['uf'] : '';
		      $valor_aporte= isset($_POST['valor']) ? $_POST['valor'] : '';
		      $vigencia= isset($_POST['vigencia']) ? $_POST['vigencia'] : '';
		      $id_empresa= isset($_POST['id_empresa']) ? $_POST['id_empresa'] : '';
		      $fk_id_modelocontrato= isset($_POST['fk_id_modelocontrato']) ? $_POST['fk_id_modelocontrato'] : '';
						
		      $idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';
		
		           $log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Gerou Contrato','contrato')";
               $insert_log_eventos = mysqli_query($con, $log_eventos);
							
		
		
		     
		     $select_dadosempresa = "SELECT SQL_CACHE * FROM dados_empresa where id ='$id_empresa' ";
         $dados_empresa = $con->query($select_dadosempresa); 
		
		     $dadosdaempresa_lista = $dados_empresa->fetch_assoc();
		     $razao_social = $dadosdaempresa_lista['razao_social'];
		     $cnpj = $dadosdaempresa_lista['cnpj'];
		     $endereco_empresa = $dadosdaempresa_lista['endereco'];
		     $cidade_empresa = $dadosdaempresa_lista['cidade'];
		     $uf_empresa = $dadosdaempresa_lista['uf'];
		 
		     
		
		     $select_modelo_contrato = "SELECT SQL_CACHE * FROM modelo_contrato where id ='$fk_id_modelocontrato' ";
         $modelo_contrato = $con->query($select_modelo_contrato); 
		     $caminho_html_modelo_contrato = $modelo_contrato->fetch_assoc();
		
		    
          
          $FK_fichaid = $idaporte;		
				
				  $pdfanexo = '../docs_empresas/'.$idadmin.'/contratos/gerados/'.$FK_fichaid.'.pdf'; 
		
		
		     setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
         date_default_timezone_set('America/Sao_Paulo');
         $dataextenso=strftime('%d de %B de %Y', strtotime('today'));
		
		
		
		    
		
		    // include 'clsTexto.php';
		
		     $valor_extenso = valorPorExtenso( $valor_aporte );
 
          //Vai exibir na tela "um milhão, quatrocentos e oitenta e sete mil, duzentos e cinquenta e sete e cinquenta e cinco"
        //  echo clsTexto::valorPorExtenso("R$ 1.487.257,55", false, false);
 
          //Vai exibir na tela "um milhão, quatrocentos e oitenta e sete mil, duzentos e cinquenta e sete reais e cinquenta e cinco centavos"
          //echo clsTexto::valorPorExtenso("R$ 1.487.257,55", true, false);
 
          //Vai exibir na tela "duas mil e setecentas e oitenta e sete"
         // $valor_extenso = valorPorExtenso($valor_aporte, false, true);
				
				
         
        //	$queryarquivo = "UPDATE arquivo_contrato SET anexo='$pdfanexo' WHERE ID='$FK_fichaid'";
        //  $update_arquivo = mysqli_query($con, $queryarquivo);



	       //************************GERANDO PDF COMPROVANTE
						
          //bliblioteca pdf
          
 
           ob_clean();

           include '../mpdf60/mpdf.php';

           include '../'.$caminho_html_modelo_contrato['caminho_html'] ;
	
           $mpdf=new mPDF(); 
 
           $mpdf->SetDisplayMode('fullpage');	
		   
		       //$css = file_get_contents("pdf/modelo/style.css");
           //$mpdf->WriteHTML($css,1);
		       
					
		       $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');					
           $mpdf->WriteHTML($html);			
			
        
        
        

           $mpdf->Output($pdfanexo, 'F'); // creates file /site/templates/invoice.pdf correctly
    
     //        echo "<script>jQuery(function(){swal({   title: 'Contrato',   text: 'Contrato Gerado com Sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../perfil_cliente.php?id=$idcliente'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../perfil_cliente.php?id=$idcliente'    } });});</script>";

          
           $mpdf->Output($pdfanexo, 'D'); // download

				
          


		//  }else{header('Location: ../clientes.php');}





    	
//--------------------------------------------------		

?>

</body>
</html>