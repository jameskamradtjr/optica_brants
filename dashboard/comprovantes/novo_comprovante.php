<?php if (!isset($_SESSION)){ session_start();};// A sessão precisa ser iniciada em cada página diferente  ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


    <!-- Custom functions file -->
    <script src="../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../sweetalert/js/sweetalert.min.js"></script>

    <title>GestãoInvest </title>

  </head>

  <body>

<?php


  include '../sys/init.php';
  
  date_default_timezone_set('America/Sao_Paulo');
  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}   

 
 	

          if(isset($_POST['salvar_comprovante']))
		  {
	
	           
						
						 $descricao_comprovante= isset($_POST['descricao_comprovante']) ? $_POST['descricao_comprovante'] : '';
						
					
             $FK_cliente_admin_id = isset($_POST['id_cliente_comprovante']) ? $_POST['id_cliente_comprovante'] : '';
	
						 $idadmin = $_SESSION['idadmin'];
            
             $relacionamento = date("dmYhis");

             $idempresa = $_SESSION['id_empresa'];
          
	
             move_uploaded_file($_FILES["comprovante"]["tmp_name"],"../docs_empresas/$idadmin/anexos/pagamentos/".$relacionamento.$_FILES["comprovante"]["name"]."");
            
             $comprovante = "docs_empresas/$idadmin/anexos/pagamentos/".$relacionamento.$_FILES["comprovante"]["name"]."";
									 
							 $query_comprovante = "INSERT INTO comprovantes (fk_id_cliente,descricao,anexo_comprovante,data,FK_usuarios_admin_id,fk_id_empresa) VALUES ('$FK_cliente_admin_id','$descricao_comprovante','$comprovante',curdate(),'$idadmin','$idempresa')";
               $insert_query_comprovante = mysqli_query($con, $query_comprovante);
							 
						
						 $log_eventos = "INSERT INTO log_eventos (FK_usuarios_admin_id,login,hora,acao,tabela) VALUES ('$idadmin','".$_SESSION['user_email']."',now(),'Anexou','comprovante')";
                $insert_log_eventos = mysqli_query($con, $log_eventos);
						
							 

		    
                if($insert_query_comprovante){
					
			     
			            
					       echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Comprovante adicionado com sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/perfil_cliente?id=$FK_cliente_admin_id'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente?id=$FK_cliente_admin_id'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível Cadastrar!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/perfil_cliente?id=$FK_cliente_admin_id'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/perfil_cliente?id=$FK_cliente_admin_id'    } });});</script>";
              }


	         }else{
	
            
			
			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/clientes'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/clientes'    } });});</script>";

			
			}





    	
//--------------------------------------------------		

?>

</body>
</html>