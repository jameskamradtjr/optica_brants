<?php 
if (!isset($_SESSION)){
	
ini_set('session.use_cookies', '1'); 
ini_set('session.use_only_cookies', '1'); 
ini_set('session.use_trans_sid', '0'); 
session_start();
	
	
}

error_reporting(0);



  include '../sys/init.php';
  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

date_default_timezone_set('America/Sao_Paulo');

//login
if(isset($_POST['botao_acessar'])){

  $email = mysqli_real_escape_string($con,$_POST['username']);

  $pass = mysqli_real_escape_string($con,$_POST['password']);

  $loja = mysqli_real_escape_string($con,$_POST['loja']);

 
	//select tabela usuario admin
	
	$select_admin = "SELECT SQL_CACHE usuarios_admin.*,dados_empresa.id,dados_empresa.FK_usuarios_admin_id FROM usuarios_admin INNER JOIN dados_empresa ON usuarios_admin.id = dados_empresa.FK_usuarios_admin_id WHERE (usuarios_admin.email = '".$email."') AND (usuarios_admin.status = 1) and (dados_empresa.id = '".$loja."') LIMIT 1";
  
    $run_admin = mysqli_query($con, $select_admin);

    $check_admin = mysqli_num_rows($run_admin);
	
	//select tabela usuario admin
	
	//verifica o id do usuario admin
	$vercampo = $con->query($select_admin); 
	$campo_admin = $vercampo->fetch_assoc();
	//verifica o id do usuario admin
	
	
	//verifica campos da tabela licenca
	$select_licenca = "SELECT SQL_CACHE * FROM licenca WHERE FK_usuarios_admin_id = '".$campo_admin['id']."' ";
	$verlicenca = $con->query($select_licenca); 
	$campo_licenca = $verlicenca->fetch_assoc();
	//verifica campos da tabela licenca

	//caso for teste e o registro + 5 dias de testes for menor que a data atual++++++++++++++++++++++++++++++++++++++	
	$dataderegistro = $campo_licenca['data_registro'];
    $registromais5dias = date('Y-m-d', strtotime($dataderegistro. ' + 5 days'));
	//caso for teste e o registro + 5 dias de testes for menor que a data atual++++++++++++++++++++++++++++++++++++++	
	
  
  

     //verifica se é login admin  
    if($check_admin>0)
		{
		

		  $stmt = $con->query($select_admin);
		  $idadmin = $stmt->fetch_assoc(); 
		
		   //pegar logo
		   $select_empresa = "SELECT SQL_CACHE id,logo_anexo,cidade,uf,nome_fantasia FROM dados_empresa WHERE id = '".$loja."' ";
	       $verempresa = $con->query($select_empresa); 
	       $idempresa = $verempresa->fetch_assoc();
				 
	
        
            $_SESSION['user_email']=$email;
            $_SESSION['user_password']=$pass;
		    $_SESSION['tipo_login']='Admin';
		    $_SESSION['idadmin']=$idadmin['FK_usuarios_admin_id'];
		    $_SESSION['tipo_login']='1';
		    $_SESSION['logado']='1';
            $_SESSION['nome']=$idempresa['nome_fantasia'];
	    	$_SESSION['foto']=$idempresa['logo_anexo'];	    	
			$_SESSION['id_empresa']=$idempresa['id'];
			$_SESSION['nivel']=1;
			
			    
			     //relatorio_acesso
		         $relatorio_acesso = "INSERT INTO relatorio_acesso (login,datahora,ip,fk_id_admin) VALUES ('$email',now(),'','".$idadmin['id']."')";
                 $insert_relatorio_acesso = mysqli_query($con, $relatorio_acesso);
		         //relatorio_acesso
			
			
				if($campo_licenca['licenca'] === 'avaliacao')
	             {
                 };
	   
                 
                //verifica se a senha está certa                
		        if (password_verify($_POST['password'], $campo_admin['passwd'])) { 
                   header("Location: clientes"); 
		         }else{

		         	  echo '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>';
	                  // echo '<script type="text/javascript" >alertify.alert("Acesso Negado");</script>';
	                  echo '<script type="text/javascript" >sweetAlert("Oops...", "Acesso Negado", "error");</script>';
                      session_destroy();
	                  exit;

		         }

                  

	


	}
    //senao for admin
    else 
	{
      
     //checa entao se é um usuario
     $select_user = "SELECT SQL_CACHE usuarios.*,usuarios.id as id_colaborador,dados_empresa.id,dados_empresa.FK_usuarios_admin_id FROM usuarios INNER JOIN dados_empresa ON usuarios.fk_id_empresa = dados_empresa.id WHERE (usuarios.login = '". $email ."') AND (usuarios.status = 1) and (dados_empresa.id = '".$loja."') LIMIT 1";

     $run_user = mysqli_query($con, $select_user);

     $check_user = mysqli_num_rows($run_user);
	


    if($check_user>0) {
      
		
		$stmt = $con->query($select_user);
		$idcolab = $stmt->fetch_assoc();
		
		//pegar foto colab
	    $select_colab = "SELECT SQL_CACHE id,logo_anexo,cidade,uf,nome_fantasia FROM dados_empresa WHERE (id = '". $idcolab['fk_id_empresa'] ."')";
		$stmtlogocolab = $con->query($select_colab);
		$idlogocolab = $stmtlogocolab->fetch_assoc(); 
		
	   
             $_SESSION['user_email']=$email;
             $_SESSION['user_password']=$pass; 
             $_SESSION['tipo_login']='Usuário'; 
             $_SESSION['nivel']=$idcolab['id_acesso'];;	 
      
			 $_SESSION['idcolab']=$idcolab['id_colaborador'];	 
			 $_SESSION['idadmin']=$idcolab['FK_usuarios_admin_id'];
	    
			 $_SESSION['tipo_login']=$idcolab['id_acesso']; 	
	         $_SESSION['logado']='1';
	         $_SESSION['nome']=$idlogocolab['nome_fantasia'];
	         $_SESSION['foto']=$idcolab['foto'];
			 $_SESSION['id_empresa']=$idlogocolab['id'];
			
			
			       //relatorio_acesso
		   	       $relatorio_acesso = "INSERT INTO relatorio_acesso (login,datahora,ip,fk_id_admin) VALUES ('$email',now(),'','".$idcolab['FK_usuarios_admin_id']."')";
                   $insert_relatorio_acesso = mysqli_query($con, $relatorio_acesso);
		           //relatorio_acesso
	   
				if($campo_licenca['licenca'] === 'avaliacao')
	            {      
			    };
			
	   
	   
	            //verifica se a senha está certa                
		        if (password_verify($_POST['password'], $idcolab['senha'])) { 
                   header("Location: clientes"); 
		         }else{

		         	  echo '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>';
	                  // echo '<script type="text/javascript" >alertify.alert("Acesso Negado");</script>';
	                  echo '<script type="text/javascript" >sweetAlert("Oops...", "Senha Inválida", "error");</script>';
                      session_destroy();
	                  exit;

		         }
	            //verifica se a senha está certa 
	   


     }else		
	 { 
	 
	         echo '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>';
	         // echo '<script type="text/javascript" >alertify.alert("Acesso Negado");</script>';
	         echo '<script type="text/javascript" >sweetAlert("Oops...", "Usuário não encontrado", "error");</script>';
       
	
    }

  } 



}


?>




