<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
	<link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="../sweetalert/css/main.css" rel="stylesheet">
	<!-- Scroll Menu -->
	<link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->


	<!-- Custom functions file -->
	<script src="../sweetalert/js/functions.js"></script>
	<!-- Sweet Alert Script -->
	<script src="../sweetalert/js/sweetalert.min.js"></script>

	<title> </title>

</head>

<body>

	<?php

//--------------------------------------------------
//cadastrar novo estudio
	if(isset($_POST['cadastrar'])){

		include '../sys/init.php';




    //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
		if (!$con->set_charset("utf8")) {}




			$empresa = mysqli_real_escape_string($con,$_POST['nome']);

		$email = mysqli_real_escape_string($con,$_POST['username']);
		$confirm_email = mysqli_real_escape_string($con,$_POST['confirm_email']);



		$senha = isset($_POST['password']) ? $_POST['password'] : '';
		$senha2 = isset($_POST['rpassword']) ? $_POST['rpassword'] : '';
		$plano = isset($_POST['plano']) ? $_POST['plano'] : '';


		$select_login = "SELECT id,email FROM usuarios_admin WHERE email = '".$email."' LIMIT 1";

		$run_select_login = mysqli_query($con, $select_login);

		$check_login = mysqli_num_rows($run_select_login);


		$select_login_colab = "SELECT id,login FROM usuarios WHERE login = '".$email."' LIMIT 1";

		$run_select_login_colab = mysqli_query($con, $select_login_colab);

		$check_login_colab = mysqli_num_rows($run_select_login_colab);




		if($senha2 !== $senha ){ echo "<script>jQuery(function(){swal({   title: 'Novo',   text: 'Confirmação de Senha Incorreta',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/login'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/login'    } });});</script>";  exit;}


		if($check_login>0 ){ echo "<script>jQuery(function(){swal({   title: 'Novo',   text: 'Login já Utilizado',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/login'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/login'    } });});</script>"; exit; }

		if($check_login_colab>0 ){ echo "<script>jQuery(function(){swal({   title: 'Novo',   text: 'Login já Utilizado',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/login'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/login'    } });});</script>"; exit; } 




		if(($check_login==0) and ($check_login_colab==0) and (isset($_POST['username'])) and (isset($_POST['password'])) )

		{

			$senha = password_hash($_POST['password'], PASSWORD_DEFAULT);

			$query_usuarios = "INSERT INTO usuarios_admin (nome,email,status,data_cadastro,passwd) VALUES ('$responsavel','$email',1,NOW(),'$senha')";
			$insert_usuarios = mysqli_query($con, $query_usuarios);
			
			if($insert_usuarios){

		       //pega chave estrangeira
				$FK_usuarios_admin_id = mysqli_insert_id($con);

				
				
				
				$query_empresa = "INSERT INTO dados_empresa (registro,nome_fantasia,email,FK_usuarios_admin_id,fk_id_plano) VALUES (CURDATE(),'$empresa','$email','$FK_usuarios_admin_id','$plano')";
				$insert_empresa = mysqli_query($con, $query_empresa);

				$FK_dados_empresa_id = mysqli_insert_id($con);

				$query_licenca = "INSERT INTO licenca (data_registro,licenca,FK_usuarios_admin_id,fk_id_plano,forma_pagamento) VALUES (CURDATE(),'avaliacao','$FK_usuarios_admin_id','$plano','Boleto')";
				$insert_licenca = mysqli_query($con, $query_licenca);


				$query_mensagembemvindo = "INSERT INTO mensagens_gerais (fk_id_cliente,mensagem,titulo,validade) VALUES ('$FK_usuarios_admin_id','Aproveite seus 5 dias Gratuitos!','Bem Vindo',DATE_ADD(CURDATE(), INTERVAL 4 DAY))";
				$insert_mensagembemvindo = mysqli_query($con, $query_mensagembemvindo);

				$query_cadastro_novaconta = "INSERT INTO conta (banco,descricao,saldo_inicial,FK_usuarios_admin_id) VALUES ('Caixa','Caixa','0','$FK_usuarios_admin_id')";
				$insert_cadastro_novaconta = mysqli_query($con, $query_cadastro_novaconta);
				


			  //CATEGORIAS DEFAULT----------------------------------------------------------

				$query_categorianovapai = "INSERT INTO categoria_pai (descricao,FK_usuarios_admin_id) VALUES ('GASTOS FIXOS','$FK_usuarios_admin_id')";
				$insert_categorianovapai = mysqli_query($con, $query_categorianovapai);

				$fk_id_catpai_desp = mysqli_insert_id($con);


				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Água','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Aluguel','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Contabilidade','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Energia Elétrica','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Internet','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Telefone','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);			


			 //-----

				$query_categorianovapai = "INSERT INTO categoria_pai (descricao,FK_usuarios_admin_id) VALUES ('IMPOSTOS','$FK_usuarios_admin_id')";
				$insert_categorianovapai = mysqli_query($con, $query_categorianovapai);

				$fk_id_catpai_desp = mysqli_insert_id($con);


				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('IPTU','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Taxa de Lixo','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

			 //-----

				$query_categorianovapai = "INSERT INTO categoria_pai (descricao,FK_usuarios_admin_id) VALUES ('MATERIAL DE ESCRITÓRIO','$FK_usuarios_admin_id')";
				$insert_categorianovapai = mysqli_query($con, $query_categorianovapai);


			 //-----

				$query_categorianovapai = "INSERT INTO categoria_pai (descricao,FK_usuarios_admin_id) VALUES ('REMUNERAÇÕES','$FK_usuarios_admin_id')";
				$insert_categorianovapai = mysqli_query($con, $query_categorianovapai);

				$fk_id_catpai_desp = mysqli_insert_id($con);


				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Benefícios','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('FGTS','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Pró-labore','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);			

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Salários','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

				$query_categorianovafilho = "INSERT INTO categoria_filho (descricao,fk_id_catpai_desp) VALUES ('Comissões','$fk_id_catpai_desp')";
				$insert_categorianovafilho = mysqli_query($con, $query_categorianovafilho);

			 //-----

				$query_categorianovapai = "INSERT INTO categoria_pai (descricao,FK_usuarios_admin_id) VALUES ('SEM CATEGORIA','$FK_usuarios_admin_id')";
				$insert_categorianovapai = mysqli_query($con, $query_categorianovapai);



			 //-----

				$query_categorianovapai = "INSERT INTO categoria_pai (descricao,FK_usuarios_admin_id) VALUES ('TAXAS E TARIFAS','$FK_usuarios_admin_id')";
				$insert_categorianovapai = mysqli_query($con, $query_categorianovapai);



			  //CATEGORIAS DEFAULT----------------------------------------------------------





			   //CRIAR PASTAS****************************************

				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/fotos/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/logo/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/modelos_pdf/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/doc_pdf/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/anexos/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/documentos/', 0777, true);


				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/fotos/clientes/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/fotos/colaboradores/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/documentos/digitalizados/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/documentos/gerados/', 0777, true);

				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/anexos/despesas/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/anexos/receitas/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/anexos/os/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/anexos/pagamentos/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/anexos/receitas_medicas/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/anexos/assistencia/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/anexos/cliente/', 0777, true);
				mkdir('../docs_empresas/'.$FK_usuarios_admin_id.'/anexos/outros/', 0777, true);


			   //CRIAR PASTAS****************************************

			     //GRAVAR IMAGEM PADRAO NA PASTA DO LOGO

				copy("../img/user.jpg","../docs_empresas/".$FK_usuarios_admin_id."/logo/logo_user".$FK_usuarios_admin_id.".jpg");
				copy("../img/logo.jpg","../docs_empresas/".$FK_usuarios_admin_id."/logo/logo_loja".$FK_usuarios_admin_id.$FK_dados_empresa_id.".jpg");
				$FOTO = "docs_empresas/".$FK_usuarios_admin_id."/logo/".$FK_usuarios_admin_id.".jpg";


				$queryupdate = "UPDATE dados_empresa SET logo_anexo='$FOTO' WHERE ID='$FK_dados_empresa_id'";
				$update = mysqli_query($con, $queryupdate);

			    //GRAVAR IMAGEM PADRAO NA PASTA DO LOGO


                




				echo "<script>jQuery(function(){swal({   title: 'Nova Empresa',   text: 'Cadastro realizado com sucesso! Anote ID Loja 1 :$FK_dados_empresa_id',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/login'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/login'    } });});</script>";

			}else
			{
				echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/login'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/login'    } });});</script>";
			}


		}else
		{
			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Login já Utilizado, Tente Recuperar a Senha!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/login'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/login'    } });});</script>";
		}

	}

//--------------------------------------------------	

	?>

</body>
</html>