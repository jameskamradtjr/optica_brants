<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Bootstrap -->
    <link href="../sweetalert/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../sweetalert/css/main.css" rel="stylesheet">
    <!-- Scroll Menu -->
    <link href="../sweetalert/css/sweetalert.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


    <!-- Custom functions file -->
    <script src="../sweetalert/js/functions.js"></script>
    <!-- Sweet Alert Script -->
    <script src="../sweetalert/js/sweetalert.min.js"></script>

    <title> </title>

  </head>

  <body>

<?php

if (!isset($_SESSION)) session_start();
session_regenerate_id(true);

  include '../sys/init.php';
  

  
  //seta utf8 no banco cadastro senão fica desconfigurado os tios e cedilhas
  if (!$con->set_charset("utf8")) {}    

  date_default_timezone_set('America/Sao_Paulo');
		
		
		//função para gravar os valores no mysql
 function moeda($get_valor) {
	$source = array('.', ','); 
	$replace = array('', '.');
	$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
	return $valor; //retorna o valor formatado para gravar no banco
  }
//função para gravar os valores no mysql


          if(isset($_POST['adicionar_servico_prestamos']))
		  {
        
      
				
			  $id = mysqli_real_escape_string($con,$_POST['id']);
        $descricao = mysqli_real_escape_string($con,$_POST['descricao']);
        $valor_cobrado = mysqli_real_escape_string($con,$_POST['valor_cobrado']);
			  $valor_cobrado = moeda($valor_cobrado);
			  
       	 $idadmin = isset($_SESSION['idadmin']) ? $_SESSION['idadmin'] : '';
		     $idempresa = $_SESSION['id_empresa'];	

            
             if(isset($_SESSION['idadmin'])){
               
             $query_insert_servico_prestamos = "INSERT INTO servico_prestamos (descricao,valor_cobrado,FK_usuarios_admin_id,fk_id_empresa) VALUES ('$descricao','$valor_cobrado','$idadmin','$idempresa')";
             $insert_go_servico_prestamos= mysqli_query($con, $query_insert_servico_prestamos);
			      
                if($insert_go_servico_prestamos){				
			            
				       	echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Serviço Cadastrado com Sucesso!',   type: 'success',   showCancelButton: false,   confirmButtonColor: '#8CD4F5',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Sucesso!', '...', 'success');top.location.href='../view/cadastro_servicos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_servicos'    } });});</script>";

                  }else{
                      echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível Cadastrar!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_servicos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_servicos'    } });});</script>";
              }


	         }else
			  {
		  	  echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não Logado!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_servicos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_servicos'    } });});</script>";
        }
	
      }else
			{
			echo "<script>jQuery(function(){swal({   title: 'Cadastro',   text: 'Não foi possível!',   type: 'warning',   showCancelButton: false,   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Ok',   cancelButtonText: 'No, cancel plx!',   closeOnConfirm: false,   closeOnCancel: false }, function(isConfirm){   if (isConfirm) {     swal('Atenção!', '...', 'warning');top.location.href='../view/cadastro_servicos'   } else {     swal('Cancelado', 'Confira a lista!', 'error');top.location.href='../view/cadastro_servicos'    } });});</script>";
      }
			





    	
//--------------------------------------------------		

?>

</body>
</html>